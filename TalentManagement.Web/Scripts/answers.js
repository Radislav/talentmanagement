﻿var answers = {
   initEx : function(answersSaveUrl,onAnswersSaved,prefix) {
         $('textarea').markItUp(mySettings);

    $('#save-draft,#share-answer').click(function () {
        var answers = [];

        $('.answer').each(function () {
            answers.push({
                Id : $(this).find('#' + prefix + '_Id').val(),
                QuestionId : $(this).find('#' + prefix + '_QuestionId').val(),
                Content : $(this).find('#' + prefix + '_Content').val(),
                QuestionText : $(this).find('#' + prefix + '_QuestionText').val(),
            });
        });
        var data = {
            indexModel : {
                ReviewId: $('#ReviewId').val(),
                Answers: answers
            },
            reviewStatus : $(this).data('review-type')
        };
        $.ajax({
            url: answersSaveUrl,
            data: JSON.stringify(data),
            success: function (response) {
                if (onAnswersSaved) {
                    onAnswersSaved(response);    
                }
            }
        });
    });
   },
   
  init: function(answersSaveUrl, prefix) {
      
      var messageExpirationMs = 3000;

       answers.initEx(answersSaveUrl, function(response) {

           $('#answers-saved-message').html(response.message);
           $('#answers-saved').show('slide');

           setTimeout(function() {
               $('#answers-saved').hide('slide');
           }, messageExpirationMs);

       }, prefix);
   }
};



