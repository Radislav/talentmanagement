﻿using System.Collections.Generic;
using System.Web.Mvc;
using TalentManagement.Web.Models;

namespace TalentManagement.Web.Infrastructure.UI
{
    public static  class ActivationButtonExtensions
    {
        public static MvcHtmlString ActivationButton<TActiveButtonModel>(
            this HtmlHelper<TActiveButtonModel> html, string name, IDictionary<string, object> attrs)
           where TActiveButtonModel : ICanActivateButton
        {
            TagBuilder tag = new TagBuilder("button");
            tag.AddCssClass("btn");

            if (html.ViewData.Model.ActiveButton == name)
            {
                tag.AddCssClass("active");
            }

            if (null != name)
            {
                tag.MergeAttribute("name", name);    
            }
            

            if (attrs != null)
            {
                tag.MergeAttributes(attrs);
            }

            tag.InnerHtml = name;

            return new MvcHtmlString(tag.ToString());
        }

          public static MvcHtmlString ActivationButton<TActiveButtonModel>(this HtmlHelper<TActiveButtonModel> html,string name, object attrs = null)
                where TActiveButtonModel : ICanActivateButton
          {
              return ActivationButton(html, name, HtmlHelper.AnonymousObjectToHtmlAttributes(attrs));
          }
    }
}