﻿using MongoDB.Bson;

namespace TalentManagement.Infrastructure.Converter
{
    class ObjectIdConverter : ITypeConverter<ObjectId,string>
    {
        public ObjectId Convert(string to)
        {
            return ObjectId.Parse(to);
        }

        public string ConvertBack(ObjectId from)
        {
            return from.ToString();
        }


        public bool IsDefault
        {
            get { return false; }
        }

        public object Convert(object to)
        {
            return Convert((string) to);
        }

        public object ConvertBack(object from)
        {
            return ConvertBack((ObjectId) from);
        }
    }
}
