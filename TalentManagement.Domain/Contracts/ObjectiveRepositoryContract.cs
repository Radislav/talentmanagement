﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using TalentManagement.Domain.Objectives;

namespace TalentManagement.Domain.Contracts
{
    [ContractClassFor(typeof(IObjectiveRepository))]
    internal  abstract class ObjectiveRepositoryContract : IObjectiveRepository
    {
        public IEnumerable<IGrouping<TProperty, Objective>> GetObjectivesGroupedBy<TProperty>(System.Linq.Expressions.Expression<Func<Objective, TProperty>> groupingExpression)
        {
            Contract.Requires(groupingExpression != null);
            Contract.Ensures(Contract.Result<IEnumerable<IGrouping<TProperty, Objective>>>() != null);

            return null;
        }

        public IEnumerable<IGrouping<TProperty, Objective>> GetObjectivesGroupedBy<TProperty>(System.Linq.Expressions.Expression<Func<Objective, TProperty>> groupingExpression, int dueYear)
        {
            Contract.Requires(groupingExpression != null);
            Contract.Ensures(Contract.Result<IEnumerable<IGrouping<TProperty, Objective>>>() != null);

            return null;
        }

        public IEnumerable<Objective> GetFocusObjectives()
        {
            Contract.Ensures(Contract.Result<IEnumerable<Objective>>() != null);
            return null;
        }

        public Objective InsertObjective(Objective objective)
        {
            Contract.Requires<ArgumentNullException>(objective != null);
            Contract.Ensures(Contract.Result<Objective>() != null);
            return null;
        }

        public void Insert(Objective entity)
        {
        }

        public void DeleteById(MongoDB.Bson.ObjectId id)
        {
        }

        public void Update(Objective entity)
        {
        }

        public IQueryable<Objective> AsQueryable()
        {
            return null;
        }

        public Objective GetById(MongoDB.Bson.ObjectId id)
        {
            return null;
        }

        public IEnumerable<Objective> GetFiltered(IFilter filter)
        {
            return null;
        }


        public Objective Upsert(Objective entity)
        {
            return null;
        }


        public IEnumerable<Objective> ApplyFilter(IFilter filter)
        {
            return null;
        }
    }
}
