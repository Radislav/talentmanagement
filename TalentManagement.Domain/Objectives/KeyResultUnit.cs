﻿namespace TalentManagement.Domain.Objectives
{
    public enum KeyResultUnit
    {
        Money,
        Percentage,
        Digit
    }
}
