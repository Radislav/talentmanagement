﻿using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain.Objectives;

namespace TalentManagement.Web.Models.Companies.Map
{
    internal class ObjectivesFocusTabMapper : IObjectiveFocusTabMapper
    {
        public ObjectivesFocusTab Map(IEnumerable<Objective> focusedObjectives, int filterByYear, string groupBy)
        {
            ObjectivesFocusTab tab = Map(focusedObjectives);
            tab.FilterByYear = filterByYear;
            tab.GroupBy = groupBy;

            return tab;
        }

        public ObjectivesFocusTab Map(IEnumerable<Objective> source)
        {
            return new ObjectivesFocusTab
                       {
                           ObjectivesInFocus = source.Select(s => new ObjectiveIconModel(s)).ToArray()
                       };
        }
    }
}