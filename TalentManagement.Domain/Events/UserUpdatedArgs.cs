﻿using System;
using System.Diagnostics.Contracts;
using TalentManagement.Domain.Users;

namespace TalentManagement.Domain.Events
{
    public class UserUpdatedArgs : IDomainEventArgs
    {
        public UserUpdatedArgs(User current,User previous)
        {
            Contract.Requires(current != null);
            UserCurrent = current;
            UserPrevious = previous;
        }

        public User UserCurrent { get; private set; }
        public User UserPrevious { get; private set; }

        public bool IsMarkedAsManager
        {
            get
            {
                return UserCurrent.IsManager &&  (UserPrevious == null || false == UserPrevious.IsManager);
            }
        }

        public bool IsMarkedAsNotManager
        {
            get
            {
                return false == UserCurrent.IsManager && (UserPrevious == null || UserPrevious.IsManager);
            }
        }

        public bool IsSubordinationAdded
        {
            get
            {
                return false == String.IsNullOrEmpty(UserCurrent.ManagerEmail) && 
                       (UserPrevious == null || String.IsNullOrEmpty(UserPrevious.ManagerEmail));
            }
        }

        public bool IsSubordinationDeleted
        {
            get
            {
                return String.IsNullOrEmpty(UserCurrent.ManagerEmail) &&
                       (UserPrevious == null || false == String.IsNullOrEmpty(UserPrevious.ManagerEmail));
            }
        }

        public bool IsSubordinationChanged
        {
            get
            {
                return false == String.IsNullOrEmpty(UserCurrent.ManagerEmail) &&
                       (UserPrevious != null && UserPrevious.ManagerEmail != UserCurrent.ManagerEmail);
            }
        }
    }
}
