﻿namespace TalentManagement.Web.Models.MyFeedbacks
{
    public class IndexModel : ICanActivateButton
    {   
        public IndexModel()
        {
            FeedbackCreateModel = new FeedbackCreateModel();
        }

        public string ActiveButton { get; set;}
        public FeedbackAttentionRequired[] FeedbacksAttentionRequired { get; set; }
        public FeedbackYouRequested[] FeedbacksYouRequested { get; set; }
        public FeedbackTheyRequested[] FeedbacksTheyRequested { get; set; }
        public FeedbackCreateModel FeedbackCreateModel { get; set; }
        public KudosCreateModel KudosCreateModel { get; set; }
        public DetailsModel Details { get; set; }

    }
}