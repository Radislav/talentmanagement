﻿using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Domain.Performance;
using TalentManagement.Web.Models.CycleDetails;
using TalentManagement.Web.Models.CycleDetails.Map;

namespace TalentManagement.Web.Controllers
{
    public class CycleDetailsController : Controller
    {
        private readonly IReviewCycleRepository _reviewCycleRepository;
        private readonly IReviewRepository _reviewRepository;
        private readonly IIndexModelMapper _indexModelMapper;

        public CycleDetailsController(IIndexModelMapper indexModelMapper,IReviewCycleRepository reviewCycleRepository,IReviewRepository reviewRepository)
        {
            _indexModelMapper = indexModelMapper;
            _reviewCycleRepository = reviewCycleRepository;
            _reviewRepository = reviewRepository;
        }
        
        public ActionResult Index(ObjectId id)
        {
            ReviewCycle cycle = _reviewCycleRepository.GetById(id);
            IndexModel indexModel = _indexModelMapper.Map(cycle,_reviewRepository.GetByCycle(cycle),_reviewCycleRepository.GetDescriptors());

            if (Request.IsAjaxRequest())
            {
                return PartialView("_ReviewsTable", indexModel);
            }
            return View(indexModel);
        }
    }
}
