﻿namespace TalentManagement.Domain.Feeds
{
    public enum FeedType
    {
        None,
        Alert,
        Objective,
        Update,
        Kudos
    }
}
