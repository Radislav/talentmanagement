﻿using System;
namespace TalentManagement.Domain
{
    [Serializable]
    public class EntityException : ApplicationException
    {
        private readonly object _entity;

        public EntityException() 
        {
        }

        public EntityException(string message)
            : base(message)
        {
          
        }

        public EntityException(string message,object entity): this(message)
        {
            _entity = entity;
        }

        public object Entity { get { return _entity; }
        }
    }
}
