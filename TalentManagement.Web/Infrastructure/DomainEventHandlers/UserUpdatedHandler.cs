﻿using System.Web;
using TalentManagement.Domain.Events;
using TalentManagement.Infrastructure.Cache;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Web.Infrastructure.Security;

namespace TalentManagement.Web.Infrastructure.DomainEventHandlers
{
    internal class UserUpdatedHandler : IHandle<UserUpdatedArgs>
    {
        private readonly IGlobalCache _globalCache;
        private readonly IAuthorizationService _authorizationService;

        public UserUpdatedHandler(IGlobalCache globalCache,IAuthorizationService authorizationService)
        {
            _globalCache = globalCache;
            _authorizationService = authorizationService;
        }

        public void Handle(UserUpdatedArgs args)
        {
            _globalCache.DeleteUserNames(args.UserCurrent.Id);

            SecurityTicket securityTicket = new SecurityTicket(HttpContext.Current);
            TenantPrincipal principal = _authorizationService.LoadPrincipal(HttpContext.Current.User.Identity.Name);
            securityTicket.SetPrincipal(principal);
        }
    }
}