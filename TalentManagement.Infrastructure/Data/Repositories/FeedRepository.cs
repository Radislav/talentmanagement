﻿using System.Collections.Generic;
using System.Linq;
using Autofac.Features.Indexed;
using TalentManagement.Domain;
using TalentManagement.Domain.Feeds;
using TalentManagement.Infrastructure.Data.Filter;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data.Repositories
{
    class FeedRepository : Repository<Feed> , IFeedRepository
    {
        private readonly IFilter _feedTypeFilter;

        public FeedRepository(IUnitOfWork unitOfWork, IPrincipalProvider principalProvider, IIndex<FilterType, IFilter> filters)
            : base(unitOfWork,principalProvider)
        {
            _feedTypeFilter = filters[FilterType.FeedType];
        }

        public IEnumerable<Feed> GetFeeds(IFilter filter)
        {
            return UnitOfWork.ApplyFilter<Feed>(filter)
                .OrderByDescending(f => f.CreatedAt);
        }


        public IEnumerable<Kudos> GetKudosByReceiver(string receiver)
        {
            _feedTypeFilter.FilterContext = new FeedTypeContext(FeedType.Kudos);

            return UnitOfWork.ApplyFilter<Feed>(_feedTypeFilter)
                             .Select(feed => new Kudos(feed))
                             .Where(kudos => kudos.Receivers.Contains(receiver))
                             .ToArray();
        }
    }
}
