﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using MongoDB.Bson;

namespace TalentManagement.Domain.Objectives
{
    [AggregateRoot]
    public class Objective : Entity , IOwned , IScope , IOwnedByCompany , IOwnedByTeam , IHasParticipants
    {
        //private const decimal ProgressCompleted = 100;
        //private const decimal ProgressNotStarted = 0;
        
        private Objective()
        {
        }
        
        public Objective(IEnumerable<KeyResult> keyResults)
        {
            KeyResults = keyResults.ListOrDefault();
            Contributors = new List<string>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Start { get; set; }
        public DateTime Due { get; set; }
        public ObjectiveVisibility Visibility { get; set; }
        public List<KeyResult> KeyResults { get; private set; }
        public List<string> Contributors { get; private set; }
        public ObjectId Icon { get; set; }

        private decimal _progress;
        public decimal Progress
        {
            get { return _progress; }
            set
            {
                _progress = value;
                //_status = _progress == ProgressNotStarted ? ObjectiveStatus.NotStarted : ObjectiveStatus.InProgress;
            }
        }

        private ObjectiveStatus _status;
        public ObjectiveStatus Status
        {
            get { return _status; }
            set
            {
                _status = value;
                /*
                switch (_status)
                {
                   case ObjectiveStatus.Completed:
                        _progress = ProgressCompleted;
                        break;
                    case ObjectiveStatus.NotStarted:
                        _progress = ProgressNotStarted;
                        break;
                }*/
            }
        }
        
        public KeyResult GetKeyResult(ObjectId keyResultId)
        {
            KeyResults = KeyResults ?? new List<KeyResult>();
            return KeyResults.GetSafe(kr => kr.SingleOrDefault(k => k.Id == keyResultId));
        }

        public void UpsertActionItem(ObjectId keyResultId,ActionItem actionItem)
        {
            Contract.Requires(actionItem != null);
            
            KeyResult keyResult = GetKeyResult(keyResultId);

            if (keyResult == null)
            {
                throw new EntityNotFoundException(typeof(KeyResult),keyResultId);
            }
            
            int actionIndex = keyResult.GetIndexOfActionItem(actionItem);

            if (actionIndex < 0)
            {
                actionItem.Id = ObjectId.GenerateNewId(DateTime.Now);
                keyResult.AddActionItem(actionItem);   
            }
            else
            {
                keyResult.InsertActionItem(actionItem,actionIndex);
            }
        }
        
        public void DeleteActionItem(ObjectId keyResultId,ObjectId actionItemId)
        {
            GetKeyResult(keyResultId).Do(kr => kr.RemoveActionItem(actionItemId));
        }

        public string CreatedBy { get; set; }

        public DateTime CreatedAt { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedAt { get; set; }

        public string ScopeId
        {
            get { return Convert.ToString(Id); }
        }

        public string ScopeName
        {
            get { return "Objective"; }
        }

        public string CompanyName { get; set; }

        public string Team { get; set; }

        public IEnumerable<string> Participants
        {
            get { return Contributors; }
        }

        public bool InFocus { get; set; }
    }
}
