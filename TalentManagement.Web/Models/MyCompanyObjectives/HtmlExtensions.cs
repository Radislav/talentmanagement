﻿using System.Web.Mvc;

namespace TalentManagement.Web.Models.MyCompanyObjectives
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString GroupingButton(this HtmlHelper<PageModel> html,
                                                           string name)
        {
            TagBuilder tag = new TagBuilder("button");
            tag.AddCssClass("btn");
            tag.AddCssClass("btn-primary");
            if (html.ViewData.Model.ActiveButton == name)
            {
                tag.AddCssClass("active");
            }
            tag.MergeAttribute("type","submit");
            tag.MergeAttribute("name",name);
            tag.InnerHtml = name;
            
            return new MvcHtmlString(tag.ToString());
        }
    }
}