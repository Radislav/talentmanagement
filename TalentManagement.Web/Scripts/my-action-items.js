﻿$(function () {

    //Filter by click
    $('#activity_stats .btn-group button').click(function (e) {
        e.preventDefault();

        $.ajax({
            url: $(this).attr('ref'),
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                $('#action-item-containers').html(response);
            }
        });
    });

    $('#new-action-item .modal-footer button').click(function () {

        var data = {
             Name : $('#NewActionItem_Name').val(),
             DueString: $('#NewActionItem_DueString').val()
        };

        $.ajax({
            url: $(this).attr('ref'),
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response.IsValid) {
                    $('#new-action-item').modal('hide');
                    location.reload();
                } else {
                    validation.showJsonErrors(response, '#new-action-item', 'NewActionItem');
                }
            }
        });
    });
});


