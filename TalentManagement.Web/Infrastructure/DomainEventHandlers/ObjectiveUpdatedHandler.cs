﻿using System.Diagnostics.Contracts;
using TalentManagement.Domain.Events;
using TalentManagement.Domain.Feeds;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Web.Infrastructure.DomainEventHandlers
{
    class ObjectiveUpdatedHandler : IHandle<ObjectiveUpdatedArgs>
    {
        private readonly IFeedFactory _feedFactory;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IFeedRepository _feedRepository;

        public ObjectiveUpdatedHandler(IFeedFactory feedFactory,IPrincipalProvider principalProvider,IFeedRepository feedRepository)
        {
            Contract.Requires(feedFactory != null);
            Contract.Requires(principalProvider != null);
            Contract.Requires(feedRepository != null);

            _feedFactory = feedFactory;
            _principalProvider = principalProvider;
            _feedRepository = feedRepository;
        }

        public void Handle(ObjectiveUpdatedArgs args)
        {
            Feed feed = _feedFactory.CreateObjectiveFeed(args.ObjectiveOld, args.ObjectiveNew,_principalProvider.GetOwnderDescriptor());
            _feedRepository.Insert(feed);
        }
    }
}