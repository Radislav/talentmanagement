﻿using System;

namespace TalentManagement.Infrastructure.Security
{
    public interface IAuthorizationService
    {
        TenantPrincipal LoadPrincipal(string userName);
        TenantPrincipal CreatePrincipal(string userName,
                                        bool isAuthenticated,
                                        DateTime lastSignIn,
                                        string firstName,
                                        string lastName,
                                        string team,
                                        string[] roles);
    }
}
