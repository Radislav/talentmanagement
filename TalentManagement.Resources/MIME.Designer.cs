﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.586
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TalentManagement.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class MIME {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal MIME() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("TalentManagement.Resources.MIME", typeof(MIME).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to image/bmp.
        /// </summary>
        public static string bmp {
            get {
                return ResourceManager.GetString("bmp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to image/gif.
        /// </summary>
        public static string gif {
            get {
                return ResourceManager.GetString("gif", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to image/jpeg.
        /// </summary>
        public static string jpeg {
            get {
                return ResourceManager.GetString("jpeg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to image/jpeg.
        /// </summary>
        public static string jpg {
            get {
                return ResourceManager.GetString("jpg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to image/png.
        /// </summary>
        public static string png {
            get {
                return ResourceManager.GetString("png", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to image/tiff.
        /// </summary>
        public static string tif {
            get {
                return ResourceManager.GetString("tif", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to imge/tiff.
        /// </summary>
        public static string tiff {
            get {
                return ResourceManager.GetString("tiff", resourceCulture);
            }
        }
    }
}
