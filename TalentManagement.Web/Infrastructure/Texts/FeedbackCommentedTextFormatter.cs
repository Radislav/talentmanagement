﻿using TalentManagement.Infrastructure;

namespace TalentManagement.Web.Infrastructure.Texts
{
    class FeedbackCommentedTextFormatter : FeedbackTextFormatter
    {
        public FeedbackCommentedTextFormatter(IUrlProvider urlProvider) : base(urlProvider)
        {
        }
    }
}
