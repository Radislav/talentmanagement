﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace TalentManagement.Web.Models.Binder
{
    public interface IEnumerableBinder<T> : ICustomModelBinder<IEnumerable<T>>
    {
        IEnumerableBinder<T> SetIdPropertyName(string propertyName);
        Array BindArray(ControllerContext controllerContext, ModelBindingContext bindingContext);
    }
}