﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Diagnostics.Contracts;

namespace TalentManagement.Web.Models.Binder
{
    public class EnumerableBinder<T> : CustomModelBinder<IEnumerable<T>> , IEnumerableBinder<T>
    {
        private readonly ICustomModelBinder<T> _modelBinder;
        private string _idPropertyName;
        public const int MaxItemsCount = 100;

        private ControllerContext _controllerContext;
        private ModelBindingContext _bindingContext;
     
        public EnumerableBinder(ICustomModelBinder<T> modelBinder)
        {
            Contract.Requires(modelBinder != null);

            _modelBinder = modelBinder;
        }
        
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            _bindingContext = bindingContext;
            _controllerContext = controllerContext;
            
            string arrayKey = GetArrayKey();

            bool doesSingleValueInContext = DoesValueExists(bindingContext, arrayKey);

            return doesSingleValueInContext ? GetSingleValueList() : 
                                              GetMultiValueList(arrayKey);
        }

        private List<T> GetSingleValueList()
        {
            List<T> models = new List<T>();
            T model = (T)_modelBinder.SetPrefix(Prefix)
                                     .SetIndex(-1)
                                     .BindModel(_controllerContext, _bindingContext);
            models.Add(model);

            return models;
        }

        private List<T> GetMultiValueList(string arrayKey)
        {
            List<T> models = new List<T>();

            for (int index = 0; index < MaxItemsCount; index++)
            {
                if (DoesValueExists(_bindingContext, arrayKey, index))
                {
                    T model = (T) _modelBinder.SetPrefix(Prefix)
                                      .SetIndex(index)
                                      .BindModel(_controllerContext, _bindingContext);
                    models.Add(model);
                }
                else
                {
                    break;
                }
            }

            return models;
        }

        private bool DoesValueExists(ModelBindingContext bindingContext, string arrayKey,int index = -1)
        {
            string currenKey = index == -1 ? arrayKey :  BinderValueProvider.GetIndexedKey(arrayKey, index);
            return null != bindingContext.ValueProvider.GetValue(currenKey);
        }
        
        private string GetArrayKey()
        {
           return String.IsNullOrEmpty(Prefix) ? _idPropertyName : String.Concat(Prefix, ".", _idPropertyName);
        }
        
        public IEnumerableBinder<T> SetIdPropertyName(string propertyName)
        {
            _idPropertyName = propertyName;
            return this;
        }
        
        public Array BindArray(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            return ((IEnumerable<T>) BindModel(controllerContext, bindingContext))
                                   .ToArray();
        }
        /*
        private int GetItemsCount(ModelBindingContext bindingContext)
        {
            string key = String.IsNullOrEmpty(Prefix) ? _idPropertyName : String.Concat(Prefix, ".", _idPropertyName);
            ValueProviderResult result = bindingContext.ValueProvider.GetValue(key);
            return result == null ? 0 : ((Array)(result.ConvertTo(typeof(Array)))).Length;
        }
         */
    }


}