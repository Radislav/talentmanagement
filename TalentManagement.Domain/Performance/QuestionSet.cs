﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using MongoDB.Bson;

namespace TalentManagement.Domain.Performance
{
    public class QuestionSet : Entity
    {
        private QuestionSet() 
        {
            QuestionIds = new List<ObjectId>();
        }

        public QuestionSet(QuestionSetType setType, IEnumerable<ObjectId> questions, DateTime dueDate)
            : this()
        {
            Contract.Requires(setType != QuestionSetType.None);
            Contract.Requires(dueDate >= DateTime.Today);

            SetType = setType;
            QuestionIds = questions ?? new List<ObjectId>();
            DueDate = dueDate;
        }

        public QuestionSetType SetType { get; private set; }
        public IEnumerable<ObjectId> QuestionIds { get; private set; }
        public DateTime DueDate { get; private set; }
    }
}
