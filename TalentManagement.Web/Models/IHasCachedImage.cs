﻿using System.Web;

namespace TalentManagement.Web.Models
{
    /// <summary>
    /// Contract for model with posted files (images). This contract allows to store image in IUserCache when it was posted. 
    /// See <see cref="TalentManagement.Web.Infrastructure.Cache.ModelImageCacheAttribute"/> for using. 
    /// </summary>
    public interface IHasCachedImage
    {
        /// <summary>
        /// The cache key wich will be used for posted image.
        /// </summary>
        /// <returns></returns>
        string GenerateCacheKey();
        /// <summary>
        /// Posted image. 
        /// </summary>
        HttpPostedFileBase Image { get; set; }
    }
}