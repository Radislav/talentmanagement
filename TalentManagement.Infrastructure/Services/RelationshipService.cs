﻿using System;
using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Data;

namespace TalentManagement.Infrastructure.Services
{
    internal class RelationshipService : IRelationshipService 
    {
        private readonly IUserRepository _userRepository;
        private readonly ITeamRepository _teamRepository;
        private readonly IFilter<IOwnedByCompany> _companyFilter;
        private readonly IUnitOfWork _unitOfWork;
        

        public RelationshipService(IUserRepository userRepository,ITeamRepository teamRepository,IFilter<IOwnedByCompany> companyFilter,IUnitOfWork unitOfWork)
        {
            _companyFilter = companyFilter;
            _userRepository = userRepository;
            _teamRepository = teamRepository;
            //Use unit of work, becuase userRepository.Update will raise event. This event will be cautch by the UserUpdatedHandler and it will call RelationshipService making loop of calls.
            _unitOfWork = unitOfWork;
        }

        public void MarkAsManager(User manager)
        {
            manager.IsManager = true;

            CreateUpdateTeamsFor(manager, () => _userRepository.GetSubordinatesOf(manager.Id, _companyFilter));
        }

        private void CreateUpdateTeamsFor(User manager, Func<IEnumerable<User>> newSubordinatesProvider)
        {
            Team[] teams = _teamRepository.GetTeams(manager)
                                          .ToArray();
            if (false == teams.Any())
            {
                teams = new[] { new Team(manager) };
            }

            //Add subordinates to the manager`s teams 
            newSubordinatesProvider().Map(member =>
                                           teams.Map(team =>
                                           {
                                               team.AddMemeber(member);
                                               team.AddMemeber(manager);
                                               
                                               _unitOfWork.Update(member);
                                               _unitOfWork.Update(manager);

                                           }));
            teams.Map(team => _teamRepository.Upsert(team));
        }

        public void NotManager(User manager)
        {
            _teamRepository.GetTeams(manager)
                          .Map(team => _teamRepository.DeleteById(team.Id));

            _userRepository.GetSubordinatesOf(manager.Id, _companyFilter)
                           .Map(teamMember =>
                           {
                               teamMember.ManagerEmail = string.Empty;
                               _unitOfWork.Update(teamMember);
                           });

            manager.IsManager = false;
            _unitOfWork.Update(manager);
        }

        public void ChangeTeam(User user, User previousManager, User newManager)
        {
            RemoveFromTeams(user,previousManager);
            AddToTeam(user,newManager);
        }

        public void AddToTeam(User user, User newManager)
        {
            user.ManagerEmail = newManager.Id;
            CreateUpdateTeamsFor(newManager,() => new []{user});
        }

        public void RemoveFromTeams(User user,User previousManager)
        {
            user.ManagerEmail = null;

            if (previousManager != null)
            {
                _teamRepository.GetTeams(previousManager)
                              .Map(team =>
                              {
                                  team.DeleteMember(user);
                                  _teamRepository.Update(team);
                              });   
            }

            _unitOfWork.Update(user);
        }
    }
}
