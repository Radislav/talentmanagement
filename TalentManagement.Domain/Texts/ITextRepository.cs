﻿using System.Collections.Generic;

namespace TalentManagement.Domain.Texts
{
    public interface ITextRepository 
    {
        IEnumerable<Text> GetTexts();
        Text GetText(TextType type);
    }
}
