﻿namespace TalentManagement.Web.Infrastructure.Bundles
{
    public enum BundleType
    {
        css,
        js
    }
}