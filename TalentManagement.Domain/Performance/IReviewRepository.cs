﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace TalentManagement.Domain.Performance
{
    public interface IReviewRepository : IRepository<Review> 
    {
        void InsertReviews(IEnumerable<Review> reviews);
        IEnumerable<Review> GetBySetIds(IEnumerable<ObjectId>  questionSetIds);
        IEnumerable<Review> GetByCycle(ReviewCycle cycle);
        IEnumerable<Review> GetByCycle(ObjectId cycleId);
    }
}
