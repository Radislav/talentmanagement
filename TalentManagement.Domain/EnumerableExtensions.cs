﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace TalentManagement.Domain
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<TEntity> ApplyFilter<TEntity>(this IEnumerable<TEntity> entities, IFilter filter)
         where TEntity : class
        {
            Contract.Requires(entities != null);
            Contract.Requires(filter != null);

            return filter.Init(entities)
                        .Filter()
                        .AsEnumerable()
                        .Cast<TEntity>();
        }
    }
}
