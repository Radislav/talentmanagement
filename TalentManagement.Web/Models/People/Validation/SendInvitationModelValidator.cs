﻿using FluentValidation;
using System.Web.Mvc;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.People.Validation
{
    public class SendInvitationModelValidator : AbstractValidator<SendInvitationModel>, TalentManagement.Infrastructure.Validation.IValidator<SendInvitationModel>
    {
        public SendInvitationModelValidator()
        {
            IResourceProvider resourceProvider = DependencyResolver.Current.GetService<IResourceProvider>();

            RuleFor(m => m.EmailLocal)
                .NotEmpty();

            RuleFor(m => m.EmailLocal)
                .Matches(RegularExpressions.EmailUserName)
                .WithMessage(resourceProvider.ErrorEmailInvalid);
        }

        public TalentManagement.Infrastructure.Validation.ValidationResult ValidateInstance(SendInvitationModel instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}