﻿using System;
using System.Text.RegularExpressions;
using TalentManagement.Infrastructure.Resources;

namespace TalentManagement.Infrastructure.Validation
{
    class BasicValidator : IBasicValidator
    {
        private readonly string _errorEmailInvalid;
        
        public BasicValidator(IResourceProvider resourceProvider)
        {
            _errorEmailInvalid = resourceProvider.ErrorEmailInvalid;
        }

        public ValidationResult ValidateEmail(string email)
        {
            if (false == String.IsNullOrEmpty(email) && Regex.IsMatch(email, RegularExpressions.Email))
            {
                return new ValidationResult();
            }

            return new ValidationResult(new [] {new ValidationFailure(null, _errorEmailInvalid, email)});
        }
    }
}
