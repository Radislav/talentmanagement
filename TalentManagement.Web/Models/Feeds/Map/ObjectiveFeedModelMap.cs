﻿using MongoDB.Bson;
using TalentManagement.Domain.Feeds;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Feeds.Map
{
    class ObjectiveFeedModelMap :  IMap<Feed,ObjectiveFeedModel>
    {
        private readonly IMapper<Feed, FeedModel> _baseMapper;
        public ObjectiveFeedModelMap(IMapper<Feed, FeedModel> baseMapper)
        {
            _baseMapper = baseMapper;
        }

        public void Configure(IMapperConfiguration<Feed, ObjectiveFeedModel> configuration)
        {
            FeedModel feedModel = null;

            configuration.BeforeMap((feed, model) => feedModel = _baseMapper.Map(feed));
            configuration.ForMember(destination => destination.Properties,source => feedModel.Properties);
            configuration.ForMember(destination => destination.CommentSection, source => feedModel.CommentSection);
            configuration.ForMember(destination => destination.Texts,source => feedModel.GetProperty("Text").Split(';'));
            configuration.ForMember(destination => destination.Progress, source => feedModel.GetProperty("Progress"));
            configuration.ForMember(destination => destination.Status, source => feedModel.GetProperty("Status"));
            configuration.ForMember(d => d.ObjectiveId, s => new ObjectId(feedModel.GetProperty("Id")));
            configuration.ForMember(d => d.ObjectiveName, s => s.GetPropertyValue("Name"));
        }
    }
}