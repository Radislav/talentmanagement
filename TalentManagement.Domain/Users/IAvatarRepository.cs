﻿namespace TalentManagement.Domain.Users
{
    public interface IAvatarRepository : IRepositoryWithTypedId<Avatar,string>
    {
        Avatar GetOnlySmallImage(string userEmail);
    }
}
