﻿using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TalentManagement.AppServices;
using TalentManagement.Infrastructure;
using TalentManagement.Infrastructure.Configuration;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Web;

namespace TalentManagement.Test.Infrastructure.Configuration
{
    [TestClass]
    public class AppConfigurationTest : MockedTestClass<IAppConfiguration>
    {
        [TestMethod]
        public void GetTenantName_should_call_GetIdentity()
        {
            IAppConfiguration target = CreateTarget();
            target.GetTenantName();

            Container.Resolve<Mock<IPrincipalProvider>>().Verify(m => m.GetIdentity(),Times.Once());
        }

        [TestMethod]
        public void GetTenantName_should_extract_tenant_as_email_domain()
        {
            IAppConfiguration target = CreateTarget();
            Assert.AreEqual("mydomain", target.GetTenantName());
        }

        [TestMethod]
        public void GetTenantName_should_return_DefaultTenant_if_identity_NULL()
        {
            ContainerBuilder container = GetContainerBuilder();
            container.RegisterMock(new Mock<IPrincipalProvider>());

            IAppConfiguration target = CreateTarget(container.Build());
            Assert.AreEqual("DefaultTenant", target.GetTenantName());
        }

        protected override void RegisterMocks(ContainerBuilder containerBuilder)
        {
            Mock<IPrincipalProvider> mock = new Mock<IPrincipalProvider>();
            mock.Setup(m => m.GetIdentity())
                .Returns(new TenantIdentity("email@mydomain.com"));
            containerBuilder.RegisterMock(mock);
            base.RegisterMocks(containerBuilder);
        }

        protected override ContainerBuilder GetContainerBuilder()
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterModule(new InfrastructureRegistrationModule());
            builder.RegisterModule(new WebRegistrationModule());
            builder.RegisterModule(new AppServiceRegistrationModule());
            return builder;
        }
    }
}
