﻿using System.Collections.Generic;
using System.Web.Mvc;
using Autofac.Features.Indexed;
using MongoDB.Bson;
using TalentManagement.Domain;
using TalentManagement.Domain.Companies;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Infrastructure.Validation;
using TalentManagement.Web.Infrastructure.Cache;
using TalentManagement.Web.Models.Companies;
using TalentManagement.Web.Models.Companies.Map;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Controllers
{
    public class CompanyController : Controller
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IIndexModelMapper _indexModelMapper;
        private readonly IMapper<GeneralTab, Company> _companyMapper;
        private readonly IMapper<BadgeModel, Badge> _badgeMapper;
        private readonly IValidator<BadgeModel> _badgeModelValidator;
        private readonly IBadgeRepository _badgeRepository;
        private readonly IObjectiveRepository _objectiveRepository;
        private readonly IObjectiveFocusTabMapper _objectivesFocusTabMapper;
   
        public CompanyController(ICompanyRepository companyRepository,
                                 IBadgeRepository badgeRepository,
                                 IIndex<FilterType, IObjectiveRepository> objectiveRepositoriesIndex,
                                 IPrincipalProvider principalProvider,
                                 IIndexModelMapper indexModelMapper,
                                 IMapper<GeneralTab,Company> companyMapper,
                                 IMapper<BadgeModel,Badge> badgeMapper,
                                 IValidator<BadgeModel> badgeModelValidator,
                                 IObjectiveFocusTabMapper objectivesFocusTabMapper)
        {
            _indexModelMapper = indexModelMapper;
            _companyMapper = companyMapper;
            _companyRepository = companyRepository;
            _badgeRepository = badgeRepository;
            _objectiveRepository = objectiveRepositoriesIndex[FilterType.OwnedByCompany];
            _principalProvider = principalProvider;
            _badgeMapper = badgeMapper;
            _badgeModelValidator = badgeModelValidator;
            _objectivesFocusTabMapper = objectivesFocusTabMapper;
        }

        [HttpGet]
        public ActionResult Index(string id)
        {
            string tenantName = _principalProvider.GetTenant();
            Company company = _companyRepository.GetByTenantWithoutPictures(tenantName);
            IEnumerable<Badge> badges;

            if (company == null)
            {
                //Create default empty company
                company = new Company {Name = tenantName};
                _companyRepository.UpsertIgnoreBadges(company);
                badges = new Badge[0];
            }
            else
            {
                badges = _badgeRepository.GetBadgesLight(company.Id);    
            }
            
            IndexModel indexModel = _indexModelMapper.Map(company,badges,_objectiveRepository.GetFocusObjectives());
            indexModel.ActiveButton = id;

            return View(indexModel);
        }
        
        [HttpPost]
        public ActionResult Index(GeneralTab generalTab)
        {
            Company company = _companyMapper.Map(generalTab);

            if (ModelState.IsValid)
            {
                _companyRepository.UpsertIgnoreBadges(company);
                company = _companyRepository.GetById(company.Id);
            }
            
            IndexModel indexModel = _indexModelMapper.Map(company,_badgeRepository.GetBadgesLight(company.Id),_objectiveRepository.GetFocusObjectives());
            return View("Index", indexModel);
        }

        [HttpPost]
        [ModelImageCache]
        public ActionResult UpsertBadge(BadgeModel badgeModel)
        {
            ActionResult result = new EmptyResult();
            
            if (badgeModel.SubmitClicked)
            {
                ValidationResult validationResult = _badgeModelValidator.ValidateInstance(badgeModel);

                if (validationResult.IsValid)
                {
                    Badge badge = _badgeRepository.Upsert(_badgeMapper.Map(badgeModel));
                    Company company = _companyRepository.GetById(badgeModel.CompanyId) ?? new Company {Name = _principalProvider.GetTenant()};
                    company.AddBadgeId(badge.Id);
                    
                    _companyRepository.UpsertBadgesOnly(company);
                }

                result = validationResult.AsJson();
            }

            return result;
        }

        [HttpPost]
        public ActionResult EnableBadge(ObjectId companyId,ObjectId badgeId,bool enable)
        {
            Badge badge = _badgeRepository.GetById(badgeId);
            badge.IsEnabled = enable;
            _badgeRepository.Update(badge);

            Company company = _companyRepository.GetById(companyId);
            if (enable)
            {
                company.AddBadgeId(badgeId);
            }
            else
            {
                company.RemoveBadgeId(badgeId);
            }
            
            return Json(new { EnableAction = BadgeModel.GetEnablingAction(enable),IsEnabled = enable });
        }

        [HttpPost]
        public ActionResult ToggleObjectiveFocus(ObjectId objectiveId,bool focus,ObjectivesFocusTab objectivesFocusTab)
        {
            Objective objective = _objectiveRepository.GetById(objectiveId);
            objective.InFocus = focus;
            _objectiveRepository.Update(objective);

            return PartialView("_ObjectivesFocus",
                               _objectivesFocusTabMapper.Map(_objectiveRepository.GetFocusObjectives(),objectivesFocusTab.FilterByYear,objectivesFocusTab.GroupBy));
        }
    }
}