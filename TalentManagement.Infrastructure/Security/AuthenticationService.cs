﻿using System;
using TalentManagement.Domain.Users;

namespace TalentManagement.Infrastructure.Security
{
    internal class AuthenticationService : IAuthenticationService
    {
        private readonly IUserRepository _userRepository;
        private readonly IHashComputer _hashComputer;

        public AuthenticationService(IUserRepository userRepository, IHashComputer hashComputer)
        {
            _userRepository = userRepository;
            _hashComputer = hashComputer;
        }

        public LoginResult Login(string email, string password)
        {
            if (string.IsNullOrEmpty(email) || String.IsNullOrEmpty(password))
            {
                return LoginResult.InvalidUserOrPassword;
            }

            User user = _userRepository.GetById(email);
            if (user == null)
            {
                return LoginResult.InvalidUserOrPassword;
            }

            if (user.HashedPassword != _hashComputer.ComputeHash(password))
            {
                return LoginResult.InvalidUserOrPassword;
            }

            user.LastSignIn = DateTime.Now;
            _userRepository.Update(user);
            return LoginResult.Ok;
        }
    }
}
