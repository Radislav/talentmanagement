﻿using TalentManagement.Infrastructure.Converter;

namespace TalentManagement.Web.Models.MyCompanyObjectives.Map
{
    class DefaultTitleFormatter<TGroupKey> : ITitleFormatter<TGroupKey>
    {
        private readonly ITypeConverter<TGroupKey, string> _typeConverter;

        public DefaultTitleFormatter(ITypeConverter<TGroupKey,string> typeConverter)
        {
            _typeConverter = typeConverter;
        }

        public string FormatTitle(TGroupKey groupKey)
        {
            return _typeConverter.ConvertBack(groupKey);
        }
    }
}