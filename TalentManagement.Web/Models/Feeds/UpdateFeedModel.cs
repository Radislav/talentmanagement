﻿namespace TalentManagement.Web.Models.Feeds
{
    public class UpdateFeedModel : FeedModel
    {
        public string ScopeName { get; set; }
        public string ScopeId { get; set; }
        public string Text { get; set; }
    }
}