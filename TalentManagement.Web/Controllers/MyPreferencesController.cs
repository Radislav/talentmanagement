﻿using System.Linq;
using System.Web.Mvc;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Web.Models.MyPreferences;

namespace TalentManagement.Web.Controllers
{
    public class MyPreferencesController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper<User, IndexModel> _indexModelMapper;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IMapper<NotificationSettingModel, NotificationSetting> _notificationSettingMapper;

        public MyPreferencesController(IUserRepository userRepository,
            IMapper<User,IndexModel> indexModelMapper,
            IMapper<NotificationSettingModel,NotificationSetting> notificationSettingMapper,
            IPrincipalProvider principalProvider)
        {
            _userRepository = userRepository;
            _indexModelMapper = indexModelMapper;
            _principalProvider = principalProvider;
            _notificationSettingMapper = notificationSettingMapper;
        }

        [HttpGet]
        public ActionResult Index()
        {
            User user = _userRepository.GetById(_principalProvider.GetUserName());
            return View(_indexModelMapper.Map(user));
        }

        [HttpPost]
        public ActionResult Index(NotificationSettingModel[] notifications)
        {
            User user = _userRepository.GetById(_principalProvider.GetUserName());
            user.NotificationSettings = notifications.Select(n => _notificationSettingMapper.Map(n));
            _userRepository.UpdateNotificationSettings(user);

            return Json("Updated");
        }

    }
}