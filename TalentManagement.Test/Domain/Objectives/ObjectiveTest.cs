﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using TalentManagement.Domain.Objectives;

namespace TalentManagement.Test.Domain.Objectives
{
    [TestClass]
    public class ObjectiveTest
    {
        [TestMethod]
        public void UpsertActionItem_should_add_new_ActionItem()
        {
            ObjectId keyResultId = new ObjectId("50cdd4182e40ad1908230ff5");
            KeyResult keyResult = new KeyResult(keyResultId);
            ActionItem actionItem = new ActionItem("","",DateTime.Now.AddDays(1),true);

            Objective target = new Objective(new [] {keyResult});
                                   
            target.UpsertActionItem(keyResultId,actionItem);

            Assert.IsTrue(keyResult.ActionItems.Contains(actionItem));
        }

        [TestMethod]
        public void UpsertActionItem_should_update_existed_ActionItem()
        {
            ObjectId actionItemId = new ObjectId("50cdd4d62e40ad1908230ff8");
            ActionItem actionItem = new ActionItem("","",DateTime.Now.AddDays(1),true) {Id = actionItemId};
            

            ObjectId keyResultId = new ObjectId("50cdd4182e40ad1908230ff5");
            KeyResult keyResult = new KeyResult(keyResultId, new []{actionItem});


            Objective target = new Objective(new[] { keyResult });

            ActionItem updatedActionItem = new ActionItem("NewName", "newowner",DateTime.Now.AddDays(1),true)
                                               {
                                                   Id = actionItemId
                                               };
            target.UpsertActionItem(keyResultId, updatedActionItem);


            Assert.IsTrue(keyResult.ActionItems.Contains(updatedActionItem));
            Assert.AreEqual(keyResult.ActionItems.Count(),1);
        }
    }
}
