﻿using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Data;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Test.Infrastructure.Data.Repositores
{
    
    
    /// <summary>
    ///This is a test class for UserServiceTest and is intended
    ///to contain all UserServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class UserRepositoryTest : MockedTestClass<IUserRepository>
    {

        private const string UserValid = "user_valid";
        private const string UserInvalid = "user_invalid";
        private const string PasswordValid = "password_valid";
        private const string PasswordValidHashed = "password_valid";
        private const string PasswordInvalid = "password_invalid";

        [TestMethod]
        public void CreateUser_should_return_Duplicate_if_user_already_exists()
        {
            IUserRepository target = CreateTarget();
            UserCreateResult result = target.CreateUser(new User(UserValid));

            Assert.AreEqual(UserCreateResult.Duplicate, result);
        }

        [TestMethod]
        public void CreateUser_should_return_OK_if_user_created()
        {
            IUserRepository target = CreateTarget();

            var ur = Container.Resolve<IUserRepository>();

            UserCreateResult result = target.CreateUser(new User(UserInvalid));

            Assert.AreEqual(UserCreateResult.Ok, result);
        }
        protected override void RegisterMocks(ContainerBuilder builder)
        {
            builder.RegisterMock<IUnitOfWork>(
                mock =>
                {
                    mock.Setup(uow => uow.GetById<User,string>(UserValid)).Returns(new User(UserValid) {  HashedPassword = PasswordValidHashed });
                    mock.Setup(uow => uow.GetById<User, string>(UserInvalid)).Returns((User)null).Verifiable();

                });

            builder.RegisterMock<IHashComputer>(
               mock =>
               {
                   mock.Setup(hasher => hasher.ComputeHash(PasswordInvalid)).Returns(PasswordInvalid);
                   mock.Setup(hasher => hasher.ComputeHash(PasswordValid)).Returns(PasswordValidHashed);
               });

        }
    }
}
