﻿using System.Collections.Generic;
using TalentManagement.Domain.Companies;
using TalentManagement.Domain.Objectives;

namespace TalentManagement.Web.Models.Companies.Map
{
    public interface IIndexModelMapper
    {
        IndexModel Map(Company company, IEnumerable<Badge> badges,IEnumerable<Objective> objectivesInFocus);
    }
}
