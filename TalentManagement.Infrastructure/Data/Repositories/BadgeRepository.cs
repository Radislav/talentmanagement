﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using TalentManagement.Domain.Companies;
using TalentManagement.Domain.Events;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data.Repositories
{
    internal class BadgeRepository : Repository<Badge> , IBadgeRepository
    {
        private readonly Lazy<IDomainEvents> _domainEvents;

        public BadgeRepository(IUnitOfWork unitOfWork, Lazy<IDomainEvents> domainEvents,IPrincipalProvider principalProvider)
            :base(unitOfWork,principalProvider)
        {
            _domainEvents = domainEvents;
        }
        
        public IEnumerable<Badge> GetBadgesLight(ObjectId companyId)
        {
            return AsQueryable()
                        .Where(b => b.CompanyId == companyId)
                        .Select(b => BadgeWithoutImage(b))
                        .ToArray();
        }

        private static Badge BadgeWithoutImage(Badge b)
        {
            return new Badge(b.Id, b.CompanyId)
            {
                Description = b.Description,
                ImageType = b.ImageType,
                IsEnabled = b.IsEnabled,
                Title = b.Title
            };
        }

        public override void Update(Badge entity)
        {
            if (entity.Image == null)
            {
                Badge previousEntity = GetById(entity.Id);
                entity.Image = previousEntity.Image;
                entity.ImageType = previousEntity.ImageType;
            }

            base.Update(entity);
            _domainEvents.Value.Raise(new BadgeUpdatedArgs(entity.CompanyId,entity.Id,entity.IsEnabled));
        }

        public override void Insert(Badge entity)
        {
            base.Insert(entity);
            _domainEvents.Value.Raise(new BadgeUpdatedArgs(entity.CompanyId, entity.Id,true));
        }


        public IEnumerable<Badge> GetBadgesLight(IEnumerable<ObjectId> badgeIds)
        {
            return AsQueryable()
                       .Where(b => badgeIds.Contains(b.Id))
                       .Select(b => BadgeWithoutImage(b))
                       .ToArray();
        }
    }
}
