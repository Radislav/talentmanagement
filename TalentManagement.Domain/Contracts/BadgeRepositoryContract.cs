﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using MongoDB.Bson;
using TalentManagement.Domain.Companies;

namespace TalentManagement.Domain.Contracts
{
    [ContractClassFor(typeof (IBadgeRepository))]
    internal abstract class BadgeRepositoryContract : IBadgeRepository
    {

        public IEnumerable<Badge> GetBadgesLight(ObjectId companyId)
        {
            Contract.Ensures(Contract.Result<IEnumerable<Badge>>() != null);
            return null;
        }


        public void Insert(Badge entity)
        {
        }

        public void DeleteById(ObjectId id)
        {
        }

        public void Update(Badge entity)
        {
        }

        public IQueryable<Badge> AsQueryable()
        {
            return null;
        }

        public Badge GetById(ObjectId id)
        {
            return null;
        }

        public Badge Upsert(Badge entity)
        {
            return null;
        }


        public IEnumerable<Badge> ApplyFilter(IFilter filter)
        {
            return null;
        }


        public IEnumerable<Badge> GetBadgesLight(IEnumerable<ObjectId> badgeIds)
        {
            Contract.Requires(badgeIds != null);
            Contract.Ensures(Contract.Result<IEnumerable<Badge>>() != null);

            return null;
        }
    }
}