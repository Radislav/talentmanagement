﻿function ObjectiveCreatePage() {
    
    function step2() {
        //Click on "Click here to add key result"
        $('#create-key-result').off('click').on("click", function (e) {
            e.preventDefault();
            var data = createStep2Model();
            data.CurrentKeyResult = null;

            setCurrentKeyResult(data);
        });
        //Update key result
        $('.key-result-container #clickToEdit a').off('click').on("click", function () {
            var data = createStep2Model();
            data.CurrentKeyResult = createKeyResult($(this).parents('.key-result-container'),'keyResult');
            setCurrentKeyResult(data);
        });

        //Add key result
        $('#add-key-result').off('click').on('click', function () {
            var data = createStep2Model();
            data.CurrentKeyResult = createKeyResult($('#new-key-result'),'CurrentKeyResult');

            addCurrentKeyResult(data);
        });

        //Delete key result
        $('.key-result-container #deleteKeyResult').off('click').on('click', function () {
            var step2Model = createStep2Model();
            var keyResult = createKeyResult($(this).parents('.key-result-container'), 'keyResult');
            var data = {
                step2Model: step2Model,
                keyResultIndex: keyResult.Index
            };
            deleteKeyResult(data);
        });
    }

    function step3() {
        //Click on user picker + button
        $('#pickUserButton').off('click').on('click', function () {
            var data = createStep3Model();
            addContributor(data);
        });
    }

    function step4() {
        //Initialize uploader
        $('#step4 input[type="file"]').fileupload({
            dataType: 'json',
            url: urls.addObjectiveIcon(),
            add: function (e, data) {
                
                data.formData = {
                    IconsJson: $('#step4 #IconsJson').val()
                };
                data.submit();
            },
            done: function (e, data) {
                if (data != null && data.result != null) {
                    var result = data.result;
                    if (result.IsValid) {
                        appendIcon(result.NewIconId);
                        clickOnObjectiveIcon();
                    } else {
                        validation.showSummaryErrors(result, '#step4');
                    }
                }
            }
        });
        
        //enableDisableSubmit();
        clickOnObjectiveIcon();
      
    }

    function clickOnObjectiveIcon() {
        //Click on icon
        $('#step4 .objective-icons img').off('click').on('click', function () {

            $('#step4 .objective-icons .image-small')
                .removeClass('image-small')
                .addClass('image-smaller');

            $('#CurrentIconId').val(extractObjectiveIconId($(this).attr('src')));

         //   enableDisableSubmit();

            $(this).removeClass('image-smaller')
                   .addClass('image-small');
        });
    }
    /*
    function enableDisableSubmit() {
        if ($('#CurrentIconId').val() == "000000000000000000000000") {
            $('#step4 input[type="submit"]').attr('disabled', 'diasabled');
        } else {
            $('#step4 input[type="submit"]').removeAttr('disabled');
        }
    }
    */
    function extractObjectiveIconId(iconUrl) {
        var re = new RegExp("GetObjectiveIcon/([0-9a-zA-Z]+)?", "g");
        iconUrl.match(re);
        return RegExp.$1;
    }

    function appendIcon(iconId) {
        $('#step4 .objective-icons img').append('<img src="/Image/GetObjectiveIcon/' + iconId + '?size=50" alt="Objective icon" class="image-smaller img-rounded">');
    }

    function ajaxPost(url,data,step) {
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                $('#step' + step).html(response);
                new ObjectiveCreatePage().initialize(true);
            }
        });
    }

    function setCurrentKeyResult(data) {
        ajaxPost(urls.setCurrentKeyResult(), data, 2);
    }

    function addCurrentKeyResult(data) {
        ajaxPost(urls.addCurrentKeyResult(), data, 2);
    }
    function deleteKeyResult(data) {
        ajaxPost(urls.deleteKeyResult(), data, 2);
    }

    function addContributor(data) {
        ajaxPost(urls.addContributor(), data, 3);
    }


    function createStep2Model() {
        var keyResults = [];
        $('.key-result-container').each(function() {
            keyResults.push(createKeyResult($(this), 'keyResult'));
        });
        return {
            KeyResults: keyResults
        };
    }

    function createStep3Model() {

        var users = [];
        $('.contributor').each(function () {
            users.push(createContributor($(this)));
        });

        return {
            Invitation : $('#Invitation').val(),
            AddMe : $('#AddMe').is(':checked'),
            MyTeam: $('#MyTeam').is(':checked'),
            UserPickerModel : new UserPicker().getPickerModel()
        };
    }

    function createContributor(container) {
        return {
            Id: $(container).find("#Id").val(),
            FirstName: $(container).find("#FirstName").val(),
            SecondName: $(container).find("#SecondName").val()
        };
        
    }

    function createKeyResult(keyResultContainer,prefix) {
        return {
            Id : keyResultValue(keyResultContainer,prefix, 'Id'),
            Index: keyResultValue(keyResultContainer,prefix, 'Index'),
            Name: keyResultValue(keyResultContainer,prefix, 'Name'),
            Due: keyResultValue(keyResultContainer,prefix, 'Due'),
            Unit: keyResultValue(keyResultContainer,prefix, 'Unit'),
            CurrentValue: keyResultValue(keyResultContainer,prefix, 'CurrentValue'),
            TargetValue: keyResultValue(keyResultContainer,prefix, 'TargetValue')
        };
    }

    function keyResultValue(container, prefix, key) {
        var input = $(container).find('input[name="' + prefix + '.' + key + '"]');

        if (input.length == 0) {
            return $(container).find('select[name="' + prefix + '.' + key + '"] :selected').text();
        }
        
         return input.val();
     }

     function submitForm(form,url) {
         $.ajax({
             url: url,
             type: 'POST',
             data: JSON.stringify(form.serializeObject()),
             success: function (response) {
                 $('#wizard').html('');
                 $('#wizard').html(response);
             }
         });
     }
     this.initialize = function (ajaxLoaded) {

         if (true != ajaxLoaded) {
             //Setup wizard tabs
             $("#tabs").tabs({
                 disabled: [1, 2, 3],
                 show: function (event, ui) {
                     $('#CurrentStep').val(ui.index);
                 }
             });
         }
         //Customize inputs
         $('input[data-type="date"]').inputExtender({ date: true });
         $('input[data-type="decimal"]').inputExtender({ decimal: true });

         if (true != ajaxLoaded) {
             //$("select, input:checkbox, input:radio").uniform();    
         }


         //Validation turn on 
         validation.show();

         typeaheadInit();

         //Click on "Next Step" button
         $('#wizard_actions button[name="next-step"]').off('click').on('click', function () {
             submitForm($(this).parents('form'), urls.createWizard());
         });

         $('#wizard_actions button[name="finish"]').off('click').on('click', function (e) {
             e.preventDefault();

             $.ajax({
                 url: urls.create(),
                 type: 'POST',
                 data: JSON.stringify($(this).parents('form').serializeObject()),
                 success: function (response) {
                     if (response.RedirectTo != null) {
                         document.location = response.RedirectTo;
                     }
                     else {
                         $('#wizard').html('');
                         $('#wizard').html(response);
                     }
                 }
             });
         });

         step2();
         step3();
         step4();
     };
}
