﻿using System;
using Autofac;
using Moq;
using TalentManagement.AppServices;
using TalentManagement.Infrastructure;
using TalentManagement.Infrastructure.Configuration;
using TalentManagement.Infrastructure.Data.Configuration;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Web;

namespace TalentManagement.Test
{
    public abstract class MockedTestClass<TITestInterface>
    {
        private IContainer _container;
        private TITestInterface _target;
        public event Action<ContainerBuilder> BeforeContainerBulilded;

        public IContainer Container
        {
            get
            {
                if (_container == null)
                {
                    CreateTarget();
                }
                return _container;
            } 
            private set { _container = value; }
        }

        protected virtual void RegisterMocks(ContainerBuilder containerBuilder)
        {

        }
        
        protected virtual TITestInterface CreateTarget(IContainer container)
        {
            return container.Resolve<TITestInterface>();
        }

        protected TITestInterface CreateTarget()
        {
            ContainerBuilder builder = GetContainerBuilder();

            RegisterMocks(builder);

            if (BeforeContainerBulilded != null)
            {
                BeforeContainerBulilded(builder);
            }

            IContainer container = builder.Build();
            Container = container;
            _target = CreateTarget(container);
            return _target;
        }

        protected virtual ContainerBuilder GetContainerBuilder()
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterModule(new InfrastructureRegistrationModule());
            builder.RegisterModule(new WebRegistrationModule());
            builder.RegisterMock<IAppConfiguration>(mock =>
            {
                mock.Setup(
                    provider =>
                    provider.GetConnectionString(It.IsAny<string>())).
                    Returns("mongodb://localhost/?safe=true");
                mock.Setup(provider => provider.GetTenantName())
                    .Returns("DefaultTenant");
                mock.Setup(
                    provider => provider.GetDbName(DataSourceType.Shared)).
                    Returns("Security");
                mock.Setup(
                    provider => provider.GetDbName(DataSourceType.Tenant)).
                    Returns("DefaultTenant");
            });
            builder.RegisterMock<IPrincipalProvider>(mock =>
                                                         {
                                                             mock.Setup(pp => pp.GetUserName()).Returns("CurrentUser");
                                                             mock.Setup(pp => pp.GetTenant()).Returns("CurrentTenant");
                                                         });
            builder.RegisterModule(new AppServiceRegistrationModule());

            builder.RegisterMock(new Mock<IRegistrar>());
            return builder;
        }

      
    }
}
