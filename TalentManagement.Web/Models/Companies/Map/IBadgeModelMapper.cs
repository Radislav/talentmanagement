﻿using MongoDB.Bson;
using TalentManagement.Domain.Companies;

namespace TalentManagement.Web.Models.Companies.Map
{
    public interface IBadgeModelMapper
    {
        BadgeModel Map(ObjectId companyId, Badge badge);
    }
}