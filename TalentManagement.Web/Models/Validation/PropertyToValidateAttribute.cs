﻿using System;

namespace TalentManagement.Web.Models.Validation
{
    [AttributeUsage(AttributeTargets.Class,AllowMultiple = false,Inherited = true)]
    public class PropertyToValidateAttribute : Attribute
    {
        private readonly string _propertyName;

        public PropertyToValidateAttribute(string propertyName)
        {
            _propertyName = propertyName;
        }

        public string PropertyName
        {
            get { return _propertyName; }
        }
    }
    
}