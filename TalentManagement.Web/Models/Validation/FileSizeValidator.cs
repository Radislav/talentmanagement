﻿using System.Globalization;
using System.Web;
using FluentValidation.Results;
using FluentValidation.Validators;

namespace TalentManagement.Web.Models.Validation
{
    public class FileSizeValidator : PropertyValidator
    {
        private readonly int _maxFileSize;

        public FileSizeValidator(int maxFileSize) : base("FilePostedTooBig",typeof(Resources.Errors))
        {
            _maxFileSize = maxFileSize;
        }

        protected override ValidationFailure CreateValidationError(PropertyValidatorContext context)
        {
            var result = base.CreateValidationError(context);
            if (false == string.IsNullOrEmpty(result.ErrorMessage))
            {
                result = new ValidationFailure(result.PropertyName, result.ErrorMessage.Replace("{MaxFileSize}", string.Format("{0:n0}", _maxFileSize)));    
            }
            return result;
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            HttpPostedFileBase value = context.PropertyValue as HttpPostedFileBase;
            if (value != null)
            {
                return value.ContentLength <= _maxFileSize;
            }

            return true;
        }
    }
}