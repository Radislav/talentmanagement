﻿using System;
using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain;

namespace TalentManagement.Infrastructure.Data.Filter
{
    internal abstract class FilterBase<TModel> : IFilter<TModel>
    {
        private IEnumerable<TModel> _data;
        protected const int DefaultPriority = 0;
        
        protected IEnumerable<TModel> Data
        {
            get { return _data; }
            set { Init(value); }
        }

        public virtual IFilter<TModel> Init(IEnumerable<TModel> data)
        {
            _data = data;
            return this;
        }

        public abstract IFilterContext FilterContext { get; set; }
        
        public abstract IFilter<TModel> Filter(IFilterContext[] filterContexts);

        public virtual IFilter<TModel> Filter()
        {
            return Filter(new[] { FilterContext });
        }

        protected TContext GetContextOrFail<TContext>(IFilterContext[] filterContexts)
            where TContext : class
        {
            TContext currentContext = FilterContext as TContext;
            TContext foundedContext = null;

            if (filterContexts != null)
            {
                foundedContext = filterContexts.OfType<TContext>().FirstOrDefault() ??
                                         filterContexts.OfType<CompositeContext>()
                                             .SelectMany(ctx => ctx.Contexts.OfType<TContext>())
                                             .FirstOrDefault();

            }
            if (foundedContext == null && currentContext == null)
            {
                throw new ArgumentException(String.Format("Context with {0} type is not found.", typeof(TContext)));    
            }

            return currentContext ?? foundedContext;
        }

        public virtual IFilter<TModel> Chain(IFilter<TModel> chainedFilter, IFilterContext[] contexts)
        {
            if (chainedFilter != null)
            {
                TModel[] chainedData = chainedFilter.AsEnumerable().ToArray();

                _data = chainedFilter.Init(_data)
                    .Filter(contexts)
                    .AsEnumerable();

                chainedFilter.Init(chainedData);
            }
            return this;
        }

        public IFilter<TModel> Chain(IFilter<TModel> chainedFilter)
        {
            return Chain(chainedFilter, null);
        }

        public virtual IEnumerable<TModel> AsEnumerable()
        {
            return _data;
        }


        public virtual int Priority
        {
            get { return DefaultPriority; }
        }

        public IFilter Init(System.Collections.IEnumerable data)
        {
            return Init(data.Cast<TModel>());
        }

        IFilter IFilter.Filter(IFilterContext[] contexts)
        {
            return Filter(contexts);
        }

        IFilter IFilter.Filter()
        {
            return Filter();
        }

        public IFilter Chain(IFilter chainedFilter, IFilterContext[] contexts)
        {
            return Chain((IFilter<TModel>) chainedFilter, contexts);
        }

        public IFilter Chain(IFilter chainedFilter)
        {
            return Chain((IFilter<TModel>) chainedFilter);
        }

        System.Collections.IEnumerable IFilter.AsEnumerable()
        {
            return _data;
        }
    }
}
