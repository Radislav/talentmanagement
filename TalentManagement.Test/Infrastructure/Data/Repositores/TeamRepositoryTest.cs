﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Data;

namespace TalentManagement.Test.Infrastructure.Data.Repositores
{
    [TestClass]
    public class TeamRepositoryTest : MockedTestClass<ITeamRepository>
    {
        [TestMethod]
        public void GetTeamsByManager_should_filter_by_Manager_And_tenant()
        {
            var target = CreateTarget();

            var result = target.GetTeams("user1@Tenant1.com", "Tenant1");

            Assert.AreEqual(2, result.Count());
            foreach (Team team in result)
            {
                Assert.AreEqual("user1@Tenant1.com", team.Manager);
                Assert.AreEqual("Tenant1", team.CompanyName);
            }
        }

        [TestMethod]
        public void GetUnassignedTeam_Should_return_Team_with_members_which_are_not_assigned()
        {
            var target = CreateTarget();

            var result = target.GetUnassignedTeam("Tenant1");

            Assert.AreEqual(1,result.Members.Count());
            Assert.AreEqual("Unassigned",result.Manager);
            Assert.AreEqual("Unassigned", result.Name);

            foreach (string member in result.Members)
            {
                Assert.AreEqual("unassigned1@Tenant1.com", member);
            }
        }

        protected override ContainerBuilder GetContainerBuilder()
        {
            ContainerBuilder builder = base.GetContainerBuilder();
            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();

            Team[] teams = new Team[]
                {
                    new Team("Team1","user1@Tenant1.com","Tenant1"),
                    new Team("Team2","user1@Tenant1.com","Tenant1"), 
                    new Team("Team3","user2@Tenant2.com","Tenant2"), 
                    new Team("Team4","user3@Tenant2.com","Tenant2"), 
                    new Team("Team5","user4@Tenant3.com","Tenant3")
                };

            User[] users = new User[]
                {
                    new User("user1@Tenant1.com"), 
                    new User("user2@Tenant2.com"), 
                    new User("user3@Tenant2.com"), 
                    new User("user4@Tenant3.com"), 
                    new User("unassigned1@Tenant1.com")
                }; 

            unitOfWork.Setup(u => u.AsQueryable<Team>()).Returns(teams.AsQueryable);
            unitOfWork.Setup(u => u.AsQueryable<User>()).Returns(users.AsQueryable);

            builder.RegisterInstance(unitOfWork.Object)
                   .As<IUnitOfWork>();

            return builder;
        }
    }
}
