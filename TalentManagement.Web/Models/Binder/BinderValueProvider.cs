﻿using System;
using System.Web.Mvc;
using System.Linq;

using TalentManagement.Infrastructure.Cache;
using TalentManagement.Infrastructure.Converter;
using TalentManagement.Web.Infrastructure;

namespace TalentManagement.Web.Models.Binder
{
    internal class BinderValueProvider
    {
        private readonly ModelBindingContext _bindingContext;
 
        public BinderValueProvider(ModelBindingContext bindingContext)
        {
            _bindingContext = bindingContext;
        }

        public ModelBindingContext BindingContext
        {
            get { return _bindingContext; }
        }

        public T GetWithCache<T>(string key)
        {
            IUserCache userCache = DependencyResolver.Current.GetService<IUserCache>();

            T value = GetValue<T>(key);

            if (false == ReferenceEquals(null, value) && false == String.IsNullOrEmpty(value.ToString()))
            {
                userCache.Set(value, key);
            }
            return userCache.Get<T>(key);
        }

        public T GetValue<T>(string key)
        {
            var value = _bindingContext.ValueProvider.GetValue(key);

            //The boolean values may goes as a array, because MVC default helper generates checkbox + hidden (2 values)
            if (value == null && typeof(T) == typeof(bool))
            {
                value = _bindingContext.ValueProvider.GetValue(GetIndexedKey(key, 0));
            }

            T result = default(T);

            if (value != null)
            {
                var converter = DependencyResolver.Current.GetServiceOptional<ITypeConverter>(typeof(T));
                if (converter == null)
                {
                    result = (T) value.ConvertTo(typeof (T));
                }
                else
                {
                    string[] raws = value.RawValue.GetType().IsArray ? 
                                            (string[])value.RawValue : 
                                            new [] {value.RawValue.ToString()};

                    object valueCasted = typeof (T).IsArray ? (object)raws : raws.First();

                    result = (T)converter.Convert(valueCasted);
                }
                
            }

            return result;
        }
        
        public T GetValue<T>(string key,string prefix)
        {
            return GetValue<T>(String.Concat(prefix, ".", key),-1);
        }

        public T GetValue<T>(string key,string prefix,int index)
        {
            return String.IsNullOrEmpty(prefix) ? GetValue<T>(key,index) :
                                                  GetValue<T>(String.Concat(prefix, ".", key), index);
        }

        public T GetValue<T>(string key,int index)
        {
            string currentKey = index < 0 ? key : GetIndexedKey(key, index);

                return GetValue<T>(currentKey);
            /*
            T[] values = GetValue<T[]>(key);
            return values[index];
             * */
        }

        public static string GetIndexedKey(string arrayKey, int index)
        {
            return String.Format("{0}[{1}]", arrayKey, index);
        }
    }
}