﻿using System;
using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain;
using TalentManagement.Domain.Performance;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.ReviewCycleCreation.Map
{
    internal class ReviewCycleMapper : IMapper<ReviewCycleModel,ReviewCycle>
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper<ReviewCycleModel,IEnumerable<QuestionSet>> _questionSetsMapper;
        private readonly IFilter<IOwnedByCompany> _companyFilter;


        public ReviewCycleMapper(IUserRepository userRepository, IMapper<ReviewCycleModel, IEnumerable<QuestionSet>> questionSetsMapper, IFilter<IOwnedByCompany> companyFilter)
        {
            _userRepository = userRepository;
            _questionSetsMapper = questionSetsMapper;
            _companyFilter = companyFilter;
        }
     
        public ReviewCycle Map(ReviewCycleModel source)
        {
            
            QuestionSet[] sets = _questionSetsMapper.Map(source).ToArray();
            IEnumerable<string> participants = GetParticipants(source);

            
            return new ReviewCycle(source.CreateStep1.Name, 
                                   DateTime.Parse(source.CreateStep1.ReviewStart),
                                   DateTime.Parse(source.CreateStep1.ReviewEnd),
                                   sets,
                                   participants,
                                   source.CreateStep3.ReviewEveryone);
        }

        private IEnumerable<string> GetParticipants(ReviewCycleModel source)
        {
            IEnumerable<User> individuals = _userRepository.GetUsers(source.CreateStep3.IndividualsPicker.UsersPicked.Select(u => u.Id)).ToArray();
            IEnumerable<User> subordinates = _userRepository.GetSubordinatesOf(source.CreateStep3.ManagersPicker.UsersPicked.Select(u => u.Id), _companyFilter).ToArray();

            IEnumerable<string> participants = individuals.Union(subordinates)
                                                          .Select(r => r.Id)
                                                          .ToArray();
            return participants;
        }
    }
}