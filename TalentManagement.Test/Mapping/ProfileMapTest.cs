﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Web.Models;

namespace TalentManagement.Test.Mapping
{
    [TestClass]
    public class ProfileMapTest : MockedTestClass<IMapper<ProfileModel,User>>
    {
        [TestMethod]
        public void Map_should_calcualte_hash_for_password()
        {
            var target = CreateTarget();
            User result = target.Map(new ProfileModel
                           {
                               Password = "Password"
                           });

            Assert.AreEqual("Hash",result.HashedPassword);
        }
        
        [TestMethod]
        public void Map_should_keep_properties()
        {
            var target = CreateTarget();
            ProfileModel source = new ProfileModel
                                      {
                                          FirstName = "firstName",
                                          SecondName = "secondName",
                                          ManagerEmail = "email",
                                          Position = "position",
                                      };
            User destination = target.Map(new ProfileModel
            {
                FirstName = "firstName",
                SecondName = "secondName",
                ManagerEmail = "email",
                Position = "position"
            });
            
            Assert.AreEqual(source.FirstName,destination.FirstName);
            Assert.AreEqual(source.SecondName, destination.SecondName);
            Assert.AreEqual(source.ManagerEmail, destination.ManagerEmail);
            Assert.AreEqual(source.Position, destination.Position);
        }
        
        protected override void RegisterMocks(Autofac.ContainerBuilder containerBuilder)
        {
            Mock<IHashComputer> hashComputer = new Mock<IHashComputer>();
            hashComputer.Setup(mock => mock.ComputeHash("Password")).Returns("Hash");

            Mock<IPrincipalProvider> principalProvider = new Mock<IPrincipalProvider>();
            principalProvider.Setup(p => p.GetUserName()).Returns("User");
            
            Mock<IDependencyResolver> resolver = new Mock<IDependencyResolver>();

            resolver.Setup(r => r.GetService(typeof(IPrincipalProvider))).Returns(principalProvider.Object);
            DependencyResolver.SetResolver(resolver.Object);
            containerBuilder.RegisterMock(hashComputer);
            base.RegisterMocks(containerBuilder);
        }
    }
}
