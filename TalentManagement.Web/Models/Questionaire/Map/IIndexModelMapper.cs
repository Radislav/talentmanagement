﻿using MongoDB.Bson;
using TalentManagement.Domain.Performance;

namespace TalentManagement.Web.Models.Questionaire.Map
{
    public interface IIndexModelMapper
    {
        IndexModel Map(AnswersCollection answers,ObjectId reviewId);
    }
}
