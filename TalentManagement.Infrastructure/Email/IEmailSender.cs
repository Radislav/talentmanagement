﻿namespace TalentManagement.Infrastructure.Email
{
    public interface IEmailSender
    {
        void Send<TEmailArgument>(string to, EmailType emailType,TEmailArgument argument);
        void Send(string to, EmailType emailType);
    }
}
