﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TalentManagement.Domain;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Web.Models;
using TalentManagement.Web.Models.MyObjectives;

namespace TalentManagement.Web.Controllers
{
    public class MyObjectivesController : Controller
    {
        private readonly IObjectiveRepository _objectiveRepository;
        private readonly IMapper<Objective, ObjectiveSimpleModel> _objectiveSimpleModelMapper;
        private readonly IResourceProvider _resourceProvider;
        private readonly IFilter<IOwnedByTeam> _teamFilter;
        private readonly IFilter<IOwned> _ownedFilter;
        private readonly IFilter<IHasParticipants> _participantsFilter;

        public MyObjectivesController(IObjectiveRepository objectiveRepository,
            IMapper<Objective, ObjectiveSimpleModel> objectiveSimpleModelMapper,
            IResourceProvider resourceProvider,
            IFilter<IOwnedByTeam> teamFilter,
            IFilter<IOwned> ownedFilter,
            IFilter<IHasParticipants> participantsFilter)
        {
            _objectiveRepository = objectiveRepository;
            _objectiveSimpleModelMapper = objectiveSimpleModelMapper;
            _resourceProvider = resourceProvider;
            _teamFilter = teamFilter;
            _ownedFilter = ownedFilter;
            _participantsFilter = participantsFilter;
        }
        
        private ObjectiveSimpleModel[] MapObjectives(IEnumerable<Objective> objectives)
        {
            return objectives.Select(o => _objectiveSimpleModelMapper.Map(o))
                             .ToArray();
        }

        [HttpGet]
        public ActionResult Index()
        {
          PageModel model =  new PageModel
                {
                    ObjectiveContainers = new []
                                {
                                    new ObjectiveContainer
                                        {
                                            Title = _resourceProvider.MyObjectivesTop,
                                            NoObjectivesMessage = _resourceProvider.MyObjectivesNoTop,
                                            Objectives = MapObjectives(_objectiveRepository.GetFocusObjectives())
                                        },
                                    new ObjectiveContainer
                                        {
                                            Title = _resourceProvider.MyObjectivesTeam,
                                            NoObjectivesMessage = _resourceProvider.MyObjectivesNoTeam,
                                            Objectives = MapObjectives(_objectiveRepository.ApplyFilter(_teamFilter))
                                        },
                                    new ObjectiveContainer
                                        {
                                            Title = _resourceProvider.MyObjectivesPersonal,
                                            NoObjectivesMessage = _resourceProvider.MyObjectivesNoPersonal,
                                            Objectives = MapObjectives(_objectiveRepository.ApplyFilter(_ownedFilter))
                                        },
                                    new ObjectiveContainer
                                        {
                                            Title = _resourceProvider.MyObjectivesContribute,
                                            NoObjectivesMessage = _resourceProvider.MyObjectivesNoContribute,
                                            Objectives = MapObjectives(_objectiveRepository.ApplyFilter(_participantsFilter))
                                        }

                                }
                };
            return View(model);

        }
    }
}
