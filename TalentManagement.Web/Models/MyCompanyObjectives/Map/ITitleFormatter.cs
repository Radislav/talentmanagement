﻿using System;

namespace TalentManagement.Web.Models.MyCompanyObjectives.Map
{
    public interface ITitleFormatter<TGroupKey>
    {
        String FormatTitle(TGroupKey groupKey);
    }
}
 