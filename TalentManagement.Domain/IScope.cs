﻿namespace TalentManagement.Domain
{
    public interface IScope
    {
        string ScopeId { get; }
        string ScopeName { get; }
    }
}
