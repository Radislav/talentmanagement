﻿using System;
using System.ComponentModel;
using FluentValidation.Attributes;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models
{
    [Validator(typeof(EmailModelValidator))]
    public class EmailModel
    {
        public EmailModel()
        {
            Email = String.Empty;
        }

        [DisplayName("Your email here")]
        public string Email { get; set; }
    }
}