﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using TalentManagement.Infrastructure;
using TalentManagement.Web.Infrastructure.Cache;
using TalentManagement.Web.Infrastructure.UI;

namespace TalentManagement.Web.Infrastructure
{
    internal class OutputCacheCleaner : IOutputCacheCleaner
    {
        public void RemoveImageFromCache(string action, string controller, object id)
        {
            RemoveImageFromCache(action, controller, new Dictionary<string, object>{{"id", id}});
        }

        public void RemoveImageFromCache(string action, string controller, Dictionary<string,object> routes)
        {
            IUrlProvider urlProvider = DependencyResolver.Current.GetService<IUrlProvider>();

            //Remove general image
            HttpResponse.RemoveOutputCacheItem(urlProvider.Generate(action, controller, routes)
                                                   .ToVirtualPath()
                                                   .Decode()
                                                   .GetUrl());

            int[] sizes = Enum.GetValues(typeof(ImageSize))
                                .Cast<int>()
                                .ToArray();
            
            //Remove all possible sized images
            sizes.Select(size =>
                             {
                                 Dictionary<string, object> sizedRoutes = new Dictionary<string, object>(routes);
                                 sizedRoutes["size"] = size;

                                 return urlProvider.Generate(action, controller,routes)
                                     .ToVirtualPath()
                                     .Decode()
                                     .GetUrl();
                             })
                            .Map(HttpResponse.RemoveOutputCacheItem);

        }
    }
}