﻿using System.Collections;
using System.Collections.Generic;

namespace TalentManagement.Domain
{
    public interface IFilter
    {
        IFilter Init(IEnumerable data);
        IFilter Filter(IFilterContext[] contexts);
        IFilter Filter();
        IFilter Chain(IFilter chainedFilter, IFilterContext[] contexts);
        IFilter Chain(IFilter chainedFilter);
        IEnumerable AsEnumerable();
        int Priority { get; }
        IFilterContext FilterContext { get; set; }
    }

    public interface IFilter<TModel> : IFilter
    {
        IFilter<TModel> Init(IEnumerable<TModel> data);
        new IFilter<TModel> Filter(IFilterContext[] contexts);
        new IFilter<TModel> Filter();
        IFilter<TModel> Chain(IFilter<TModel> chainedFilter, IFilterContext[] contexts);
        IFilter<TModel> Chain(IFilter<TModel> chainedFilter);
        new IEnumerable<TModel> AsEnumerable();
        new int Priority { get; }
    }
}
