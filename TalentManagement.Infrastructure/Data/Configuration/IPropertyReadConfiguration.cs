﻿using TalentManagement.Infrastructure.Validation;

namespace TalentManagement.Infrastructure.Data.Configuration
{
    public interface IPropertyReadConfiguration<TEntity> : IValidator<TEntity>
    {
        bool IsIgnored { get; }
        string Name { get; }
    }
}
