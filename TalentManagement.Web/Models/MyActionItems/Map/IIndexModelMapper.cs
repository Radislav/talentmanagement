﻿using System.Collections.Generic;
using TalentManagement.Domain.Objectives;

namespace TalentManagement.Web.Models.MyActionItems.Map
{
    public interface IIndexModelMapper
    {
        IndexModel Map(ActionItemsDictionary relatedActionItems,IEnumerable<ActionItem> notRelatedActionItem);
    }
}
