﻿using System;
using Resource = TalentManagement.Resources.DomainResources;

namespace TalentManagement.Domain.Performance
{
    [Flags]
    public enum ReviewCycleVisibility
    {
        [LocalizedDescription("None", typeof(Resource))]
        None                 = 0x00,
        [LocalizedDescription("ReviewCycleVisibility_Employee", typeof(Resource))]
        Employee             = 0x01,
        [LocalizedDescription("ReviewCycleVisibility_Manager", typeof(Resource))]
        Manager              = 0x02,
        [LocalizedDescription("ReviewCycleVisiblity_EmployeeAndManager", typeof(Resource))]
        EmployeeAndManager   = Employee | Manager
    }
}
