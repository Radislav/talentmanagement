﻿using System.Collections.Generic;
using TalentManagement.Domain.Performance;

namespace TalentManagement.Web.Models.CycleDetails.Map
{
    public interface IIndexModelMapper
    {
        IndexModel Map(ReviewCycle cycle,IEnumerable<Review> reviews , IEnumerable<ReviewCycleDescriptor> descriptors);
    }
}
