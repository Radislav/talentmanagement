﻿$(function () {

    Comments({
        containerSelector: '.feedback',
        holderId: 'response_ResponseId',
        handlerUrl: urls.addCommentForResponse()
    });

    initUserPicker();
    initSend();

    $('#activity_stats [data-toggle="buttons-radio"] .btn')
        .click(function () {
            document.location = urls.feedbacks() + '/' + $(this).attr('name');
        });
});