﻿using MongoDB.Bson;
using TalentManagement.Domain.Feeds;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Feeds.Map
{
    internal class KudosModelMapper : IMapper<Feed,KudosModel>
    {
        private readonly IMapper<Feed, FeedModel> _baseMapper;

        public KudosModelMapper(IMapper<Feed, FeedModel> baseMapper)
        {
            _baseMapper = baseMapper;
        }
       
        public KudosModel Map(Feed source)
        {
            FeedModel feedModel = _baseMapper.Map(source);

            return new KudosModel
                       {
                           Properties = feedModel.Properties,
                           FeedType = feedModel.FeedType,
                           Id = feedModel.Id,
                           CreatedAt = feedModel.CreatedAt,
                           CreatedBy = feedModel.CreatedBy,
                           BadgeId = ObjectId.Parse(feedModel.GetProperty("BadgeId")),
                           CommentSection = feedModel.CommentSection,
                           Content = feedModel.GetProperty("Content"),
                           Receivers = feedModel.GetProperty("Receivers").Split(',')
                       };
        }
    }
}