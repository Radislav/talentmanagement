﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using TalentManagement.Domain.Feeds;

namespace TalentManagement.Domain.Contracts
{
    [ContractClassFor(typeof(IFeedRepository))]
    internal  abstract class FeedRepositoryContract : IFeedRepository
    {
        public IEnumerable<Feed> GetFeeds(IFilter filter)
        {
            Contract.Requires<ArgumentNullException>(filter != null);
            Contract.Ensures(Contract.Result<IEnumerable<Feed>>() != null);

            return null;
        }

        public void CreateUpdatingFeed(IScope scope, string text)
        {
            Contract.Requires<ArgumentNullException>(scope != null);
        }

        public void Insert(Feed entity)
        {
        }

        public void DeleteById(MongoDB.Bson.ObjectId id)
        {
        }

        public void Update(Feed entity)
        {
        }

        public IQueryable<Feed> AsQueryable()
        {
            return null;
        }

        public Feed GetById(MongoDB.Bson.ObjectId id)
        {
            return null;
        }


        public Feed Upsert(Feed entity)
        {
            return null;
        }


        public IEnumerable<Feed> ApplyFilter(IFilter filter)
        {
            return null;
        }


        public IEnumerable<Kudos> GetKudosByReceiver(string receiver)
        {
            Contract.Requires(receiver != null);
            Contract.Ensures(Contract.Result<IEnumerable<Kudos>>() != null);

            return null;
        }
    }
}
