﻿using System.Collections.Generic;

namespace TalentManagement.Domain.Performance
{
    public class AnswersCollection
    {
        private readonly IEnumerable<Answer> _answers;
        private readonly IEnumerable<Question> _questions;

        public AnswersCollection(QuestionSetType setType)
        {
            _answers = new List<Answer>();
            _questions = new Question[0];
            SetType = setType;
        }

        public AnswersCollection(IEnumerable<Answer> answers,IEnumerable<Question> questions,QuestionSetType setType) : this(setType)
        {
            _answers = answers;
            _questions = questions;
        }

        public IEnumerable<Answer> Answers {
            get { return _answers; }
        }

        public IEnumerable<Question>  Questions {
            get { return _questions; }
        }

        public QuestionSetType SetType { get; private set; }
    }
}