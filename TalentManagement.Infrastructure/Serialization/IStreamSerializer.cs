﻿namespace TalentManagement.Infrastructure.Serialization
{
    public interface IStreamSerializer<TEntity> : ISerializer<TEntity,byte[]>
    {
    }
}
