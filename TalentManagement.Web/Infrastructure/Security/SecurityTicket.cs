﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Web.Infrastructure.Security
{
    internal class SecurityTicket
    {
        private const string TicketCookieName = "TMTicket";
        private const string PrincipalKey = "Principal";
        private const int ExpiraionMinutes = 20;
        
        private readonly HttpCookie _ticketCookie;
        private readonly HttpContext _httpContext;

        readonly IAuthorizationService _authorizationService = DependencyResolver.Current.GetService<IAuthorizationService>();
        readonly JavaScriptSerializer _serializer = new JavaScriptSerializer();

        public SecurityTicket()
        {
            _ticketCookie = CreateNewCookie();
        }

        public SecurityTicket(HttpContext httpContext)
        {
            _httpContext = httpContext;
            _ticketCookie = _httpContext.Request.Cookies[TicketCookieName] ??
                          CreateNewCookie();
            
        }

        public SecurityTicket(HttpContextBase httpContext)
        {
            _ticketCookie = httpContext.Request.Cookies[TicketCookieName] ??
                          CreateNewCookie();
        }

        public void AddCookieToContext(HttpContext context)
        {
            context.Response.Cookies.Add(Cookie);
        }

        public void AddCookieToContext(HttpContextBase context)
        {
            context.Response.Cookies.Add(Cookie);
        }

        private  HttpCookie CreateNewCookie()
        {
            return new HttpCookie(TicketCookieName) { Expires = DateTime.Now.AddMinutes(ExpiraionMinutes),Name = TicketCookieName};
        }

        public HttpCookie Cookie
        {
            get { return _ticketCookie; }
        }

        public bool IsExpired
        {
            get { return _ticketCookie == null || _ticketCookie.Expires <= DateTime.Now; }
        }

        public void Expire()
        {
            _ticketCookie.Expires = DateTime.Now.AddDays(-1);
        }

        public bool DoesRequestHasCookie
        {
           get { return _httpContext.Request.Cookies[TicketCookieName] != null; }
        }

        public TenantPrincipal GetPrincipal()
        {
            if (_ticketCookie[PrincipalKey] == null)
            {
                return null;
            }

            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(_ticketCookie.Values[PrincipalKey]);
            if (ticket == null)
            {
                return null;
            }
            TenantPrincipalModel principalModel = _serializer.Deserialize<TenantPrincipalModel>(ticket.UserData);
            return _authorizationService.CreatePrincipal(principalModel.UserName,
                                                                             principalModel.IsAuthenticated,
                                                                             principalModel.LastSignIn,
                                                                             principalModel.FirstName,
                                                                             principalModel.SecondName,
                                                                             principalModel.Team,
                                                                             principalModel.Roles);
        }
        
        public void SetPrincipal(TenantPrincipal principal)
        {
            if (principal == null)
            {
                return;
            }
            string userData = _serializer.Serialize(principal.AsModel());
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket
            (1, _httpContext.User.Identity.Name, DateTime.Now, DateTime.Now.AddMinutes(ExpiraionMinutes), false, userData);

            _ticketCookie[PrincipalKey] = FormsAuthentication.Encrypt(ticket);
        }
    }
}