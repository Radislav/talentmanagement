﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TalentManagement.Domain;
using TalentManagement.Domain.Feeds;

namespace TalentManagement.Test.Domain.Feeds
{
    [TestClass]
    public class FeedTest
    {
        [TestMethod]
        public void GetPropertyValue_should_return_null_by_default()
        {
            Feed target = new Feed();
            string value =target.GetPropertyValue(String.Empty);
            Assert.IsNull(value);
        }

        [TestMethod]
        public void GetPropertyValue_should_return_existing_value()
        {
            Feed target = new Feed(null, new[]
                                             {
                                                 new FeedProperty {Name = "Name", Value = "Value"}
                                             });
                            
            string value = target.GetPropertyValue("Name");
            Assert.AreEqual("Value",value);
        }

        [TestMethod]
        public void SetPropertyValue_should_add_new_property_if_name_not_exists()
        {
            Feed target = new Feed();
            target.SetPropertyValue("Name","Value");
            
            Assert.AreEqual(1,target.Properties.Count());
            Assert.AreEqual("Name",target.Properties.First().Name);
            Assert.AreEqual("Value", target.Properties.First().Value);
        }

        [TestMethod]
        public void SetPropertyValue_should_update_existing_property()
        {
            Feed target = new Feed(null, new[]
                                             {
                                                 new FeedProperty {Name = "Name", Value = "Value"}
                                             });
          
            target.SetPropertyValue("Name", "NewValue");

            Assert.AreEqual(1, target.Properties.Count());
            Assert.AreEqual("Name", target.Properties.First().Name);
            Assert.AreEqual("NewValue", target.Properties.First().Value);
        }

        [TestMethod]
        public void Scope_should_be_global_by_default()
        {
            Feed target = new Feed();
            Assert.AreEqual(Scope.Global,target.Scope.ScopeName);
        }

        [TestMethod]
        public void Scope_can_be_set_through_the_property()
        {
            Feed target = new Feed {Scope = new Scope("Name", "222")};

            Assert.AreEqual(target.Scope.ScopeName,"Name");
            Assert.AreEqual(target.Scope.ScopeId, "222");
        }
    }
}
