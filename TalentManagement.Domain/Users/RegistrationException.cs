﻿using System;

namespace TalentManagement.Domain.Users
{
    [Serializable]
    public class RegistrationException : ApplicationException
    {
        public string ActivationCode { get; set; }
        public string Email { get; set; }

        public RegistrationException(string message) : base(message)
        {

        }
    }
}
