﻿using System.Linq;
using TalentManagement.Domain;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data.Filter
{
    class ParticipantFilter : FilterBase<IHasParticipants>
    {
        private readonly IPrincipalProvider _principalProvider;
        private IFilterContext _filterContext;

        public ParticipantFilter(IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
        }

        public override IFilter<IHasParticipants> Filter(IFilterContext[] contexts)
        {
            ParticipantFilterContext context = GetContextOrFail<ParticipantFilterContext>(contexts);

            Data = Data.Where(o => o.Participants.Contains(context.Participant));        
            
            return this;
        }

        public override IFilterContext FilterContext
        {
            get
            {
                return _filterContext ?? new ParticipantFilterContext(_principalProvider.GetUserName());
            }
            set { _filterContext = value; }
        }
    }
}
