﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using TalentManagement.Web.Models;

namespace TalentManagement.Web.Infrastructure.UI
{
    public static class WatermarkExtensions
    {
        public static MvcHtmlString WatermarkFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                      Expression<Func<TModel, TValue>> expression,
                                                      IDictionary<string, object> htmlAttributes)
        {
            WatermarkAttribute watermarkAttribute = HtmlExtensions.GetMemberAttribute<WatermarkAttribute>((expression.Body as MemberExpression));

            string watermark = watermarkAttribute == null ?
                               ModelMetadata.FromLambdaExpression(expression, html.ViewData).DisplayName :
                               watermarkAttribute.Watermark;

            htmlAttributes = htmlAttributes ?? new Dictionary<string, object>();
            htmlAttributes.Add("placeholder", watermark);
            return html.TextBoxFor(expression, htmlAttributes);
        }

        public static MvcHtmlString Watermark<TModel>(this HtmlHelper<TModel> html,
                                                      string name,
                                                      object value,  
                                                      string watermark,
                                                      IDictionary<string, object> htmlAttributes)
        {
            htmlAttributes = htmlAttributes ?? new Dictionary<string, object>();
            htmlAttributes.Add("placeholder", watermark);
            return HtmlExtensions.ControlStyled(html, html.TextBox, name,value, htmlAttributes);
        }

        public static MvcHtmlString WatermarkFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                             Expression<Func<TModel, TValue>> expression)
        {
            return WatermarkFor(html, expression, new Dictionary<string, object>());
        }

        public static MvcHtmlString WatermarkStyledFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                             Expression<Func<TModel, TValue>> expression,
                                                             IDictionary<string, object> attributes)
        {
            return HtmlExtensions.ControlStyledFor(html, html.WatermarkFor, expression, attributes);
        }

        public static MvcHtmlString WatermarkStyledFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                           Expression<Func<TModel, TValue>> expression,
                                                           object attributes)
        {
            return HtmlExtensions.ControlStyledFor(html, html.WatermarkFor, expression, attributes);
        }

        public static MvcHtmlString WatermarkStyledFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                          Expression<Func<TModel, TValue>> expression,
                                                          UiDataType uiDataType,
                                                          object attributes = null)
        {
            IDictionary<string, object> dictionaryAttrs = attributes == null ?
                new Dictionary<string, object>() :
                (IDictionary<string, object>)HtmlHelper.AnonymousObjectToHtmlAttributes(attributes);

            dictionaryAttrs.Add("data-type",(string)uiDataType);
            return HtmlExtensions.ControlStyledFor(html, html.WatermarkFor, expression, dictionaryAttrs);
        }

        public static MvcHtmlString WatermarkStyledFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                              Expression<Func<TModel, TValue>> expression)
        {
            return HtmlExtensions.ControlStyledFor(html, html.WatermarkFor, expression, null);
        }
     

    }
}