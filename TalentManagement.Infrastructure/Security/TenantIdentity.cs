﻿using System.Security.Principal;
using System;

namespace TalentManagement.Infrastructure.Security
{
    public class TenantIdentity : IIdentity
    {
        public const string DefaultTenant = "DefaultTenant";
        private const string TenantAuthenticationType = "TenantAuthentication";
        private readonly string _userName;
        private string _tenant;

        public TenantIdentity(string userName)
        {
            _userName = userName;
        }

        public string AuthenticationType
        {
            get { return TenantAuthenticationType; }
        }

        public bool IsAuthenticated { get; set; }

        public string FirstName { get; set; }
        public string SecondName { get; set; }

        public DateTime LastSignIn { get; set; }

        public string Tenant 
        { 
            get
            {
                if (String.IsNullOrEmpty(_tenant))
                {
                    _tenant = GetTenantFor(_userName);    
                }
                return _tenant;
            }
        }

        public string Team { get; set; }

        public static string GetTenantFor(string email)
        {
            if (false == String.IsNullOrEmpty(email))
            {
                return email.EmailDomainShort();
            }
            return DefaultTenant;
        }


        public string Name
        {
            get { return _userName; }
        }
    }
}
