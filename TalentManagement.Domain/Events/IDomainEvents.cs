﻿namespace TalentManagement.Domain.Events
{
    public interface IDomainEvents
    {
        void Raise<T>(T args)
        where T : IDomainEventArgs;
    }
}
