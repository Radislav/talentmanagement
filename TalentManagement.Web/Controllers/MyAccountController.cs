﻿using System.Web.Mvc;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Infrastructure.Validation;
using TalentManagement.Web.Models.MyAccount;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Controllers
{
    public class MyAccountController : Controller
    {
        private readonly IMapper<User, IndexModel> _indexModelMapper;
        private readonly IMapper<IndexModel, User> _userMapper;
        private readonly IMapper<IndexModel, Avatar> _avatarMapper;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IUserRepository _userRepository;
        private readonly IAvatarRepository _avatarRepository;
        private readonly IValidator<IndexModel> _validator;

        public MyAccountController( IMapper<User, IndexModel> indexModelMapper,
                                    IMapper<IndexModel, User> userMapper,
                                    IMapper<IndexModel, Avatar> avatarMapper,
                                    IPrincipalProvider principalProvider,
                                    IUserRepository userRepository,
                                    IAvatarRepository avatarRepository,
                                    IValidator<IndexModel> validator

                                    )
        {
            _avatarMapper = avatarMapper;
            _indexModelMapper = indexModelMapper;
            _userMapper = userMapper;
            _principalProvider = principalProvider;
            _userRepository = userRepository;
            _avatarRepository = avatarRepository;
            _validator = validator;
        }

        [HttpGet]
        public ActionResult Index()
        {
            User user = _userRepository.GetById(_principalProvider.GetUserName());
            return View(_indexModelMapper.Map(user));
        }

        [HttpPost]
        public ActionResult Index(IndexModel indexModel)
        {
            if (ModelState.IsValid)
            {
                _userRepository.UpdateSecondaryProperties(_userMapper.Map(indexModel));

                if (indexModel.HasImage)
                {
                    _avatarRepository.Update(_avatarMapper.Map(indexModel));
                }
            }
            return View(indexModel);
        }

    }
}