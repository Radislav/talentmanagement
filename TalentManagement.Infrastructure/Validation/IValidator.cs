﻿namespace TalentManagement.Infrastructure.Validation
{
    public interface IValidator<T> 
    {
        ValidationResult ValidateInstance(T instance);
    }
}
