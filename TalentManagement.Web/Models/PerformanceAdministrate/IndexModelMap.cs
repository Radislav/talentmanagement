﻿using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.PerformanceAdministrate
{
    internal class IndexModelMap : IMap<IEnumerable<ReviewCycle>,IndexModel>
    {
        private readonly IMapper<ReviewCycle, ReviewCycleModel> _reviewCycleModelMapper;

        public IndexModelMap(IMapper<ReviewCycle,ReviewCycleModel> reviewCycleModelMapper)
        {
            _reviewCycleModelMapper = reviewCycleModelMapper;
        }

        public void Configure(IMapperConfiguration<IEnumerable<ReviewCycle>, IndexModel> configuration)
        {
            configuration.ForMember(d => d.ReviewCycles, s => s.Select(cycle => _reviewCycleModelMapper.Map(cycle)));
            configuration.UseValue(d => d.ActiveButton, Resources.Models.PerformanceAdministrate.IndexModel.Active);
        }
    }
}