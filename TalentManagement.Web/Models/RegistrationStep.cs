﻿namespace TalentManagement.Web.Models
{
    public enum RegistrationStep
    {
        Profile = 0,
        Invitation = 1,
        Finish = 2
    }
}