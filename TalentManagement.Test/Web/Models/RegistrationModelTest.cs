﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TalentManagement.Web.Models;

namespace TalentManagement.Test.Web.Models
{
    [TestClass]
    public class RegistrationModelTest
    {
        [TestMethod]
        public void IsFlowFinished_true_when_Step_is_invitation_and_child_models_are_readyToGo()
        {
            RegistrationModel target = new RegistrationModel();
            target.Invitation.IsReadyToGo = target.Profile.IsReadyToGo = true;
            target.CurrentStep = (int)RegistrationStep.Invitation;
            
            Assert.IsTrue(target.IsFlowFinished);
        }

        [TestMethod]
        public void IsRegistrationFinished_false_by_default()
        {
            RegistrationModel target = new RegistrationModel();
            Assert.IsFalse(target.IsFlowFinished);
        }
    }
}
