﻿using System.Web.Mvc;
using FluentValidation;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Resources;

namespace TalentManagement.Web.Models.Validation
{
    public class EmailUniqueModelValidator : EmailModelValidator
    {
        public EmailUniqueModelValidator() 
        {
            IResourceProvider resourceProvider = DependencyResolver.Current.GetService<IResourceProvider>();
  
            IUserRepository userRepository = DependencyResolver.Current.GetService<IUserRepository>();

            RuleFor(model => model.Email)
                .Must(email => null == userRepository.GetById(email))
                .WithMessage(resourceProvider.ErrorEmailBusy);

          
        }
    }
}