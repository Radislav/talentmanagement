﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using MongoDB.Bson;

namespace TalentManagement.Domain.Performance
{
    [AggregateRoot]
    public class Review : Entity , IHasParticipants , IOwned
    {
        private Review()
        {
            Status = ReviewStatus.NotStarted;
            _answers = new List<Answer>();
            QuestionIds = new List<ObjectId>();
        }

        public Review(  ObjectId questionSetId,
                        ObjectId reviewCycleId,
                        IEnumerable<ObjectId> questionIds,
                        DateTime dueDate,
                        QuestionSetType setType,
                        string respondent,
                        string reviewer,
                        string createdBy) : this()
        {
            Contract.Requires(createdBy != null);
            Contract.Requires(respondent != null);
            Contract.Requires(reviewer != null);
            Contract.Requires(questionSetId != ObjectId.Empty);

            CreatedBy = createdBy;
            Respondent = respondent;
            Reviewer = reviewer;
            QuestionSetId = questionSetId;
            DueDate = dueDate;
            SetType = setType;
            ReviewCycleId = reviewCycleId;
            QuestionIds = questionIds;
        }

        [ContractInvariantMethod]
        private void Invariant()
        {
            Contract.Invariant(Participants != null);
            Contract.Invariant(Answers != null);
        }

        public ReviewStatus Status { get; set; }
        public ObjectId QuestionSetId { get; private set; }
        public ObjectId ReviewCycleId { get; private set; }
        public IEnumerable<ObjectId> QuestionIds { get; private set; }
        public string Respondent { get; private set; }
        public QuestionSetType SetType { get; private set; }
        public string Reviewer { get; set; }

        public IEnumerable<string> Participants
        {
            get
            {
                return new []{Respondent};
            }
        }

        public DateTime DueDate { get; private set; }

        private List<Answer> _answers;
        public IEnumerable<Answer> Answers
        {
            get { return _answers; }
            private set { _answers = new List<Answer>(value); }
        }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        
        public void UpsertAnswer(Answer answer,string owner)
        {
            Contract.Requires(answer != null);
            Contract.Requires(owner != null);
            
            answer.UpdatedBy = owner;
            answer.UpdatedAt = DateTime.Now; 

            if (answer.IsNew)
            {
                answer.Id = ObjectId.GenerateNewId();
                answer.CreatedBy = owner;
                answer.CreatedAt = DateTime.Now;
                _answers.Add(answer);
            }
            else
            {
                _answers.SingleOrDefault(a => a.Id == answer.Id)
                        .Do(a => a.Content = answer.Content);
            }
            
        }
     
    }
}
