﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using TalentManagement.Domain.Objectives;

namespace TalentManagement.Test.Domain.Objectives
{
    [TestClass]
    public class ActionItemsDictionaryTest
    {
        [TestMethod]
        public void ToTuples_should_not_fail_for_empty_dictionary()
        {
            var tuples = new ActionItemsDictionary().ToTuples().ToArray();
        }

        [TestMethod]
        public void ToTuples_should_not_create_tuples_if_objective_has_no_action_items()
        {
            ActionItemsDictionary target = new ActionItemsDictionary{{ObjectId.Empty,new ActionItem[0] }};
            var tuples = target.ToTuples().ToArray();

            Assert.AreEqual(0,tuples.Count());
        }

        [TestMethod]
        public void ToTuples_should_create_tuples_if_objective_has__action_items()
        {
            ActionItem actionItem = new ActionItem("", "", DateTime.Now.AddDays(1), true);
            ActionItemsDictionary target = new ActionItemsDictionary { { ObjectId.Empty, new[] { actionItem } } };
            var tuples = target.ToTuples().ToArray();

            Assert.AreEqual(1,tuples.Count());
            Assert.AreEqual(ObjectId.Empty, tuples.First().Item1);
            Assert.AreEqual(actionItem,tuples.First().Item2);
        }

        
    }
}
