﻿using System;
using System.Linq;
using TalentManagement.Domain;
using TalentManagement.Domain.Feeds;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Feeds.Map
{
    class FeedModelMap : IMap<Feed,FeedModel>
    {
        private readonly IMapper<Comment, CommentModel> _commentMapper;

        public FeedModelMap(IMapper<Comment,CommentModel> commentMapper)
        {
            _commentMapper = commentMapper;
        }

        public void Configure(IMapperConfiguration<Feed, FeedModel> configuration)
        {
            configuration.ForMember(dest => dest.Properties,
                                    source =>
                                    source.Properties.GetSafe(fp => fp.ToDictionary(p => p.Name, p => p.Value)));

            configuration.ForMember(dest => dest.CommentSection,
                                    source =>
                                        {
                                            var comments = source.Comments == null
                                                                          ? new CommentModel[0]
                                                                          : source.Comments.Select(c => _commentMapper.Map(c));
                                            return new CommentSectionModel
                                                       {
                                                           Comments = comments.ToArray(),
                                                           HolderId = Convert.ToString(source.Id)
                                                       };

                                        });

        }
    }
}