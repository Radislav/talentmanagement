﻿using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Data.Configuration;

namespace TalentManagement.Infrastructure.Data.Map
{
    public class ReviewMap : EntityConfiguration<Review>
    {
        public ReviewMap()
        {
            AsCollection("Reviews")
                .WithDataSource(DataSourceType.Tenant);

            Property(p => p.Respondent)
                .Required();

            Property(p => p.Participants)
                .Ignore();

        }
    }
}
