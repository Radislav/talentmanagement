﻿using System.Linq;
using FluentValidation;
using TalentManagement.Web.Models.Validation;
using TalentManagementValidation = TalentManagement.Infrastructure.Validation;

namespace TalentManagement.Web.Models.Objectives.Validation
{
    internal class CreateStep2Validator : AbstractValidator<CreateStep2>, TalentManagementValidation.IValidator<CreateStep2>
    {
        public CreateStep2Validator()
        {
            RuleFor(step2 => step2.KeyResults)
                .Must(keyResults => keyResults.Any())
                .When(step2 => step2.IsPageSelected || step2.WasPageSubmitted)
                .WithLocalizedMessage(() => Resources.Errors.KeyResultAtLeastOne);
        }

        public TalentManagementValidation.ValidationResult ValidateInstance(CreateStep2 instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}