﻿namespace TalentManagement.Infrastructure.Converter
{
    public interface ITypeConverter
    {
        object Convert(object to);
        object ConvertBack(object from);
    }

    public interface ITypeConverter<TFrom,TTo> : ITypeConverter
    {
        TFrom Convert(TTo to);
        TTo ConvertBack(TFrom from);
    }
}
