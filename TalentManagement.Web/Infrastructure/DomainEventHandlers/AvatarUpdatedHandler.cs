﻿using TalentManagement.Domain.Events;
using TalentManagement.Web.Infrastructure.Cache;

namespace TalentManagement.Web.Infrastructure.DomainEventHandlers
{
    internal class AvatarUpdatedHandler : IHandle<AvatarUpdatedArgs>
    {
         private readonly IOutputCacheCleaner _cacheCleaner;

         public AvatarUpdatedHandler(IOutputCacheCleaner cacheCleaner)
        {
            _cacheCleaner = cacheCleaner;
        }

        public void Handle(AvatarUpdatedArgs args)
        {
            _cacheCleaner.RemoveImageFromCache("GetSmallAvatar","Image",args.AvatarId);
        }
    }
}