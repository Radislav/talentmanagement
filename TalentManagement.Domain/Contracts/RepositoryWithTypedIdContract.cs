﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace TalentManagement.Domain.Contracts
{
    [ContractClassFor(typeof(IRepositoryWithTypedId<,>))]
    internal abstract class RepositoryWithTypedIdContract<TEntity,TId> : IRepositoryWithTypedId<TEntity,TId>
         where TEntity : EntityWithTypedId<TId>  
    {
        public void Insert(TEntity entity)
        {
            Contract.Requires<ArgumentNullException>(entity != null);
            Contract.Requires<EntityException>(entity.IsNew, "Enity should has default Id or no Id");
        }

        public void DeleteById(TId id)
        {
        }

        public void Update(TEntity entity)
        {
            Contract.Requires<ArgumentNullException>(entity != null);
            Contract.Requires<EntityException>(false == entity.IsNew, "Enity should has Id");
        }

        public IQueryable<TEntity> AsQueryable()
        {
            Contract.Ensures(Contract.Result<IQueryable<TEntity>>() != null);
            return null;
        }

        public TEntity GetById(TId id)
        {
            return default(TEntity);
        }


        public TEntity Upsert(TEntity entity)
        {
            Contract.Requires<ArgumentNullException>(entity != null);
            Contract.Ensures(Contract.Result<TEntity>() != null);

            return null;
        }


        public IEnumerable<TEntity> ApplyFilter(IFilter filter)
        {
            Contract.Requires(filter != null);
            Contract.Ensures(Contract.Result<IEnumerable<TEntity>>() != null);

            return null;
        }
    }
}
