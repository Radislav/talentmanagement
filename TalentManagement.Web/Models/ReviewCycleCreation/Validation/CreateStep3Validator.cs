﻿using System.Linq;
using FluentValidation;
using TalentManagement.Resources;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.ReviewCycleCreation.Validation
{
    public  class CreateStep3Validator : AbstractValidator<CreateStep3>, TalentManagement.Infrastructure.Validation.IValidator<CreateStep3>
    {
        public CreateStep3Validator()
        {
            RuleFor(m => m.IndividualsPicker)
                .Must((model, picker) => picker.UsersPicked.Any())
                .When(m => m.MustBeValidated && false == m.ManagersPicker.UsersPicked.Any() && false == m.ReviewEveryone)
                .OverridePropertyName("IndividualsPicker_CurrentNames")
                .WithLocalizedMessage(() => Errors.UserAtLeastOne);

            RuleFor(m => m.ManagersPicker)
               .Must((model, picker) => picker.UsersPicked.Any())
               .When(m => m.MustBeValidated && false == m.IndividualsPicker.UsersPicked.Any() && false == m.ReviewEveryone)
               .OverridePropertyName("ManagersPicker_CurrentNames")
               .WithLocalizedMessage(() => Errors.UserAtLeastOne);
        }

        public TalentManagement.Infrastructure.Validation.ValidationResult ValidateInstance(CreateStep3 instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}