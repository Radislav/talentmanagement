﻿using System;
using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain.Companies;
using TalentManagement.Domain.Feeds;
using TalentManagement.Domain.Objectives;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Web.Models.Questionaire;

namespace TalentManagement.Web.Models.PerformanceSummary.Map
{
    internal class IndexModelMap : IMap<ReviewCycleSummary,IndexModel>
    {
        private readonly IMapper<Tuple<Kudos,Badge>,KudosModel> _kudosModelMapper;
        private readonly IMapper<Objective, ObjectiveModel> _objectiveModelMapper;
        private readonly IResourceProvider _resourceProvider;
        private readonly IMapper<AnswersCollection, IEnumerable<AnswerModel>> _answersModelMapper;

        public IndexModelMap(IMapper<Tuple<Kudos, Badge>, KudosModel> kudosModelMapper, IMapper<Objective, ObjectiveModel> objectiveModelMapper, IMapper<AnswersCollection, IEnumerable<AnswerModel>> answersModelMapper, IResourceProvider resourceProvider)
        {
            _kudosModelMapper = kudosModelMapper;
            _objectiveModelMapper = objectiveModelMapper;
            _resourceProvider = resourceProvider;
            _answersModelMapper = answersModelMapper;
        }
        

        public void Configure(IMapperConfiguration<ReviewCycleSummary, IndexModel> configuration)
        {
            configuration.ForMember(d => d.Answers, s => _answersModelMapper.Map(s.GetAnswers(QuestionSetType.Summary)));
            configuration.ForMember(d => d.Kudos, s => s.KudosWithBadges.Select(tuple => _kudosModelMapper.Map(tuple)));
            configuration.ForMember(d => d.Objectives, s => s.Objectives.Select(o => _objectiveModelMapper.Map(o)));
            configuration.ForMember(d => d.PeersAnswers,
                                    s => _answersModelMapper.Map(s.GetAnswers(QuestionSetType.Pears)));
            configuration.ForMember(d => d.PersonalAnswers,
                                    s => _answersModelMapper.Map(s.GetAnswers(QuestionSetType.Self)));
            configuration.ForMember(d => d.Respondent,s => s.SummaryReviewRespondent.Id);
            configuration.ForMember(d => d.ReviewCreatedAt, s => String.Format("{0} {1}", _resourceProvider.Created, s.SummaryReview.CreatedAt.AsLondDateString()));
            configuration.ForMember(d => d.ReviewDue, s => String.Format("{0} {1}", _resourceProvider.Due, s.SummaryReview.DueDate.AsLondDateString()));
            configuration.ForMember(d => d.ReviewId, s => s.SummaryReview.Id);
            configuration.ForMember(d => d.CycleName, s => s.Cycle.Name);
            configuration.ForMember(d => d.RespondentNames, s => s.SummaryReviewRespondent.Names);
        }
    }
}