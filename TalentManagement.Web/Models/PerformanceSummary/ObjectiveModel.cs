﻿using MongoDB.Bson;

namespace TalentManagement.Web.Models.PerformanceSummary
{
    public class ObjectiveModel
    {
        public bool Achieved { get; set; }
        public string Name { get; set; }
        public string ObjectiveDue { get; set; }
        public decimal Progress { get; set; }
        public ObjectId IconId { get; set; }
        public ObjectId Id { get; set; }
    }
}