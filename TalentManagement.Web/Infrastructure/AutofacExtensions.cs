﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Autofac;
using Autofac.Builder;
using Autofac.Features.Metadata;
using TalentManagement.Web.Models.Binder;

namespace TalentManagement.Web.Infrastructure
{
    public static class AutofacExtensions
    {
        private const string ModelType = "ModelType";
        private const string OnDemand = "OnDemand";
        /// <summary>
        /// Register type as model binder.
        /// </summary>
        /// <typeparam name="TBinder">Type of model binder.</typeparam>
        /// <typeparam name="TModel">Type of model to be bounded.</typeparam>
        /// <param name="builder">Container builder</param>
        ///  <param name="useOnDemandOnly">When true, then default binder will be used. When false, then default binder will be overridden 
        /// and TBinder will be used any time if instance of TModel is passing through a view to a controller.</param>
        /// <returns>Registration builder instance.</returns>
        public static IRegistrationBuilder<TBinder, ConcreteReflectionActivatorData,SingleRegistrationStyle>
            RegisterModelBinderFor<TBinder, TModel>(this ContainerBuilder builder,bool useOnDemandOnly)
            where TBinder : IModelBinder
        {
            return builder.RegisterType<TBinder>()
                .As(typeof (ICustomModelBinder<TModel>))
                .As<IModelBinder>()
                .WithMetadata(ModelType, typeof (TModel))
                .WithMetadata(OnDemand, useOnDemandOnly);
        }

        public static IRegistrationBuilder<EnumBinder<TEnum>, ConcreteReflectionActivatorData, SingleRegistrationStyle>
            RegisterModelBinderForEnum<TEnum>(this ContainerBuilder builder)
            where TEnum : struct
        {
            return RegisterModelBinderFor<EnumBinder<TEnum>, TEnum>(builder,false)
                .WithParameter("defaultValue", default(TEnum));
        }

        public static void AddModelBinders(this IContainer container, ModelBinderDictionary binderDictionary)
        {
            container.Resolve<IEnumerable<Meta<IModelBinder>>>()
                    .Where(ShouldAddBinderPredicate())
                    .Map(binder => binderDictionary.Add((Type) binder.Metadata[ModelType], binder.Value));
        }

        private static Func<Meta<IModelBinder>, bool> ShouldAddBinderPredicate()
        {
            return b => b.Metadata.ContainsKey(ModelType) && b.Metadata.ContainsKey(OnDemand) && false == (bool)b.Metadata[OnDemand];
        }
     
    }
}