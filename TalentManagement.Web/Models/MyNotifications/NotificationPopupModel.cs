﻿namespace TalentManagement.Web.Models.MyNotifications
{
    public class NotificationPopupModel
    {
        public string Sender { get; set; }
        public string Text { get; set; }
        public string CreatedAt { get; set; }
    }
}