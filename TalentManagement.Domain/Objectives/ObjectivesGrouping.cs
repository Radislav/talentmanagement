﻿namespace TalentManagement.Domain.Objectives
{
    public enum ObjectivesGrouping
    {
        None,
        Date,
        Type,
        Team,
        Person,
        Status
    }
}