﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Autofac.Features.Indexed;
using TalentManagement.AppServices;
using TalentManagement.Domain;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Data.Filter;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Infrastructure.Validation;
using TalentManagement.Web.Infrastructure.Security;
using TalentManagement.Web.Models.People;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Controllers
{
    [TenantAuthorization(Roles = SecurityConstants.ContributorRole)]
    public class PeopleController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper<AddUserModel, User> _addUserModelToUserMapper;
        private readonly IMapper<User, UserRowModel> _userToUserRowModelMapper;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IResourceProvider _resourceProvider;
        private readonly IGroupUploader _groupUploader;
        private readonly IMapper<IGrouping<string, User>, TeamRowModel> _groupingUserToTeamRowModelMapper;
        private readonly IIndex<FilterType, IFilter> _filterIndexer;
        private readonly IRegistrationFlow _registrationFlow;
        private readonly IValidator<AddUserModel> _addUserModelValidator;
 

        public PeopleController(IUserRepository userRepository, 
            IMapper<AddUserModel, User> addUserModelToUserMapper,
            IMapper<User, UserRowModel> userToUserRowModelMapper,
            IResourceProvider resourceProvider,
            IPrincipalProvider principalProvider,
            IGroupUploader groupUploader,
            IMapper<IGrouping<string, User>, TeamRowModel> groupingUserToTeamRowModelMapper,
            IIndex<FilterType, IFilter> filterIndexer,
            IRegistrationFlow registrationFlow,
            IValidator<AddUserModel> addUserModelValidator)
        {
            _userRepository = userRepository;
            _addUserModelToUserMapper = addUserModelToUserMapper;
            _userToUserRowModelMapper = userToUserRowModelMapper;
            _resourceProvider = resourceProvider;
            _principalProvider = principalProvider;
            _groupUploader = groupUploader;
            _groupingUserToTeamRowModelMapper = groupingUserToTeamRowModelMapper;
            _filterIndexer = filterIndexer;
            _registrationFlow = registrationFlow;
            _addUserModelValidator = addUserModelValidator;
        }

        [HttpGet]
        public ViewResult Staff(StaffModel model)
        {
            var filter = GetCurrentFilter(model);
            model.Rows = _userRepository.GetUsersForTenant(_principalProvider.GetTenant(), filter)
                .Select(x => _userToUserRowModelMapper.Map(x))
                .ToArray();

            return View(model);
        }

        [HttpGet]
        public ViewResult Team(TeamModel model)
        {
            var filter = GetCurrentFilter(model);
            model.Teams = _userRepository.GetGroupedUsersForTenant(_principalProvider.GetTenant(), filter)
                .Select(x => _groupingUserToTeamRowModelMapper.Map(x))
                .ToArray();

            return View(model);
        }

        [HttpGet]
        public ActionResult AddUser()
        {
            return AjaxFormView(new AddUserModel());
        }

        [HttpPost]
        public ActionResult AddUser(AddUserModel model)
        {
            ValidationResult validationResult = _addUserModelValidator.ValidateInstance(model);
            if (validationResult.IsValid)
            {
                User user = _addUserModelToUserMapper.Map(model);
                if (_userRepository.CreateUser(user) == UserCreateResult.Ok)
                {
                    return AjaxFormView("AddUserResult");
                }
                
                List<ValidationFailure> failures = new List<ValidationFailure>(validationResult)
                    {
                        new ValidationFailure("Email", _resourceProvider.ErrorEmailBusy)
                    };
                validationResult = new ValidationResult(failures);
            }

            return validationResult.AsJson();
        }

        [HttpGet]
        public ActionResult GroupUpload()
        {
            return AjaxFormView(new GroupUploadModel());
        }

        [HttpPost]
        public ActionResult GroupUpload(GroupUploadModel model)
        {
            if (ModelState.IsValid)
            {
                var warnings = _groupUploader.Upload(model.Template.InputStream);
                if (warnings.Any())
                {
                    warnings.Map(x => ModelState.AddModelError(string.Empty, x));
                }
                else
                {
                    return AjaxFormView("GroupUploadResult");
                }
            }

            return AjaxFormView(model);
        }

        [HttpGet]
        public FileContentResult UploadTemplate()
        {
            byte[] contents = _groupUploader.GetTemplateContents(_principalProvider.GetUserName().EmailDomainLong());
            return File(contents, _groupUploader.TemplateContentType,_groupUploader.FileName);
        }

        [HttpGet]
        public ActionResult SendInvitation()
        {
            var model = new SendInvitationModel();
            return AjaxFormView(model);
        }

        [HttpPost]
        public ActionResult SendInvitation(SendInvitationModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _registrationFlow.Invite(model.Email);
                    return AjaxFormView("SendInvitationResult");
                }
                catch(RegistrationException e)
                {
                    ModelState.AddModelError("EmailLocal", e.Message);
                }
            }

            return AjaxFormView(model);
        }

        [HttpPost]
        public ActionResult ToggleIsManager(string userId)
        {
            User user = _userRepository.GetById(userId);
            user.IsManager = !user.IsManager;
            _userRepository.Update(user);

            return Json(user.IsManager ? _resourceProvider.PeopleNotManager : _resourceProvider.PeopleMarkAsManager);
        }

        #region Private methods
        private IFilter GetCurrentFilter(PeopleModel peopleModel)
        {
            var userFilter = _filterIndexer[FilterType.User];
            userFilter.FilterContext = new UserContext
                {
                    UserNamePrefix = peopleModel.SecondNamePrefix,
                    Status = peopleModel.UserStatus,
                };

            return userFilter;
        }
        
        private ActionResult AjaxFormView(string action)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView(action);
            }

            return View(action);
        }

        private ActionResult AjaxFormView(object model)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView(model);
            }

            return View(model);
        }

        #endregion
    }
}
