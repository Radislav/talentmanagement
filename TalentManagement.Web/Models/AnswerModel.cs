﻿using MongoDB.Bson;

namespace TalentManagement.Web.Models
{
    public class AnswerModel
    {
        public ObjectId Id { get; set; }
        public ObjectId QuestionId { get; set; }

        public string Content { get; set; }
        public string QuestionText { get; set; }
        
    }
}