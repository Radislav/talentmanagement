﻿namespace TalentManagement.Infrastructure.Session
{
    public interface ISessionFactory
    {
        ISession Create();
    }
}
