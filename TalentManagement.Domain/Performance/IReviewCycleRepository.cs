﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using MongoDB.Bson;
using TalentManagement.Domain.Contracts;

namespace TalentManagement.Domain.Performance
{
    [ContractClass(typeof(ReviewCycleRepositoryContract))]
    public interface IReviewCycleRepository : IRepository<ReviewCycle>  
    {
        IEnumerable<ReviewCycle> GetActiveCycles();
        IEnumerable<ReviewCycle> GetCompletedCycles();
        IEnumerable<ReviewCycleDescriptor> GetDescriptors();
    }
}
