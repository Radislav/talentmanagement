﻿namespace TalentManagement.Web.Models
{
    public class RegistrationModel : WizardMaster
    {
        public RegistrationModel()
        {
            Invitation = new InvitationModel();
            Profile = new ProfileModel();
            ShowSubmit = true;
        }
        public InvitationModel Invitation { get; set; }
        public ProfileModel Profile { get; set; }
        public bool ShowSubmit { get; set; }

        public override System.Collections.Generic.IEnumerable<WizardPage> Pages
        {
            get
            {
                return new WizardPage[] { Invitation,Profile};
            }
        }

        public override bool IsFlowFinished
        {
            get
            {
                if (Profile.InviteColleagues)
                {
                    return Profile.IsReadyToGo && Invitation.IsReadyToGo && Invitation.ThisStep == CurrentStep;
                }

                return Profile.IsReadyToGo;
            }
        }
    }
}