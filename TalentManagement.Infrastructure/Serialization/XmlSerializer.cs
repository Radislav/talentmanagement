﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace TalentManagement.Infrastructure.Serialization
{
    class XmlSerializer<TEntity> : IXmlSerializer<TEntity>
    {
        private readonly XmlSerializerNamespaces _emptyNameSpaces;

        public XmlSerializer()
        {
            _emptyNameSpaces = new XmlSerializerNamespaces();
            _emptyNameSpaces.Add(String.Empty,String.Empty);

        }

        public XElement Serialize(TEntity entity)
        {
            if (ReferenceEquals(entity,null))
            {
                return null;
            }

            XElement result;

            StringBuilder buffer = new StringBuilder();
            using (StringWriter writer = new StringWriter(buffer))
            {
                new XmlSerializer(typeof (TEntity),string.Empty).Serialize(writer, entity,_emptyNameSpaces);
                using (StringReader reader = new StringReader(buffer.ToString()))
                {
                    result = XElement.Load(reader);    
                }
            }
            return result;
        }

        public TEntity Deserialize(XElement serializedEntity)
        {
            TEntity result = default(TEntity);

            XmlSerializer serializer = new XmlSerializer(typeof(TEntity),String.Empty);
            XmlReader reader = serializedEntity.CreateReader();
            if (serializer.CanDeserialize(reader))
            {
                result = (TEntity)serializer.Deserialize(reader);
            }
            return result;
        }
    }
}
