﻿using System.Web.Script.Serialization;
using TalentManagement.Infrastructure.Serialization;

namespace TalentManagement.Web.Infrastructure
{
    class JsonSerializer<TEntity> : IJsonSerializer<TEntity>
    {
        private readonly JavaScriptSerializer _javaScriptSerializer = new JavaScriptSerializer();
        public string Serialize(TEntity entity)
        {
            return _javaScriptSerializer.Serialize(entity);
        }

        public TEntity Deserialize(string serializedEntity)
        {
            return _javaScriptSerializer.Deserialize<TEntity>(serializedEntity);
        }
    }
}