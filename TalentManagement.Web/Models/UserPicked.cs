﻿using TalentManagement.Domain;

namespace TalentManagement.Web.Models
{
    public class UserPicked : IHasId<string>
    {
        readonly IdHolderComparer<string> _comparer = new IdHolderComparer<string>();

        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Id { get; set; }

        public string Prefix { get; set; }

        public string Names
        {
            get { return string.Concat(SecondName, " ", FirstName); }
        }

        public override string ToString()
        {
            return string.Concat(SecondName, ", ",FirstName);
        }

        public override bool Equals(object obj)
        {
            return _comparer.Equals(this, obj as UserPicked);
        }

        public override int GetHashCode()
        {
            return _comparer.GetHashCode(this);
        }
    }
}