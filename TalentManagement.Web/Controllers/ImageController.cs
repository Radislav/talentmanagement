﻿using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Domain.Companies;
using TalentManagement.Domain.Objectives;
using TalentManagement.Domain.Users;
using TalentManagement.Web.Infrastructure;
using TalentManagement.Web.Infrastructure.UI;

namespace TalentManagement.Web.Controllers
{
    public class ImageController : Controller
    {
        private readonly IObjectiveIconRepository _objectiveIconRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IAvatarRepository _avatarRepository;
        private readonly IBadgeRepository _badgeRepository;
        
        public ImageController(
            IObjectiveIconRepository objectiveIconRepository,
            ICompanyRepository companyRepository,
            IBadgeRepository badgeRepository,
            IAvatarRepository avatarRepository)
        {
            _objectiveIconRepository = objectiveIconRepository;
            _companyRepository = companyRepository;
            _avatarRepository = avatarRepository;
            _badgeRepository = badgeRepository;
        }

        [HttpGet]
        [OutputCache(CacheProfile = "ImageCacheServer")]
        public ActionResult GetSmallAvatar(string id)
        {
            Avatar avatar = _avatarRepository.GetOnlySmallImage(id);
            return new PictureResult(avatar,(int)ImageSize.Small, Url, "~/Images/default_avatar.gif","image/gif");
        }

        [HttpGet]
        [OutputCache(CacheProfile = "ImageCache")]
        public ActionResult GetObjectiveIcon(ObjectId id, int size)
        {
            ObjectiveIcon icon = _objectiveIconRepository.GetById(id);
            return new PictureResult(icon,size, Url, "~/Images/default_objective.gif", "image/gif");
        }

        [HttpGet]
        [OutputCache(CacheProfile = "ImageCacheServer")]
        public ActionResult GetCompanyLogo(ObjectId id, int size)
        {
            Logo logo = _companyRepository.GetLogo(id);
            return new PictureResult(logo,size, Url, "~/Images/default_objective.gif", "image/gif");
        }

        [HttpGet]
        [OutputCache(CacheProfile = "ImageCacheServer")]
        public ActionResult GetCompanyLogoForTenant(string id, int size)
        {
            Logo logo = _companyRepository.GetByTenant(id)
                                .GetSafe(c => c.Logo);

            return new PictureResult(logo,size, Url, "~/Content/themes/base/images/logo.png","image/png");
        }

        [HttpGet]
        [OutputCache(CacheProfile = "ImageCacheServer")]
        public ActionResult GetBadge(ObjectId badgeId, int size)
        {
            Badge badge = _badgeRepository.GetById(badgeId);
            return new PictureResult(badge,size, Url, "~/Images/default_badge.png", "image/png");
        }
    }
}
