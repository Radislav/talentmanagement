﻿namespace TalentManagement.Domain.Objectives
{
    public enum ObjectiveVisibility
    {
        Public,
        Private
    }
}
