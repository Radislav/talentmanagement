﻿using MongoDB.Bson;

namespace TalentManagement.Web.Models.MyPreferences
{
    public class NotificationSettingModel
    {
        public bool IsEnabled { get; set; }
        public string Text { get; set; }
        public ObjectId SettingId { get; set; }
    }
}