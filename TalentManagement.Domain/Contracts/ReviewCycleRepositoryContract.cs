﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using MongoDB.Bson;
using TalentManagement.Domain.Performance;

namespace TalentManagement.Domain.Contracts
{
    [ContractClassFor(typeof(IReviewCycleRepository))]
    internal abstract class ReviewCycleRepositoryContract :  IReviewCycleRepository
    {
        public IEnumerable<ReviewCycle> GetActiveCycles()
        {
            Contract.Ensures(Contract.Result<IEnumerable<ReviewCycle>>() != null);
            return null;
        }

        public IEnumerable<ReviewCycle> GetCompletedCycles()
        {
            Contract.Ensures(Contract.Result<IEnumerable<ReviewCycle>>() != null);
            return null;
        }

        public IEnumerable<ReviewCycleDescriptor> GetDescriptors()
        {
            Contract.Ensures(Contract.Result<IEnumerable<ReviewCycleDescriptor>>() != null);
            return null;
        }

        public void Insert(ReviewCycle entity)
        {
            
        }

        public void DeleteById(ObjectId id)
        {
            
        }

        public void Update(ReviewCycle entity)
        {
            
        }

        public IQueryable<ReviewCycle> AsQueryable()
        {
            return null;
        }

        public ReviewCycle GetById(ObjectId id)
        {
            return null;
        }

        public ReviewCycle Upsert(ReviewCycle entity)
        {
            return null;
        }


        public IEnumerable<ReviewCycle> ApplyFilter(IFilter filter)
        {
            return null;
        }
    }
}