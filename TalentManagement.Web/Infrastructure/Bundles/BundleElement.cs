﻿using System.Linq;
using System.Configuration;

namespace TalentManagement.Web.Infrastructure.Bundles
{
    public class BundleElement : ConfigurationElement
    {
        [ConfigurationProperty("type",IsRequired = true)]
        public BundleType BundleType 
        {
            get { return (BundleType)base["type"]; }
            set { base["type"] = value; }
        }

        [ConfigurationProperty("forView", IsRequired = false)]
        public string ForView
        {
            get { return base["forView"] as string; }
            set { base["forView"] = value; }
        }

        [ConfigurationProperty("name", IsRequired = true,IsKey = true)]
        public string Name
        {
            get { return base["name"] as string; }
            set { base["name"] = value; }
        }

        public string[] Urls { get { return Content.Cast<ContentElement>().Select(e => e.Url).ToArray(); }
        }
        
        [ConfigurationProperty("contents", IsDefaultCollection = false,IsRequired = true),
         ConfigurationCollection(typeof (ContentCollection), AddItemName = "content", ClearItemsName = "clear",
             RemoveItemName = "remove")]
        public ContentCollection Content { get { return ((ContentCollection)(base["contents"])); } }
    }
}