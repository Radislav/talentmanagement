﻿using System;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Email;
using TalentManagement.Resources;

namespace TalentManagement.Web.Infrastructure.Resources
{
    internal class ApplicationResourceProvider : IResourceProvider
    {
        public string GetEmailBody(EmailType emailType)
        {
            switch (emailType)
            {
                case EmailType.ForgotPassword:
                    return Email.ForgotPasswordTemplate;
                case EmailType.Activation:
                    return Email.ActivationEmail;
                case EmailType.Invitation:
                    return Email.InvitationEmail;
                default:
                    throw new NotSupportedException();
            }
        }

        public string GetEmailSubject(EmailType emailType)
        {
            switch (emailType)
            {
                case EmailType.ForgotPassword:
                    return Email.ForgotPasswordSubject;
                case EmailType.Activation:
                    return Email.ActivationSubject;
                case EmailType.Invitation:
                    return Email.InvitationSubject;
                default:
                    throw new NotSupportedException();
            }
        }

        public string GroupUploadFileNotValid
        {
            get { return Errors.GroupUploadFileNotValid; }
        }

        public string GroupUploadRowWarning(int rowIndex, string warning)
        {
            return string.Format(Errors.GroupUploadRowWarning, rowIndex, warning);
        }

        public string ErrorEmailNotFounded
        {
            get { return Errors.EmailNotFounded; }
        }
        
        public string ErrorEmailInvalid
        {
            get { return Errors.email_error; }
        }
        
        public string ErrorInvalidUserOrPassword
        {
            get { return Errors.InvalidUserOrPassword; }
        }

        public string ErrorLoginsAttemptReached
        {
            get { return Errors.LoginAttemptsReached; }
        }
        
        public string ErrorPasswordRequired
        {
            get { return Errors.PasswordRequired; }
        }

        public string ErrorPasswordsMustMatch
        {
            get { return Errors.PasswordsMustMatch; }
        }
        
        public string ErrorDateStartAfterDateEnd(string dateStartTitle,string dateEndTitle)
        {
            return String.Format(Errors.DateStartAfterDateEnd, dateStartTitle, dateEndTitle);
        }

        public string UiAppendixEmail
        {
            get { return TalentManagement.Resources.UI.AppendixEmail; }
        }
        
        public string GetMimeType(string fileType)
        {
            return MIME.ResourceManager.GetString(fileType) ?? "application/octet-stream";
        }
        
        public string ErrorEmailBusy
        {
            get { return Errors.EmailBusy; }
        }
        
        public string UiNextStep
        {
            get { return TalentManagement.Resources.UI.NextStep; }
        }

        public string ObjectiveDetailsProgressUpdated(string objectiveName, decimal progress)
        {
            return String.Format(TalentManagement.Resources.Views.ObjectiveDetails.Index.ProgressUpdated,objectiveName, progress);
        }

        public string ObjectiveDetailsCompleted
        {
            get { return TalentManagement.Resources.Views.ObjectiveDetails.Index.ObjectiveCompleted; }
        }
        
        public string ErrorUserShouldBeObjectiveContributor
        {
            get { return Errors.ErrorUserShouldBeObjectiveContributor; }
        }
        
        public string MyCompanyObjectivesDue
        {
            get { return TalentManagement.Resources.Views.MyCompanyObjectives.Index.Due; }
        }
        
        public string MyCompanyObjectiveGoto
        {
            get { return TalentManagement.Resources.Views.MyCompanyObjectives.Index.Goto; }
        }
        
        public string MyCompanyObjectiveDateRange
        {
            get { return TalentManagement.Resources.Views.MyCompanyObjectives.Index.DateRange; }
        }

        public string MyCompanyObjectivePerson
        {
            get { return TalentManagement.Resources.Views.MyCompanyObjectives.Index.Person; }
        }

        public string MyCompanyObjectiveStatus
        {
            get { return TalentManagement.Resources.Views.MyCompanyObjectives.Index.Status; }
        }
        
        public string MyCompanyObjectiveCurrentYear
        {
            get { return TalentManagement.Resources.Models.MyCompanyObjectives.PageModel.CurrentYear; }
        }

        public string MyCompanyObjectivePreviousYear
        {
            get { return TalentManagement.Resources.Models.MyCompanyObjectives.PageModel.PreviousYear; }
        }

        public string MyCompanyObjectiveAllYears
        {
            get { return TalentManagement.Resources.Models.MyCompanyObjectives.PageModel.AllYears; }
        }


        public string MyObjectivesTop
        {
            get { return TalentManagement.Resources.Models.MyObjectives.PageModel.MyTopObjectives; }
        }

        public string MyObjectivesTeam
        {
            get { return TalentManagement.Resources.Models.MyObjectives.PageModel.MyTeamObjectives; }
        }

        public string MyObjectivesPersonal
        {
            get { return TalentManagement.Resources.Models.MyObjectives.PageModel.PersonalObjectives; }
        }

        public string MyObjectivesContribute
        {
            get { return TalentManagement.Resources.Models.MyObjectives.PageModel.ContributesObjectives; }
        }


        public string FeedbackTheyRequestedTitle
        {
            get { return TalentManagement.Resources.Models.MyFeedbacks.FeedbackAttentionRequired.QuestionFormat; }
        }


        public string FeedbackActiveButton
        {
            get { return TalentManagement.Resources.Models.MyFeedbacks.IndexModel.RequiredAttention; }
        }


        public string FeedbackYourRequestedTitle
        {
            get { return TalentManagement.Resources.Models.MyFeedbacks.FeedbackYouRequested.TitleFormat; }
        }

        public string FeedbackYourRequestedResponse
        {
            get { return TalentManagement.Resources.Models.MyFeedbacks.FeedbackYouRequested.ResponsesTextFormat; }
        }


        public string FeedbackAttentionRequired
        {
            get { return TalentManagement.Resources.Models.MyFeedbacks.IndexModel.RequiredAttention; }
        }

        public string FeedbackYouRequested
        {
            get { return TalentManagement.Resources.Models.MyFeedbacks.IndexModel.YouRequested; }
        }

        public string FeedbackTheyRequested
        {
            get { return TalentManagement.Resources.Models.MyFeedbacks.IndexModel.TheyRequested; }
        }
        
        public string FeedbackYouProvidedFeedback
        {
            get { return TalentManagement.Resources.Models.MyFeedbacks.FeedbackTheyRequested.YouProvidedFeedback; }
        }


        public string ErrorAtLeastOneUser
        {
            get { return Errors.UserAtLeastOne; }
        }


        public string FeedbackDetails
        {
           get { return TalentManagement.Resources.Models.MyFeedbacks.IndexModel.Details; }
        }


        public string Enable
        {
            get { return TalentManagement.Resources.UI.Enable; }
        }

        public string Disable
        {
            get { return TalentManagement.Resources.UI.Disable; }
        }


        public string ErrorPasswordInvalid
        {
            get { return Errors.PasswordInvalid; }
        }


        public string Today
        {
            get { return TalentManagement.Resources.UI.Today; }
        }

        public string Yesterday
        {
            get { return TalentManagement.Resources.UI.Yesterday; }
        }


        public string MyObjectivesNoTop
        {
            get { return TalentManagement.Resources.Models.MyObjectives.PageModel.NoTopObjectives; }
        }

        public string MyObjectivesNoTeam
        {
            get { return TalentManagement.Resources.Models.MyObjectives.PageModel.NoTeamObjectives; }
        }

        public string MyObjectivesNoPersonal
        {
            get { return TalentManagement.Resources.Models.MyObjectives.PageModel.NoPersonalObjectives; }
        }

        public string MyObjectivesNoContribute
        {
            get { return TalentManagement.Resources.Models.MyObjectives.PageModel.NoContributedObjectives; }
        }


        public string MyActionItemsRelated
        {
            get { return TalentManagement.Resources.Models.MyActionItems.IndexModel.Related; }
        }

        public string MyActionItemsNotRelated
        {
            get { return TalentManagement.Resources.Models.MyActionItems.IndexModel.NotRelated; }
        }
        
        public string Due
        {
            get { return TalentManagement.Resources.UI.Due; }
        }
        
        public string ReviewCycleDateStart
        {
            get { return TalentManagement.Resources.Models.ReviewCycleCreation.CreateStep1.ReviewStart; }
        }
        public string ReviewCycleDateEnd
        {
            get { return TalentManagement.Resources.Models.ReviewCycleCreation.CreateStep1.ReviewEnd; }
        
        }


        public string ReviewCyclePearsQuestions
        {
            get { return TalentManagement.Resources.Models.ReviewCycleCreation.CreateStep2.PearsQuestions; }
        }

        public string ReviewCycleSelfQuestions
        {
            get
            {
                return TalentManagement.Resources.Models.ReviewCycleCreation.CreateStep2.SelfQuestions; 
            }
        }

        public string ReviewCycleSummaryQuestions
        {
            get { return TalentManagement.Resources.Models.ReviewCycleCreation.CreateStep2.SummaryQuestions; }
        }

        public string ReviewCycleQuestion
        {
              get { return TalentManagement.Resources.Models.ReviewCycleCreation.CreateStep2.Question; }
        }


        public string ReviewCycleAddInvidivual
        {
            get { return TalentManagement.Resources.Models.ReviewCycleCreation.CreateStep3.AddIndividual; }
        }

        public string ReviewCycleAddManager
        {
            get { return TalentManagement.Resources.Models.ReviewCycleCreation.CreateStep3.AddManager; }
        }

        public string ReviewCycleHasQuestions
        {
            get { return TalentManagement.Resources.Models.ReviewCycleCreation.CreateStep2.HasQuestions; }
        }


        public string ReviewCycleNoQuestions
        {
            get { return TalentManagement.Resources.Models.ReviewCycleCreation.CreateStep2.NoQuestions; }
        }


        public string MyReviewsRequiredAttention
        {
            get { return TalentManagement.Resources.Models.MyReviews.IndexModel.RequiredAttention; }
        }

        public string MyReviewsYouRequested
        {
            get { return TalentManagement.Resources.Models.MyReviews.IndexModel.YouRequested; }
        }

        public string MyReviewsTheyRequested
        {
            get { return TalentManagement.Resources.Models.MyReviews.IndexModel.TheyRequested; }
        }


        public string CycleDetailsSelectCycle
        {
            get { return TalentManagement.Resources.Models.CycleDetails.IndexModel.SelectCycle; }
        }

        public string CycleDetailsProgress
        {
            get { return TalentManagement.Resources.Models.CycleDetails.IndexModel.ReviewProgress; }
        }


        public string Created
        {
            get { return TalentManagement.Resources.UI.Created; }
        }


        public string PerformanceSummaryKudosThankingFormat
        {
            get { return TalentManagement.Resources.Models.PerformanceSummary.IndexModel.KudosThankingFormat; }
        }


        public string QuestionnaireAnswersSaved
        {
            get { return TalentManagement.Resources.Models.Questionaire.IndexModel.AnswersSaved; }
        }


        public string PeopleNotManager
        {
            get { return TalentManagement.Resources.Views.People.PeopleLayout.NotManager; }
        }

        public string PeopleMarkAsManager
        {
            get { return TalentManagement.Resources.Views.People.PeopleLayout.MarkManager; }
        }
    }
}