﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Web.Models.Teams;
using TalentManagement.Web.Models.Teams.Map;

namespace TalentManagement.Web.Controllers
{
    public class TeamController : Controller
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly ITeamRepository _teamRepository;
        private readonly ITeamModelMapper _teamModelMapper;
        private readonly IUserRepository _userRepository;

        public TeamController(IPrincipalProvider principalProvider,
                             ITeamRepository teamRepository,
                             ITeamModelMapper teamModelMapper,
                             IUserRepository userRepository)
        {
            _principalProvider = principalProvider;
            _teamRepository = teamRepository;
            _teamModelMapper = teamModelMapper;
            _userRepository = userRepository;
        }

        public ActionResult Index()
        {
            return View(new IndexModel {Teams = GetTeamModels()});
        }

       
        [HttpPost]
        public ActionResult SetMembership(SetMembershipModel model)
        {
            User user = _userRepository.GetById(model.UserId);
            user.ManagerEmail = model.ManagerId;
            _userRepository.Update(user);

            return PartialView("_Teams", GetTeamModels(model));
        }

        [HttpPost]
        public ActionResult UpdateTeamName(ObjectId teamId, string teamName)
        {
            Team team = _teamRepository.GetById(teamId);
            team.Name = teamName;
            _teamRepository.Update(team);
            return Json(Convert.ToString(team.Id));
        }

        private IEnumerable<TeamModel> GetTeamModels(SetMembershipModel membershipModel = null)
        {
            string tenant = _principalProvider.GetTenant();
            IEnumerable<Team> teams = _teamRepository.GetTeams(tenant, true).ToArray();
            IEnumerable<User> users = _userRepository.GetUsersForTenant(tenant);

            IEnumerable<TeamModel> teamModels = teams.Select(t =>
                _teamModelMapper.Map(t, users, membershipModel != null && membershipModel.IsTeamExpanded(t.Id)));

            return teamModels.OrderBy(t => t.Id);
        }
    }
}