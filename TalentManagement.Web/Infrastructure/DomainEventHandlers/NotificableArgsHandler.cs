﻿using System.Diagnostics.Contracts;
using System.Linq;
using TalentManagement.Domain.Events;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Cache;

namespace TalentManagement.Web.Infrastructure.DomainEventHandlers
{
    internal class NotificableArgsHandler<TNotificationArgs> : IHandle<TNotificationArgs>
         where TNotificationArgs : NotificableEventArgs
    {
        private readonly IUserService _userService;
        private readonly IGlobalCache _globalCache;
        private readonly IUserRepository _userRepository;

        public NotificableArgsHandler(IUserService userService, IGlobalCache globalCache, IUserRepository userRepository)
        {
            _userService = userService;
            _globalCache = globalCache;
            _userRepository = userRepository;
        }

        public virtual void Handle(TNotificationArgs args)
        {
            User[] receivers = _userRepository.GetUsers(args.ReceiversIds).ToArray();
            User sender = _userRepository.GetById(args.SenderId);
            Contract.Assert(sender != null);
            receivers.Map(receiver =>
                              {
                                  Contract.Assert(receiver != null);
                                  //If user commented it`s own feedback, there is no sense to create notification
                                  if (args.SenderId != receiver.Id)
                                  {
                                      _userService.InsertNotification(sender, receiver, args.NotificationTextType, args);
                                      _globalCache.SetNotificationsCountFor(receiver.Id, receiver.UserNotifications.Count());
                                  }
                              });
        }
    }
}