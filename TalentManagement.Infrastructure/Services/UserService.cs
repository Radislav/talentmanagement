﻿using System.Diagnostics.Contracts;
using Autofac.Features.Indexed;
using TalentManagement.Domain;
using TalentManagement.Domain.Texts;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Services
{
    class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IHashComputer _hashComputer;
        private readonly ITextRepository _textRepository;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IIndex<TextType, ITextFormatter> _textFormatters;
        public UserService(IUserRepository userRepository,
            IHashComputer hashComputer,
            ITextRepository textRepository,
            IPrincipalProvider principalProvider,
            IIndex<TextType, ITextFormatter> textFormatters)
        {
            _userRepository = userRepository;
            _hashComputer = hashComputer;
            _textRepository = textRepository;
            _principalProvider = principalProvider;
            _textFormatters = textFormatters;
        }
        
        public void ChangeUserPassword(string userId, string password)
        {
            User user = _userRepository.GetById(userId);

            if (user == null)
            {
                throw new EntityNotFoundException(typeof(User),userId);
            }
            user.HashedPassword = _hashComputer.ComputeHash(password);
            _userRepository.Update(user);
        }
        
        public void InsertNotification(User sender, User receiver, TextType type,object formatterArgument = null)
        {
            string text = _textRepository.GetText(type).Content;
            Contract.Assert(text != null);
            ITextFormatter textFormatter;
            
            if (formatterArgument != null && _textFormatters.TryGetValue(type, out textFormatter))
            {
                text = textFormatter.Format(text, formatterArgument);
            }
            UserNotification notification = new UserNotification(sender.Id, text,type);
            _principalProvider.FillOWned(notification);
            receiver.AddUserNotification(notification);

            _userRepository.Update(receiver);
        }
    }
}
