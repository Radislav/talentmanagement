﻿using MongoDB.Bson;
using ModelResources = TalentManagement.Resources.Models.Feeds;

namespace TalentManagement.Web.Models.Feeds
{
    public class ObjectiveFeedModel : FeedModel
    {
        [LocalizedDisplayName("KeyResult",typeof(ModelResources.ObjectiveFeedModel))]
        public string KeyResult { get; set; }

        [LocalizedDisplayName("Progress", typeof(ModelResources.ObjectiveFeedModel))]
        public string Progress { get; set; }

        [LocalizedDisplayName("Status", typeof(ModelResources.ObjectiveFeedModel))]
        public string Status { get; set; }

        public string ObjectiveName { get; set; }

        public ObjectId ObjectiveId { get; set; }

        public string[] Texts { get; set; }
    }
}