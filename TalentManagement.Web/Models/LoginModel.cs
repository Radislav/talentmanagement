﻿using System;
using System.Web.Mvc;
using FluentValidation.Attributes;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Web.Models.Validation;

using ModelResources = TalentManagement.Resources.Models.LoginModel;

namespace TalentManagement.Web.Models
{
    [Validator(typeof(LoginModelValidator))]
    public class LoginModel
    {
        [LocalizedDisplayName("Email", typeof(ModelResources))]
        [Watermark("EmailWatermark", typeof(ModelResources))]
        public string Email { get; set; }

        [LocalizedDisplayName("Password", typeof(ModelResources))]
        [Watermark("PasswordWatermark", typeof(ModelResources))]
        public string Password { get; set; }

        private string _error;
        public LoginResult LoginResult { get; set; }
        public string Error
        {
            get
            {
                if (_error == null)
                {
                    IResourceProvider resourceProvider = DependencyResolver.Current.GetService<IResourceProvider>();

                    switch (LoginResult)
                    {
                        case LoginResult.InvalidUserOrPassword:
                            return resourceProvider.ErrorInvalidUserOrPassword;
                        case LoginResult.LoginsAttemptReached:
                            return resourceProvider.ErrorLoginsAttemptReached;
                        default:
                            return String.Empty;
                    }
                }

                return _error;
            }
            set { _error = value; }
        }
        

    }
}