﻿using System;
using System.IO;
using System.Linq;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Packaging;

namespace TalentManagement.Infrastructure.Parsers
{
    internal abstract class ExcelRowParser<TRowParsedEventArgs> : IRowsParser<IXLRow, TRowParsedEventArgs>
        where TRowParsedEventArgs :RowParsedEventArgs
    {
        private readonly string _sheetName;
        private readonly int _headerRowIndex;
        private readonly string _invalidFileMessage;

        protected ExcelRowParser(string sheetName,int headerRowIndex,string invalidFileMessage)
        {
            _headerRowIndex = headerRowIndex;
            _invalidFileMessage = invalidFileMessage;
            _sheetName = sheetName;
        }


        public object Parse(Stream contents)
        {
            try
            {
                var workbook = new XLWorkbook(contents);
                var worksheet = workbook.Worksheets.FirstOrDefault(x => x.Name == _sheetName);
                if (worksheet == null)
                {
                    throw new InvalidFileException(string.Format("There is not sheet with name {0}.", _sheetName));
                }

                foreach (var row in worksheet.Rows())
                {
                    if (row.RowNumber() != _headerRowIndex)
                    {
                        var args = ParseRow(row);
                        if (args != null && RowParsed != null)
                        {
                            RowParsed(this, args);
                        }
                    }
                }

                return workbook;
            }
            catch (OpenXmlPackageException ex)
            {
                throw new InvalidFileException(_invalidFileMessage, ex);
            }
            catch (FileFormatException ex)
            {
                throw new InvalidFileException(_invalidFileMessage, ex);
            }
        }

        public virtual TRowParsedEventArgs ParseRow(IXLRow row)
        {
            return default(TRowParsedEventArgs);
        }

        public event EventHandler<TRowParsedEventArgs> RowParsed;
    }
}
