﻿using System.Linq;

namespace TalentManagement.Web.Models.Feeds
{
    public class FeedPageModel
    {
        private FeedModel[] _feeds;

        public FeedModel[] Feeds
        {
            get { return _feeds; }
            set
            {
                _feeds = value;
                _feeds.Do(
                            feeds => feeds.Where(f => f != feeds.Last())
                                          .Map(f => f.HasBottomLine = true)
                        );

            }
        }
    }
}