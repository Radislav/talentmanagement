﻿using System;
using TalentManagement.Domain.Users;

namespace TalentManagement.Infrastructure.Security
{
    internal class AuthorizationService : IAuthorizationService
    {
        private readonly IUserRepository _userRepository;

        public AuthorizationService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public TenantPrincipal LoadPrincipal(string userName)
        {
            User user = _userRepository.GetById(userName);

            if (user == null)
            {
                return null;
            }

            TenantIdentity identity = new TenantIdentity(userName)
                                          {
                                              FirstName = user.FirstName,
                                              SecondName = user.SecondName,
                                              LastSignIn = user.LastSignIn,
                                              Team = user.Team
                                          };
            return new TenantPrincipal(identity, _userRepository.GetUserRoles(userName));
        }

        public TenantPrincipal CreatePrincipal(string userName,
                                               bool isAuthenticated,
                                               DateTime lastSignIn,
                                               string firstName,
                                               string lastName,
                                               string team,
                                               string[] roles)
        {
            return new TenantPrincipal(new TenantIdentity(userName)
                                           {
                                               FirstName = firstName,
                                               IsAuthenticated = isAuthenticated,
                                               LastSignIn = lastSignIn,
                                               SecondName = lastName,
                                               Team = team
                                           }, roles);
        }
    }
}
