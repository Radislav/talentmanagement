﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using TalentManagement.Web.Models.Binder;

namespace TalentManagement.Web.Models.ReviewCycleCreation.Binder
{
    public class QuestionSetsModelBinder : ICustomModelBinder<IEnumerable<QuestionSetModel>>
    {
        private readonly IEnumerableBinder<QuestionModel> _questionsBinder;

        public QuestionSetsModelBinder(IEnumerableBinder<QuestionModel> questionsBinder)
        {
            _questionsBinder = questionsBinder;
        }

        public object BindModel(ControllerContext controllerContext,ModelBindingContext bindingContext)
        {
            IEnumerable<QuestionModel> questions = BindQuestions(controllerContext, bindingContext);

            List<QuestionSetModel> result = QuestionSetModel.DefaultSets;

            result.Map(set =>
                           {
                               set.Questions = questions.Where(q => q.SetType == set.SetType).ToList();
                           });

            return result;
        }

        private IEnumerable<QuestionModel> BindQuestions(ControllerContext controllerContext,
                                                         ModelBindingContext bindingContext)
        {
            return new List<QuestionModel>(
                (IEnumerable<QuestionModel>)
                _questionsBinder.SetIdPropertyName("Id")
                                .SetPrefix("question")
                                .BindModel(controllerContext, bindingContext));
        }
        
        public ICustomModelBinder<IEnumerable<QuestionSetModel>> SetPrefix(string prefix)
        {
            return this;
        }

        public ICustomModelBinder<IEnumerable<QuestionSetModel>> SetIndex(int index)
        {
            return this;
        }
    }
}