<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <div>
      <div style="padding: 5 5 5 5; font-size:15.0pt; backround-color: #CCCCCC">
        <p>The best company</p>
      </div>
      <div>
        <div style="font-size:12.0pt;">
          <p>You login information.</p>
          <p>  Email: <xsl:value-of select="ForgotPasswordArgument/Email" /> </p>
          <p>  Password: <xsl:value-of select="ForgotPasswordArgument/Password" /> </p>
        </div>
        <div>
          Go to login page
          <p>
            <xsl:value-of select="ForgotPasswordArgument/LoginUrl"  disable-output-escaping="yes"/>
          </p>
          <p>Thank you for the using the best company</p>
        </div>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>