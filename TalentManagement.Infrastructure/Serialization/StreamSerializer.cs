﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace TalentManagement.Infrastructure.Serialization
{
    internal class StreamSerializer<TEntity> : IStreamSerializer<TEntity>
    {
        public byte[] Serialize(TEntity entity)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            binaryFormatter.Serialize(stream, entity);
            return stream.GetBuffer();
        }

        public TEntity Deserialize(byte[] serializedEntity)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            return  (TEntity)binaryFormatter.Deserialize(new MemoryStream(serializedEntity));
        }
    }
}
