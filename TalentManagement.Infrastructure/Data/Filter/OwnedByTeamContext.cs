﻿using TalentManagement.Domain;

namespace TalentManagement.Infrastructure.Data.Filter
{
    public class OwnedByTeamContext : IFilterContext
    {
         public OwnedByTeamContext()
        {
        }

         public OwnedByTeamContext(string team)
        {
            Team = team;
        }

        public string Team { get; set; }
    }
}
