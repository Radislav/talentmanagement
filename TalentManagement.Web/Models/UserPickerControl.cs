﻿using System.Collections.Generic;
using TalentManagement.Web.Models.Validation;
using Resource = TalentManagement.Resources.Models.UserPickerControl;

namespace TalentManagement.Web.Models
{
    /// <summary>
    /// Model for the user picker control
    /// </summary>
    public class UserPickerControl : IPropertyToValidate
    {
        private readonly UserPickedCollection _users = new UserPickedCollection();
        public UserPickerControl()
        {
            UsersPicked = new List<UserPicked>();
            ShowAddButton = true;
            ShowPickedUsers = true;
        }

        /// <summary>
        /// Title of the control
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// When true then "+" button is rendered, it allows to add severals users by clicking on it.
        /// </summary>
        public bool ShowAddButton { get; set; }

        private string _prefix;
        /// <summary>
        /// Custom string prefix which would be concateneted with the Id and Name html attributes of the html elements.
        /// </summary>
        public string Prefix
        {
            get { return _prefix; }
            set { _prefix = value;
                _users.Prefix = value;
                if (_usersPicked != null)
                {
                    _usersPicked.Map(user => user.Prefix = Prefix);
                }
            }
        }

        private List<UserPicked> _usersPicked;
        /// <summary>
        /// The list of users which where already picked. 
        /// </summary>
        public List<UserPicked> UsersPicked
        {
            get { return _usersPicked; }
            set {
                _usersPicked = value;
                _usersPicked.Map(user => user.Prefix = Prefix);
            }
        }
        
        /// <summary>
        /// List of all users available for picking.
        /// </summary>
        public List<UserPicked> Users
        {
            get { return _users.Users; }
            set { _users.Users = value; }
        }

        /// <summary>
        /// List of all users available for picking in JSON format.
        /// </summary>
        public string UsersJson
        {
            get { return _users.UsersJson; }
            set { _users.UsersJson = value; }
        }

        /// <summary>
        /// User namess of all users available for picking.
        /// </summary>
        public string UserNames
        {
            get { return _users.UserNames; }
            set { _users.UserNames = value; }
        }

        /// <summary>
        /// Currently typed user name in format "[last-name], [first-name]"
        /// </summary>
        [LocalizedDisplayName("CurrentNames",typeof(Resource))]
        public string CurrentNames { get; set; }

        /// <summary>
        /// When true, then already picked users would be displayed in the bottom of the control
        /// </summary>
        public bool ShowPickedUsers { get; set; }

        /// <summary>
        /// Currently typed user
        /// </summary>
        public UserPicked CurrentUser
        {
            get { return string.IsNullOrEmpty(CurrentNames) ? null : _users.GetUserByNames(CurrentNames); }
        }

        /// <summary>
        /// Adds <see cref="CurrentUser"/> to the <see cref="UsersPicked"/> collection.
        /// </summary>
        /// <returns></returns>
        public bool AddCurrentUserToUsersPicked()
        {
            return AddUserToUsersPicked(CurrentUser);
        }

        /// <summary>
        /// Adds <paramref name="user"/> to the <see cref="UsersPicked"/> collection.
        /// </summary>
        /// <param name="user">User to add.</param>
        /// <returns>True when user successfully added, false otherwise.</returns>
        public bool AddUserToUsersPicked(UserPicked user)
        {
            if (user != null && false == UsersPicked.Contains(user))
            {
                UsersPicked.Add(CurrentUser);
                CurrentNames = null;
                return true;
            }
            return false;
        }

        /// <summary>
        /// The hint for validator to execute validation for the provided property name, instead of <see cref="CurrentNames"/> property. 
        /// </summary>
        public string PropertyToValidateName { get; set; }
    }
}