﻿using System.Web.Mvc;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Infrastructure.Validation;
using TalentManagement.Web.Models.MyPassword;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Controllers
{
    public class MyPasswordController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IUserService _userService;

        public MyPasswordController(IUserRepository userRepository,
            IPrincipalProvider principalProvider,
            IUserService userService)
        {
            _userRepository = userRepository;
            _principalProvider = principalProvider;
            _userService = userService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(new IndexModel());
        }

        [HttpPost]
        public ActionResult Index(IndexModel indexModel)
        {
            //Due the security reasons do not want to pass password from model.
            indexModel.OldPasswordHashed = _userRepository.GetById(_principalProvider.GetUserName())
                                                       .HashedPassword;

            ValidationResult validationResult = ModelState.ValidateAndKeepState(indexModel);

            if (validationResult.IsValid)
            {
                _userService.ChangeUserPassword(_principalProvider.GetUserName(),indexModel.NewPassword);
            }

            indexModel.ValidationOk = validationResult.IsValid;
            return View(indexModel);
        }
    }
}
