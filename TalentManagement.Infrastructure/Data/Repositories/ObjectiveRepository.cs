﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TalentManagement.Domain;
using TalentManagement.Domain.Events;
using TalentManagement.Domain.Objectives;
using MongoDB.Bson;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data.Repositories
{
    class ObjectiveRepository : DecoratedRepository<Objective,ObjectId>, IObjectiveRepository
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly Lazy<IDomainEvents> _domainEvents;

        public ObjectiveRepository(IRepositoryWithTypedId<Objective, ObjectId> repositoryDecorated, 
            IFilter filter,
            IPrincipalProvider principalProvider,
            Lazy<IDomainEvents> domainEvents)
            : base(repositoryDecorated, filter)
        {
            _principalProvider = principalProvider;
            _domainEvents = domainEvents;
        }
        
        public IEnumerable<IGrouping<TProperty, Objective>> GetObjectivesGroupedBy<TProperty>
           (Expression<Func<Objective, TProperty>> groupingExpression)
        {
            return AsQueryable().GroupBy(groupingExpression.Compile())
                                  .ToArray();
        }


        public IEnumerable<IGrouping<TProperty, Objective>> GetObjectivesGroupedBy<TProperty>(Expression<Func<Objective, TProperty>> groupingExpression, int dueYear)
        {
            return dueYear == 0
                       ? GetObjectivesGroupedBy(groupingExpression)
                       : AsQueryable().Where(o => o.Due.Year == dueYear)
                                      .GroupBy(groupingExpression.Compile())
                                       .ToArray();
        }


        public IEnumerable<Objective> GetFiltered(IFilter filter)
        {
            return AsQueryable().ApplyFilter(filter)
                                .ToArray();
        }

        public IEnumerable<Objective> GetFocusObjectives()
        {
            return AsQueryable().Where(o => o.InFocus).ToArray();
        }

        public override void Insert(Objective objective)
        {
            _principalProvider.FillOWned(objective)
                              .FillOWnedByCompany(objective);
      
            base.Insert(objective);
            _domainEvents.Value.Raise(new ObjectiveCreatedArgs(objective, _principalProvider.GetUserName(), objective.Contributors.ToArray()));
        }

        public override void Update(Objective objectiveNew)
        {
            Objective objectiveOld = GetById(objectiveNew.Id);
            base.Update(objectiveNew);
            _domainEvents.Value.Raise(
                new ObjectiveUpdatedArgs(objectiveNew,
                                        objectiveOld,
                                        _principalProvider.GetUserName(), 
                                        objectiveNew.Contributors.ToArray()));
        }
    }
}
