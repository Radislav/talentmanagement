﻿using System.Linq;
using System.Web;
using FluentValidation.Validators;

namespace TalentManagement.Web.Models.Validation
{
    public class FileImageTypeValidator : PropertyValidator 
    {
        private static readonly string[] ImageContentTypes = new []
                                                                 {
                                                                     "image/bmp",
                                                                     "image/cis-cod",
                                                                     "image/gif",
                                                                     "image/ief",
                                                                     "image/jpeg",
                                                                     "image/jpg",
                                                                     "image/png",
                                                                     "image/pipeg",
                                                                     "image/svg+xml",
                                                                     "image/tiff",
                                                                     "image/tiff",
                                                                     "image/x-cmu-raster",
                                                                     "image/x-cmx",
                                                                     "image/x-icon",
                                                                     "image/x-portable-anymap",
                                                                     "image/x-portable-bitmap",
                                                                     "image/x-portable-graymap",
                                                                     "image/x-portable-pixmap",
                                                                     "image/x-rgb",
                                                                     "image/x-xbitmap",
                                                                     "image/x-xpixmap",
                                                                     "image/x-xwindowdump"
                                                                 };

        public FileImageTypeValidator():base("FileNotImage",typeof(Resources.Errors))
        {

        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
              HttpPostedFileBase value = context.PropertyValue as HttpPostedFileBase;
            if (value != null && value.InputStream != null)
            {
                return ImageContentTypes.Contains(value.ContentType);
            }

            return true;
        }
    }
}