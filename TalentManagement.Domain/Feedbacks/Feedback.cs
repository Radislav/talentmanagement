﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using MongoDB.Bson;

namespace TalentManagement.Domain.Feedbacks
{
    [AggregateRoot]
    public class Feedback : Entity , IOwned , IOwnedByCompany , IHasParticipants
    {
        private List<Response> _responses = new List<Response>();

        public Feedback() : this(new Response[0])
        {
        }

        public Feedback(IEnumerable<Response> responses)
        {
            _responses = responses.ListOrDefault();
        }

        public string Question { get; set; }

        public IEnumerable<Response> Responses
        {
            get { return _responses; }
            private set { _responses = value.ListOrDefault(); }
        }

        public void AddResponse(Response response)
        {
            Contract.Requires<EntityNullException>(response != null);
            
            response.Id = ObjectId.GenerateNewId();
            _responses.Add(response);
        }

        public string[] Responders { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedAt { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedAt { get; set; }

        public string CompanyName { get; set; }
        
        public Response GetResponseFor(string user)
        {
            if (Participants == null || Responses == null || false == Participants.Contains(user))
            {
                return null;
            }
            return Responses.FirstOrDefault(r => r.CreatedBy == user);
        }

        public Response GetResponse(ObjectId responseId)
        {
            return Responses.GetSafe(responses => responses.SingleOrDefault(r => r.Id == responseId));
        }

        public IEnumerable<string> Participants
        {
            get { return Responders ?? new string[0]; }
        }

        public IEnumerable<Comment> NewComments
        {
            get
            {
                Contract.Assume(Responses != null);
                return Responses.SelectMany(r => r.NewComments);
            }
        }
    }
}
