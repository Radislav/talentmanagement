﻿using MongoDB.Bson;
using TalentManagement.Domain.Feedbacks;

namespace TalentManagement.Web.Models.MyFeedbacks.Map
{
    public interface IIndexModelMapper
    {
        IndexModel Map(string activeButton,Feedback[] currentFeedbacks,ObjectId[] badgeIds);
    }
}
