﻿using System;
namespace TalentManagement.Domain
{
    public class Comment : Entity , IOwned
    {
        public string Content { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
