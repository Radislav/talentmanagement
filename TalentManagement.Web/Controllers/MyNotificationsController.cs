﻿using System.Web.Mvc;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Web.Models.MyNotifications;

namespace TalentManagement.Web.Controllers
{
    public class MyNotificationsController : Controller
    {
        private readonly IMapper<User, IndexModel> _indexModelMapper;
        private readonly IMapper<User, PopupModel> _popupModelMapper;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IUserRepository _userRepository;

        public MyNotificationsController(IMapper<User,IndexModel> indexModelMapper,
            IMapper<User, PopupModel> popupModelMapper,
            IPrincipalProvider principalProvider,
            IUserRepository userRepository)
        {
            _indexModelMapper = indexModelMapper;
            _principalProvider = principalProvider;
            _userRepository = userRepository;
            _popupModelMapper = popupModelMapper;
        }

        [HttpGet]
        public ActionResult Index()
        {
            User user = _userRepository.GetById(_principalProvider.GetUserName());
            return View(_indexModelMapper.Map(user));
        }
        
        public ActionResult Popup()
        {
            User user = _userRepository.GetById(_principalProvider.GetUserName());
            return PartialView("_Popup",_popupModelMapper.Map(user));
        }



    }
}
