﻿using System.Web.Helpers;
using TalentManagement.Domain;
using TalentManagement.Web.Infrastructure.UI;

namespace TalentManagement.Web.Infrastructure
{
    public static class PictureExtensions
    {
        public static byte[] GetResizedBytes(this IPicture picture,int size)
        {
            if (picture == null || picture.Image == null)
            {
                return new byte[0];
            }

            return size == (int)ImageSize.Actual ? picture.Image :
                  new WebImage(picture.Image)
                            .Resize(size, size)
                            .GetBytes();
        }
    }
}