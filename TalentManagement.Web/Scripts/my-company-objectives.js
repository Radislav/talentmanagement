﻿function MyCompanyObjectives(onFiltered) {

    this.onFiltered = onFiltered;

    this.initialize = function() {

        $('#FilterByYear').on('change', function() {
            $('#activity_stats .btn-group .active').trigger('click');
        });

        $('#activity_stats .btn-group button').off('click').on('click', function(e) {

            e.preventDefault();
            var data = {
                filterByYear: $('#FilterByYear').val(),
                groupBy: $(this).attr('name'),
                showAddToFocus: $('[name="ShowAddToFocus"]').val()
            };

            var form = $(this).parents('form');

            $.ajax({
                url: form.attr('action'),
                data: JSON.stringify(data),
                type: 'POST',
                success: function(response) {
                    form.html(response);
                    var objectives = new MyCompanyObjectives(onFiltered);
                    objectives.initialize();
                    if (onFiltered) {
                        onFiltered();
                    }
                }
            });
        });
    };
}



