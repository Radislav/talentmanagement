﻿using System;
using TalentManagement.Infrastructure.Validation;

namespace TalentManagement.Infrastructure.Data.Configuration
{
    public interface IEntityReadConfiguration : IValidator<object>
    {
        string CollectionName { get; }
        bool IsOfType(Type type);
        DataSourceType DataSourceType { get; }
        void ValidateConfiguration();
        bool SafeMode { get; }
    }
}
