﻿namespace TalentManagement.Infrastructure
{
    public enum DataSourceType
    {
        None,
        Tenant,
        Shared
    }
}
