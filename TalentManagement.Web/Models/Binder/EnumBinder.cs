﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace TalentManagement.Web.Models.Binder
{
    public class EnumBinder<TEnum> : CustomModelBinder<TEnum>
    {
        private const string Id = "id";
        private TEnum DefaultValue { get; set; }

        public EnumBinder(TEnum defaultValue)
        {
            DefaultValue = defaultValue;
        }

        #region IModelBinder Members
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var valueProvider = bindingContext.ValueProvider;

            ValueProviderResult value = valueProvider.GetValue(bindingContext.ModelName) ?? valueProvider.GetValue(Id);

            return value == null ? DefaultValue : GetEnumValue(DefaultValue, value.AttemptedValue);
        }

        #endregion

        public static T GetEnumValue<T>(T defaultValue, string value)
        {
            T enumType = defaultValue;

            if ((!String.IsNullOrEmpty(value)) && (Contains(typeof(T), value)))
                enumType = (T)Enum.Parse(typeof(T), value, true);

            return enumType;
        }
        public static bool Contains(Type enumType, string value)
        {
            return Enum.GetNames(enumType).Contains(value, StringComparer.OrdinalIgnoreCase);
        }
    }
}
