﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using TalentManagement.Infrastructure.Data;

namespace TalentManagement.Infrastructure.Contracts
{
    [ContractClassFor(typeof(IUnitOfWork))]
    internal abstract class UnitOfWorkContract : IUnitOfWork
    {
        public void Insert<TEntity>(TEntity entity) where TEntity : class
        {
            Contract.Requires<ArgumentNullException>(entity != null);
        }

        public void InsertBatch<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            Contract.Requires(entities != null);
        }

        public void DeleteById<TEntity, TId>(TId id) where TEntity : class
        {
        }

        public void Update<TEntity>(TEntity entity) where TEntity : class
        {
            Contract.Requires<ArgumentNullException>(entity != null);
        }

        public TEntity GetById<TEntity, TId>(TId id) where TEntity : class
        {
            return null;
        }

        public IQueryable<TEntity> AsQueryable<TEntity>() where TEntity : class
        {
            Contract.Ensures(Contract.Result<IQueryable<TEntity>>() != null);
            return null;
        }


        public IQueryable<TEntity> Find<TEntity>(object command) where TEntity : class
        {
            Contract.Requires(command != null);
            Contract.Ensures(Contract.Result<IQueryable<TEntity>>() != null);
            return null;
        }
    }
}
