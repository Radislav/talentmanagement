﻿using System;
using System.Linq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using Moq;
using TalentManagement.Domain;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure;
using TalentManagement.Infrastructure.Data;
using TalentManagement.Infrastructure.Services;

namespace TalentManagement.Test.Infrastructure.Services
{
    [TestClass]
    public class RelationshipServiceTest
    {
        private string _managerWithTeamsId;
        private User _managerWithTeams;

        private string _managerWithoutTeamsId;
        private User _managerWithoutTeams;

        private string _subordinateId1;
        private User _subordinate1;

        private Team _team;
        private ObjectId _teamId;

        [TestInitialize]
        public void Initialize()
        {
            _managerWithTeamsId = "manager1@gmail.com";
            _managerWithTeams = new User(_managerWithTeamsId);
            _managerWithoutTeamsId = "manager2@gmail.com";
            _managerWithoutTeams = new User(_managerWithoutTeamsId);

            _teamId = ObjectId.GenerateNewId();
            _team = new Team(_managerWithTeams) {Id = _teamId};
            _subordinateId1 = "subordinate1@gmail.com";
            _subordinate1 = new User(_subordinateId1)
                {
                    ManagerEmail = _managerWithTeamsId
                };
        }

        [TestMethod]
        public void MarkAsManager_Should_create_and_insert_new_team_if_manager_has_no_teams()
        {
            IContainer container = GetContainer();
            var teamRepository = container.Resolve<Mock<ITeamRepository>>();
            Team actual = null;
            teamRepository.Setup(r => r.Upsert(It.IsAny<Team>())).Callback<Team>(t => actual = t);
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterMock(teamRepository);
            builder.Update(container);

            var target = container.Resolve<IRelationshipService>();
            target.MarkAsManager(_managerWithoutTeams);

            Assert.AreEqual(actual.Manager,_managerWithoutTeams.Id);
            Assert.AreNotEqual(_team,actual);
        }

        [TestMethod]
        public void MarkAsManager_Should_update_existing_teams_if_manager_has_teams()
        {
            IContainer container = GetContainer();
            var teamRepository = container.Resolve<Mock<ITeamRepository>>();
            teamRepository.Setup(r => r.Update(It.IsAny<Team>()));
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterMock(teamRepository);
            builder.Update(container);

            var target = container.Resolve<IRelationshipService>();
            target.MarkAsManager(_managerWithTeams);
            
            teamRepository.Verify(t => t.Upsert(_team),Times.Once());
        }

        [TestMethod]
        public void MarkAsManager_Should_add_subordinates_and_manager_to_the_managers_teams()
        {
            IContainer container = GetContainer();
            
            var target = container.Resolve<IRelationshipService>();
            target.MarkAsManager(_managerWithTeams);

            CollectionAssert.Contains(_team.Members.ToArray(),_subordinateId1);
            CollectionAssert.Contains(_team.Members.ToArray(), _managerWithTeamsId);
        }

        [TestMethod]
        public void MarkAsManager_Should_set_IsManager_to_true()
        {
            IContainer container = GetContainer();
            var uow = container.Resolve<Mock<IUnitOfWork>>();
            uow.Setup(u => u.Update(_managerWithTeams));
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterMock(uow);
            builder.Update(container);

            var target = container.Resolve<IRelationshipService>();
            _managerWithoutTeams.IsManager = false;
            target.MarkAsManager(_managerWithTeams);

            uow.Verify(t => t.Update(_managerWithTeams), Times.Once());
            Assert.IsTrue(_managerWithTeams.IsManager);
        }

        [TestMethod]
        public void NotManager_should_remove_managers_teams()
        {
            IContainer container = GetContainer();
            var teamRepository = container.Resolve<Mock<ITeamRepository>>();
            teamRepository.Setup(u => u.DeleteById(_teamId));
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterMock(teamRepository);
            builder.Update(container);

            var target = container.Resolve<IRelationshipService>();
            target.NotManager(_managerWithTeams);

            teamRepository.Verify(t => t.DeleteById(_teamId),Times.Once());
        }

        [TestMethod]
        public void NotManager_should_update_subordinates_to_have_empty_ManagerEmail()
        {
            IContainer container = GetContainer();
            var uow = container.Resolve<Mock<IUnitOfWork>>();
            uow.Setup(u => u.Update(_subordinate1));
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterMock(uow);
            builder.Update(container);

            var target = container.Resolve<IRelationshipService>();
            _subordinate1.ManagerEmail = _managerWithTeamsId;
            target.NotManager(_managerWithTeams);

            uow.Verify(t => t.Update(_subordinate1), Times.Once());
            Assert.AreEqual(String.Empty,_subordinate1.ManagerEmail);
        }

        [TestMethod]
        public void NotManager_Should_set_IsManager_to_false()
        {
            IContainer container = GetContainer();
            var uow = container.Resolve<Mock<IUnitOfWork>>();
            uow.Setup(u => u.Update(_managerWithTeams));
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterMock(uow);
            builder.Update(container);

            var target = container.Resolve<IRelationshipService>();
            _managerWithoutTeams.IsManager = true;
            target.NotManager(_managerWithTeams);

            uow.Verify(t => t.Update(_managerWithTeams), Times.Once());
            Assert.IsFalse(_managerWithTeams.IsManager);
        }

        [TestMethod]
        public void AddToTeam_Should_set_ManagerEmail_value()
        {
            IContainer container = GetContainer();
            var uow = container.Resolve<Mock<IUnitOfWork>>();
            uow.Setup(u => u.Update(_subordinate1));
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterMock(uow);
            builder.Update(container);

            var target = container.Resolve<IRelationshipService>();
            _subordinate1.ManagerEmail = "";
            target.AddToTeam(_subordinate1,_managerWithTeams);

            uow.Verify(t => t.Update(_subordinate1), Times.Once());
            Assert.AreEqual(_managerWithTeamsId,_subordinate1.ManagerEmail);
        }

        [TestMethod]
        public void AddToTeam_Should_Add_user_to_managers_team_When_IsManager_false()
        {
            IContainer container = GetContainer();
            var target = container.Resolve<IRelationshipService>();
            _managerWithTeams.IsManager = false;
            target.AddToTeam(_subordinate1, _managerWithTeams);

            CollectionAssert.Contains(_team.Members.ToArray(),_subordinateId1);
        }

        [TestMethod]
        public void AddToTeam_Should_Add_user_to_managers_team_When_IsManager_true()
        {
            IContainer container = GetContainer();
            var target = container.Resolve<IRelationshipService>();
            _managerWithTeams.IsManager = true;
            target.AddToTeam(_subordinate1, _managerWithTeams);

            CollectionAssert.Contains(_team.Members.ToArray(), _subordinateId1);
        }

        [TestMethod]
        public void AddToTeam_Should_update_users_Team_property()
        {
            IContainer container = GetContainer();
            var target = container.Resolve<IRelationshipService>();
            target.AddToTeam(_subordinate1, _managerWithTeams);

            Assert.IsFalse(string.IsNullOrEmpty(_subordinate1.Team));
            Assert.AreEqual(_subordinate1.Team,_team.Name);
            Assert.AreEqual(_managerWithTeams.Team,_team.Name);
        }


        [TestMethod]
        public void RemoveFromTeams_Should_set_ManagerEmail_to_null()
        {
            IContainer container = GetContainer();
            var uow = container.Resolve<Mock<IUnitOfWork>>();
            uow.Setup(u => u.Update(_subordinate1));
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterMock(uow);
            builder.Update(container);

            var target = container.Resolve<IRelationshipService>();
            _subordinate1.ManagerEmail = _managerWithTeamsId;
            target.RemoveFromTeams(_subordinate1, _managerWithTeams);

            uow.Verify(t => t.Update(_subordinate1), Times.Once());
            Assert.IsNull(_subordinate1.ManagerEmail);
        }

        [TestMethod]
        public void RemoveFromTeams_Should_remove_user_membership()
        {
            IContainer container = GetContainer();
            var teamRepository = container.Resolve<Mock<ITeamRepository>>();
            teamRepository.Setup(u => u.DeleteById(_teamId));
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterMock(teamRepository);
            builder.Update(container);

            var target = container.Resolve<IRelationshipService>();
            _team.AddMemeber(_subordinate1);
            _subordinate1.ManagerEmail = _managerWithTeamsId;
            target.RemoveFromTeams(_subordinate1,_managerWithTeams);

            teamRepository.Verify(t => t.Update(_team), Times.Once());
            CollectionAssert.DoesNotContain(_team.Members.ToArray(),_subordinateId1);
        }

        [TestMethod]
        public void RemoveFromTeams_should_unset_users_Team_property()
        {
            IContainer container = GetContainer();
            var target = container.Resolve<IRelationshipService>();
            _managerWithTeams.Team = _subordinate1.Team = "the team";
            target.RemoveFromTeams(_subordinate1, _managerWithTeams);

            Assert.IsTrue(String.IsNullOrEmpty(_subordinate1.Team));
            Assert.IsFalse(String.IsNullOrEmpty(_managerWithTeams.Team));
        }

        private IContainer GetContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterModule(new InfrastructureRegistrationModule());

            Mock<ITeamRepository> teamRepository = new Mock<ITeamRepository>();
            teamRepository.Setup(t => t.GetTeams(_managerWithTeams)).Returns(new Team[] {_team});
            teamRepository.Setup(t => t.GetTeams(_managerWithoutTeams)).Returns(new Team[0]);
            builder.RegisterMock(teamRepository);

            Mock<IUserRepository> userRepository = new Mock<IUserRepository>();
            userRepository.Setup(r => r.GetSubordinatesOf(_managerWithTeamsId, It.IsAny<IFilter>()))
                          .Returns(new [] {_subordinate1});
            builder.RegisterMock(userRepository);

            Mock<IFilter<IOwnedByCompany>> companyFilter = new Mock<IFilter<IOwnedByCompany>>();
            builder.RegisterMock(companyFilter);

            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();
            builder.RegisterMock(unitOfWork);

            return builder.Build();
        }

    }
}
