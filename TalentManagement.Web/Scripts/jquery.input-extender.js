﻿(function (jQuery) {

    jQuery.fn.inputExtender = function (options) {

        var opts = jQuery.extend({}, jQuery.fn.inputExtender.defaults, options);

        return this.each(function () {
            var $this = jQuery(this);
            
            if (opts.date) {
                $this.datepicker();
            }
            if (opts.decimal) {
                $this.jStepper({ decimalSeparator: '.' });
            }
        });
    };
    
    jQuery.fn.inputExtender.defaults = {
        date: false,
        decimal: false
    };

    
})(jQuery);


            