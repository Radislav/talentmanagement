﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using TalentManagement.Infrastructure.Contracts;

namespace TalentManagement.Infrastructure.Data
{
    [ContractClass(typeof(UnitOfWorkContract))]
    public interface IUnitOfWork
    {
        void Insert<TEntity>(TEntity entity) where TEntity : class;
        void InsertBatch<TEntity>(IEnumerable<TEntity> entities) where TEntity : class;
        void DeleteById<TEntity, TId>(TId id) where TEntity : class;
        void Update<TEntity>(TEntity entity) where TEntity : class;
        TEntity GetById<TEntity, TId>(TId id) where TEntity : class;
        IQueryable<TEntity> AsQueryable<TEntity> () where TEntity : class;
        IQueryable<TEntity> Find<TEntity>(object command) where TEntity : class;
    }
}
