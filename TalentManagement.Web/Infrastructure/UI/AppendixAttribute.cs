﻿using System;

namespace TalentManagement.Web.Infrastructure.UI
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Event)]
    public class AppendixAttribute : Attribute
    {
        private readonly string _appendix;

        public AppendixAttribute(string appendix)
        {
            _appendix = appendix;
        }

        public string Appendix { get { return _appendix; }
        }
    }
}