﻿using Resource = TalentManagement.Resources.DomainResources;

namespace TalentManagement.Domain.Performance
{
    public enum AnswerType
    {
        [LocalizedDescription("None", typeof(Resource))]
        None,
        [LocalizedDescription("AnswerType_Text",typeof(Resource))]
        Text,
        [LocalizedDescription("AnswerType_MultipleChoice", typeof(Resource))]
        MultipleChoice
    }
}
