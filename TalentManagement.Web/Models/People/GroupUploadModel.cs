﻿using FluentValidation.Attributes;
using System.Web;
using TalentManagement.Web.Models.People.Validation;

namespace TalentManagement.Web.Models.People
{
    [Validator(typeof(GroupUploadModelValidator))]
    public class GroupUploadModel
    {
        public HttpPostedFileBase Template { get; set; }
        public bool IsEmbedded { get; set; }
    }
}