﻿using System.Web.Mvc;
using TalentManagement.Infrastructure.Security;
using Resource = TalentManagement.Resources.Models.People.AddUserModel;

namespace TalentManagement.Web.Models.People
{
    public class AddUserModel
    {
        private readonly string _managerEmailAppendix;

        public AddUserModel()
        {
            var principalProvider = DependencyResolver.Current.GetService<IPrincipalProvider>();
            _managerEmailAppendix = principalProvider.GetUserName().EmailDomainLong();
        }

        [LocalizedDisplayName("Email", typeof(Resource))]
        public string Email { get; set; }

        [LocalizedDisplayName("Password", typeof(Resource))]
        public string Password { get; set; }

        [LocalizedDisplayName("FirstName", typeof(Resource))]
        public string FirstName { get; set; }

        [LocalizedDisplayName("SecondName", typeof(Resource))]
        public string SecondName { get; set; }

        [LocalizedDisplayName("Position", typeof(Resource))]
        public string Position { get; set; }

        [LocalizedDisplayName("Team", typeof(Resource))]
        public string Team { get; set; }

        [LocalizedDisplayName("ManagerEmailLocal", typeof(Resource))]
        public string ManagerEmailLocal { get; set; }

        public string ManagerEmailAppendix { get { return "@" + _managerEmailAppendix; } }

        public string ManagerEmail { get { return ManagerEmailLocal + ManagerEmailAppendix; } }

        public bool IsEmbedded { get; set; }
    }
}