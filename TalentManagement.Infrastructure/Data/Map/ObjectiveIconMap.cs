﻿using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Data.Configuration;

namespace TalentManagement.Infrastructure.Data.Map
{
    internal class ObjectiveIconMap : EntityConfiguration<ObjectiveIcon>
    {
        public ObjectiveIconMap()
        {
            AsCollection("ObjectiveIcon")
                .WithDataSource(DataSourceType.Tenant);

            Property(p => p.Image)
                .Required();

            Property(p => p.ImageType)
                .Required();
        }
    }
}
