﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using TalentManagement.Domain.Contracts;

namespace TalentManagement.Domain.Feeds
{
    [ContractClass(typeof(FeedRepositoryContract))]
    public interface IFeedRepository : IRepository<Feed>
    {
        IEnumerable<Feed> GetFeeds(IFilter filter);
        IEnumerable<Kudos> GetKudosByReceiver(string receiver);
    }
}
