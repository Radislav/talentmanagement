﻿using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Data.Configuration;

namespace TalentManagement.Infrastructure.Data.Map
{
    class ActionItemMap : EntityConfiguration<ActionItem>
    {
        public ActionItemMap()
        {
            AsCollection("Actions")
              .WithDataSource(DataSourceType.Tenant);

            Property(a => a.Name)
                .Required();

            Property(a => a.Owner)
                .Required();

        }
    }
}
