﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace TalentManagement.Web.Infrastructure.UI
{
    public static class AppendixExtensions
    {
        public static MvcHtmlString Appendix<TModel>(this HtmlHelper<TModel> html, string value)
        {
            string result = String.Empty;

            if (false == String.IsNullOrEmpty(value))
            {
                TagBuilder tagBuilder = new TagBuilder("span");
                tagBuilder.AddCssClass("appendix");
                tagBuilder.SetInnerText(value);
                result = tagBuilder.ToString();
            }

            return new MvcHtmlString(result);
        }

        public static MvcHtmlString AppendixBootstrap<TModel>(this HtmlHelper<TModel> html, string value)
        {
            string result = String.Empty;

            if (false == String.IsNullOrEmpty(value))
            {
                TagBuilder tagBuilder = new TagBuilder("span");
                tagBuilder.AddCssClass("add-on");
                tagBuilder.SetInnerText(value);
                result = tagBuilder.ToString();
            }

            return new MvcHtmlString(result);
        }

        public static MvcHtmlString Appendix<TModel, TValue>(this HtmlHelper<TModel> html,
                                                             Expression<Func<TModel, TValue>> expression)
        {
            ModelMetadata metaData = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            string value = html.ViewData.Model
                .GetType()
                .GetProperty(metaData.PropertyName)
                .GetCustomAttributes(typeof(AppendixAttribute), false)
                .Cast<AppendixAttribute>()
                .FirstOrDefault()
                .GetSafe(attr => attr.Appendix);

            return Appendix(html, value);
        }

    }
}