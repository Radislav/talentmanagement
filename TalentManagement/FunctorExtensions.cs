﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TalentManagement
{
    public static class FunctorExtensions
    {
        public static void Map<T>(this IEnumerable<T> enumerable,Action<T> functor)
        {
            if (enumerable != null && functor != null)
            {
                foreach (T item in enumerable)
                {
                    functor(item);
                }
            }
        }
        
        public static void Do<TObject>(this TObject obj, Action<TObject> action)
            where TObject: class
        {
            if (obj == null || action == null) return;
            action(obj);
        }

        public static TResult GetSafe<TInput, TResult>(this TInput target, Func<TInput, TResult> evaluator)
        {
            if (IsDefault(target) || evaluator == null) return default(TResult);
            return evaluator(target);
        }
        
        public static bool IsDefault<TInput>(this TInput obj)
        {
            bool referenceType = ReferenceEquals(default(TInput), null);
            if (referenceType)
            {
                return ReferenceEquals(obj, null) || String.IsNullOrEmpty(obj.ToString());
            }

            return default(TInput).Equals(obj);
        }

        public static List<T> ListOrDefault<T>(this IEnumerable<T> enumerable)
        {
            return enumerable == null ? new List<T>() : new List<T>(enumerable);
        }

        public static T[] ArrayOrDefault<T>(this IEnumerable<T> enumerable)
        {
            return enumerable == null ? new T[0] : enumerable.ToArray();
        }

    }
}
