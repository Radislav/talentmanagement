﻿namespace TalentManagement.Web.Models.MyFeedbacks
{
    public class DetailsResponseModel
    {
        public string NumberString { get; set; }
        public string Response { get; set; }
        public string CreatedAt { get; set; }
        public CommentSectionModel CommentSectionModel { get; set; }
        public string ResponseId { get; set; }
    }
}