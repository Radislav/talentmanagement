﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TalentManagement.Domain.Performance;
using TalentManagement.Web.Models.Binder;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.ReviewCycleCreation.Binder
{
    public class ReviewCycleModelBinder : CustomModelBinder<ReviewCycleModel>
    {
        private const string Individuals = "Individuals";
        private const string Managers = "Managers";

        private readonly ICustomModelBinder<IEnumerable<QuestionSetModel>> _setsBinder;
        private readonly IEnumerableBinder<UserPicked> _usersPickedCollectionBinder;

        public ReviewCycleModelBinder(ICustomModelBinder<IEnumerable<QuestionSetModel>> setsBinder, 
            IEnumerableBinder<UserPicked> usersPickedCollectionBinder)
        {
            _setsBinder = setsBinder;
            _usersPickedCollectionBinder = usersPickedCollectionBinder;
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            BinderValueProvider valueProvider = new BinderValueProvider(bindingContext);

            int currentStep = valueProvider.GetValue<int>("CurrentStep");
            ReviewCycleModel model = new ReviewCycleModel
            {
                CurrentStep = currentStep,
                CreateStep1 = new CreateStep1
                    {
                        CurrentStep = currentStep,
                        ManagerDue = valueProvider.GetValue<string>("ManagerDue"),
                        Name = valueProvider.GetValue<string>("Name"),
                        PearsDue = valueProvider.GetValue<string>("PearsDue"),
                        ReviewEnd = valueProvider.GetValue<string>("ReviewEnd"),
                        ReviewStart = valueProvider.GetValue<string>("ReviewStart"),
                        SelfDue = valueProvider.GetValue<string>("SelfDue"),
                        SummaryDue = valueProvider.GetValue<string>("SummaryDue"),
                        Visibility = valueProvider.GetValue<ReviewCycleVisibility>("Visibility")
                    },
                    CreateStep2 = new CreateStep2
                        {
                            CurrentStep = currentStep,
                            SelectedSetType = valueProvider.GetValue<QuestionSetType>("SelectedSetType"),
                            QuestionSets = (IEnumerable<QuestionSetModel>)_setsBinder.BindModel(controllerContext,bindingContext)
                        },
                     CreateStep3 = new CreateStep3
                                       {
                                           CurrentStep = currentStep,
                                           ReviewEveryone = valueProvider.GetValue<bool>("ReviewEveryone")
                                       }
            };

            model.CreateStep3.IndividualsPicker.UsersPicked = BindPickedUsers(controllerContext, bindingContext, Individuals);
            model.CreateStep3.ManagersPicker.UsersPicked = BindPickedUsers(controllerContext, bindingContext,Managers);


            var validationResult = bindingContext.ModelState.ValidateAndKeepState(model.CreateStep1);
            model.CreateStep1.IsReadyToGo = model.CreateStep2.IsReadyToGo = validationResult.IsValid;

            validationResult = bindingContext.ModelState.ValidateAndKeepState(model.CreateStep3);
            model.CreateStep3.IsReadyToGo = validationResult.IsValid;

            return model;
        }

        private List<UserPicked> BindPickedUsers(ControllerContext controllerContext, ModelBindingContext bindingContext,string prefix)
        {
            if (bindingContext.ValueProvider.ContainsPrefix(prefix))
            {
                return ((IEnumerable<UserPicked>)_usersPickedCollectionBinder.SetIdPropertyName("Id")
                                                                             .SetPrefix(prefix)
                                                                             .BindModel(controllerContext, bindingContext))
                                                                             .ToList();    
            }
            return new List<UserPicked>();
        }
    }
}