﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using Moq;
using TalentManagement.Domain.Events;
using TalentManagement.Domain.Feedbacks;
using TalentManagement.Domain.Objectives;
using TalentManagement.Domain.Texts;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure;

namespace TalentManagement.Test.Infrastructure.Services
{
    class TextRepositoryFake : ITextRepository
    {
        public IEnumerable<Text> GetTexts()
        {
            string[] texts = new string[]
                                 {
                                     "Inform me about news",
                                     "Allow my colleagues to send email to me",
                                     "Share my email",
                                     "Send me the company`s report"
                                 };

            return texts.Select(t => new Text(ObjectId.GenerateNewId(t.Length), t, TextType.Setting));
        }

        public Text GetText(TextType type)
        {
            Text[] texts = new Text[]
                               {
                                 new Text(ObjectId.GenerateNewId(),"{SenderName} has requested {EntityUrl}",TextType.NotificationFeedbackRequested),    
                                 new Text(ObjectId.GenerateNewId(),"{SenderName} commented {EntityUrl}",TextType.NotificationFeedbackCommented),    
                                 new Text(ObjectId.GenerateNewId(),"{SenderName} created objective {EntityUrl}",TextType.NotificationObjectiveCreated),    
                                 new Text(ObjectId.GenerateNewId(),"{SenderName} updated objective {EntityUrl}",TextType.NotificationObjectiveUpdated)    
                               };
            return texts.SingleOrDefault(t => t.Type == type);
        }
    }

    [TestClass]
    public class UserServiceTest : MockedTestClass<IUserService>
    {
        private User _updatedUser;

        [TestMethod]
        public void InsertNotification_should_generate_prefedined_FormattedText_for_NotificationFeedbackRequested()
        {
            IUserService target = CreateTarget();
            target.InsertNotification(new User(), 
                                      new User(), 
                                      TextType.NotificationFeedbackRequested,
                                      new FeedbackRequestedArgs(new Feedback(),"Sender",new []{"1","2"} ));

            UserNotification notification = _updatedUser.UserNotifications.First();
            Assert.AreEqual("Sender has requested <a href='feedbacksUrl'>feedback</a>",notification.Text);
        }

        [TestMethod]
        public void InsertNotification_should_generate_prefedined_FormattedText_for_NotificationFeedbackCommented()
        {
            IUserService target = CreateTarget();
            target.InsertNotification(new User(),
                                      new User(),
                                      TextType.NotificationFeedbackCommented,
                                      new FeedbackCommentedArgs(new Feedback(), "Sender", new[] { "1", "2" }));

            UserNotification notification = _updatedUser.UserNotifications.First();
            Assert.AreEqual("Sender commented <a href='feedbacksUrl'>feedback</a>", notification.Text);
        }

        [TestMethod]
        public void InsertNotification_should_generate_prefedined_FormattedText_for_NotificationObjectiveCreated()
        {
            IUserService target = CreateTarget();
            ObjectId objectiveId = ObjectId.GenerateNewId();
            target.InsertNotification(new User(),
                                      new User(),
                                      TextType.NotificationObjectiveCreated,
                                      new ObjectiveCreatedArgs(new Objective(new KeyResult[0])
                                                                   {
                                                                       Id = objectiveId
                                                                   }, "Sender", new[] { "1", "2" }));

            UserNotification notification = _updatedUser.UserNotifications.First();
            Assert.AreEqual(String.Format("Sender created objective <a href='objectiveUrl{0}'>objective</a>", objectiveId),
                notification.Text);
        }

        [TestMethod]
        public void InsertNotification_should_generate_prefedined_FormattedText_for_NotificationObjectiveUpdated()
        {
            IUserService target = CreateTarget();
            ObjectId objectiveId = ObjectId.GenerateNewId();
            target.InsertNotification(new User(),
                                      new User(),
                                      TextType.NotificationObjectiveUpdated,
                                      new ObjectiveUpdatedArgs(new Objective(new KeyResult[0]){Id = objectiveId},
                                                               new Objective(new KeyResult[0]), "Sender",new []{ "1"})); 
                                    

            UserNotification notification = _updatedUser.UserNotifications.First();
            Assert.AreEqual(String.Format("Sender updated objective <a href='objectiveUrl{0}'>objective</a>", objectiveId),
                notification.Text);
        }

        protected override void RegisterMocks(Autofac.ContainerBuilder containerBuilder)
        {
            base.RegisterMocks(containerBuilder);
            FakeUserRepository(containerBuilder);
            FakeUrlProvider(containerBuilder);

            containerBuilder.RegisterType<TextRepositoryFake>()
                .As<ITextRepository>();
        }

        private void FakeUserRepository(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterMock<IUserRepository>(mock => mock.Setup(ur => ur.Update(It.IsAny<User>())).
                                                                       Callback<User>(user => _updatedUser = user));
        }

        private void FakeUrlProvider(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterMock<IUrlProvider>(mock =>
                                                            {
                                                                mock.Setup(up => up.FeedbacksUrl).Returns("feedbacksUrl");
                                                                mock.Setup(up => up.GetObjectiveDetailsUrl(It.IsAny<ObjectId>()))
                                                                    .Returns<ObjectId>(id => "objectiveUrl" + id.ToString());
                                                            });
        }
    }
}
