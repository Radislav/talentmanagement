﻿using System.Collections.Generic;
using TalentManagement.Domain.Events;
using TalentManagement.Domain.Performance;

namespace TalentManagement.Infrastructure.Events
{
    class ReviewCycleInsertedHandler : IHandle<ReviewCycleInsertedArgs>
    {
        private readonly IPerformanceObjectsFactory _factory;
        private readonly IReviewRepository _reviewRepository;

        public ReviewCycleInsertedHandler(IPerformanceObjectsFactory factory,IReviewRepository reviewRepository)
        {
            _reviewRepository = reviewRepository;
            _factory = factory;
        }

        public void Handle(ReviewCycleInsertedArgs args)
        {
            //Review cycle has been inserted. Corresponded reviews should be inserted too.
            List<Review> reviews = new List<Review>();
            args.ReviewCycle.QuestionSets.Map(set =>
            {
                var reviewsSubset = _factory.CreateReviews(args.ReviewCycle.Id, args.ReviewCycle.Participants, args.ReviewCycle.CreatedBy, set);
                reviews.AddRange(reviewsSubset);
            });
            _reviewRepository.InsertReviews(reviews);
        }
    }
}
