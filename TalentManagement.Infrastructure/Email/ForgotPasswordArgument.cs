﻿namespace TalentManagement.Infrastructure.Email
{
    public class ForgotPasswordArgument
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string LoginUrl { get; set; }
    }
}
