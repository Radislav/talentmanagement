﻿namespace TalentManagement.Web.Models.MyCompanyObjectives
{
    public enum ObjectivesGrouping
    {
        None,
        Date,
        Type,
        Team,
        Person,
        Status
    }
}