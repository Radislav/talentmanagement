﻿namespace TalentManagement.Domain.Feeds
{
    public class FeedProperty : Entity
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
