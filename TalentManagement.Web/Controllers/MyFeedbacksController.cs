﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Domain;
using TalentManagement.Domain.Companies;
using TalentManagement.Domain.Feedbacks;
using TalentManagement.Domain.Feeds;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Infrastructure.Validation;
using TalentManagement.Web.Models;
using TalentManagement.Web.Models.MyFeedbacks;
using TalentManagement.Web.Models.MyFeedbacks.Map;
using TalentManagement.Infrastructure;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Controllers
{
    public class MyFeedbacksController : Controller
    {
        private readonly IFeedbackRepository _feedbackRepository;
        private readonly IFeedRepository _feedRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IResourceProvider _resourceProvider;
        private readonly IFilter<IOwned> _ownedFilter;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IMapper<CommentModel, Comment> _commentMapper;
        private readonly IIndexModelMapper _indexModelMapper;
        private readonly IValidator<FeedbackCreateModel> _feedbackCreateModelValidator;
        private readonly IValidator<KudosCreateModel> _kudosCreateModelValidator;
        private readonly IMapper<FeedbackCreateModel, Feedback> _feedbackMapper;
        private readonly IValidator<FeedbackAttentionRequired> _attentionRequiredValidator;
        private readonly IMapper<KudosCreateModel, Feed> _kudosToFeedMapper;

        public MyFeedbacksController(IFeedbackRepository feedbackRepository,
            IFeedRepository feedRepository,
            ICompanyRepository companyRepository,
            IMapper<CommentModel,Comment> commentMapper,
            IResourceProvider resourceProvider,
            IFilter<IOwned> ownedFilter,
            IPrincipalProvider principalProvider,
            IIndexModelMapper indexModelMapper,
            IMapper<FeedbackCreateModel,Feedback> feedbackMapper,
            IValidator<FeedbackCreateModel> feedbackCreateModelValidator,
            IValidator<FeedbackAttentionRequired> attentionRequiredValidator,
            IValidator<KudosCreateModel> kudosCreateModelValidator,
            IMapper<KudosCreateModel, Feed> kudosToFeedMapper)
        {
            _feedbackRepository = feedbackRepository;
            _feedRepository = feedRepository;
            _companyRepository = companyRepository;
            _resourceProvider = resourceProvider;
            _ownedFilter = ownedFilter;
            _principalProvider = principalProvider;
            _commentMapper = commentMapper;
            _indexModelMapper = indexModelMapper;
            _feedbackCreateModelValidator = feedbackCreateModelValidator;
            _feedbackMapper = feedbackMapper;
            _attentionRequiredValidator = attentionRequiredValidator;
            _kudosCreateModelValidator = kudosCreateModelValidator;
            _kudosToFeedMapper = kudosToFeedMapper;

        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(CreateIndexModel(_resourceProvider.FeedbackActiveButton));
        }
        
        [HttpGet]
        public ActionResult Feedbacks(string id)
        {
            return PartialView("_Feedbacks", CreateIndexModel(id));    
       
        }

        [HttpPost]
        public ActionResult CommentTheirFeedback(CommentModel comment,ObjectId holderId)
        {
            string currentUser = _principalProvider.GetUserName();
            Feedback feedback = _feedbackRepository.GetById(holderId);

            if (feedback != null)
            {
                Response response = feedback.GetResponseFor(currentUser);
                response.AddComment(_commentMapper.Map(comment));
                comment.CreatedBy = currentUser;

                _feedbackRepository.Update(feedback);
            }
            return PartialView("_Comment", comment);
        }

        [HttpPost]
        public ActionResult CommentMyFeedback(CommentModel comment, ObjectId holderId)
        {
            Feedback feedback = _feedbackRepository.GetByResponseId(holderId);

            if (feedback != null)
            {
                Response response = feedback.GetResponse(holderId);
                response.AddComment(_commentMapper.Map(comment));
                comment.CreatedBy = _principalProvider.GetUserName();

                _feedbackRepository.Update(feedback);
            }
            return PartialView("_Comment", comment);
        }
        
        [HttpGet]
        public ActionResult Details(ObjectId id)
        {
            IndexModel model = CreateIndexModel(_resourceProvider.FeedbackDetails,id);
            return PartialView("_Feedbacks", model);
        }

        [HttpPost]
        public ActionResult RefreshUserPicker(UserPickerControl picker)
        {
            if (picker.CurrentUser != null)
            {
                picker.UsersPicked.Add(picker.CurrentUser);    
            }
            return PartialView("_UserPickerControl", picker);
        }

        [HttpPost]
        public ActionResult CreateFeedback(FeedbackCreateModel model)
        {
            ValidationResult validationResult =  _feedbackCreateModelValidator.ValidateInstance(model);

            if (validationResult.IsValid)
            {
                _feedbackRepository.Insert(_feedbackMapper.Map(model));
            }

            return validationResult.AsJson();
        }

        [HttpPost]
        public ActionResult CreateKudos(KudosCreateModel model)
        {
            ValidationResult validationResult = _kudosCreateModelValidator.ValidateInstance(model);

            if (validationResult.IsValid)
            {
                _feedRepository.Insert(_kudosToFeedMapper.Map(model));
            }

            return validationResult.AsJson();
        }

        [HttpPost]
        public ActionResult AddResponse(FeedbackAttentionRequired feedbackAttentionRequired)
        {
            ValidationResult validationResult =  _attentionRequiredValidator.ValidateInstance(feedbackAttentionRequired);
            if (validationResult.IsValid)
            {
                Feedback feedback = _feedbackRepository.GetById(new ObjectId(feedbackAttentionRequired.FeedbackId));
                Response response = new Response {Answer = feedbackAttentionRequired.Response};
                _principalProvider.FillOWned(response);
                feedback.AddResponse(response);
                _feedbackRepository.Update(feedback);
            }

            return Json(new
                            {
                                validationResult.IsValid,
                                Errors = validationResult.Failures.Select(f => new { f.PropertyName, f.ErrorMessage }),
                                feedbackAttentionRequired.FeedbackId
                            });
        }

        private IndexModel CreateIndexModel(string activeButton,ObjectId? feedbackId = null)
        {
            IEnumerable<Feedback> currentFeedbacks = new Feedback[0];

            if (activeButton == _resourceProvider.FeedbackAttentionRequired)
            {
                currentFeedbacks = _feedbackRepository.GetRequiredAttentionFeedbacks();
            }
            if (activeButton == _resourceProvider.FeedbackYouRequested)
            {
                currentFeedbacks = _feedbackRepository.GetFilteredFeedBacks(_ownedFilter);
            }
            if (activeButton == _resourceProvider.FeedbackTheyRequested)
            {
                currentFeedbacks = _feedbackRepository.GetTheirRequestedFeedbacks();
            }

            if (activeButton == _resourceProvider.FeedbackDetails && feedbackId.HasValue)
            {
                currentFeedbacks = new [] {_feedbackRepository.GetById(feedbackId.Value)};
            }
            
            return _indexModelMapper.Map(activeButton,currentFeedbacks.ToArray(),GetBadgeIds());
        }

        private ObjectId[] GetBadgeIds()
        {
           Company company = _companyRepository.GetByTenantWithoutPictures(_principalProvider.GetTenant());
           
            List<ObjectId> badgeIds = new List<ObjectId>(company.GetSafe(c => c.BadgeIds) ?? new ObjectId[0])
                                          {
                                              Company.DefaultBadgeId
                                          };

            return badgeIds.ToArray();
        }
    }
}
