﻿namespace TalentManagement.Infrastructure.Configuration
{
    public class EmailSettings
    {
        public int Port { get; set; }
        public string From { get; set; }
        public string Host { get; set; }
    }
}
