﻿using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.ObjectiveDetails.Map
{
    public class ActionItemMap : IMap<ActionItemModel,ActionItem>
    {
        public void Configure(IMapperConfiguration<ActionItemModel, ActionItem> configuration)
        {
            configuration.ForMember(d => d.Name, s => s.Name);
            configuration.ForMember(d => d.Owner, s => s.Owner);
            configuration.UseValue(d => d.RelatedToObjective, true);
        }
    }
}