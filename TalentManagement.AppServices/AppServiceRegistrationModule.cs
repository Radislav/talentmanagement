﻿using Autofac;

namespace TalentManagement.AppServices
{
    public class AppServiceRegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RegistrationFlow>()
                .As<IRegistrationFlow>()
                .InstancePerDependency();

            builder.RegisterType<GroupUploader>()
                .As<IGroupUploader>();

            base.Load(builder);
        }
    }
}
