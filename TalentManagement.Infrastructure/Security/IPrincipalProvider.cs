﻿using TalentManagement.Domain;

namespace TalentManagement.Infrastructure.Security
{
    public interface IPrincipalProvider
    {
        TenantPrincipal GetPrincipal();
        TenantIdentity GetIdentity();
        string GetUserName();
        string GetTenant();
        string GetTeam();
        string GetTenantFor(string email);
        OwnedDescriptor GetOwnderDescriptor();
    }
}
