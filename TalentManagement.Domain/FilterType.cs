﻿using System;

namespace TalentManagement.Domain
{
    [Flags]
    public enum FilterType
    {
        None = 0x0,
        Empty = 0x1,
        Composite = 0x2,
        Union = 0x4,
        Owned = 0x8,
        OwnedByCompany = 0x10,
        FeedType = 0x20,
        OwnedByTeam = 0x40,
        User = 0x80,
        Feed = Owned | OwnedByCompany,
        Participants = 0x100,
        ReviewRequiredAttention = 0x200,
        Managers = 0x400
    }
}
