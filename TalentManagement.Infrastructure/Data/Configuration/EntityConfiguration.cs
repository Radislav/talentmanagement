﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Expressions;
using TalentManagement.Domain;
using TalentManagement.Infrastructure.Validation;

namespace TalentManagement.Infrastructure.Data.Configuration
{
    public class EntityConfiguration<TEntity> : IEntityConfiguration<TEntity> 
        where TEntity : class 
    {   
        private string _collectionName;
        private bool _safeMode;
        
        private DataSourceType _dataSourceType = DataSourceType.None;
        private readonly List<IPropertyReadConfiguration<TEntity>> _propertyConfigurations = new List<IPropertyReadConfiguration<TEntity>>();

        public IEnumerable<IPropertyReadConfiguration<TEntity>> PropertyConfigurations
        {
            get { return _propertyConfigurations; }
        }

        public IEntityConfiguration<TEntity> AsCollection(string collectionName)
        {
            _collectionName = collectionName;
            return this;
        }   

        public bool IsOfType(Type type)
        {
            return typeof (TEntity) == type;
        }
        
        public string CollectionName
        {
            get { return _collectionName; }
        }
        
        public IEntityConfiguration<TEntity> WithDataSource(DataSourceType dataSourceType)
        {
            _dataSourceType = dataSourceType;
            return this;
        }
        
        public DataSourceType DataSourceType
        {
            get { return _dataSourceType; }
        }
        
        public void ValidateConfiguration()
        {
            if (_dataSourceType == DataSourceType.None)
            {
                throw new EntityException("DataSourceType must be set.");
            }

            if(String.IsNullOrEmpty(_collectionName))
            {
                throw new EntityException("Collection name must be set.");
            }
        }
        
        public IEntityConfiguration<TEntity> UseSafeMode(bool safeMode)
        {
            _safeMode = safeMode;
            return this;
        }


        public bool SafeMode
        {
            get { return _safeMode; }
        }


        public IPropertyConfiguration<TEntity, TProperty> Property<TProperty>(Expression<Func<TEntity, TProperty>> property)
        {
            var propertyConfiguration = new PropertyConfiguration<TEntity, TProperty>(property);
            Contract.Assume(propertyConfiguration != null);
            _propertyConfigurations.Add(propertyConfiguration);
            return propertyConfiguration;
        }
        
        public ValidationResult ValidateInstance(object entity)
        {
            if (entity == null)
            {
                return new ValidationResult("NULL entity passed");
            }
            TEntity castedEntity = entity as TEntity;

            if (castedEntity == null)
            {
                return new ValidationResult(String.Format("Entity must have {0} type, but it has {1}", typeof(TEntity), entity.GetType()));    
            }

            ValidationResult[] validationResults = _propertyConfigurations.Select(propCfg => propCfg.ValidateInstance(castedEntity))
                                                                            .ToArray();
            return new ValidationResult(validationResults);
        }
    }
}
