﻿using System.IO;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TalentManagement.AppServices;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Web.Controllers;
using TalentManagement.Web.Models;

namespace TalentManagement.Test.Web.Controllers
{
    [TestClass]
    public class RegistrationControllerTest : MockedTestClass<RegistrationController>
    {
        [TestMethod]
        public void Register_should_return_View()
        {
            RegistrationController target = CreateTarget();
            ViewResult result  = (ViewResult)target.Register();
        }

        [TestMethod]
        public void Register_should_call_profileuser_mapper_for_User_if_registration_finished()
        {
            ProfileModel profile = new ProfileModel {IsReadyToGo = true};
            RegistrationController target = CreateTarget();
            RegistrationModel registration = new RegistrationModel()
                                                 {
                                                     Profile = profile,
                                                     Invitation = new InvitationModel
                                                                      {
                                                                          IsReadyToGo = true
                                                                      },
                                                     CurrentStep = (int) RegistrationStep.Invitation
                                                 };
            target.Register(registration);
            Assert.IsTrue(registration.IsFlowFinished);

            Container.Resolve<Mock<IMapper<ProfileModel,User>>>()
                .Verify(m => m.Map(profile),Times.Once());
                
        }

        [TestMethod]
        public void Register_should_call_profileavatar_mapper_if_profile_valid_and_profile_has_avatar()
        {
            var streamMock = new Mock<Stream>();
            streamMock.Setup(m => m.CanRead).Returns(true);

            var imageMock = new Mock<HttpPostedFileBase>();
            imageMock.Setup(m => m.InputStream).Returns(streamMock.Object);

            ProfileModel profile = new ProfileModel { IsReadyToGo = true, ProfileImage = imageMock.Object};
            RegistrationController target = CreateTarget();
            RegistrationModel registration = new RegistrationModel()
            {
                Profile = profile,
                Invitation = new InvitationModel
                {
                    IsReadyToGo = true
                },
                CurrentStep = (int)RegistrationStep.Profile
            };
            target.Register(registration);
            
            Container.Resolve<Mock<IMapper<ProfileModel, Avatar>>>()
                .Verify(m => m.Map(profile), Times.Once());
        }

        [TestMethod]
        public void Register_should_NOT_call_profileuser_mapper_for_User_if_registration__NOT_finished()
        {
            ProfileModel profile = new ProfileModel {  };
            RegistrationController target = CreateTarget();
            RegistrationModel registration = new RegistrationModel()
            {
                Profile = profile,
                Invitation = new InvitationModel{},
                CurrentStep = (int)RegistrationStep.Profile
            };
            target.Register(registration);
            Assert.IsFalse(registration.IsFlowFinished);

            Container.Resolve<Mock<IMapper<ProfileModel, User>>>()
                .Verify(m => m.Map(profile), Times.Never());


        }

        protected override RegistrationController CreateTarget(IContainer container)
        {
            return new RegistrationController(Container.Resolve<IMapper<ProfileModel,User>>(),
                                              Container.Resolve<IMapper<ProfileModel, Avatar>>(),
                                              Container.Resolve<IRegistrationFlow>(),
                                              Container.Resolve<IAvatarRepository>(),Container.Resolve<IAuthenticationService>());
        }

        protected override void RegisterMocks(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterMock(new Mock<IRegistrationFlow>());
            containerBuilder.RegisterMock(new Mock<IMapper<ProfileModel, User>>());
            containerBuilder.RegisterMock(new Mock<IMapper<ProfileModel, Avatar>>());
            containerBuilder.RegisterMock(new Mock<IAvatarRepository>());
            base.RegisterMocks(containerBuilder);
        }
    }
}
