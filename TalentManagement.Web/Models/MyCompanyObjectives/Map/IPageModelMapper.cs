﻿using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain.Objectives;

namespace TalentManagement.Web.Models.MyCompanyObjectives.Map
{
    public interface IPageModelMapper
    {
        PageModel Map(IEnumerable<IGrouping<object, Objective>> source, bool showAddToFocus);
    }
    public interface IPageModelMapper<TGroupKey> : IPageModelMapper
    {

    }
}
