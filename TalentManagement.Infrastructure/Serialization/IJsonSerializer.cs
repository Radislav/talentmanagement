﻿namespace TalentManagement.Infrastructure.Serialization
{
    public interface IJsonSerializer<TEntity> : ISerializer<TEntity,string>
    {
    }
}
