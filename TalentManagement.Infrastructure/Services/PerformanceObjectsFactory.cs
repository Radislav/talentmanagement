﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac.Features.Indexed;
using MongoDB.Bson;
using TalentManagement.Domain;
using TalentManagement.Domain.Companies;
using TalentManagement.Domain.Feeds;
using TalentManagement.Domain.Objectives;
using TalentManagement.Domain.Performance;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Data.Filter;

namespace TalentManagement.Infrastructure.Services
{
    internal class PerformanceObjectsFactory : IPerformanceObjectsFactory
    {
        private readonly IReviewCycleRepository _cycleRepository;
        private readonly IReviewRepository _reviewRepository;
        private readonly IQuestionRepository _questionRepository;
        private readonly IFeedRepository _feedRepository;
        private readonly IObjectiveRepository _objectiveRepository;
        private readonly IBadgeRepository _badgeRepository;
        private readonly IFilter _participantsFilter;
        private readonly IUserRepository _userRepository;
      
        public PerformanceObjectsFactory(IReviewRepository reviewRepository,
            IQuestionRepository questionRepository,
            IFeedRepository feedRepository,
            IObjectiveRepository objectiveRepository,
            IReviewCycleRepository cycleRepository,
            IBadgeRepository badgeRepository,
            IIndex<FilterType,IFilter> filters,
            IUserRepository userRepository)
        {
            _cycleRepository = cycleRepository;
            _reviewRepository = reviewRepository;
            _questionRepository = questionRepository;
            _feedRepository = feedRepository;
            _objectiveRepository = objectiveRepository;
            _participantsFilter = filters[FilterType.Participants];
            _badgeRepository = badgeRepository;
            _userRepository = userRepository;
        }

        public ReviewCycleSummary CreateSummary(ObjectId reviewCycleId,string respondent)
        {
            ReviewCycle cycle = _cycleRepository.GetById(reviewCycleId);
            Review[] reviews = _reviewRepository.GetByCycle(cycle)
                                                .Where(r => r.Respondent == respondent)
                                                .ToArray();

            Review summaryReview = reviews.Single(r => r.SetType == QuestionSetType.Summary);
            IEnumerable<Kudos> kudos = _feedRepository.GetKudosByReceiver(summaryReview.Respondent);
            IEnumerable<Kudos> respondentKudos = kudos as Kudos[] ?? kudos.ToArray();
            IEnumerable<Badge> badges = _badgeRepository.GetBadgesLight(respondentKudos.Select(k => k.BadgeId));

            _participantsFilter.FilterContext = new ParticipantFilterContext(summaryReview.Respondent);
            IEnumerable<Objective> objectives = _objectiveRepository.ApplyFilter(_participantsFilter);

            User summaryReviewRespondent = _userRepository.GetById(summaryReview.Respondent);

            return new ReviewCycleSummary(cycle, reviews, CreateAnswersCollections(reviews), objectives, respondentKudos, badges,summaryReviewRespondent);
        }

        private IEnumerable<AnswersCollection> CreateAnswersCollections(IEnumerable<Review> reviews)
        {
            
            var setTypes = Enum.GetValues(typeof (QuestionSetType))
                               .Cast<QuestionSetType>()
                               .Where(set => set != QuestionSetType.None)
                               .ToArray();

            Review[] reviewsArray = reviews.ToArray();

            return setTypes.Select(questionSetType => CreateAnswersCollection(reviewsArray, questionSetType));
        }

        private AnswersCollection CreateAnswersCollection(IEnumerable<Review> reviews, QuestionSetType setType)
        {
            Review review = reviews.SingleOrDefault(r => r.SetType == setType);
            return review == null ? new AnswersCollection(setType) :
                                   CreateAnswersCollection(review);
        }


        public AnswersCollection CreateAnswersCollection(Review review)
        {
            return new AnswersCollection(review.Answers, _questionRepository.GetByIds(review.QuestionIds), review.SetType);
        }


      public IEnumerable<Review> CreateReviews(ObjectId reviewCycleId, IEnumerable<string> participants,string owner,QuestionSet set)
        {
           string[] participantsArray = participants.ToArray();
            
            if (set.SetType == QuestionSetType.Self)
            {
                return participantsArray.Select(participant => new Review(set.Id, reviewCycleId, set.QuestionIds, set.DueDate, set.SetType, participant, participant, owner)
                {
                    UpdatedBy = owner,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                });
            }

            if (set.SetType == QuestionSetType.Summary)
            {
                return _userRepository.GetUsers(participantsArray)
                                      .Where(u => false == String.IsNullOrEmpty(u.ManagerEmail))
                                      .Select(user => new Review(set.Id, reviewCycleId,set.QuestionIds, set.DueDate, set.SetType, user.Id,user.ManagerEmail, owner)
                                        {
                                            UpdatedBy = owner,
                                            CreatedAt = DateTime.Now,
                                            UpdatedAt = DateTime.Now
                                        });
            }
            
            return new Review[0];
        }
    }
}
