﻿using System;

namespace TalentManagement.Web.Models
{
    public class UserInfoModel
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public DateTime LastSignIn { get; set; }
        public int NotificationsCount { get; set; }
    }
}