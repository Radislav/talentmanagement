﻿using System;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Web.Models.MyActionItems.Map
{
    internal class ActionItemMapper : IMapper<ActionItemModel,ActionItem>
    {
        private readonly IPrincipalProvider _principalProvider;

        public ActionItemMapper(IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
        }

        public ActionItem Map(ActionItemModel source)
        {
            return new ActionItem(source.Name, _principalProvider.GetUserName(), DateTime.Parse(source.DueString), false);
        }
    }
}