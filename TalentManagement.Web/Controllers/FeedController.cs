﻿using System.Linq;
using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Domain;
using TalentManagement.Domain.Feeds;
using TalentManagement.Infrastructure.Data.Filter;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Infrastructure.Session;
using TalentManagement.Web.Infrastructure.Security;
using TalentManagement.Web.Models;
using TalentManagement.Web.Models.Feeds;
using TalentManagement.Web.Models.Feeds.Map;

namespace TalentManagement.Web.Controllers
{
    public class FeedController : Controller
    {
        private readonly IFeedRepository _feedRepository;
        private readonly IMyFeedRepository _myFeedRepository;
        private readonly IFeedUniversalMapper _feedMapper;
        private readonly IMapper<CommentModel, Comment> _commentMapper;
        private readonly IPrincipalProvider _principalProvider;
        private readonly ISessionFactory _sessionFactory;
        private readonly IFilter _emptyFilter; 
        

        public FeedController(IFeedRepository feedRepository,
                              IMyFeedRepository myFeedRepository,
                              IFeedUniversalMapper feedMapper,
                              IMapper<CommentModel,Comment> commentMapper,
                              IPrincipalProvider principalProvider,
                              IFilter emptyFilter,
                              ISessionFactory sessionFactory)
        {
            _feedRepository = feedRepository;
            _myFeedRepository = myFeedRepository;
            _feedMapper = feedMapper;
            _commentMapper = commentMapper;
            _principalProvider = principalProvider;
            _sessionFactory = sessionFactory;
            _emptyFilter = emptyFilter;
        }

        [HttpGet]
        [TenantAuthorization(Roles = SecurityConstants.ContributorRole)]
        public ActionResult Index()
        {
            FeedModel[] feeds = _myFeedRepository.GetFeeds(_emptyFilter)
                                           .Select(feed => _feedMapper.Map(feed))
                                           .ToArray();

            return View(new FeedPageModel { Feeds = feeds });
        }
        [HttpGet]
        [TenantAuthorization(Roles = SecurityConstants.ContributorRole)]
        public ActionResult Filter(FilterType filterType)
        {
            var currentFilter = GetCurrentFilter(filterType);

            FeedModel[] feeds = _myFeedRepository.GetFeeds(currentFilter)
                                           .Select(feed => _feedMapper.Map(feed))
                                           .ToArray();

            return PartialView("_FeedsPanel", feeds);
        }
        
        private IFilter GetCurrentFilter(FilterType filterType)
        {
            using (var scope = _sessionFactory.Create())
            {
                IFilter filter = scope.Instance<IFilter>(filterType);
                if (filterType == FilterType.FeedType)
                {
                    filter.FilterContext = new FeedTypeContext(FeedType.Objective);
                }
                return filter;
            }
        }

        [HttpPost]
        [TenantAuthorization(Roles = SecurityConstants.ContributorRole)]
        public ActionResult AddComment(CommentModel comment,ObjectId holderId)
        {
            comment.CreatedBy = _principalProvider.GetUserName();
            
            Feed feed = _feedRepository.GetById(holderId);
            feed.AddComment(_commentMapper.Map(comment));
            _feedRepository.Update(feed);
            
            return  PartialView("_Comment",comment);
        }
    }
}
