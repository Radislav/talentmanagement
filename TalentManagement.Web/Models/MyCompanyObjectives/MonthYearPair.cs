﻿namespace TalentManagement.Web.Models.MyCompanyObjectives
{
    public struct MonthYearPair
    {
        public MonthYearPair(int month, int year)
        {
            _month = month;
            _year = year;
        }

        private readonly int _month;
        private int _year;

        public int Month
        {
            get { return _month; }
        }

        public int Year
        {
            get { return _year; }
        }
    }
}