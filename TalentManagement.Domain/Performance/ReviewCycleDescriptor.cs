﻿using MongoDB.Bson;

namespace TalentManagement.Domain.Performance
{
    public struct ReviewCycleDescriptor
    {
        private readonly ObjectId _id;
        private readonly string _name;
        private readonly decimal _progress;

        public ReviewCycleDescriptor(ObjectId id, string name,decimal progress)
        {
            _name = name;
            _id = id;
            _progress = progress;
        }

        public ObjectId Id {
            get { return _id; }
        }

        public string Name {
            get { return _name; }
        }

        public decimal Progress
        {
            get { return _progress; }
        }
    }
}
