﻿using System;
using TalentManagement.Domain.Performance;
using Resource = TalentManagement.Resources.Models.ReviewCycleCreation.CreateStep1;

namespace TalentManagement.Web.Models.ReviewCycleCreation
{
    public class CreateStep1 : WizardPage
    {
        public CreateStep1()
        {
            //TODO: Remove this, just for testing.
            Name = "Test step1";
            ReviewStart = DateTime.Today.AsShortDateString();
            string tomorrow = DateTime.Today.AddDays(1).AsShortDateString();
            ReviewEnd = tomorrow;
            SummaryDue = tomorrow;
            PearsDue = tomorrow;
            SelfDue = tomorrow;
            ManagerDue = tomorrow;
            Visibility = ReviewCycleVisibility.EmployeeAndManager;
        }

        [RequiredUi]
        [LocalizedDisplayName("Name",typeof(Resource))]
        public string Name { get; set; }

        [RequiredUi]
        [LocalizedDisplayName("ReviewStart", typeof(Resource))]
        public string ReviewStart { get; set; }

        [RequiredUi]
        [LocalizedDisplayName("ReviewEnd", typeof(Resource))]
        public string ReviewEnd { get; set; }

        [RequiredUi]
        [LocalizedDisplayName("SummaryDue", typeof(Resource))]
        public string SummaryDue { get; set; }

        [RequiredUi]
        [LocalizedDisplayName("PearsDue", typeof(Resource))]
        public string PearsDue { get; set; }

        [RequiredUi]
        [LocalizedDisplayName("SelfDue", typeof(Resource))]
        public string SelfDue { get; set; }

        [RequiredUi]
        [LocalizedDisplayName("ManagerDue", typeof(Resource))]
        public string ManagerDue { get; set; }

        [LocalizedDisplayName("Visibility",typeof(Resource))]
        public ReviewCycleVisibility Visibility { get; set; }

        public override int NextStep
        {
            get { return 1; }
        }

        public override int ThisStep
        {
            get { return 0; }
        }
    }
}