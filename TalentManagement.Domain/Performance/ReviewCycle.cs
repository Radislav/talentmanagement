﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using MongoDB.Bson;

namespace TalentManagement.Domain.Performance
{
    [AggregateRoot]
    public class ReviewCycle : Entity , IHasParticipants , IOwned
    {
        private List<QuestionSet> _questionSets;

        private ReviewCycle()
        {
            _questionSets = new List<QuestionSet>();
            Participants = new List<string>();
            Status = ReviewCycleStatus.NotStarted;
            Name = string.Empty;
            ReviewStart = DateTime.Today;
            ReviewEnd = DateTime.Today.AddDays(1);
            Visibility = ReviewCycleVisibility.EmployeeAndManager;
        }

        public ReviewCycle(string name,
                           DateTime reviewStart,
                           DateTime reviewEnd,
                           IEnumerable<QuestionSet> questionSets,
                           IEnumerable<string> participants,
                           bool reviewEveryone) : this()
        {
            Contract.Requires(false == String.IsNullOrEmpty(name));
            Contract.Requires(reviewStart <= reviewEnd);
            Contract.Requires(questionSets != null);
            Contract.Requires(questionSets.Any());
            Contract.Requires(participants != null);
            Contract.Requires(reviewEveryone || participants.Any());
            
            Name = name;
            ReviewStart = reviewStart;
            ReviewEnd = reviewEnd;
            QuestionSets = questionSets;
            Participants = participants;
            ReviewEveryone = reviewEveryone;
        }
   
        public string Name { get; set; }

        public DateTime ReviewStart { get; set; }

        public DateTime ReviewEnd { get; set; }

        public IEnumerable<QuestionSet> QuestionSets
        {
            get { return _questionSets; }
            private set
            {
                _questionSets = new List<QuestionSet>(value);
                _questionSets.Map(set =>
                    {
                        set.Id = set.IsNew ? ObjectId.GenerateNewId() : set.Id;
                    });
            }
        }

        public QuestionSet GetQuestionSet(ObjectId questionSetId)
        {
            return QuestionSets.SingleOrDefault(q => q.Id == questionSetId);
        }

        public IEnumerable<string> Participants { get; private set; }
        
        public bool ReviewEveryone { get; set; }

        public ReviewCycleStatus Status { get; set; }

        public decimal Progress {get; set;}

        public ReviewCycleVisibility Visibility { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedAt { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedAt { get; set; }

        [ContractInvariantMethod]
        private void Invariant()
        {
            Contract.Invariant(Name != null);
            Contract.Invariant(QuestionSets != null);
            Contract.Invariant(Participants != null);
            Contract.Invariant(Progress >= 0 && Progress <= 100);
        }
    }
}
