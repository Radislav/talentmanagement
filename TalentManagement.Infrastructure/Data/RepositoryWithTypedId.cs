﻿using System;
using System.Linq;
using TalentManagement.Domain;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data
{
    public class RepositoryWithTypedId<TEntity,TId> : IRepositoryWithTypedId<TEntity,TId>
          where TEntity : EntityWithTypedId<TId>
    {
        protected readonly IUnitOfWork UnitOfWork;
        protected readonly IPrincipalProvider PrincipalProvider;

        public RepositoryWithTypedId(IUnitOfWork unitOfWork,IPrincipalProvider principalProvider)
        {
            PrincipalProvider = principalProvider;
            UnitOfWork = unitOfWork;
        }

        public virtual void Insert(TEntity entity)
        {
            IOwned owned = entity as IOwned;
            if (owned != null)
            {
                PrincipalProvider.FillOWned(owned);
            }
            IOwnedByCompany ownedByCompany = entity as IOwnedByCompany;
            if (ownedByCompany != null)
            {
                PrincipalProvider.FillOWnedByCompany(ownedByCompany);
            }

            UnitOfWork.Insert(entity);
        }

        public virtual void DeleteById(TId id)
        {
            UnitOfWork.DeleteById<TEntity,TId>(id);
        }

        public virtual void Update(TEntity entity)
        {
            IOwned owned = entity as IOwned;
            if (owned != null)
            {
                owned.UpdatedBy = PrincipalProvider.GetUserName();
                owned.UpdatedAt = DateTime.Now;
            }
            IOwnedByCompany ownedByCompany = entity as IOwnedByCompany;
            if (ownedByCompany != null)
            {
                ownedByCompany.CompanyName = PrincipalProvider.GetTenant();
            }

            UnitOfWork.Update(entity);
        }

        public virtual TEntity GetById(TId id)
        {
            if (ReferenceEquals(id,null) || id.Equals(default(TId)))
            {
                return null;
            }

            return UnitOfWork.GetById<TEntity, TId>(id);
        }

        public virtual IQueryable<TEntity> AsQueryable()
        {
            return UnitOfWork.AsQueryable<TEntity>();
        }


        public virtual TEntity Upsert(TEntity entity)
        {
            if (entity.IsNew)
            {
                Insert(entity);
            }
            else
            {
                Update(entity);
            }
            return entity;
        }


        public System.Collections.Generic.IEnumerable<TEntity> ApplyFilter(IFilter filter)
        {
            return UnitOfWork.ApplyFilter<TEntity>(filter);
        }
    }
}
