﻿using System.Diagnostics.Contracts;
using TalentManagement.Domain.Feedbacks;
using TalentManagement.Domain.Texts;

namespace TalentManagement.Domain.Events
{
    public class FeedbackCommentedArgs : NotificableEventArgs
    {
        public FeedbackCommentedArgs(Feedback feedback, string senderId,params string[] receiversIds)
            : base(TextType.NotificationFeedbackCommented, senderId,feedback, receiversIds)
        {
            Contract.Requires(feedback != null);
            Contract.Requires(senderId != null);
            Contract.Requires(receiversIds != null);

            Feedback = feedback;
        }
        
        public Feedback Feedback { get; private set; }
    }
}
