﻿using System.Collections.Generic;

namespace TalentManagement.Web.Models.Teams
{
    public class IndexModel
    {
        public IEnumerable<TeamModel> Teams { get; set; }
    }
}