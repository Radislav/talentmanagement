﻿using TalentManagement.Domain;

namespace TalentManagement.Web.Models
{
    public class Picture : IPicture 
    {
        public Picture()
        {

        }

        public Picture(IPicture picture)
        {
            Image = picture.Image;
            ImageType = picture.ImageType;
        }

        public byte[] Image { get; set; }

        public string ImageType { get; set; }
    }
}