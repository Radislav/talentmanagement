﻿using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Questionaire.Map
{
    public class AnswersMapper : IMapper<IndexModel,IEnumerable<Answer>>
    {
        public IEnumerable<Answer> Map(IndexModel source)
        {
            return source.Answers.Select(answer => new Answer(answer.Id, answer.QuestionId, answer.Content))
                                 .ToArray();
        }
    }
}