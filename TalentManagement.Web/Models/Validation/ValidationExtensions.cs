﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FluentValidation;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Validation;

namespace TalentManagement.Web.Models.Validation
{
    public static class ValidationExtensions
    {
        public static ValidationResult ValidateAsModel<T>(this AbstractValidator<T> fluentValidator, T instance)
        {
            return new ValidationResult(fluentValidator.Validate(instance).Errors.Select(e => new ValidationFailure(e.PropertyName, e.ErrorMessage, e.AttemptedValue)));
        }

        public static ValidationResult ValidateAndKeepState<T>(this ModelStateDictionary modelStateDictionary,T instance)
        {
            return ValidateAndKeepState(modelStateDictionary, instance, null);
        }

        public static ValidationResult ValidateAndKeepState<T>(this ModelStateDictionary modelStateDictionary, T instance,string prefix)
        {
            ValidationResult result = DependencyResolver.Current.GetService<TalentManagement.Infrastructure.Validation.IValidator<T>>()
                                                        .ValidateInstance(instance);

            result.Failures.Map(failure => modelStateDictionary.AddModelError(GetPropertyName(failure.PropertyName,prefix), failure.ErrorMessage));

            return result;
        }

        public static JsonResult AsJson(this ModelStateDictionary modelStateDictionary)
        {
            var errors =
                modelStateDictionary.Select(
                    ms =>
                    new KeyValuePair<string, IEnumerable<string>>(ms.Key, ms.Value.Errors.Select(e => e.ErrorMessage)));
                
            
            return new JsonResult
                       {
                           Data = new {
                                          modelStateDictionary.IsValid,
                                          Errors = errors.Select(e => new {PropertyValue = e.Key, })
                                      }
                       };
        }
        
        public static JsonResult AsJson(this ValidationResult validationResult)
        {
            return new JsonResult
            {
                Data = new{
                            validationResult.IsValid,
                            Errors = validationResult.Failures.Select(f => new { f.PropertyName, f.ErrorMessage })
                        }
            };    
        }
        
        private static string GetPropertyName(string propertyName,string prefix)
        {
            return string.IsNullOrEmpty(prefix) ? propertyName : string.Concat(prefix, ".", propertyName);
        }

        public static IRuleBuilderOptions<T, string> DateTime<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new DateTimeValidator());
        }
        public static IRuleBuilderOptions<T, string> DueDate<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new DueDateValidator());
        }
        
        public static IRuleBuilderOptions<T, string> Owner<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new OwnerValidator(DependencyResolver.Current.GetService<IUserRepository>()));
        }

        public static IRuleBuilderOptions<T, HttpPostedFileBase> FilePosted<T>(this IRuleBuilder<T, HttpPostedFileBase> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new FilePostedValidator());
        }

        public static IRuleBuilderOptions<T, HttpPostedFileBase> FileSize<T>(this IRuleBuilder<T, HttpPostedFileBase> ruleBuilder,int maxFileSize)
        {
            return ruleBuilder.SetValidator(new FileSizeValidator(maxFileSize));
        }

        public static IRuleBuilderOptions<T, HttpPostedFileBase> ImageFile<T>(this IRuleBuilder<T, HttpPostedFileBase> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new FileImageTypeValidator());
        }

    }
}