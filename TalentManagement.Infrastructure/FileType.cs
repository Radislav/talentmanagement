﻿namespace TalentManagement.Infrastructure
{
    public enum FileType
    {
        None,
        Csv,
        Xlsx,
    }
}
