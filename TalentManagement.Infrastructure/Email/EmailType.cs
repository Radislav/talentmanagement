﻿namespace TalentManagement.Infrastructure.Email
{
    public enum EmailType
    {
        None,
        Activation,
        Invitation,
        ForgotPassword,
    }
}
