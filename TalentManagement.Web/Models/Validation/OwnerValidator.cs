﻿using System;
using FluentValidation.Validators;
using TalentManagement.Domain.Users;

namespace TalentManagement.Web.Models.Validation
{
    internal class OwnerValidator : PropertyValidator
    {
        private readonly IUserRepository _userRepository;

        public OwnerValidator(IUserRepository userRepository)
            : base("OwnerNotFounded",typeof(Resources.Errors))
        {
            _userRepository = userRepository;
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            string value = context.PropertyValue as string;
            if (String.IsNullOrEmpty(value))
            {
                return false;
            }

            return _userRepository.GetById(value) != null;
            
        }
    }
}