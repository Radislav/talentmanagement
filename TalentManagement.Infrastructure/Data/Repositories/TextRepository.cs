﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using TalentManagement.Domain;
using TalentManagement.Domain.Texts;

namespace TalentManagement.Infrastructure.Data.Repositories
{
    class TextRepository : ITextRepository
    {
        //TODO: Update this fake
        public IEnumerable<Text> GetTexts()
        {
            string[] texts = new string[]
                                 {
                                     "Inform me about news",
                                     "Allow my colleagues to send email to me",
                                     "Share my email",
                                     "Send me the company`s report"
                                 };
            
            return texts.Select(t => new Text(ObjectId.GenerateNewId(t.Length),t,TextType.Setting));
        }
        
        public Text GetText(TextType type)
        {
            Text[] texts = new Text[]
                               {
                                 new Text(ObjectId.GenerateNewId(),"{SenderName} has requested {EntityUrl}",TextType.NotificationFeedbackRequested),    
                                 new Text(ObjectId.GenerateNewId(),"{SenderName} commented {EntityUrl}",TextType.NotificationFeedbackCommented),    
                                 new Text(ObjectId.GenerateNewId(),"{SenderName} created objective {EntityUrl}",TextType.NotificationObjectiveCreated),    
                                 new Text(ObjectId.GenerateNewId(),"{SenderName} updated objective {EntityUrl}",TextType.NotificationObjectiveUpdated)    
                               };
            return texts.SingleOrDefault(t => t.Type == type);
        }
    }
}
