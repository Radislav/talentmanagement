﻿using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Data.Configuration;

namespace TalentManagement.Infrastructure.Data.Map
{
    class TeamMap : EntityConfiguration<Team>
    {
        public TeamMap()
        {
            AsCollection("Teams")
                .WithDataSource(DataSourceType.Shared);

            Property(t => t.Manager)
                .Required();

            Property(t => t.Name)
                .Required();
        }
    }
}
