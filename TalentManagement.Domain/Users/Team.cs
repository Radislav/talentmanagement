﻿using System;
using System.Diagnostics.Contracts;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;

namespace TalentManagement.Domain.Users
{
    public class Team : Entity , IOwnedByCompany
    {
        private const string Unassigned = "Unassigned";
        private List<string> _members = new List<string>();

        public static Team UnassignedTeam 
        {
            get
            {
                return new Team(Unassigned,Unassigned,Unassigned)
                    {
                        Id = ObjectId.Empty
                    };
            }
        }

        private Team()
        {

        }
        
        public Team(string name, string manager,string tenant)
        {
            Contract.Requires(false == string.IsNullOrEmpty(name));
            Contract.Requires(false == string.IsNullOrEmpty(manager));
            Contract.Requires(false == string.IsNullOrEmpty(tenant));

            Name = name;
            Manager = manager;
            CompanyName = tenant;
            if (manager != Unassigned)
            {
                _members.Add(manager);    
            }
            
        }
        public Team(User manager)
            : this(PopulateDefaultTeamNameFor(manager), manager.Id,manager.Tenant)
        {
            Contract.Requires(manager != null);
        }
        
        public static string PopulateDefaultTeamNameFor(User manager)
        {
            Contract.Requires(manager != null);
            Contract.Ensures(false == String.IsNullOrEmpty(Contract.Result<string>()));
            return manager.Names + "`s team";
        }

        public bool IsUnassigned
        {
            get { return Name == Unassigned; }
        }
        public string Name { get; set; }
        public string Manager { get; set; }
        public string CompanyName { get; set; }
        
        public IEnumerable<string> Members
        {
            get { return _members; }
            private set { _members = value == null ? new List<string>() : value.ToList(); }
        }

        public IEnumerable<string> MembersWithoutManager
        {
            get
            {
                return Members.Where(m => m != Manager);
            }
        }

        public bool AddMemeber(User member)
        {
            Contract.Requires(member != null);

            member.Team = Name;

            if (false == _members.Contains(member.Id))
            {
                _members.Add(member.Id);
                return true;
            }
            return false;
        }

        public bool DeleteMember(User member)
        {
            Contract.Requires(member != null);
            member.Team = null;
            return _members.Remove(member.Id);
        }


     
    }
}
