﻿using System;
using System.Web.Mvc;

namespace TalentManagement.Web.Models.Binder
{
    public abstract  class CustomModelBinder<T> :ICustomModelBinder<T>
    {
        protected string Prefix;
        protected int Index = -1;
        
        public abstract object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext);
        
        public ICustomModelBinder<T> SetPrefix(string prefix)
        {
            Prefix = prefix;
            return this;
        }

        public ICustomModelBinder<T> SetIndex(int index)
        {
            Index = index;
            return this;
        }
    }
}