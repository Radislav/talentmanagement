﻿using System.Diagnostics.Contracts;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Contracts;

namespace TalentManagement.Infrastructure.Services
{
    [ContractClass(typeof(RelationshipServiceContract))]
    internal interface IRelationshipService
    {
        void MarkAsManager(User manager);
        void NotManager(User manager);
        void ChangeTeam(User user, User previousManager, User newManager);
        void AddToTeam(User user, User newManager);
        void RemoveFromTeams(User user,User previousManager);
    }
}
