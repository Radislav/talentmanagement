﻿using FluentValidation;
using MongoDB.Bson;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.Companies.Validation
{
    internal class BadgeModelValidator : AbstractValidator<BadgeModel>,
                                        TalentManagement.Infrastructure.Validation.IValidator<BadgeModel>
    {
        public BadgeModelValidator()
        {
            RuleFor(m => m.Title)
                .NotEmpty();

            RuleFor(m => m.Image)
                .FilePosted()
                .When(m => m.BadgeId == default(ObjectId));

            RuleFor(m => m.Image)
                .FileSize(Constants.Megabyte)
                .ImageFile();

        }

        public TalentManagement.Infrastructure.Validation.ValidationResult ValidateInstance(BadgeModel instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}