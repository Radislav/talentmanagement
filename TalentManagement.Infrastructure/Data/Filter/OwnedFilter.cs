﻿using System.Linq;
using TalentManagement.Domain;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data.Filter
{
    internal class OwnedFilter : FilterBase<IOwned>
    {
        private readonly IPrincipalProvider _principalProvider;
        private  IFilterContext _context;

        public OwnedFilter(IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
        }

        public override IFilter<IOwned> Filter(IFilterContext[] contexts)
        {
            OwnedFilterContext ownedFilterContext = GetContextOrFail<OwnedFilterContext>(contexts);

            if (ownedFilterContext != null && (false == string.IsNullOrEmpty(ownedFilterContext.CreatedBy)))
            {
                Data = Data.Where(o => o.CreatedBy == ownedFilterContext.CreatedBy);
            }

            return this;
        }

        public override IFilterContext FilterContext
        {
            get
            {
                return _context ?? new OwnedFilterContext(_principalProvider.GetUserName());
            }
            set { _context = value; }
        }
    }
}
