﻿using System;

namespace TalentManagement.Infrastructure.Mapping
{
    public interface IMap<TSource,TDestination>
    {
        void Configure(IMapperConfiguration<TSource, TDestination> configuration);
    }
}
