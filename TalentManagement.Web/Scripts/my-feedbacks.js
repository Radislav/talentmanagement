﻿$(function () {
    init();

    //--------------------------------     USER PICKERS ----------------------------------------
    function initUserPicker(container) {

        $(container).find('#pickUserButton').off('click').on('click', function () {

            var data = new UserPicker({ container: container }).getPickerModel();

            $.ajax({
                url: urls.refreshUserPicker(),
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    $(container).find('.user-picker-control').html(response);
                    initUserPicker(container);
                }
            });
        });
    }


    //--------------------------------     ASK FOR FEEDBACK  ----------------------------------------
    function initCreateFeedback() {
        $('#create-feedback').off('click').on('click', function () {
            var data = {
                Question: $('#ask-feedback-modal #Question').val(),
                Users: new UserPicker({ container: $('#ask-feedback-modal') }).getPickerModel().UsersPicked
            };

            $.ajax({
                url: urls.createFeedback(),
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.IsValid) {
                        document.location = urls.index();
                    } else {
                        validation.showJsonErrors(response, '#ask-feedback-modal', 'FeedbackCreateModel');
                    }
                }
            });

        });

    }

    //--------------------------------   PROVIDE FEEDBACK  ----------------------------------------
    function initProvideFeedback() {
        //Click on provide feedback
        $('.attention-required .span7 a').off('click').on('click', function () {
            $(this).parents('.feedback').find('[id^=provide-feedback-modal]').modal('show');
        });

        $('[name="provide-feedback-send"]').off('click').on('click', function () {

            var data = {
                Response: $(this).parents('[id^=provide-feedback-modal]').find('#feedback_Response').val(),
                FeedbackId: $(this).parents('[id^=provide-feedback-modal]').find('#feedback_FeedbackId').val()
            };

            $.ajax({
                url: urls.addResponse(),
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.IsValid) {
                        $('#provide-feedback-modal-' + response.FeedbackId).modal('hide');
                        $('#activity_stats button').removeClass('active');
                        $('#they-requested').addClass('active');
                        updateContent(urls.feedbacks() + '/' + $('#they-requested').attr('name'));
                    } else {
                        validation.showJsonErrors(response, '#provide-feedback-modal-' + response.FeedbackId, 'feedback');
                    }
                }
            });
        });
    }

    function updateContent(url) {
        $.ajax({
            url: url,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                $('#feedbacks').html(response);
                init();
            }
        });
    }

    function getActiveButton() {
        return $('#activity_stats button.active').attr('id');
    }

    function extractBadgeId(badgeUrl) {
        var re = new RegExp("badgeId=([0-9a-zA-Z]+)", "g");
        badgeUrl.match(re);
        return RegExp.$1;
    }

    //--------------------------------   SAY KUDOS  ----------------------------------------
    function initKudos() {

        //Click on badge 
        $('#say-kudos-modal .badges img').each(function () {
            $(this).off('click').on('click', function () {

                var badgeUrl = $(this).attr('src');

                $('#kudos-current-badge img').attr('src', badgeUrl);
                $('#CurrentBadgeId').val(extractBadgeId(badgeUrl));
                $('#say-kudos-modal .badges img').removeClass('badge-selected');
                $(this).addClass('badge-selected');
                $('#kudos-back').trigger('click');
            });
        });

        //Click on "Change badge" link
        $('#kudos-change-badge').off('click').on('click', function () {
            $('#kudos-generic-properties, .modal-footer, .user-picker-control').css('display', 'none');
            $('#say-kudos-modal .badges').css('display', 'block');
        });

        //Click on "Back" link
        $('#kudos-back').off('click').on('click', function () {
            $('#kudos-generic-properties, .modal-footer, .user-picker-control').css('display', 'block');
            $('#say-kudos-modal .badges').css('display', 'none');
        });

        //Post KUDOS
        $('#kudos-post').off('click').on('click', function (e) {

            e.preventDefault();

            var picker = new UserPicker({ container: $('#say-kudos-modal') });

            var data = {
                Users: picker.getPickerModel().UsersPicked,
                Kudos: $('#Kudos').val(),
                CurrentBadgeId: $('#CurrentBadgeId').val(),
                CurrentUser : picker.getPickedUser()
            };
            $.ajax({
                url: $(this).attr('href'),
                data: JSON.stringify(data),
                success: function (response) {
                    if (response.IsValid) {
                        document.location = urls.index();
                    } else {
                        validation.showJsonErrors(response, '#say-kudos-modal', '');
                    }
                }
            });
        });
    }

    function init() {
        var activeButton = getActiveButton();

        if (activeButton == 'you-requested') {
            Comments({
                containerSelector: '.details',
                holderId: 'Details_response_ResponseId',
                handlerUrl: urls.commentMyFeedback()
            });
        }
        if (activeButton == 'they-requested') {
            Comments({
                containerSelector: '.feedback',
                holderId: 'feedbackTheyRequested_FeedbackId',
                handlerUrl: urls.commentTheirFeedback()
            });
        }

        //Click on details.response link. 
        $('.feedback .details-link a').off('click').on('click', function (event) {
            event.preventDefault();
            updateContent($(this).attr('href'));
        });

        initProvideFeedback();
        initUserPicker($('#ask-feedback-modal'));
        initUserPicker($('#say-kudos-modal'));
        initCreateFeedback();
        initKudos();
    }

    //Click on filtering buttons (You requested, They requested)
    $('#activity_stats [data-toggle="buttons-radio"] .btn')
        .click(function () {
            updateContent(urls.feedbacks() + '/' + $(this).attr('name'));
        });
});





