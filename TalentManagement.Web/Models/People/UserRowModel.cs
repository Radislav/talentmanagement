﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TalentManagement.Web.Models.People
{
    public class UserRowModel
    {
        public string FirstName { get; set; }

        public string SecondName { get; set; }

        public string FullName
        {
            get
            {
                if (string.IsNullOrEmpty(FirstName))
                {
                    return SecondName;
                }

                if (string.IsNullOrEmpty(SecondName))
                {
                    return FirstName;
                }

                return string.Format("{0}, {1}", SecondName, FirstName); 
            }
        }

        public string Position { get; set; }

        public string Team { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public bool IsManager { get; set; }
    }
}