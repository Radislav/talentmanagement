﻿using System.Configuration;

namespace TalentManagement.Web.Infrastructure.Bundles
{
    public class BundleSection : ConfigurationSection
    {
        [ConfigurationProperty("bundles", IsDefaultCollection = false),
         ConfigurationCollection(typeof (BundleElementsCollection), AddItemName = "bundle", ClearItemsName = "clear",RemoveItemName = "remove")]
        public BundleElementsCollection Bundles
        {
            get { return ((BundleElementsCollection)(base["bundles"])); }
        }
    }
}