﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TalentManagement.Domain;

namespace TalentManagement.Infrastructure.Converter
{
    class EnumLocalizationConverter<TEnum> : ITypeConverter<TEnum,string>
        where TEnum : struct
    {
        private readonly List<Tuple<string, string>> _localizedDescriptions;

        public EnumLocalizationConverter()
        {
            _localizedDescriptions = new List<Tuple<string, string>>();

            foreach (FieldInfo enumFieldInfo in typeof(TEnum).GetFields())
            {
                LocalizedDescriptionAttribute attribute = enumFieldInfo.GetCustomAttributes(typeof(LocalizedDescriptionAttribute), true)
                                                                       .OfType<LocalizedDescriptionAttribute>()
                                                                       .FirstOrDefault();

                _localizedDescriptions.Add(new Tuple<string, string>(enumFieldInfo.Name,attribute == null ? enumFieldInfo.Name : attribute.Description));
    
            }
        }
        
        public TEnum Convert(string to)
        {
            var description = FindDescriptionFor(to);

            if (description == null)
            {
                throw new FormatException(String.Format("Can not convert string '{0}' to type {1}",to,typeof(TEnum)));
            }

            TEnum result;
            if (false == Enum.TryParse(to,out result))
            {
                throw new FormatException(String.Format("Can not convert string '{0}' to type {1}", to, typeof(TEnum)));
            }

            return result;
        }

        private Tuple<string, string> FindDescriptionFor(string to)
        {
            return _localizedDescriptions.FirstOrDefault(d => d.Item2 == to) ?? _localizedDescriptions.FirstOrDefault(d => d.Item1 == to);
        }

        public string ConvertBack(TEnum from)
        {
            var description = FindDescriptionFor(from.ToString());
            return description == null ? from.ToString() : description.Item2;
        }


        public bool IsDefault
        {
            get { return false; }
        }


        public object Convert(object to)
        {
            return Convert((string)to);
        }

        public object ConvertBack(object from)
        {
            return ConvertBack((TEnum)from);
        }
    }
}
