﻿using System.Linq;
using TalentManagement.Domain.Users;

namespace TalentManagement.Infrastructure.Data.Filter
{
    internal class UserFilter : FilterBase<User>
    {
        public override Domain.IFilterContext FilterContext { get; set; }

        public override Domain.IFilter<User> Filter(Domain.IFilterContext[] filterContexts)
        {
            var context = GetContextOrFail<UserContext>(filterContexts);
            if (context != null)
            {
                Data = Data.Where(u => u.Status == context.Status);
                if (context.UserNamePrefix != null)
                {
                    Data = Data.Where(u => (u.SecondName ?? string.Empty).ToLower().StartsWith(context.UserNamePrefix.ToLower()));
                }
            }

            return this;
        }
    }
}
