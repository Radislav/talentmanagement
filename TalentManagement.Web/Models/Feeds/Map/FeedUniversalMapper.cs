﻿using TalentManagement.Domain.Feeds;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Session;

namespace TalentManagement.Web.Models.Feeds.Map
{
    public class FeedUniversalMapper : IFeedUniversalMapper
    {
        private readonly ISessionFactory _sessionFactory;
        public FeedUniversalMapper(ISessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;
        }

        public FeedModel Map(Feed source)
        {
            using (var session = _sessionFactory.Create())
            {
                return session.Instance<IMapper<Feed, FeedModel>>(source.FeedType).Map(source);
            }
        }
    }
}