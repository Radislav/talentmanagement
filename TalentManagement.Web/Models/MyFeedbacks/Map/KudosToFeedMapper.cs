﻿using System;
using System.Linq;
using TalentManagement.Domain.Feeds;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Web.Models.MyFeedbacks.Map
{
    internal class KudosToFeedMapper : IMapper<KudosCreateModel, Feed>
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly IFeedFactory _feedFactory;

        public KudosToFeedMapper(IPrincipalProvider principalProvider,IFeedFactory feedFactory)
        {
            _principalProvider = principalProvider;
            _feedFactory = feedFactory;
        }
        
       
        public Feed Map(KudosCreateModel source)
        {
            return _feedFactory.CreateKudos(source.Kudos, source.CurrentBadgeId, new []{source.CurrentUser}, 
                                            _principalProvider.GetOwnderDescriptor());
        }
    }
}