﻿using TalentManagement.Domain;
using TalentManagement.Domain.Users;

namespace TalentManagement.Infrastructure.Data.Filter
{
    public class UserContext : IFilterContext
    {
        public string UserNamePrefix { get; set; }

        public UserStatus Status { get; set; }
    }
}
