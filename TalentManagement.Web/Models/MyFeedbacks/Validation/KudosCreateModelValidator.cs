﻿using System.Linq;
using FluentValidation;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.MyFeedbacks.Validation
{
    internal class KudosCreateModelValidator  : AbstractValidator<KudosCreateModel>,
                                             TalentManagement.Infrastructure.Validation.IValidator<KudosCreateModel>
    {
        public KudosCreateModelValidator()
        {
            RuleFor(m => m.Kudos)
                .NotEmpty();

            RuleFor(m => m.CurrentUser)
                .NotEmpty()
                .OverridePropertyName("CurrentNames");

            /*
                .Must(users => users != null && users.Any())
                .OverridePropertyName("CurrentNames")
                .WithLocalizedMessage(() => Resources.Errors.UserAtLeastOne);
                 * */
        }

        public TalentManagement.Infrastructure.Validation.ValidationResult ValidateInstance(KudosCreateModel instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}