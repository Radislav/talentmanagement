﻿using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Domain.Objectives;
using TalentManagement.Web.Models.Binder;

namespace TalentManagement.Web.Models.Objectives.Binder
{
    internal class KeyResultModelBinder : CustomModelBinder<KeyResultModel>
    {
        private BinderValueProvider _valueProvider;
   
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            _valueProvider = new BinderValueProvider(bindingContext);

            return new KeyResultModel
            {
                CurrentValue = _valueProvider.GetValue<decimal>("CurrentValue", Prefix,Index),
                Due = _valueProvider.GetValue<string>("Due", Prefix,Index),
                Index = _valueProvider.GetValue<int>("Index",Prefix,Index),
                Name = _valueProvider.GetValue<string>("Name", Prefix, Index),
                TargetValue = _valueProvider.GetValue<decimal>("TargetValue", Prefix, Index),
                Unit = _valueProvider.GetValue<KeyResultUnit>("Unit", Prefix, Index),
                Id = _valueProvider.GetValue<ObjectId>("Id",Prefix,Index)
            };
        }
    }
}