﻿using FluentValidation;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.MyAccount.Validation
{
    internal class IndexModelValidator : AbstractValidator<IndexModel>,
                                        TalentManagement.Infrastructure.Validation.IValidator<IndexModel>
    {
        public IndexModelValidator()
        {
            RuleFor(m => m.FirstName)
                .NotEmpty();

            RuleFor(m => m.SecondName)
                .NotEmpty();

            RuleFor(m => m.ManagerEmailUserName)
                .Matches(RegularExpressions.EmailUserName)
                .WithLocalizedMessage(() => Resources.Errors.email_error);
        }

        public TalentManagement.Infrastructure.Validation.ValidationResult ValidateInstance(IndexModel instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}