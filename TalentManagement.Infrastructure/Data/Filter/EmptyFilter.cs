﻿using System.Collections;
using TalentManagement.Domain;

namespace TalentManagement.Infrastructure.Data.Filter
{
    class EmptyFilter : IFilter
    {
        private IEnumerable _data;
        public IFilter Init(IEnumerable data)
        {
            _data = data;
            return this;
        }

        public IFilter Filter(IFilterContext[] contexts)
        {
            return this;
        }

        public IFilter Filter()
        {
            return this;
        }

        public IFilter Chain(IFilter chainedFilter, IFilterContext[] contexts)
        {
            return this;
        }

        public IFilter Chain(IFilter chainedFilter)
        {
            return this;
        }

        public IEnumerable AsEnumerable()
        {
            return _data;
        }

        public int Priority
        {
            get { return 0; }
        }

        public IFilterContext GetContext()
        {
            return null;
        }


        public IFilterContext FilterContext { get; set; }
    }
}
