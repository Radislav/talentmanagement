﻿using TalentManagement.Domain.Companies;

namespace TalentManagement.Web.Models.Companies.Map
{
    internal class BadgeModelMapper : IBadgeModelMapper
    {
        public BadgeModel Map(MongoDB.Bson.ObjectId companyId, Badge badge)
        {
            return new BadgeModel
                       {
                           CompanyId = companyId,
                           Description = badge.Description,
                           BadgeId = badge.Id,
                           Title = badge.Title,
                           IsEnabled = badge.IsEnabled
                       };
        }
    }
}