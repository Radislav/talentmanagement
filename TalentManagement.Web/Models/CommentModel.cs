﻿using System;
using System.Web.Mvc;
using TalentManagement.Infrastructure.Cache;

namespace TalentManagement.Web.Models
{
    public class CommentModel
    {
        public CommentModel()
        {
            CreatedAt = DateTime.Now;
        }

        public CommentModel(string createdBy,DateTime createdAt,string content)
        {
            CreatedBy = createdBy;
            CreatedAt = createdAt;
            Content = content;
        }

        private string _createdBy;
        public string CreatedBy
        {
            get { return _createdBy; }
            set {
                _createdBy = value;
                if (false == string.IsNullOrEmpty(value))
                {
                    CreatorNames = DependencyResolver.Current.GetService<IGlobalCache>().GetUserNames(_createdBy);
                }
            }
        }

        public string CreatorNames { get;private set; }

        [Watermark("Content",typeof(Resources.Models.CommentModel))]
        public string Content { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}