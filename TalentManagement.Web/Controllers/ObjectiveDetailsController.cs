﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Domain;
using TalentManagement.Domain.Feeds;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Data.Filter;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Infrastructure.Validation;
using TalentManagement.Web.Infrastructure.Security;
using TalentManagement.Web.Models.Feeds.Map;
using TalentManagement.Web.Models.ObjectiveDetails;
using TalentManagement.Web.Models.ObjectiveDetails.Map;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Controllers
{
    public class ObjectiveDetailsController : Controller
    {
        private readonly IObjectiveRepository _objectiveRepository;
        private readonly IObjectiveDetailsMapper _objectiveDetailsModelMapper;
        private readonly IMapper<ActionItemModel, ActionItem> _actionItemMapper;
        private readonly IMapper<KeyResult, KeyResultModel> _keyResultDetailsModelMapper;
        private readonly IFeedRepository _feedRepository;
        private readonly IMyFeedRepository _myFeedRepository;
        private readonly IFeedUniversalMapper _feedModelMapper;
        private readonly IFilter<IHasScope> _scopeFilter;
        private readonly IResourceProvider _resourceProvider;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IFeedFactory _feedFactory;

        public ObjectiveDetailsController(IObjectiveDetailsMapper objectiveDetailsModelMapper,
                                   IMapper<ActionItemModel,ActionItem> actionItemMapper,
                                   IMapper<KeyResult, KeyResultModel> keyResultDetailsModelMapper,
                                   IObjectiveRepository objectiveRepository,
                                   IFeedRepository feedRepository,
                                   IMyFeedRepository myFeedRepository,
                                   IFeedUniversalMapper feedModelMapper,
                                   IFilter<IHasScope> scopeFilter,
                                   IResourceProvider resourceProvider,
                                   IPrincipalProvider principalProvider,
                                   IFeedFactory feedFactory)
        {

            _objectiveRepository = objectiveRepository;
            _feedRepository = feedRepository;
            _myFeedRepository = myFeedRepository;
            _feedModelMapper = feedModelMapper;
            _scopeFilter = scopeFilter;
            _objectiveDetailsModelMapper = objectiveDetailsModelMapper;
            _actionItemMapper = actionItemMapper;
            _keyResultDetailsModelMapper = keyResultDetailsModelMapper;
            _resourceProvider = resourceProvider;
            _principalProvider = principalProvider;
            _feedFactory = feedFactory;
        }

        [HttpGet]
        [TenantAuthorization(Roles = SecurityConstants.ContributorRole)]
        public ActionResult Index(ObjectId id)
        {
            Objective objective = _objectiveRepository.GetById(id);
            _scopeFilter.FilterContext = new ScopeFilterContext(objective);

            IEnumerable<Feed> feeds = _myFeedRepository.GetFeeds(_scopeFilter);
            ObjectiveDetailsModel model = _objectiveDetailsModelMapper.Map(objective,feeds);
            
            return View(model);
        }

        [HttpPost]
        [TenantAuthorization(Roles = SecurityConstants.ContributorRole)]
        public ActionResult AddActionItem(ObjectId objectiveId, KeyResultModel keyResultDetailsModel)
        {
            ValidationResult validationResult = ModelState.ValidateAndKeepState(keyResultDetailsModel.NewActionItem, "NewActionItem");
            Objective objective = _objectiveRepository.GetById(objectiveId);

            if (validationResult.IsValid)
            {
                objective.UpsertActionItem(keyResultDetailsModel.Id, _actionItemMapper.Map(keyResultDetailsModel.NewActionItem));
                _objectiveRepository.Update(objective);
            }

            KeyResult keyResult = objective.GetKeyResult(keyResultDetailsModel.Id);
            
            return PartialView("_ActionItemsList", keyResultDetailsModel.PersistPickedContributors(objective, keyResult, validationResult.IsValid));
        }

        [HttpPost]
        [TenantAuthorization(Roles = SecurityConstants.ContributorRole)]
        public ActionResult DeleteActionItem(ObjectId objectiveId,
                                             ObjectId keyResultId,
                                             ObjectId actionItemId)
        {
            Objective objective = _objectiveRepository.GetById(objectiveId);
            objective.DeleteActionItem(keyResultId, actionItemId);
            _objectiveRepository.Update(objective);

            KeyResult keyResult = objective.GetKeyResult(keyResultId);
            
            return PartialView("_ActionItemsList", _keyResultDetailsModelMapper.Map(keyResult)
                                                                               .PersistPickedContributors(objective,keyResult,true));
        }

        [HttpPost]
        public ActionResult UpdateProgress(ObjectId objectiveId, decimal progress)
        {
            Objective objective = _objectiveRepository.GetById(objectiveId);
            objective.Progress = progress;
            _objectiveRepository.Update(objective);

            return Json(new { message = _resourceProvider.ObjectiveDetailsProgressUpdated(objective.Name,objective.Progress) });
        }

        [HttpPost]
        public ActionResult CompleteObjective(ObjectId objectiveId)
        {
            Objective objective = _objectiveRepository.GetById(objectiveId);
            objective.Status = ObjectiveStatus.Completed;
            _objectiveRepository.Update(objective);
            return Json(new { message = string.Format(_resourceProvider.ObjectiveDetailsCompleted,objective.Name),progress = objective.Progress });
        }

        [HttpPost]
        public ActionResult PostFeed(ObjectId objectiveId,string feedText)
        {
            IScope scope = new Scope("Objective", Convert.ToString(objectiveId));
            Feed feed = _feedFactory.CreateUpdatingFeed(feedText, scope, _principalProvider.GetOwnderDescriptor());
            _feedRepository.Insert(feed);

            _scopeFilter.FilterContext = new ScopeFilterContext(scope);
            IEnumerable<Feed> feeds = _myFeedRepository.GetFeeds(_scopeFilter);
            FeedsModel feedsModel = new FeedsModel
                                        {
                                            Feeds = feeds.Select(f => _feedModelMapper.Map(f)).ToArray()
                                        };

            return PartialView("_Feeds", feedsModel);

        }

    }
}
