﻿using System;
using System.Linq;
using System.Collections.Generic;
using TalentManagement.Domain;
using TalentManagement.Domain.Events;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data.Repositories
{
    class UserRepository : RepositoryWithTypedId<User,string> , IUserRepository
    {
        private readonly Lazy<IDomainEvents> _domainEvents;
        
        public UserRepository(IUnitOfWork unitOfWork,Lazy<IDomainEvents> domainEvents,IPrincipalProvider principalProvider)
            : base(unitOfWork,principalProvider)
        {
            _domainEvents = domainEvents;
        }

        public string[] GetUserRoles(string userName)
        {
            return new []{"Contributor"};
        }
        
        public IEnumerable<User> GetUsersForTenant(string tenantName)
        {
            return UnitOfWork.AsQueryable<User>()
                .Where(u => u.Tenant == tenantName)
                .ToArray();
        }

      
        public IEnumerable<User> GetUsers(IEnumerable<string> userNames)
        {
            return UnitOfWork.AsQueryable<User>()
                             .Where(u => userNames.Contains(u.Id))
                             .ToArray();
        }


        public void UpdateSecondaryProperties(User user)
        {
            User userToUpdate = GetById(user.Id);
            if (userToUpdate == null)
            {
                throw new EntityNotFoundException(typeof(User),user.Id);
            }
            User previous = (User)userToUpdate.Clone();

            userToUpdate.FirstName = user.FirstName;
            userToUpdate.ManagerEmail = user.ManagerEmail;
            userToUpdate.Position = user.Position;
            userToUpdate.SecondName = user.SecondName;

            base.Update(userToUpdate);
            _domainEvents.Value.Raise(new UserUpdatedArgs(userToUpdate, previous));
        }

        public override void Update(User entity)
        {
            User previousUser = GetById(entity.Id);
            base.Update(entity);
            _domainEvents.Value.Raise(new UserUpdatedArgs(entity, previousUser));
        }
        
        public void UpdateNotificationSettings(User user)
        {
            User userToUpdate = GetById(user.Id);
            if (userToUpdate == null)
            {
                throw new EntityNotFoundException(typeof(User), user.Id);
            }

            User previous = (User)userToUpdate.Clone();
            userToUpdate.NotificationSettings = user.NotificationSettings;
            base.Update(userToUpdate);
            _domainEvents.Value.Raise(new UserUpdatedArgs(userToUpdate, previous));
        }


        public UserCreateResult CreateUser(User user)
        {
            User previouslySaved = GetById(user.Id);
            if (previouslySaved != null)
            {
                return UserCreateResult.Duplicate;
            }
            Update(user);
            return UserCreateResult.Ok;
        }
        
        public IEnumerable<IGrouping<string, User>> GetGroupedUsersForTenant(string tenantName, IFilter filter)
        {
            var users = GetUsersForTenantInternal(tenantName, filter)
                .OrderBy(x => x.Team)
                .ThenBy(x => x.SecondName)
                .ToArray();

            return users.GroupBy(x => x.Team);
        }

        public IEnumerable<User> GetUsersForTenant(string tenantName, IFilter filter)
        {
            return GetUsersForTenantInternal(tenantName, filter)
                .OrderBy(x => x.SecondName)
                .ToArray();
        }
        
        private IEnumerable<User> GetUsersForTenantInternal(string tenantName, IFilter filter)
        {
            var users = UnitOfWork.AsQueryable<User>()
                .Where(u => u.Tenant == tenantName)
                .ToArray();

            if (filter != null)
            {
                return filter.Init(users)
                    .Filter()
                    .AsEnumerable()
                    .Cast<User>();
            }

            return users;
	}

        public IEnumerable<User> GetSubordinatesOf(IEnumerable<string> managersUserNames, IFilter filter)
        {
            return UnitOfWork.ApplyFilter<User>(filter)
                             .Where(u => managersUserNames.Contains(u.ManagerEmail));
        }

        public IEnumerable<User> GetSubordinatesOf(string manager, IFilter filter)
        {
            return GetSubordinatesOf(new [] {manager}, filter);
        }
    }
}
