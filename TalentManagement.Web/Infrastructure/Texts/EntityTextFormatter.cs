﻿using TalentManagement.Domain.Events;
using TalentManagement.Domain.Texts;

namespace TalentManagement.Web.Infrastructure.Texts
{
    internal class EntityTextFormatter : ITextFormatter<NotificableEventArgs>
    {
        public virtual string Format(string format, NotificableEventArgs argument)
        {
            return format.Replace("{SenderName}", argument.SenderId);
        }

        public virtual string Format(string format, object argument)
        {
            return Format(format, argument as NotificableEventArgs);
        }

        protected string Hyperlink(string text, string url)
        {
            return string.Format("<a href='{0}'>{1}</a>", url, text);
        }
    }
}
