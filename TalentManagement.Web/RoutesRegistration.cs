﻿using System.Web.Mvc;
using System.Web.Routing;

namespace TalentManagement.Web
{
    public static class RoutesRegistration
    {

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            
            routes.MapRoute("Activation", "Activation/{code}", new { controller = "Activation", action = "Activate" });
            routes.MapRoute("Registration", "Registration/{code}", new { controller = "Registration", action = "Register" });
            routes.MapRoute("Logout", "Logout", new {controller = "Login", action = "Logout"});
            routes.MapRoute("Login", "Login", new {controller = "Login", action = "Login"});

            routes.MapRoute("Objective", "Objective/Details/{id}",
                            new { controller = "ObjectiveDetails", action = "Index" });

            routes.MapRoute("PerformanceSummary", "PerformanceSummary/{cycleId}/{respondent}",
                            new {controller = "PerformanceSummary", action = "Index"});

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new {controller = "Login", action = "Index", id = UrlParameter.Optional}); //Defaults

            routes.MapRoute("ObjectiveDetails", "ObjectiveDetails/{id}",
                            new { controller = "ObjectiveDetails", action = "Index" });
            

           
        }
    }
}