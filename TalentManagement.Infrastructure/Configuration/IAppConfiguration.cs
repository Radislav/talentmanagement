﻿using System.Diagnostics.Contracts;
using TalentManagement.Infrastructure.Contracts;

namespace TalentManagement.Infrastructure.Configuration
{
    [ContractClass(typeof(AppConfigurationContract))]
    public interface IAppConfiguration
    {
        string GetConfiguration(string key);
        string GetConnectionString(string name);
        string GetTenantName();
        string GetDbName(DataSourceType dataSourceType);
        int MinPasswordLength { get; }
        EmailSettings EmailSettings { get; }
        int AvatarWidth { get; }
        int AvatarHeight { get; }
        string AvatarImageType { get; }
    }
}
