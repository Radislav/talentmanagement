﻿using System;
using System.Resources;

namespace TalentManagement.Web.Models
{
    [AttributeUsage(AttributeTargets.Property |AttributeTargets.Class, AllowMultiple = false)]
    public class WatermarkAttribute : Attribute
    { 
        private readonly string _watermark;

        public WatermarkAttribute(string resorceKey, Type resourceType)
        {
            _watermark = new ResourceManager(resourceType).GetString(resorceKey);
        }

        public string Watermark
        {
            get { return _watermark; }
        }
    }
}