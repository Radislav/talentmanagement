﻿namespace TalentManagement.Web.Infrastructure.UI
{
    public enum ImageSize
    {
        Actual = 0,
        Default = 79,
        Small = Default,
        Smaller = 50,
        Smallest = 40,
        Medium = 100
    }
}