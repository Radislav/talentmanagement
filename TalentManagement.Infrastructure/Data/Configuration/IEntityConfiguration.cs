﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace TalentManagement.Infrastructure.Data.Configuration
{
    public interface IEntityConfiguration<TEntity> : IEntityReadConfiguration
    {
        IEntityConfiguration<TEntity> AsCollection(string collectionName);
        IEntityConfiguration<TEntity> WithDataSource(DataSourceType dataSourceType);
        IEntityConfiguration<TEntity> UseSafeMode(bool safeMode);
        IEnumerable<IPropertyReadConfiguration<TEntity>> PropertyConfigurations { get; }
        IPropertyConfiguration<TEntity, TProperty> Property<TProperty>(Expression<Func<TEntity, TProperty>>  property);
    }
}
