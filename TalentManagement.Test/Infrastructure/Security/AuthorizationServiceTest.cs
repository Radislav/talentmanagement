﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Autofac;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Test.Infrastructure.Security
{
    [TestClass]
    public class AuthorizationServiceTestClass : MockedTestClass<IAuthorizationService>
    {
        [TestMethod]
        public void CreatePrincipal_should_load_user_roles()
        {
            var target = CreateTarget();
            target.LoadPrincipal(String.Empty);

            Container.Resolve<Mock<IUserRepository>>()
                .Verify(rep => rep.GetUserRoles(String.Empty),Times.Once());
        }

        [TestMethod]
        public void CreatePrincipal_should_create_Identity()
        {
            var target = CreateTarget();

            TenantPrincipal result = target.LoadPrincipal("name");

            Assert.IsTrue(result.Identity.IsAuthenticated);
            Assert.AreEqual("name",result.Identity.Name);
        }

        protected override void RegisterMocks(ContainerBuilder containerBuilder)
        {
            Mock<IUserRepository> mock = new Mock<IUserRepository>();
            mock.Setup(m => m.GetUserRoles(It.IsAny<String>())).Returns(new string[] {"Role"});

            string email = null;
            mock.Setup(m => m.GetById(It.IsAny<string>())).Callback<string>(id => email = id).Returns(new User(email));

            containerBuilder.RegisterMock(mock);
        }

    }
}
