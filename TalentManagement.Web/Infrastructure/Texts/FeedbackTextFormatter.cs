﻿using TalentManagement.Domain.Events;
using TalentManagement.Infrastructure;

namespace TalentManagement.Web.Infrastructure.Texts
{
    class FeedbackTextFormatter : EntityTextFormatter
    {
      private readonly IUrlProvider _urlProvider;

         public FeedbackTextFormatter(IUrlProvider urlProvider)
        {
            _urlProvider = urlProvider;
        }

         public override string Format(string format, NotificableEventArgs argument)
         {
             string hyperlink = Hyperlink("feedback", _urlProvider.FeedbacksUrl);

             return base.Format(format, argument).Replace("{EntityUrl}", hyperlink);
         }
    }
}
