﻿using System;

namespace TalentManagement.Infrastructure.Cache
{
    public interface ICache
    {
        TItem Get<TItem>(string key);
        TItem Get<TItem>();

        void Set<TItem>(TItem item, string key);
        void Set<TItem>(TItem item);

        void Delete(string key);
        void Delete<TItem>();

        TItem GetOrRefresh<TItem>(string key, Func<TItem> refreshFunctor);
        TItem GetOrRefresh<TItem>(Func<TItem> refreshFunctor);
    }
}
