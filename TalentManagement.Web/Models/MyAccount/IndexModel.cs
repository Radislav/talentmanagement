﻿using System.Web;
using FluentValidation.Attributes;
using TalentManagement.Web.Models.MyAccount.Validation;
using Resource = TalentManagement.Resources.Models.MyAccount.IndexModel;
using ProfileResource = TalentManagement.Resources.Models.ProfileModel;

namespace TalentManagement.Web.Models.MyAccount
{
    [LocalizedDisplayName("Title", typeof(Resource))]
    [Validator(typeof(IndexModelValidator))]
    public class IndexModel
    {
        private readonly EmailAppendixModel _emailAppendixModel = new EmailAppendixModel("");
        
        [LocalizedDisplayName("FirstName",typeof(ProfileResource))]
        public string FirstName { get; set; }

        [LocalizedDisplayName("SecondName", typeof(ProfileResource))]
        public string SecondName { get; set; }
        [LocalizedDisplayName("Position", typeof(ProfileResource))]
        public string Position { get; set; }

        [LocalizedDisplayName("ManagerEmail", typeof(ProfileResource))]
        public string ManagerEmailUserName { get; set; }

        public string ManagerEmail
        {
            get { return _emailAppendixModel.CreateEmail(ManagerEmailUserName); }
        }

        public string EmailAppendix
        {
            get { return _emailAppendixModel.EmailAppendix; }
        }

        private HttpPostedFileBase _profileImage;

        [LocalizedDisplayName("ProfileImage", typeof(ProfileResource))]
        public HttpPostedFileBase ProfileImage
        {
            get { return _profileImage; }
            set
            {
                _profileImage = value;
                HttpPostedFileWrapper wrapper = new HttpPostedFileWrapper(value);
                _profileImageType = wrapper.ContentType;
                _profileImageBytes = wrapper.Bytes;
            }
        }

        public bool HasImage {
            get { return false == string.IsNullOrEmpty(_profileImageType) && null != _profileImageBytes; }
        }

        private byte[] _profileImageBytes;
        public byte[] ProfileImageBytes
        {
            get { return _profileImageBytes; }
        }

        private string _profileImageType;
        public string ProfileImageType
        {
            get { return _profileImageType; }
        }

        public string UserId { get; set; }
        
    }
}