﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using MongoDB.Bson;
using TalentManagement.Domain.Contracts;

namespace TalentManagement.Domain.Companies
{
    [ContractClass(typeof(BadgeRepositoryContract))]
    public interface IBadgeRepository : IRepository<Badge>
    {
        IEnumerable<Badge> GetBadgesLight(ObjectId companyId);
        IEnumerable<Badge> GetBadgesLight(IEnumerable<ObjectId> badgeIds);
    }
}
