﻿using System.Collections.Generic;

namespace TalentManagement.Domain.Feeds
{
    public interface IMyFeedRepository : IRepository<Feed>
    {
        IEnumerable<Feed> GetFeeds(IFilter filter);
    }
}
