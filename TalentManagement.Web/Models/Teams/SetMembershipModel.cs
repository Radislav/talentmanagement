﻿using System.Linq;
using MongoDB.Bson;

namespace TalentManagement.Web.Models.Teams
{
    public class SetMembershipModel
    {
        public string UserId { get; set; }
        public string ManagerId { get; set; }
        public TeamExpandPair[] ExpandMap { get; set; }

        public bool IsTeamExpanded(ObjectId teamId)
        {
            return ExpandMap.SingleOrDefault(p => p.TeamId == teamId)
                             .GetSafe(p => p.IsExpanded);
        }
    }
}