﻿namespace TalentManagement.Domain.Users
{
    public enum UserStatus
    {
        Active,
        Inactive,
        Disabled,
        Deleted
    }
}
