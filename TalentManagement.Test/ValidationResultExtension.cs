﻿using System.Linq;
using TalentManagement.Infrastructure.Validation;

namespace TalentManagement.Test
{
    public static class ValidationResultExtension
    {
        public static  bool HasMessage(this ValidationResult result, string message, string propertyName)
        {
            if (result.IsValid)
            {
                return false;
            }
            string formattedMessage = message.Contains("{PropertyName}")
                                          ? message.Replace("{PropertyName}", propertyName)
                                          : message;
            return result.Failures.Any(f => f.PropertyName == propertyName && f.ErrorMessage == formattedMessage);
        }
    }
}
