﻿using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Map
{
    class ObjectiveSimpleModelMap : IMap<Objective,ObjectiveSimpleModel>
    {
        public void Configure(IMapperConfiguration<Objective, ObjectiveSimpleModel> configuration)
        {
            configuration.ForMember(d => d.Due, s => s.Due.AsShortDateString());
            configuration.ForMember(d => d.IconId, s => s.Icon);
            configuration.ForMember(d => d.Name, s => s.Name);
            configuration.ForMember(d => d.Id, s => s.Id);
        }
    }
}