﻿using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data.Repositories
{
    class TeamRepository : Repository<Team> , ITeamRepository
    {
        private readonly IUserRepository _userRepository;

        public TeamRepository(IUnitOfWork unitOfWork, IPrincipalProvider principalProvider,IUserRepository userRepository)
            : base(unitOfWork,principalProvider)
        {
            _userRepository = userRepository;
        }
        
        public IEnumerable<Team> GetTeams(User manager)
        {
            return GetTeams(manager.Id, manager.Tenant);
        }


        public Team GetUnassignedTeam(string tenant)
        {
            Team team = Team.UnassignedTeam;

            IEnumerable<string> assignedUserIds = AsQueryable()
                .Where(t => t.CompanyName == tenant)
                .Select(t => t.Members)
                .ToArray()
                .SelectMany(group => group.Select(g => g));

            IEnumerable<User> users = _userRepository.GetUsersForTenant(tenant).ToArray();
                                    
            users.Select(u => u.Id)
                 .Except(assignedUserIds)
                 .Map(unassignedUserId =>
                     {
                         User unassignedUser = users.SingleOrDefault(u => u.Id == unassignedUserId);
                         team.AddMemeber(unassignedUser);
                     });

            return team;
        }


        public IEnumerable<Team> GetTeams(string managerId, string tenant)
        {
            return UnitOfWork.AsQueryable<Team>()
                            .Where(t => t.Manager == managerId && t.CompanyName == tenant)
                            .ToArray();
        }


        public bool HasTeams(User manager)
        {
            return UnitOfWork.AsQueryable<Team>()
                             .Any(t => t.Manager == manager.Id && t.CompanyName == manager.Tenant);
        }


        public IEnumerable<Team> GetTeams(string tenant, bool includeUnassigned)
        {
            List<Team> teams = UnitOfWork.AsQueryable<Team>()
                                         .Where(t => t.CompanyName == tenant)
                                         .ToList();

            if (includeUnassigned)
            {
                teams.Add(GetUnassignedTeam(tenant));
            }

            return teams;
        }
    }
}
