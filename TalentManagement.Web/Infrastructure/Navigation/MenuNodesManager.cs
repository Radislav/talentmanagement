﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MvcSiteMapProvider.Web.Html.Models;
using TalentManagement.Infrastructure.Cache;

namespace TalentManagement.Web.Infrastructure.Navigation
{
    public static class MenuNodesManager
    {
        private const string NodesKey = "MenuNodesShowForced";

        public static void ShowMenuNode(params string[] descriptions)
        {
            IUserCache cache = DependencyResolver.Current.GetService<IUserCache>();
            cache.Set(descriptions,NodesKey);
        }

        public static SiteMapNodeModel[] GetMenuNodesToShow(IEnumerable<SiteMapNodeModel> nodes)
        {
            IUserCache cache = DependencyResolver.Current.GetService<IUserCache>();
            string[] menusToShow = cache.Get<string[]>(NodesKey);

            if (menusToShow == null)
            {
                return nodes.Where(n => n.IsInCurrentPath).ToArray();    
            }
            cache.Delete(NodesKey);
            return nodes.Where(n => n.IsInCurrentPath || menusToShow.Contains(n.Description)).ToArray();
        }
    }
}