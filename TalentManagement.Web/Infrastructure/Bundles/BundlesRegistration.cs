﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;
using System.Web.Optimization;
using BundleTransformer.Core.Orderers;
using BundleTransformer.Core.Transformers;
using BundleTransformer.Yui.Minifiers;


namespace TalentManagement.Web.Infrastructure.Bundles
{
    public static class BundlesRegistration
    {
        private static readonly IBundleTransform CssTransformer = new CssTransformer(new YuiCssMinifier());
        private static readonly IBundleTransform JsTransformer = new JsTransformer(new YuiJsMinifier());
        private static readonly IBundleOrderer Orderer = new NullOrderer();
        private static BundleCollection _bundles;

        private static readonly SortedList<string, List<string>> ViewsMap = new SortedList<string, List<string>>();
        private static readonly SortedList<string, BundleElement> NamesMap = new SortedList<string, BundleElement>();

        public static void RegisterBundles(BundleCollection bundles)
        {
            _bundles = bundles;

            BundleSection bundleSection = (BundleSection)WebConfigurationManager.GetSection("webBundle");
            foreach (BundleElement bundleElement in bundleSection.Bundles)
            {
                RegisterBundle(bundleElement.Name,
                          GetTransform(bundleElement.BundleType),
                          bundleElement.Urls);

                NamesMap.Add(bundleElement.Name,bundleElement);

                string viewName = bundleElement.ForView;
                if (false == string.IsNullOrEmpty(viewName))
                {
                    if (false == ViewsMap.ContainsKey(viewName))
                    {
                        ViewsMap[viewName] = new List<string>();
                    }
                    ViewsMap[viewName].Add(bundleElement.Name);
                }
            }
        }

        public static IEnumerable<BundleElement> GetBundlesForName(string name)
        {
            return NamesMap.Where(m => m.Key == name).Select(m => m.Value);
        }

        public static IEnumerable<BundleElement> GetBundlesForView(string viewUrl)
        {
            return ViewsMap.Where(m => m.Key == viewUrl)
                .SelectMany(map => map.Value)
                .Select(name => NamesMap[name]);
        }

        private static  IBundleTransform GetTransform(BundleType bundleType)
        {
            switch (bundleType)
            {
               case BundleType.css:
                    return CssTransformer;
               case BundleType.js:
                    return JsTransformer;
                default : throw new NotSupportedException(String.Format("Bundle type {0} is not supported.",bundleType));
            }
        }

        private static void RegisterBundle(string name, IBundleTransform transform, params string[] files)
        {
            var bundle = new Bundle(name);
            bundle.Include(files);
            bundle.Transforms.Add(transform);
            bundle.Orderer = Orderer;
            _bundles.Add(bundle);
        }
    }
}