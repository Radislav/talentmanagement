﻿using System;
using System.Web;
using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Infrastructure.Resources;

namespace TalentManagement.Web.Models.Companies
{
    public class BadgeModel : IHasCachedImage
    {
        public BadgeModel()
        {
            IsEnabled = true;
        }

        public string Title { get; set; }
        public ObjectId BadgeId { get; set; }
        public string Description { get; set; }
        public ObjectId CompanyId { get; set; }

        private HttpPostedFileBase _image;
        private HttpPostedFileWrapper _imageWrapper;
        public HttpPostedFileBase Image
        {
            get { return _image; }
            set
            {
                _image = value;
                _imageWrapper = new HttpPostedFileWrapper(_image);

            }
        }

        public byte[] ImageBytes
        {
            get { return _imageWrapper.GetSafe(w => w.Bytes); }
        }

        public string ImageType
        {
            get { return _imageWrapper.GetSafe(w => w.ContentType); }
        }

        public bool SubmitClicked { get; set; }

        public string GenerateHtmlId(string prefix)
        {
            return string.Format("{0}-{1}", BadgeId, prefix);
        }

        public bool IsEnabled { get; set; }

        public string EnableAction
        {
            get {
                return GetEnablingAction(IsEnabled);
            }
        }

        public static string GetEnablingAction(bool isEnabled)
        {
            var resourceProvider = DependencyResolver.Current.GetService<IResourceProvider>();
            return isEnabled ? resourceProvider.Disable : resourceProvider.Enable;
        }
        
        public string GenerateCacheKey()
        {
            return GenerateCacheKey(CompanyId, BadgeId);
        }

        public static string GenerateCacheKey(ObjectId companyId,ObjectId badgeId)
        {
            return String.Format("Badge_{0}_{1}", companyId, badgeId);
        }
    }
}