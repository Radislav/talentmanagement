﻿using TalentManagement.Domain.Feedbacks;
using TalentManagement.Infrastructure.Data.Configuration;

namespace TalentManagement.Infrastructure.Data.Map
{
    class FeedbackMap : EntityConfiguration<Feedback>
    {
        public FeedbackMap()
        {
            AsCollection("Feedbacks")
                .WithDataSource(DataSourceType.Tenant);

            Property(p => p.Participants)
                .Ignore();

            Property(p => p.CompanyName)
                .Required();

            Property(p => p.CreatedBy)
                .Required();

            Property(p => p.Question)
                .Required();

            Property(p => p.Responders)
                .Required();

        }
    }
}
