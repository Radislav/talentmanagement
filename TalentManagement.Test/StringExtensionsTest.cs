﻿using System;
using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TalentManagement.Test
{
    [TestClass]
    public class StringExtensionsTest
    {
        [TestMethod]
        public void EmailUserName_should_extract_username_from_email()
        {
            Assert.AreEqual("username",StringExtensions.EmailUserName("username@domain.org"));
        }

        [TestMethod]
        public void EmailUserName_should_not_fail()
        {
            StringExtensions.EmailUserName("");
            StringExtensions.EmailUserName(null);
            StringExtensions.EmailUserName("*(&^*(&^!@");

            string v = DateTime.Now.ToString("YYYY");
        }
    }
}
