﻿using TalentManagement.Domain.Events;
using TalentManagement.Infrastructure;

namespace TalentManagement.Web.Infrastructure.Texts
{
    class ObjectiveTextFormatter : EntityTextFormatter
    {
      private readonly IUrlProvider _urlProvider;

      public ObjectiveTextFormatter(IUrlProvider urlProvider)
        {
            _urlProvider = urlProvider;
        }

      public override string Format(string format, NotificableEventArgs argument)
         {
             string hyperlink = Hyperlink("objective",_urlProvider.GetObjectiveDetailsUrl(argument.Entity.Id));

             return base.Format(format, argument).Replace("{EntityUrl}", hyperlink);
         }
    }
}
