﻿using System.Web.Mvc;
using TalentManagement.Infrastructure.Session;

namespace TalentManagement.Web.Infrastructure
{
    public static class DependencyResolverExtensions
    {
        public static TService GetService<TService>(this IDependencyResolver resolver, object key)
        {
            return resolver.GetService<ISessionFactory>().Create().Instance<TService>(key);
        }
        public static TService GetServiceOptional<TService>(this IDependencyResolver resolver, object key)
            where TService : class 
        {
            return resolver.GetService<ISessionFactory>().Create().InstanceOptional<TService>(key);
        }
    }
}