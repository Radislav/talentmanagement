﻿using System.Web;
using System.Web.Helpers;

namespace TalentManagement.Web.Models
{
    public class HttpPostedFileWrapper
    {
        private readonly HttpPostedFileBase _file;
        
        public HttpPostedFileWrapper(HttpPostedFileBase file)
        {
            _file = file;
        }

        public HttpPostedFileBase File
        {
            get { return _file; }
        }

        public bool IsReady
        {
            get
            {
                return _file != null && _file.InputStream != null && _file.ContentLength != 0 &&
                       _file.InputStream.CanRead;
            }
        }

        public string ContentType
        {
            get { return _file.GetSafe(f => f.ContentType); }
        }

        private byte[] _bytes;
        public byte[] Bytes
        {
            get
            {
                if (_bytes == null && IsReady)
                {
                    _bytes = new WebImage(_file.InputStream).Crop(1, 1).GetBytes();
                }

                return _bytes;
            }
        }

        public byte[] ResizeToSquare(int squareSize)
        {
            return IsReady  ? new WebImage(_file.InputStream)
                                         .Crop(1, 1)
                                         .Resize(squareSize, squareSize, false, true)
                                         .GetBytes()
                            : new byte[0];
        }
    }
}