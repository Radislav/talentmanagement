﻿using System.Collections.Generic;
using MongoDB.Bson;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Questionaire.Map
{
    public class IndexModelMapper : IIndexModelMapper
    {
        private readonly IMapper<AnswersCollection, IEnumerable<AnswerModel>> _answersModelMapper;

        public IndexModelMapper(IMapper<AnswersCollection, IEnumerable<AnswerModel>> answersModelMapper)
        {
            _answersModelMapper = answersModelMapper;
        }

        public IndexModel Map(AnswersCollection answers, ObjectId reviewId)
        {
            return new IndexModel
            {
                ReviewId = reviewId,
                Answers = _answersModelMapper.Map(answers)
            };
        }
    }
}