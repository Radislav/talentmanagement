﻿using TalentManagement.Domain;
using TalentManagement.Infrastructure.Cache;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Map
{
    class CommentModelMap : IMap<Comment,CommentModel>
    {
        private readonly IGlobalCache _globalCache;

        public CommentModelMap(IGlobalCache globalCache)
        {
            _globalCache = globalCache;
        }

        public void Configure(IMapperConfiguration<Comment, CommentModel> configuration)
        {
            configuration.ForMember(d => d.CreatorNames, s => _globalCache.GetUserNames(s.CreatedBy));

        }
    }
}