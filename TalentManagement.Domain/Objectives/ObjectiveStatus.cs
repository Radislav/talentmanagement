﻿using Resource = TalentManagement.Resources.DomainResources;

namespace TalentManagement.Domain.Objectives
{
    public enum ObjectiveStatus
    {
        None,
        [LocalizedDescription("NotStarted", typeof(Resource))]
        NotStarted,
        [LocalizedDescription("InProgress", typeof(Resource))]
        InProgress,
        [LocalizedDescription("Completed", typeof(Resource))]
        Completed
    }
}
