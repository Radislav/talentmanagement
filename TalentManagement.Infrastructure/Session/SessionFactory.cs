﻿using Autofac;

namespace TalentManagement.Infrastructure.Session
{
    class SessionFactory : ISessionFactory
    {
        private readonly ILifetimeScope _parentScope;

        public SessionFactory(ILifetimeScope scope)
        {
            _parentScope = scope;
        }

        public ISession Create()
        {
            return new Session(_parentScope.BeginLifetimeScope());
        }
    }
}
