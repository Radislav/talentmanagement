﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using MongoDB.Bson;

namespace TalentManagement.Domain.Objectives
{
    public class ActionItemsDictionary : Dictionary<ObjectId,IEnumerable<ActionItem>>
    {
        public ActionItemsDictionary()
        {
        }

        public ActionItemsDictionary(IDictionary<ObjectId,IEnumerable<ActionItem>> dictionary)
        {
            Contract.Requires(dictionary != null);
            dictionary.Map(pair => Add(pair.Key,pair.Value));
        }

        public IEnumerable<Tuple<ObjectId,ActionItem>> ToTuples()
        {
            return from pair in this 
                   from actionItem in pair.Value 
                   select new Tuple<ObjectId, ActionItem>(pair.Key, actionItem);
        }
    }
}
