﻿using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.ObjectiveDetails.Map
{
    public class ActionItemModelMap : IMap<ActionItem, ActionItemModel>
    {
        public void Configure(IMapperConfiguration<ActionItem, ActionItemModel> configuration)
        {
            configuration.ForMember(d => d.Name, s => s.Name);
            configuration.ForMember(d => d.Owner, s => s.Owner);
        }
    }
}