﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TalentManagement.Domain;

namespace TalentManagement.Test.Domain
{
    
    
    /// <summary>
    ///This is a test class for EntityWithTypedIdTest and is intended
    ///to contain all EntityWithTypedIdTest Unit Tests
    ///</summary>
    [TestClass()]
    public class EntityWithTypedIdTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion
        

        [TestMethod]
        public void EqualsTest_should_return_false_for_null()
        {
            EntityWithTypedId<GenericParameterHelper> target = new EntityWithTypedId<GenericParameterHelper>();
            Assert.IsFalse(target.Equals(null));
        }

        [TestMethod()]
        public void EqualsTest_should_return_false_for_objects_with_other_type()
        {
            EntityWithTypedId<GenericParameterHelper> target = new EntityWithTypedId<GenericParameterHelper>();
            Assert.IsFalse(target.Equals(new object()));
        }

        [TestMethod()]
        public void EqualsTest_should_compare_references_if_Id_is_NULL()
        {
            EntityWithTypedId<GenericParameterHelper> target = new EntityWithTypedId<GenericParameterHelper>();
            Assert.IsFalse(target.Equals(new  EntityWithTypedId<GenericParameterHelper>()));
        }

        [TestMethod()]
        public void EqualsTest_should_compare_references_if_Id_is_default()
        {
            EntityWithTypedId<int> target = new EntityWithTypedId<int>();
            Assert.IsFalse(target.Equals(new EntityWithTypedId<int>(1)));
        }

        [TestMethod()]
        public void EqualsTest_should_compare_references_if_ObjectId_is_NULL()
        {
            EntityWithTypedId<GenericParameterHelper> target = new EntityWithTypedId<GenericParameterHelper>(new GenericParameterHelper());
            Assert.IsFalse(target.Equals(new EntityWithTypedId<GenericParameterHelper>()));
        }

        [TestMethod()]
        public void EqualsTest_should_compare_references_if_ObjectId_is_default()
        {
            EntityWithTypedId<int> target = new EntityWithTypedId<int>(1);
            Assert.IsFalse(target.Equals(new EntityWithTypedId<int>()));
        }

        [TestMethod()]
        public void EqualsTest_should_compare_IDs_if_they_are_Value_type()
        {
            EntityWithTypedId<int> target = new EntityWithTypedId<int>(1001);
                                                
            Assert.IsTrue(target.Equals(new EntityWithTypedId<int>(1001)));
        }

        [TestMethod()]
        public void EqualsTest_should_compare_IDs_if_they_are_Reference_type()
        {
            GenericParameterHelper id = new GenericParameterHelper();

            EntityWithTypedId<GenericParameterHelper> target = new EntityWithTypedId<GenericParameterHelper>(id);

            Assert.IsTrue(target.Equals(new EntityWithTypedId<GenericParameterHelper>(id)));

        }

        [TestMethod()]
        public void op_Equality_should_compare_NULL_correctly()
        {
            EntityWithTypedId<int> target = new EntityWithTypedId<int>();
            Assert.IsFalse(target == null);
            Assert.IsFalse(null == target);
        }
        
        [TestMethod()]
        public void GetHashCode_should_return_code_based_on_references_if_ID_not_set_for_reference_types()
        {
            EntityWithTypedId<GenericParameterHelper> target = new EntityWithTypedId<GenericParameterHelper>();
            Assert.AreNotEqual(0,target.GetHashCode());
            Assert.AreNotEqual(new  EntityWithTypedId<GenericParameterHelper>().GetHashCode(),target.GetHashCode());
        }

        [TestMethod()]
        public void GetHashCode_should_return_code_based_on_references_if_ID_not_set_for_value_types()
        {
            EntityWithTypedId<int> target = new EntityWithTypedId<int>();
            Assert.AreNotEqual(0, target.GetHashCode());
            Assert.AreNotEqual(new EntityWithTypedId<int>().GetHashCode(), target.GetHashCode());
        }

        [TestMethod()]
        public void GetHashCode_should_return_code_based_on_IDs()
        {
            EntityWithTypedId<int> target = new EntityWithTypedId<int>(1001);
                                                
            Assert.AreEqual(1001, target.GetHashCode());
            Assert.AreEqual(new EntityWithTypedId<int>(1001).GetHashCode(), target.GetHashCode());
        }

    }
}
