﻿using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Infrastructure.Validation;
using TalentManagement.Web.Models.MyPassword;
using System.Linq;

namespace TalentManagement.Test.Web.Models.MyPassword.Validation
{
    [TestClass]
    public class IndexModelValidatorTest : MockedTestClass<IValidator<IndexModel>>
    {
        [TestMethod]
        public void Should_fail_if_old_password_empty()
        {
            var target = CreateTarget();
            ValidationResult result =  target.ValidateInstance(new IndexModel { OldPassword = null });

            Assert.IsTrue(result.HasMessage("'Old Password' should not be empty.", "OldPassword"));
        }

        [TestMethod]
        public void Should_fail_if_old_password_invalid()
        {
            var target = CreateTarget();
            ValidationResult result = target.ValidateInstance(new IndexModel { OldPassword = null });

            Assert.IsTrue(result.HasMessage("Password is invalid.", "OldPassword"));
        }

        [TestMethod]
        public void Should_fail_if_new_password_and_confirmation_are_mismatched()
        {
            var target = CreateTarget();
            ValidationResult result = target.ValidateInstance(new IndexModel { NewPassword = "1",ConfirmPassword = "2"});

            Assert.IsTrue(result.HasMessage("Passwords must match.", "ConfirmPassword"));
        }

        [TestMethod]
        public void Should_pass_if_old_password_valid_and_new_matches_with_confirmation()
        {
            var target = CreateTarget();

            const string oldPassword = "old";
            IndexModel model = new IndexModel
                                   {
                                       OldPassword = oldPassword,
                                       OldPasswordHashed = Container.Resolve<IHashComputer>().ComputeHash(oldPassword),
                                       ConfirmPassword = "new",
                                       NewPassword = "new"
                                   };
            ValidationResult result = target.ValidateInstance(model);
            Assert.IsTrue(result.IsValid);
        }
    }
}
