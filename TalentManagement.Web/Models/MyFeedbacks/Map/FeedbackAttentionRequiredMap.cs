﻿using System;
using TalentManagement.Domain.Feedbacks;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Resources;

namespace TalentManagement.Web.Models.MyFeedbacks.Map
{
    class FeedbackAttentionRequiredMap : IMap<Feedback,FeedbackAttentionRequired>
    {
        private readonly IUserRepository _userRepository;
        private readonly IResourceProvider _resourceProvider;

        public FeedbackAttentionRequiredMap(IUserRepository userRepository, IResourceProvider resourceProvider)
        {
            _userRepository = userRepository;
            _resourceProvider = resourceProvider;
        }

        public void Configure(IMapperConfiguration<Feedback, FeedbackAttentionRequired> configuration)
        {
            configuration.ForMember(d => d.FeedbackId, s => s.Id);
            configuration.ForMember(d => d.CreatedId, s => s.CreatedBy);
            configuration.ForMember(d => d.CreationDate, s => s.CreatedAt.AsLondDateString());
            configuration.ForMember(d => d.Title,
                s =>
                    {
                        string firstName = _userRepository.GetById(s.CreatedBy).FirstName;
                        return String.Format(_resourceProvider.FeedbackTheyRequestedTitle, firstName, s.Question);
                    });
        }
    }
}