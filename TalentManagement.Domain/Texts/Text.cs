﻿using MongoDB.Bson;

namespace TalentManagement.Domain.Texts
{
    public class Text : Entity
    {
        protected Text()
        {
        }

        public Text(ObjectId objectId,string content,TextType type) 
            : base(objectId)
        {
            Type = type;
            Content = content;
        }

        public string Content { get; private set; }

        public TextType Type { get; private set; }

        
    }
}
