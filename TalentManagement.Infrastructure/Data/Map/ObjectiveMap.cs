﻿using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Data.Configuration;

namespace TalentManagement.Infrastructure.Data.Map
{
    class ObjectiveMap : EntityConfiguration<Objective>
    {
        public ObjectiveMap()
        {
            AsCollection("Objectives")
                .WithDataSource(DataSourceType.Tenant);

            Property(p => p.ScopeId)
                .Ignore();

            Property(p => p.ScopeName)
                .Ignore();

            Property(p => p.CompanyName)
                .Required();

            Property(p => p.Contributors)
                .Required();

            Property(p => p.CreatedBy)
                .Required();

            Property(p => p.Name)
                .Required();

        }
    }
}
