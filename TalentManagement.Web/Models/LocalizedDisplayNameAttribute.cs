﻿using System;
using System.ComponentModel;
using System.Resources;

namespace TalentManagement.Web.Models
{
    /// <summary>
    /// Use this attribute to setup object display name over the resources. 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Class, AllowMultiple = false)]
    public class LocalizedDisplayNameAttribute : DisplayNameAttribute
    {
        private readonly string _displayName;

        public LocalizedDisplayNameAttribute(string resorceKey,Type resourceType)
        {
            _displayName = new ResourceManager(resourceType).GetString(resorceKey);
        }

        public override string DisplayName
        {
            get { return _displayName; }
        }
    }
}