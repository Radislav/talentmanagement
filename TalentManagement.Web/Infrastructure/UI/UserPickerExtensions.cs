﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using TalentManagement.Web.Models;

namespace TalentManagement.Web.Infrastructure.UI
{
    public static class UserPickerExtensions
    {
        public static MvcHtmlString UserPicker<TModel>(this HtmlHelper<TModel> html,
                                                      Expression<Func<TModel, string>> titleExpression,
                                                       List<UserPicked> users,
                                                      bool showAddButton = true,
                                                      bool showPickedUsers = true)
        {

            return html.Partial("_UserPickerControl", new UserPickerControl
            {
                Users = users,
                Title = html.LocalizedTextFor(titleExpression).ToHtmlString(),
                PropertyToValidateName = ModelMetadata.FromLambdaExpression(titleExpression, html.ViewData).PropertyName,
                ShowPickedUsers = showPickedUsers,
                ShowAddButton = showAddButton
            });
        }

        public static MvcHtmlString UserPicker<TModel>(this HtmlHelper<TModel> html,
                                                     string title,
                                                      List<UserPicked> users,
                                                     bool showAddButton = true,
                                                     bool showPickedUsers = true)
        {

            return html.Partial("_UserPickerControl", new UserPickerControl
            {
                Users = users,
                Title = title,
                PropertyToValidateName = null,
                ShowPickedUsers = showPickedUsers,
                ShowAddButton = showAddButton
            });
        }

        public static MvcHtmlString UserPickerForModel<TModel>(this HtmlHelper<TModel> html, bool showAddButton, bool showPickedUsers)
            where TModel : ICanPickUsers<TModel>
        {
            ICanPickUsers<TModel> model = html.ViewData.Model;
            return html.Partial("_UserPickerControl", new UserPickerControl
            {
                Users = model.Users,
                Title = html.LocalizedTextFor(html.ViewData.Model.TitleExpression).ToHtmlString(),
                PropertyToValidateName = ModelMetadata.FromLambdaExpression(model.TitleExpression, html.ViewData).PropertyName,
                ShowPickedUsers = showPickedUsers,
                ShowAddButton = showAddButton
            });
        }

        public static MvcHtmlString UserPickerForModel<TModel>(this HtmlHelper<TModel> html,bool showAddButton, bool showPickedUsers,IEnumerable<UserPicked> pickedUsers,string prefix)
           where TModel : ICanPickUsers<TModel>
        {
            ICanPickUsers<TModel> model = html.ViewData.Model;
            return html.Partial("_UserPickerControl", new UserPickerControl
            {
                Users = model.Users,
                Title = html.LocalizedTextFor(html.ViewData.Model.TitleExpression).ToHtmlString(),
                PropertyToValidateName = ModelMetadata.FromLambdaExpression(model.TitleExpression, html.ViewData).PropertyName,
                ShowPickedUsers = showPickedUsers,
                ShowAddButton = showAddButton,
                UsersPicked = new List<UserPicked>(pickedUsers),
                Prefix = prefix
            });
        }
    }
}