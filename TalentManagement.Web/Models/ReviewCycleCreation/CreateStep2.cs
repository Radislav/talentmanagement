﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using TalentManagement.Domain.Performance;

namespace TalentManagement.Web.Models.ReviewCycleCreation
{
    public class CreateStep2 : WizardPage
    {
        public CreateStep2()
        {
            QuestionNew = new QuestionModel();
            QuestionEdit = new QuestionModel();
            QuestionSets = QuestionSetModel.DefaultSets;
        }

        private QuestionModel _questionNew;
        public QuestionModel QuestionNew
        {
            get { return _questionNew ?? new QuestionModel(); }
            set { _questionNew = value; }
        }

        private QuestionModel _questionEdit;
        public QuestionModel QuestionEdit
        {
            get { return _questionEdit ?? new QuestionModel(); }
            set { _questionEdit = value; }
        }
        public IEnumerable<QuestionSetModel> QuestionSets { get; set; }

        public QuestionSetModel GetQuestionSet(QuestionSetType setType)
        {
            return QuestionSets.Single(q => q.SetType == setType);
        }

        public void AddQuestion(QuestionModel question,QuestionSetType setType)
        {
            QuestionSetModel questionSet = GetQuestionSet(setType);
            questionSet.AddQuestion(question);
            //To refresh the creation modal dialog
            QuestionNew = new QuestionModel();
        }

        public void EditQuestion(QuestionModel question, QuestionSetType setType)
        {
            QuestionModel questionToEdit = GetQuestion(setType, question.Id);
            questionToEdit.Settings = question.Settings;
            questionToEdit.Text = question.Text;
            questionToEdit.AnswerType = question.AnswerType;
            //To refresh the editing modal dialog
            QuestionEdit = new QuestionModel();
        }

        public void DeleteQuestion(ObjectId questionId, QuestionSetType setType)
        {
            QuestionSetModel questionSet = GetQuestionSet(setType);
            questionSet.RemoveQuestion(questionId);
        }

        public QuestionModel GetQuestion(QuestionSetType setType, ObjectId questionId)
        {
            QuestionSetModel questionSet = GetQuestionSet(setType);
            return questionSet.Questions.Single(q => q.Id == questionId);
        }

        public QuestionSetType SelectedSetType { get; set; }


        public override int NextStep
        {
            get { return 2; }
        }

        public override int ThisStep
        {
            get { return 1; }
        }
    }
}