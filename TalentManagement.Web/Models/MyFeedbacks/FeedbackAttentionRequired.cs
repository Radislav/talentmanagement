﻿using Resource = TalentManagement.Resources.Models.MyFeedbacks.FeedbackAttentionRequired;

namespace TalentManagement.Web.Models.MyFeedbacks
{
    public class FeedbackAttentionRequired
    {
        public string CreatedId { get; set; }
        public string CreationDate { get; set; }
        public string Title { get; set; }
        public string FeedbackId { get; set; }

        [LocalizedDisplayName("Response",typeof(Resource))]
        public string Response { get; set; }
    }
}