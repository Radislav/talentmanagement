﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace TalentManagement.Web.Models.Questionaire
{
    public interface IQuestionnarie
    {
        IEnumerable<AnswerModel> Answers { get; set; }
        ObjectId ReviewId { get; set; }
    }
}
