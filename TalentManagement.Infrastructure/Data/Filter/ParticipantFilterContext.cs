﻿using TalentManagement.Domain;

namespace TalentManagement.Infrastructure.Data.Filter
{
    public class ParticipantFilterContext : IFilterContext
    {
        public ParticipantFilterContext()
        {
        }

        public ParticipantFilterContext(string participant)
        {
            Participant = participant;
        }

        public string Participant { get; set; }
    }
}
