﻿using MongoDB.Bson;
using TalentManagement.Domain;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data
{
    class Repository<TEntity> : RepositoryWithTypedId<TEntity,ObjectId> , IRepository<TEntity> 
        where TEntity : EntityWithTypedId<ObjectId>
    {
        public Repository(IUnitOfWork unitOfWork,IPrincipalProvider principalProvider) : base(unitOfWork,principalProvider)
        {
        }
    }
}
