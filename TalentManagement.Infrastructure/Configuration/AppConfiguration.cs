﻿using System.Configuration;
using System.Diagnostics.Contracts;
using System.Net.Configuration;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Configuration
{
    class AppConfiguration : IAppConfiguration
    {
        private const string SharedDbName = "Shared@";
        private readonly IPrincipalProvider _principalProvider;

        public AppConfiguration(IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
        }

        public string GetConfiguration(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public string GetConnectionString(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }
        
        public string GetTenantName()
        {
            TenantIdentity identity =  _principalProvider.GetIdentity();
            return identity == null ? TenantIdentity.DefaultTenant : identity.Tenant;
        }
        
        public string GetDbName(DataSourceType dataSourceType)
        {
            return dataSourceType == DataSourceType.Shared ? SharedDbName : GetTenantName();
        }
        
        public EmailSettings EmailSettings
        {
            get
            {
                SmtpSection smtpSection = (SmtpSection) ConfigurationManager.GetSection("system.net/mailSettings/smtp");
                Contract.Assume(smtpSection != null);
                return new EmailSettings
                           {
                               From = smtpSection.From,
                               Host = smtpSection.Network.Host,
                               Port = smtpSection.Network.Port
                           };
            }
            
        }
        
        public int MinPasswordLength
        {
            get
            {
                return int.Parse(GetConfiguration("MinPasswordLength"));    
            }
        }
        
        public int AvatarWidth
        {
            get { return int.Parse(GetConfiguration("AvatarWidth")); }
        }

        public int AvatarHeight
        {
            get { return int.Parse(GetConfiguration("AvatarHeight")); }
        }

        public string AvatarImageType
        {
            get { return GetConfiguration("AvatarImageType"); }
        }
    }
}
