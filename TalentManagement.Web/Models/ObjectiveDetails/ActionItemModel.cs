﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MongoDB.Bson;
using Resource = TalentManagement.Resources.Models.ObjectiveDetails.ActionItemModel;

namespace TalentManagement.Web.Models.ObjectiveDetails
{
    public class ActionItemModel : ICanPickUsers<ActionItemModel>
    {
        public ObjectId Id { get; set; }

        [LocalizedDisplayName("Name", typeof(Resource))]
        public string Name { get; set; }

        [LocalizedDisplayName("Owner", typeof(Resource))]
        public string Owner { get; set; }

        public List<UserPicked> Users { get; set; }
       
        public Expression<Func<ActionItemModel, string>> TitleExpression
        {
            get { return model => model.Owner; }
        }
    }
}