﻿using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain.Feeds;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Web.Models.Feeds.Map;

namespace TalentManagement.Web.Models.ObjectiveDetails.Map
{
    class ObjectiveDetailsMapper  : IObjectiveDetailsMapper
    {
        private readonly IMapper<Objective, ObjectiveDetailsModel> _baseMapper;
        private readonly IFeedUniversalMapper _feedMapper; 

        public ObjectiveDetailsMapper(IMapper<Objective, ObjectiveDetailsModel> baseMapper, IFeedUniversalMapper feedMapper)
        {
            _baseMapper = baseMapper;
            _feedMapper = feedMapper;
        }

        public ObjectiveDetailsModel Map(Objective objective, IEnumerable<Feed> feeds)
        {
            ObjectiveDetailsModel objectiveDetailsModel =  _baseMapper.Map(objective);
            objectiveDetailsModel.FeedsModel.Feeds = feeds.Select(f => _feedMapper.Map(f)).ToArray();
            return objectiveDetailsModel;
        }

        public ObjectiveDetailsModel Map(Objective source)
        {
            return _baseMapper.Map(source);
        }
    }
}