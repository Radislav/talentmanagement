﻿using System.Diagnostics.Contracts;
using TalentManagement.Domain.Objectives;
using TalentManagement.Domain.Texts;

namespace TalentManagement.Domain.Events
{
    public class ObjectiveCreatedArgs : NotificableEventArgs
    {
        public ObjectiveCreatedArgs(Objective objective, string senderId, string[] receiversIds)
            : base(TextType.NotificationObjectiveCreated, senderId,objective, receiversIds)
        {
            Contract.Requires(objective != null);
            Contract.Requires(senderId != null);
            Contract.Requires(receiversIds != null);

            Objective = objective;
        }

        public Objective Objective { get; private set; }
    }
}
