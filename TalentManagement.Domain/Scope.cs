﻿namespace TalentManagement.Domain
{
    public class Scope : IScope
    {
        public const string Global = "Global";
        public string ScopeId { get; private set; }
        public string ScopeName { get; private set; }

        public Scope()
        {
            ScopeName = Global;
        }

        public Scope(string name, string id)
        {
            ScopeName = name;
            ScopeId = id;
        }

        public override bool Equals(object obj)
        {
            Scope scope = obj as Scope;
            if (scope == null)
            {
                return false;
            }
            return ScopeId == scope.ScopeId && ScopeName == scope.ScopeName;
        }

        public override int GetHashCode()
        {
            int idHash = ScopeId == null ? 0 : ScopeId.GetHashCode();
            int nameHash = ScopeName == null ? 0 : ScopeName.GetHashCode();

            return idHash | nameHash;
        }
    }
}
