﻿namespace TalentManagement.Web.Models.MyObjectives
{
    public class ObjectiveContainer
    {
        public ObjectiveContainer()
        {
            Objectives = new ObjectiveSimpleModel[0];
        }
        
        public string Title { get; set; }
        public string NoObjectivesMessage { get; set; }
        public ObjectiveSimpleModel[] Objectives { get; set; }
    }
}