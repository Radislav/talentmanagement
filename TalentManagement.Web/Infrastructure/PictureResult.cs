﻿using System.Web.Mvc;
using TalentManagement.Domain;

namespace TalentManagement.Web.Infrastructure
{
    public class PictureResult : ActionResult
    {
        private readonly ActionResult _result;

        public PictureResult(IPicture picture,int size,UrlHelper urlHelper,string defaultImageUrl,string defaultImageType)
        {
            _result = IsPictureValid(picture)
                          ? (ActionResult) new FileContentResult(picture.GetResizedBytes(size), picture.ImageType)
                          : new FilePathResult(urlHelper.Content(defaultImageUrl), defaultImageType);
        }

        private bool IsPictureValid(IPicture picture)
        {
            return picture != null && picture.Image != null && picture.ImageType != null;
        }   

        public override void ExecuteResult(ControllerContext context)
        {
            _result.ExecuteResult(context);
        }
    }
}