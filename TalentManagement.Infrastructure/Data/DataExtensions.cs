﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using TalentManagement.Domain;

namespace TalentManagement.Infrastructure.Data
{
    public static class DataExtensions
    {

        public static IEnumerable<TEntity> ApplyFilter<TEntity>(this IQueryable<TEntity> entities, IFilter filter)
          where TEntity : class
        {
            Contract.Requires(entities != null);
            Contract.Requires(filter != null);

            return entities.AsEnumerable().ApplyFilter(filter);
        }

        public static IEnumerable<TEntity> ApplyFilter<TEntity>(this IUnitOfWork unitOfWork, IFilter filter)
            where TEntity : class
        {
            Contract.Requires(unitOfWork != null);
            Contract.Requires(filter != null);

            return ApplyFilter(unitOfWork.AsQueryable<TEntity>(), filter);
        }
    }
}
