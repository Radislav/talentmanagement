﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Parsers;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Validation;

namespace TalentManagement.AppServices
{
    internal class GroupUploader : IGroupUploader
    {
        private readonly IRowsParser<string[], GroupRowParsedEventArgs> _parser;
        private readonly ITemplateProvider<GroupRowParsedEventArgs> _templateProvider;
        private readonly IResourceProvider _resourceProvider;
        private readonly IUserRepository _userRepository;
        private readonly IValidator<GroupRowParsedEventArgs> _validator;
        private readonly IMapper<GroupRowParsedEventArgs, User> _rowParsedEventArgsToUserMapper;

        private List<string> _warnings;

        public GroupUploader(IRowsParser<string[], GroupRowParsedEventArgs> parser,
            IResourceProvider resourceProvider,
            IUserRepository userRepository,
            IValidator<GroupRowParsedEventArgs> validator,
            IMapper<GroupRowParsedEventArgs, User> rowParsedEventArgsToUserMapper,
            ITemplateProvider<GroupRowParsedEventArgs> templateProvider)
        {
            _parser = parser;
            _resourceProvider = resourceProvider;
            _userRepository = userRepository;
            _validator = validator;
            _rowParsedEventArgsToUserMapper = rowParsedEventArgsToUserMapper;
            _templateProvider = templateProvider;
        }

        public ICollection<string> Upload(Stream contentsStream)
        {
            _warnings = new List<string>();
            _parser.RowParsed += RowParsedHandler;
            try
            {
                _parser.Parse(contentsStream);
            }
            catch (InvalidFileException)
            {
                _warnings.Add(_resourceProvider.GroupUploadFileNotValid);
            }

            return _warnings;
        }

        private void RowParsedHandler(object sender, GroupRowParsedEventArgs e)
        {
            var validationResult = _validator.ValidateInstance(e);
            if (validationResult.IsValid)
            {
                var user = _rowParsedEventArgsToUserMapper.Map(e);
                var createUserResult = _userRepository.CreateUser(user);
                if (createUserResult != UserCreateResult.Ok)
                {
                    _warnings.Add(_resourceProvider.GroupUploadRowWarning(e.RowNumber, _resourceProvider.ErrorEmailBusy));
                }
            }
            else
            {
                _warnings.AddRange(validationResult.Failures.Select(
                    x => _resourceProvider.GroupUploadRowWarning(e.RowNumber, x.ErrorMessage)));
            }
        }

        public string TemplateContentType
        {
            get { return _templateProvider.TemplateContentType; }
        }

        public byte[] GetTemplateContents(object argument)
        {
            return _templateProvider.GetTemplateContents(argument);
        }


        public string FileName
        {
            get { return _templateProvider.FileName; }
        }
    }
}
