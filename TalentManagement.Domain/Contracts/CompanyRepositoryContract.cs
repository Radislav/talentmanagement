﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using MongoDB.Bson;
using TalentManagement.Domain.Companies;

namespace TalentManagement.Domain.Contracts
{
    [ContractClassFor(typeof (ICompanyRepository))]
    internal abstract class CompanyRepositoryContract : ICompanyRepository
    {
        #region ICompanyRepository Members

        public Company GetWithoutPictures(ObjectId companyId)
        {
            return default(Company);
        }

        public Company UpsertIgnoreBadges(Company company)
        {
            Contract.Requires<EntityNullException>(company != null);
            Contract.Ensures(Contract.Result<Company>() != null);

            return default(Company);
        }

        public Company UpsertBadgesOnly(Company company)
        {
            Contract.Requires<EntityNullException>(company != null);
            Contract.Ensures(Contract.Result<Company>() != null);

            return default(Company);
        }

        public Company GetByTenantWithoutPictures(string tenantName)
        {
            Contract.Requires<ArgumentNullException>(false == String.IsNullOrEmpty(tenantName));
            return default(Company);
        }

        public Company GetByTenant(string tenantName)
        {
            Contract.Requires<ArgumentNullException>(false == String.IsNullOrEmpty(tenantName));
            return default(Company);
        }

        public Logo GetLogo(ObjectId companyId)
        {
            return default(Logo);
        }

        public Badge GetBadge(ObjectId companyId, ObjectId badgeId)
        {
            return default(Badge);
        }

        public void Insert(Company entity)
        {
        }

        public void DeleteById(ObjectId id)
        {
        }

        public void Update(Company entity)
        {
        }

        public IQueryable<Company> AsQueryable()
        {
            return default(IQueryable<Company>);
        }

        public Company GetById(ObjectId id)
        {
            return default(Company);
        }

        #endregion


        public Company Upsert(Company entity)
        {
            return null;
        }


        public IEnumerable<Company> ApplyFilter(IFilter filter)
        {
            return null;
        }
    }
}