﻿namespace TalentManagement.Infrastructure.Validation
{
    public interface IBasicValidator
    {
        ValidationResult ValidateEmail(string email);
    }
}
