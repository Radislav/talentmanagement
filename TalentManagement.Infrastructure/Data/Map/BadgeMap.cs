﻿using TalentManagement.Domain.Companies;
using TalentManagement.Infrastructure.Data.Configuration;

namespace TalentManagement.Infrastructure.Data.Map
{
    class BadgeMap : EntityConfiguration<Badge>
    {
        public BadgeMap()
        {
            AsCollection("Badges")
                .WithDataSource(DataSourceType.Tenant);

            Property(b => b.Image)
                .Required();

            Property(b => b.ImageType)
                .Required();

            Property(b => b.Title)
                .Required();
        }
    }
}
