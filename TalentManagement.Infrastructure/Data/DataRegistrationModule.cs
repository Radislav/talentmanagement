﻿using System;
using Autofac;
using MongoDB.Bson;
using TalentManagement.Domain;
using TalentManagement.Domain.Companies;
using TalentManagement.Domain.Events;
using TalentManagement.Domain.Feedbacks;
using TalentManagement.Domain.Feeds;
using TalentManagement.Domain.Objectives;
using TalentManagement.Domain.Performance;
using TalentManagement.Domain.Texts;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Data.Configuration;
using TalentManagement.Infrastructure.Data.Filter;
using TalentManagement.Infrastructure.Data.Map;
using TalentManagement.Infrastructure.Data.Repositories;
using TalentManagement.Infrastructure.Security;


namespace TalentManagement.Infrastructure.Data
{
    public class DataRegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            #region Core

            builder.RegisterType<UnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof (RepositoryWithTypedId<,>))
                .As(typeof (IRepositoryWithTypedId<,>))
                .InstancePerDependency();

            builder.RegisterGeneric(typeof(Repository<>))
               .As(typeof(IRepository<>))
               .InstancePerDependency();


            builder.RegisterType<Registrar>()
              .As<IRegistrar>()
              .OnActivated(b =>
              {
                  b.Instance.Add(new CompanyMap());
                  b.Instance.Add(new CompanyLogoMap());
                  b.Instance.Add(new BadgeMap());
                  b.Instance.Add(new ObjectiveMap());
                  b.Instance.Add(new UserMap());
                  b.Instance.Add(new AvatarMap());
                  b.Instance.Add(new ObjectiveIconMap());
                  b.Instance.Add(new FeedMap());
                  b.Instance.Add(new FeedbackMap());
                  b.Instance.Add(new ActionItemMap());
                  b.Instance.Add(new ReviewCycleMap());
                  b.Instance.Add(new ReviewMap());
                  b.Instance.Add(new QuestionMap());
                  b.Instance.Add(new TeamMap());
              })
              .SingleInstance();

            #endregion

            #region Shared

            builder.RegisterType<TextRepository>()
                .As<ITextRepository>();
                
            #endregion
            
            #region Filters

            builder.RegisterType<EmptyFilter>()
                .As<IFilter>()
                .Keyed<IFilter>(FilterType.Empty);

            builder.RegisterType<OwnedFilter>()
                .As<IFilter<IOwned>>()
                .Keyed<IFilter>(FilterType.Owned);

            builder.RegisterType<OwnedByCompanyFilter>()
                .As<IFilter<IOwnedByCompany>>()
                .Keyed<IFilter>(FilterType.OwnedByCompany);

            builder.RegisterType<OwnedByTeamFilter>()
                .As<IFilter<IOwnedByTeam>>();

            builder.RegisterType<ParticipantFilter>()
                   .As<IFilter<IHasParticipants>>()
                   .Keyed<IFilter>(FilterType.Participants);

            builder.Register(ctx => new UnionFilter<Feed>(new[]
                                                              {
                                                                  ctx.ResolveKeyed<IFilter>(FilterType.Owned),
                                                                  ctx.ResolveKeyed<IFilter>(FilterType.OwnedByCompany)
                                                              }))
                .As<IFilter<Feed>>()
                .Keyed<IFilter>(FilterType.Feed);

            builder.RegisterType<FeedTypeFilter>()
                .Keyed<IFilter>(FilterType.FeedType);

            builder.RegisterType<UserFilter>()
                   .Keyed<IFilter>(FilterType.User);

            builder.RegisterType<ScopeFilter>()
                .As<IFilter<IHasScope>>();

            builder.RegisterType<ReviewRequiredAttentionFilter>()
                .Keyed<IFilter>(FilterType.ReviewRequiredAttention);

            builder.RegisterType<ManagersFilter>()
                   .Keyed<IFilter>(FilterType.Managers);

            #endregion

            #region User

            builder.RegisterType<UserRepository>()
                .As<IUserRepository>();

            builder.RegisterType<AvatarRepository>()
                .As<IAvatarRepository>();

            builder.RegisterType<TeamRepository>()
                   .As<ITeamRepository>();

            #endregion

            #region Feeds

            builder.RegisterType<MyFeedRepository>()
                .As<IMyFeedRepository>();

            builder.RegisterType<FeedRepository>()
                .As<IFeedRepository>();

            #endregion 

            #region Objectives
            
            builder.RegisterType<ObjectiveIconRepository>()
                .As<IObjectiveIconRepository>();

            //Default ObjectiveRepository
            builder.Register((ctx, p) => new ObjectiveRepository(
                                             ctx.Resolve<IRepositoryWithTypedId<Objective, ObjectId>>(),
                                             default(IFilter),
                                             ctx.Resolve<IPrincipalProvider>(),
                                             ctx.Resolve<Lazy<IDomainEvents>>())
                            )
                  .As<IObjectiveRepository>();


            //ObjectiveRepository filtered by company
            builder.Register((ctx, p) => new ObjectiveRepository(
                                             ctx.Resolve<IRepositoryWithTypedId<Objective, ObjectId>>(),
                                             ctx.Resolve<IFilter<IOwnedByCompany>>(),
                                             ctx.Resolve<IPrincipalProvider>(),
                                             ctx.Resolve<Lazy<IDomainEvents>>()
                                             ))
                .Keyed<IObjectiveRepository>(FilterType.OwnedByCompany);

            builder.RegisterType<ActionItemRepository>()
                .As<IActionItemRepository>();

            #endregion 

            #region Feedbacks

            builder.RegisterType<FeedbackRepository>()
                .As<IFeedbackRepository>();

            #endregion

            #region Companies

            builder.RegisterType<CompanyRepository>()
                .As<ICompanyRepository>();

            builder.RegisterType<BadgeRepository>()
                .As<IBadgeRepository>();

            #endregion

            #region Performance

            builder.RegisterType<ReviewCycleRepository>()
                .As<IReviewCycleRepository>();

            builder.RegisterType<ReviewRepository>()
                   .As<IReviewRepository>();

            builder.RegisterType<QuestionRepository>()
                   .As<IQuestionRepository>();

            #endregion

            base.Load(builder);
        }
    }
}
