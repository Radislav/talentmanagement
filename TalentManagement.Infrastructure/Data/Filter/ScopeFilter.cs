﻿using System.Linq;
using TalentManagement.Domain;

namespace TalentManagement.Infrastructure.Data.Filter
{
    class ScopeFilter : FilterBase<IHasScope>
    {
        public override IFilterContext FilterContext { get; set; }

        public override IFilter<IHasScope> Filter(IFilterContext[] filterContexts)
        {
            ScopeFilterContext context =  GetContextOrFail<ScopeFilterContext>(filterContexts);

            Data = Data.Where(d => d.Scope.ScopeName == context.ScopeName && d.Scope.ScopeId == context.ScopeId);

            return this;
        }
    }
}
