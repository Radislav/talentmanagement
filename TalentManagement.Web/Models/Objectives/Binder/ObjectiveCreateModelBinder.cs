﻿using System.Linq;
using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Domain.Objectives;
using TalentManagement.Web.Models.Binder;
using TalentManagement.Web.Models.Validation;
using System.Collections.Generic;
using System.Web;

namespace TalentManagement.Web.Models.Objectives.Binder
{
    public class ObjectiveCreateModelBinder : CustomModelBinder<ObjectiveCreateModel>
    {
        private readonly ICustomModelBinder<KeyResultModel> _keyResultBinder;
        private readonly IEnumerableBinder<KeyResultModel> _keyResultsCollectionBinder;
        private readonly IEnumerableBinder<UserPicked> _usersPickedCollectionBinder;

        public ObjectiveCreateModelBinder(ICustomModelBinder<KeyResultModel> keyResultBinder, 
                                          IEnumerableBinder<KeyResultModel> keyResultsCollectionBinder,
                                          IEnumerableBinder<UserPicked> usersPickedCollectionBinder)
        {
            _keyResultBinder = keyResultBinder;
            _keyResultsCollectionBinder = keyResultsCollectionBinder;
            _usersPickedCollectionBinder = usersPickedCollectionBinder;
        }
        
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) 
        {
            BinderValueProvider valueProvider = new BinderValueProvider(bindingContext);
         
            int currentStep = valueProvider.GetValue<int>("CurrentStep");
            ObjectiveCreateModel model = new ObjectiveCreateModel
                                {
                                    CurrentStep = currentStep,
                                    CreateStep1 = new CreateStep1
                                                    {
                                                        CurrentStep = currentStep,
                                                        Description = valueProvider.GetValue<string>("Description"),
                                                        Due = valueProvider.GetValue<string>("Due"),
                                                        Name = valueProvider.GetValue<string>("Name"),
                                                        Visibility = valueProvider.GetValue<ObjectiveVisibility>("Visibility")
                                                    },
                                    CreateStep2 = new CreateStep2
                                                      {
                                                          CurrentStep = currentStep,
                                                          CurrentKeyResult = (KeyResultModel)_keyResultBinder.SetPrefix("CurrentKeyResult")
                                                                                                            .BindModel(controllerContext,bindingContext),
                                                          KeyResults = BindKeyResults(controllerContext,bindingContext),
                                                          IsReadyToGo = true
                                                      },
                                    CreateStep3 = new CreateStep3
                                                      {
                                                         CurrentStep = currentStep,
                                                         Invitation = valueProvider.GetValue<string>("Invitation"),
                                                         AddMe = valueProvider.GetValue<bool>("AddMe"),
                                                         MyTeam = valueProvider.GetValue<bool>("MyTeam"),
                                                         IsReadyToGo = true,
                                                         UserPickerModel = new UserPickerControl
                                                                               {
                                                                                   UserNames = valueProvider.GetValue<string>("UserNames"),
                                                                                   UsersJson = valueProvider.GetValue<string>("UsersJson"),
                                                                                   CurrentNames = valueProvider.GetValue<string>("CurrentNames"),
                                                                                   UsersPicked = BindPickedUsers(controllerContext,bindingContext)
                                                                               }
                                                      },
                                    CreateStep4 = new CreateStep4
                                                      {
                                                          CurrentStep = currentStep,
                                                          IsReadyToGo = true,
                                                          Image = valueProvider.GetValue<HttpPostedFileBase>("ObjectiveIcon"),
                                                          IconsJson = valueProvider.GetValue<string>("IconsJson"),
                                                          CurrentIconId = valueProvider.GetValue<ObjectId>("CurrentIconId")
                                                      }
                                };

            var validationResult = bindingContext.ModelState.ValidateAndKeepState(model.CreateStep1);
            model.CreateStep1.IsReadyToGo = validationResult.IsValid;

            validationResult = bindingContext.ModelState.ValidateAndKeepState(model.CreateStep2);
            model.CreateStep2.IsReadyToGo = validationResult.IsValid;

            validationResult = bindingContext.ModelState.ValidateAndKeepState(model.CreateStep3);
            model.CreateStep3.IsReadyToGo = validationResult.IsValid;

            validationResult = bindingContext.ModelState.ValidateAndKeepState(model.CreateStep4);
            model.CreateStep4.IsReadyToGo = validationResult.IsValid;

            return model;
        }

        private List<KeyResultModel> BindKeyResults(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            return ((IEnumerable<KeyResultModel>) _keyResultsCollectionBinder.SetIdPropertyName("Name")
                                                      .SetPrefix("keyResult")
                                                      .BindModel(controllerContext, bindingContext))
                                                      .ToList();
        }

        private List<UserPicked> BindPickedUsers(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            return ((IEnumerable<UserPicked>)_usersPickedCollectionBinder.SetIdPropertyName("Id")
                                                                         .BindModel(controllerContext, bindingContext))
                                                                         .ToList();
        }
    }
}