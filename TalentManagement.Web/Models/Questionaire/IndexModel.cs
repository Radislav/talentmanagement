﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace TalentManagement.Web.Models.Questionaire
{
    public class IndexModel : IQuestionnarie
    {
        public IndexModel()
        {
            Answers = new List<AnswerModel>();
        }

        public IEnumerable<AnswerModel> Answers { get; set; }
        public ObjectId ReviewId { get; set; }
    }
}