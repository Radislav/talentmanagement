﻿using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Teams.Map
{
    internal class TeamModelMapper : ITeamModelMapper
    {
        private readonly IMapper<User, UserPicked> _userPickedMapper;
        private IEnumerable<User> _users;

        public TeamModelMapper(IMapper<User, UserPicked> userPickedMapper)
        {
            _userPickedMapper = userPickedMapper;
        }

        public TeamModel Map(Team team, IEnumerable<User> users,bool isExpanded)
        {
            _users = users;

            return new TeamModel
                {
                    Name = team.Name,
                    Id = team.Id,
                    IsUnassigned = team.IsUnassigned,
                    Manager = ExtractManager(team),
                    Members = MapMembers(team),
                    Users = MapNotBindedUsers(team.Members),
                    IsExpanded = isExpanded
                };

          
        }

        private List<UserPicked> MapNotBindedUsers(IEnumerable<string> members)
        {
            return _users.Where(u => false == members.Contains(u.Id))
                           .Select(u => _userPickedMapper.Map(u))
                           .ToList();
        }

        private List<UserPicked> MapMembers(Team team)
        {
            return team.MembersWithoutManager.Select(userId => _userPickedMapper.Map(GetUser(userId)))
                       .ToList();
        }

        private UserPicked ExtractManager(Team team)
        {
            return team.IsUnassigned ? new UserPicked() : _userPickedMapper.Map(GetUser(team.Manager));
        }

        private User GetUser(string userId)
        {
            return _users.Single(u => u.Id == userId);
        }
    }
}