﻿namespace TalentManagement
{
    public static class RegularExpressions
    {
        public const string Email = @"^[A-Za-z][A-Za-z0-9]*([._-]?[A-Za-z0-9]+)@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
        public const string EmailUserName = @"^[A-Za-z][A-Za-z0-9]*([._-]?[A-Za-z0-9]+)$";
        public const string EmailDomainShort = @"^.*@(?<domain>.*)\..*$";
        public const string EmailDomainFull = @"^.*@(?<domain>.*)$";
        public const string EmailUserNameGroup = @"^(?<username>.*)@.*$";
        

    }
}
