﻿using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.MyReviews.Map
{
    public class IndexModelMap : IMap<IEnumerable<Review> ,IndexModel>
    {
        public void Configure(IMapperConfiguration<IEnumerable<Review>, IndexModel> configuration)
        {
            configuration.UseValue(d => d.ActiveButton, Resources.Models.MyReviews.IndexModel.RequiredAttention);
            configuration.ForMember(d => d.Reviews, s => s.Select(review => new ReviewModel
                {
                    Action = Resources.Models.MyReviews.IndexModel.Action,
                    CycleOwner = review.CreatedBy,
                    DueDate = review.DueDate.AsShortDateString(),
                    Id =  review.Id,
                    ReviewAbout = review.Respondent,
                    QuestionSetType = review.SetType,
                    CycleId = review.ReviewCycleId
                }));
        }
    }
}