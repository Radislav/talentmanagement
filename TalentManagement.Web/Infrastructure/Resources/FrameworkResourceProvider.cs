﻿using System;
using System.Web.Compilation;
using TalentManagement.Resources.Views.Shared;

namespace TalentManagement.Web.Infrastructure.Resources
{
    public class FrameworkResourceProvider : IResourceProvider
    {
        private readonly string _resourceClass;
        public FrameworkResourceProvider(string resourceClass)
        {
            _resourceClass = resourceClass;
        }

        public object GetObject(string resourceKey, System.Globalization.CultureInfo culture)
        {
            return new System.Resources.ResourceManager(_resourceClass, typeof(Layout).Assembly)
                .GetObject(resourceKey);
        }

        public System.Resources.IResourceReader ResourceReader
        {
            get { throw new NotImplementedException(); }
        }
    }
}