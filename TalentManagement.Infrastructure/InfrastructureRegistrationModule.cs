﻿using Autofac;
using ClosedXML.Excel;
using MongoDB.Bson;
using TalentManagement.Domain;
using TalentManagement.Domain.Events;
using TalentManagement.Domain.Objectives;
using TalentManagement.Domain.Performance;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Cache;
using TalentManagement.Infrastructure.Configuration;
using TalentManagement.Infrastructure.Converter;
using TalentManagement.Infrastructure.Data;
using TalentManagement.Infrastructure.Email;
using TalentManagement.Infrastructure.Events;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Parsers;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Infrastructure.Serialization;
using TalentManagement.Infrastructure.Services;
using TalentManagement.Infrastructure.Session;
using TalentManagement.Infrastructure.Validation;

namespace TalentManagement.Infrastructure
{
    public class InfrastructureRegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new DomainRegistrationModule());
            builder.RegisterModule(new DataRegistrationModule());
            
            #region Cache

            builder.RegisterType<GlobalCache>()
                .As<IGlobalCache>()
                .SingleInstance();

            #endregion 

            #region Events

            builder.RegisterType<DomainEvents>()
                .As<IDomainEvents>();

            builder.RegisterType<ReviewCycleInsertedHandler>()
                   .As<IHandle<ReviewCycleInsertedArgs>>();

            builder.RegisterType<UserUpdatedHandler>()
                   .As<IHandle<UserUpdatedArgs>>();

            builder.RegisterType<BadgeUpdatedHandler>()
                .As<IHandle<BadgeUpdatedArgs>>();

            #endregion

            #region Configuration

            builder.RegisterType<AppConfiguration>()
                .As<IAppConfiguration>()
                .SingleInstance();

            #endregion 

            #region Converters

            builder.RegisterGeneric(typeof (DefaultTypeConverter<,>))
                .As(typeof (ITypeConverter<,>));

            builder.RegisterConverterForEnum<ObjectiveStatus>();
            builder.RegisterConverterForEnum<ReviewCycleStatus>();
            builder.RegisterConverterForEnum<AnswerType>();
            builder.RegisterConverterForEnum<ReviewCycleVisibility>();
            builder.RegisterConverterForEnum<ReviewStatus>();
            
            builder.RegisterType<ObjectIdConverter>()
                .As<ITypeConverter<ObjectId, string>>()
                .Keyed<ITypeConverter>(typeof(ObjectId));

            builder.RegisterType<ObjectIdArrayConverter>()
                .As<ITypeConverter<ObjectId[], string[]>>()
                .Keyed<ITypeConverter>(typeof(ObjectId[]));

            
            
            #endregion

            #region Security

            builder.RegisterType<AuthenticationService>()
                .As<IAuthenticationService>()
                .InstancePerDependency();

            builder.RegisterType<AuthorizationService>()
                .As<IAuthorizationService>()
                .InstancePerDependency();

            builder.RegisterType<HashComputer>()
                .As<IHashComputer>()
                .InstancePerDependency();

            builder.RegisterType<SecurityTokenGenerator>()
                .As<IPasswordGenerator>()
                .As<ISecurityLinkGenerator>()
                .SingleInstance();

            #endregion

            #region Serialization

            builder.RegisterGeneric(typeof(XmlSerializer<>))
                .As(typeof(IXmlSerializer<>))
                .InstancePerDependency();

            builder.RegisterGeneric(typeof(StreamSerializer<>))
                 .As(typeof(IStreamSerializer<>))
                 .InstancePerDependency();

            #endregion 

            #region Email

            builder.RegisterType<EmailSender>()
                .As<IEmailSender>()
                .InstancePerDependency();

            #endregion 

            #region Parsers

            builder.RegisterType<UserGroupParserExcel>()
                .As<IRowsParser<IXLRow, GroupRowParsedEventArgs>>()
                .Keyed<ITemplateProvider<GroupRowParsedEventArgs>>(FileType.Xlsx);

            builder.RegisterType<GroupRowParsedEventArgsValidator>()
                .As<IValidator<GroupRowParsedEventArgs>>();

            builder.RegisterType<GroupRowToUser>()
                .As<IMap<GroupRowParsedEventArgs, User>>();

            builder.RegisterType<UserGroupParserCsv>()
                .As<IRowsParser<string[], GroupRowParsedEventArgs>>()
                .As<ITemplateProvider<GroupRowParsedEventArgs>>();

            #endregion

            #region Services

            builder.RegisterType<UserService>()
                   .As<IUserService>();
            
            builder.RegisterType<PerformanceObjectsFactory>()
                   .As<IPerformanceObjectsFactory>();

            builder.RegisterType<RelationshipService>()
                   .As<IRelationshipService>();

            #endregion 

            #region Mapper

            builder.RegisterGeneric(typeof(Mapper<,>))
                .As(typeof(IMapper<,>))
                .SingleInstance();

            builder.RegisterGeneric(typeof (DefaultMap<,>))
                .As(typeof (IMap<,>))
                .InstancePerDependency();

            #endregion

            #region Session

            builder.RegisterType<SessionFactory>()
                .As<ISessionFactory>()
                .InstancePerDependency();

            #endregion 

            #region Validation

            builder.RegisterType<BasicValidator>()
                .As<IBasicValidator>()
                .InstancePerDependency();

            #endregion
          
            base.Load(builder);
        }
    }
}
