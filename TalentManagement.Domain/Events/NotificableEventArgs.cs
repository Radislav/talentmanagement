﻿using System.Diagnostics.Contracts;
using TalentManagement.Domain.Texts;

namespace TalentManagement.Domain.Events
{
    public abstract class NotificableEventArgs : IDomainEventArgs
    {
        protected NotificableEventArgs(TextType notificationTextType, string senderId,Entity entity, params string[] receiversIds) : this(notificationTextType,senderId,receiversIds)
        {
            Contract.Requires(senderId != null);
            Contract.Requires(receiversIds != null);

            Entity = entity;
        }


        protected NotificableEventArgs(TextType notificationTextType,string senderId, params string[] receiversIds)
        {
            Contract.Requires(senderId != null);
            Contract.Requires(receiversIds != null);

            SenderId = senderId;
            ReceiversIds = receiversIds;
            NotificationTextType = notificationTextType;
        }

       
        public string SenderId { get; private set; }
        public string[] ReceiversIds { get; private set; }
        public TextType NotificationTextType { get; private set; }
        public Entity Entity { get; private set; }
    }
}
