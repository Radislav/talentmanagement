﻿using System.Linq;
using TalentManagement.Domain.Feedbacks;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.MyFeedbacks.Map
{
    internal class DetailsModelMap : IMap<Feedback,DetailsModel>
    {
        private readonly IMapper<Response, DetailsResponseModel> _responseModelMapper;

        public DetailsModelMap(IMapper<Response, DetailsResponseModel> responseModelMapper)
        {
            _responseModelMapper = responseModelMapper;

        }

        public void Configure(IMapperConfiguration<Feedback, DetailsModel> configuration)
        {
            configuration.ForMember(d => d.Feedback, s => s.Question);
            configuration.ForMember(d => d.FeedbackDate, s => s.CreatedAt.AsLondDateString());
            configuration.ForMember(d => d.ParticipantsCount, s => s.Participants.Count());
            configuration.ForMember(d => d.ResponsesCount, s => s.Responses.Count());
            configuration.ForMember(d => d.Responses,
                                    s => s.Responses.Select(r => _responseModelMapper.Map(r)).ToArray());
        }
    }
}