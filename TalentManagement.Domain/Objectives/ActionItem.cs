﻿using System;
using System.Diagnostics.Contracts;

namespace TalentManagement.Domain.Objectives
{
    public class ActionItem : Entity 
    {
        private ActionItem() 
        {
            //For Serializers and Data mappers
        }

        public ActionItem(string name,string owner,DateTime dueDate,bool relatedToObjective)
        {
            Contract.Requires(name != null);
            Contract.Requires(owner != null);
            Contract.Requires(dueDate > DateTime.Now);
            
            Name = name;
            Owner = owner;
            RelatedToObjective = relatedToObjective;
            DueDate = dueDate;
            Status = ActionItemStatus.NotStarted;
        }
        
        public string Name { get;  private set; }
        public string Owner { get; private set; }
        public bool RelatedToObjective { get; internal set; }
        public ActionItemStatus Status { get; set; }
        public DateTime DueDate { get; internal set; }
       
    }
}
