﻿using System.Web;
using System.Web.Mvc;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.Binder
{
    public class RegistrationModelBinder : CustomModelBinder<RegistrationModel> 
    {
        public override  object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            BinderValueProvider valueProvider = new BinderValueProvider(bindingContext);
            
            int currentStep =  valueProvider.GetValue<int>("CurrentStep");

            RegistrationModel model = new RegistrationModel
                                          {
                                              CurrentStep = currentStep,
                                              Invitation = new InvitationModel
                                                               {
                                                                   UserEmail = valueProvider.GetValue<string>("Email"),
                                                                   Email1 = valueProvider.GetValue<string>("Email1"),
                                                                   Email2 = valueProvider.GetValue<string>("Email2"),
                                                                   Email3 = valueProvider.GetValue<string>("Email3"),
                                                                   Email4 = valueProvider.GetValue<string>("Email4"),
                                                                   Email5 = valueProvider.GetValue<string>("Email5"),
                                                                   CurrentStep = currentStep
                                                               },
                                              Profile = new ProfileModel
                                                            {
                                                                Email = valueProvider.GetValue<string>("Email"),
                                                                ConfirmPassword = valueProvider.GetWithCache<string>("ConfirmPassword"),
                                                                FirstName = valueProvider.GetValue<string>("FirstName"),
                                                                ManagerEmail = valueProvider.GetValue<string>("ManagerEmail"),
                                                                Password = valueProvider.GetWithCache<string>("Password"),
                                                                Position = valueProvider.GetValue<string>("Position"),
                                                                ProfileImage = valueProvider.GetWithCache<HttpPostedFileBase>("ProfileImage"),
                                                                SecondName = valueProvider.GetValue<string>("SecondName"),
                                                                InviteColleagues = valueProvider.GetValue<bool>("InviteColleagues"),
                                                                CurrentStep = currentStep
                                                            }
                                          };

           var validationResult =  bindingContext.ModelState.ValidateAndKeepState(model.Profile);
           model.Profile.IsReadyToGo = validationResult.IsValid;

           validationResult = bindingContext.ModelState.ValidateAndKeepState(model.Invitation);
           model.Invitation.IsReadyToGo = validationResult.IsValid;

            return model;
        }
    }
}