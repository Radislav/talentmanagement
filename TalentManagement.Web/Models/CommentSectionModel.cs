﻿using System.Linq;

namespace TalentManagement.Web.Models
{
    public class CommentSectionModel
    {
        public CommentModel[] Comments { get; set; }
        public CommentModel NewComment { get; set; }
        public bool ShowNewComment { get; set; }
        public bool HasComments
        {
            get { return Comments != null && Comments.Any(); }
        }

        public string HolderId { get; set; }
        
    }
}