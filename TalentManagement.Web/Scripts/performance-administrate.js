﻿$(function () {
    $('#activity_stats .btn-group .btn').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('href'),
            success: function (response) {
                $('#review-cycle-table').html(response);
            }
        });
    });
});


