﻿using System.Diagnostics.Contracts;
using System.Linq;
using TalentManagement.Domain;

namespace TalentManagement.Infrastructure.Data.Repositories
{
    internal class DecoratedRepository<TEntity, TId> : IRepositoryWithTypedId<TEntity, TId>
        where TEntity : EntityWithTypedId<TId>
    {
        protected readonly IRepositoryWithTypedId<TEntity, TId> RepositoryDecorated;
        private readonly IFilter _filter;

        public DecoratedRepository(IRepositoryWithTypedId<TEntity, TId> repositoryDecorated)
        {
            RepositoryDecorated = repositoryDecorated;
        }
        
        public DecoratedRepository(IRepositoryWithTypedId<TEntity,TId> repositoryDecorated,IFilter filter)
            : this(repositoryDecorated)
        {
            _filter = filter;
        }

        [ContractInvariantMethod]
        private void Invariant()
        {
            Contract.Invariant(RepositoryDecorated != null);
        }

        public virtual void Insert(TEntity entity)
        {
            RepositoryDecorated.Insert(entity);
        }

        public virtual void DeleteById(TId id)
        {
            RepositoryDecorated.DeleteById(id);
        }

        public virtual void Update(TEntity entity)
        {
            RepositoryDecorated.Update(entity);
        }

        public virtual IQueryable<TEntity> AsQueryable()
        {
            return _filter == null
                       ? RepositoryDecorated.AsQueryable()
                       : RepositoryDecorated.AsQueryable()
                                            .ApplyFilter(_filter)
                                            .AsQueryable();
        }

        public virtual TEntity GetById(TId id)
        {
            return RepositoryDecorated.GetById(id);
        }


        public TEntity Upsert(TEntity entity)
        {
            return RepositoryDecorated.Upsert(entity);
        }


        public System.Collections.Generic.IEnumerable<TEntity> ApplyFilter(IFilter filter)
        {
            return AsQueryable()
                        .ApplyFilter(filter)
                        .ToList();
        }
    }
}
