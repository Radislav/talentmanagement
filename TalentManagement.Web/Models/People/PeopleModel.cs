﻿using TalentManagement.Domain.Users;

namespace TalentManagement.Web.Models.People
{
    public class PeopleModel
    {
        public string SecondNamePrefix { get; set; }

        public UserStatus UserStatus { get; set; }
    }
}