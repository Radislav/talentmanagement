﻿namespace TalentManagement.Domain
{
    public interface IHasScope
    {
        IScope Scope { get; }
    }
}
