﻿using System;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Converter;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.PerformanceAdministrate
{
    internal class ReviewCycleModelMap : IMap<ReviewCycle,ReviewCycleModel>
    {
        private readonly ITypeConverter<ReviewCycleStatus,string> _statusConverter;

        public ReviewCycleModelMap(ITypeConverter<ReviewCycleStatus,string> statusConverter)
        {
            _statusConverter = statusConverter;
        }

        public void Configure(IMapperConfiguration<ReviewCycle, ReviewCycleModel> configuration)
        {
            configuration.ForMember(d => d.Id, s => s.Id);
            configuration.ForMember(d => d.Action, s => _statusConverter.ConvertBack(s.Status));
            configuration.ForMember(d => d.Name, s => s.Name);
            configuration.ForMember(d => d.Progress, s => String.Format("{0:n0}%", s.Progress));
            configuration.ForMember(d => d.ReviewPeriod,
                                    s =>
                                    String.Format("{0} - {1}", s.ReviewStart.AsShortDateString(),s.ReviewEnd.AsShortDateString()));
        }
    }
}