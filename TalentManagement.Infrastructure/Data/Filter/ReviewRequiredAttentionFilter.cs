﻿using TalentManagement.Domain.Performance;

namespace TalentManagement.Infrastructure.Data.Filter
{
    internal class ReviewRequiredAttentionFilter : FilterBase<Review>
    {
        public override Domain.IFilterContext FilterContext
        {
            get
            {
                return new EmptyFilterContext();
            }
            set
            {}
        }

        public override Domain.IFilter<Review> Filter(Domain.IFilterContext[] filterContexts)
        {
            //TODO : update following the requeirements
            Data = Data;

            return this;
        }
    }
}
