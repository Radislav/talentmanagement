﻿using Autofac;
using TalentManagement.Domain.Feeds;

namespace TalentManagement.Domain
{
    public class DomainRegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FeedFactory>()
                .As<IFeedFactory>();

            base.Load(builder);
        }
    }
}
