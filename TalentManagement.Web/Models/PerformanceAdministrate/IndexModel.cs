﻿using System.Collections.Generic;

namespace TalentManagement.Web.Models.PerformanceAdministrate
{
    public class IndexModel : ICanActivateButton
    {
        public IndexModel()
        {
            ReviewCycles = new List<ReviewCycleModel>();
            ActiveButton = Resources.Models.PerformanceAdministrate.IndexModel.Active;
        }

        public IEnumerable<ReviewCycleModel> ReviewCycles { get; set; }
        public string ActiveButton { get; set; }
    }
}