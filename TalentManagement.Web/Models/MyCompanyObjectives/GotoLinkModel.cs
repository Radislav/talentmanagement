﻿namespace TalentManagement.Web.Models.MyCompanyObjectives
{
    public class GotoLinkModel
    {
        public string Text { get; set; }
        public string Reference { get; set; }
    }
}