﻿using System;
using System.Diagnostics.Contracts;
using TalentManagement.Domain.Texts;

namespace TalentManagement.Domain.Users
{
    public class UserNotification : Entity , IOwned
    {
        protected  UserNotification()
        {
        }

        //TODO: just for testing. remove it later
        public UserNotification(string senderId, 
                                string text,TextType textType) 
        {
            Contract.Requires(senderId != null);

            SenderId = senderId;
            Text = text;
            TextType = textType;
        }

        public UserNotification(string senderId,
                                Text text)
            : this(senderId, text.Content, text.Type)
        {
            Contract.Requires(senderId != null);
            Contract.Requires(text != null);
        }
        

        public TextType TextType { get; private set; }

        public string Text { get; private set; }

        public string SenderId { get; private set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedAt { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}
