﻿using System.Diagnostics.Contracts;
using TalentManagement.Domain.Objectives;
using TalentManagement.Domain.Texts;

namespace TalentManagement.Domain.Events
{
    public class ObjectiveUpdatedArgs : NotificableEventArgs
    {
        public ObjectiveUpdatedArgs(Objective objectiveNew,Objective objectiveOld, string senderId, string[] receiversIds)
            : base(TextType.NotificationObjectiveUpdated, senderId, objectiveNew, receiversIds)
        {
            Contract.Requires(objectiveNew != null);
            Contract.Requires(objectiveOld != null);
            Contract.Requires(senderId != null);
            Contract.Requires(receiversIds != null);

            ObjectiveNew = objectiveNew;
            ObjectiveOld = objectiveOld;

        }

        public Objective ObjectiveNew { get; private set; }
        public Objective ObjectiveOld { get; private set; }
    }
}
