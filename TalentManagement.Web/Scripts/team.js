﻿$(function () {

    init();

    function getManagerId(teamId) {
        var container = $('article[data-team-id="' + teamId + '"]');
        return container.attr('readonly') ? '' : container.data('manager');
    }

    function getExpandMap() {
        var mapItems = [];
        $('.team').each(function () {
            mapItems.push({
                TeamId: $(this).data('team-id'),
                IsExpanded: $(this).find('.accordion-body').hasClass('in')
            });
        });
        return mapItems;
    }

    function setMembership(userId, managerId) {
        var data = {
            userId: userId,
            managerId: managerId,
            expandMap: getExpandMap()
        };
        $.ajax({
            url: urls.setMembership(),
            data: JSON.stringify(data),
            success: function (response) {
                $('#teams').html(response);
                init();
            }
        });
    }

    function updateTeamName(teamContainer) {

        var teamId = $(teamContainer).data('team-id');
        var teamName = $(teamContainer).find('#team_Name').val();

        var data = {
            teamId: teamId,
            teamName: teamName
        };
        $.ajax({
            url: urls.updateTeamName(),
            data: JSON.stringify(data),
            success: function (response) {
                $('#updated_' + response).css('display', 'block');

                setTimeout(function () {
                    $('#updated_' + response).hide('slide');
                }, 3000);
            }
        });
    }

    function init() {
        $('.team').each(function () {
            var picker = new UserPicker({
                prefix: $(this).data('team-id'),
                container: $(this)
            });

            //On user picked
            picker.bindUserPicked(function (event, pickerModel, userPicked) {
                setMembership(userPicked, getManagerId(pickerModel.Prefix));
            });

            //On user removed
            picker.bindUserRemoved(function (event, pickerModel, userPicked) {
                setMembership(userPicked, '');
            });

            //Team name focus leaved
            $(this).find('#team_Name').on('focusout', function () {
                updateTeamName($(this).parents('.team'));
            });

            //Team name - enter or tab pressed
            $(this).find('#team_Name').on('keyup', function (e) {
                if (e.which == 13 || e.which == 9) {
                    updateTeamName($(this).parents('.team'));    
                }
            });
        });
    }

});



