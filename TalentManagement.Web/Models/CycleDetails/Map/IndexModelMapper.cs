﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using TalentManagement.Domain.Performance;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Converter;
using TalentManagement.Infrastructure.Resources;

namespace TalentManagement.Web.Models.CycleDetails.Map
{
    internal class IndexModelMapper : IIndexModelMapper
    {
        private readonly IUserRepository _userRepository;
        private readonly IResourceProvider _resourceProvider;
        private readonly ITypeConverter<ReviewStatus, string> _reviewStatusConverter;


        public IndexModelMapper(IUserRepository userRepository, IResourceProvider resourceProvider,ITypeConverter<ReviewStatus, string> reviewStatusConverter)
        {
            _userRepository = userRepository;
            _resourceProvider = resourceProvider;
            _reviewStatusConverter = reviewStatusConverter;
        }

        public IndexModel Map(ReviewCycle source,IEnumerable<Review> reviews, IEnumerable<ReviewCycleDescriptor> descriptors)
        {
            if (source == null)
            {
                return new IndexModel
                    {
                        ActiveCycle = ObjectId.Empty,
                        CycleProgressText = _resourceProvider.CycleDetailsProgress,
                        Cycles = descriptors
                    };
            }
              
            var participants = _userRepository.GetUsers(source.Participants);
            return new IndexModel
                {
                    ActiveCycle = source.Id,
                    Progress = source.Progress,
                    CycleProgressText = _resourceProvider.CycleDetailsProgress,
                    Cycles = descriptors,
                    Reviews = reviews.Select(review => new ReviewModel
                                                                    {
                                                                        EmployeeName = participants.First(p => p.Id == review.Respondent).Names,
                                                                        Id = review.Id,
                                                                        Status = _reviewStatusConverter.ConvertBack(review.Status)
                                                                    })

                };
        }
    }
}