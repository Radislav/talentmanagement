﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using TalentManagement.Infrastructure.Converter;
using TalentManagement.Web.Models.Validation;
using UiResources = TalentManagement.Resources.UI;

namespace TalentManagement.Web.Infrastructure.UI
{
    public static class HtmlExtensions
    {
        public static IDictionary<string, object> CreateIdAttributesFor<TModel, TValue>
            (this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TValue>> expression, string prefix)
        {
            if (prefix == null)
            {
                return null;
            }

            string propertyName = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData).PropertyName;
            return new Dictionary<string, object>
                {
                    {"id", String.Concat(prefix, "_", propertyName)},
                    {"name", String.Concat(prefix, "_", propertyName)}
                };
        }

        internal static TAttribute GetMemberAttribute<TAttribute>(MemberExpression memberExpression)
          where TAttribute : Attribute
        {
            return memberExpression.GetSafe(m => m.Member.GetCustomAttributes(typeof(TAttribute), true))
                            .OfType<TAttribute>()
                            .SingleOrDefault();
        }

        public static MvcHtmlString FileFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, HttpPostedFileBase>> expression)
        {
            TagBuilder containerSpan = new TagBuilder("span");
            containerSpan.AddCssClass("file-upload");
            
            TagBuilder button = new TagBuilder("a");
            button.SetInnerText(UiResources.Upload);
            
            MvcHtmlString file =  htmlHelper.TextBoxFor(expression, new { type = "file"});
            TagBuilder chosenFilename = new TagBuilder("span");
            chosenFilename.MergeAttribute("name", "filename");
            var postedFile = expression.Compile().Invoke(htmlHelper.ViewData.Model);
            if (postedFile != null )
            {
               chosenFilename.SetInnerText(postedFile.FileName);
            }

            containerSpan.InnerHtml = String.Concat(button, file, chosenFilename);

            return new MvcHtmlString(containerSpan.ToString());
        }
        
	    public static IHtmlString TipsyValidationMessageFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                            Expression<Func<TModel, TValue>> expression)
        {
            string result = String.Empty;
            string[] errorMessages = GetValidationMessagesFrom(html, expression);
            if (errorMessages.GetSafe(e => e.Any()))
            {
                TagBuilder tagBuilder = new TagBuilder("span");
                tagBuilder.AddCssClass("field-validation-error");
                tagBuilder.MergeAttribute("original-title", String.Join(". ", errorMessages));
                result = tagBuilder.ToString();
            }
            return new MvcHtmlString(result);
        }
     
        /// <summary>
        /// Returns tuple of enum names. Item1 - enum property name, Item2 - localized name
        /// </summary>
        /// <param name="metaData">Model meta</param>
        /// <param name="excludeNoneValue">When true then Enum value "none" will not be in result.</param>
        /// <returns></returns>
        internal static EnumNamesPair[] GetEnumNames(ModelMetadata metaData,bool excludeNoneValue)
        {
            Type enumType = metaData.ModelType.IsGenericType && metaData.ModelType.GetGenericTypeDefinition() == typeof(Nullable<>)
                                ? metaData.ModelType.GetGenericArguments()[0]
                                : metaData.ModelType;

            ITypeConverter converter = DependencyResolver.Current.GetServiceOptional<ITypeConverter>(enumType);

            return Enum.GetValues(enumType)
                       .Cast<object>()
                       .Select(enumValue =>
                           {
                               string enumName = Enum.GetName(enumType, enumValue);
                               string displayName = converter == null ? enumName : (string) converter.ConvertBack(enumValue);
                               return new EnumNamesPair(enumName, displayName);
                           })
                         .Where(pair => false == excludeNoneValue || pair.Name != TalentManagement.Resources.DomainResources.None)
                        .ToArray();
        }
        
        public  static MvcHtmlString DropDownForEnum<TModel, TProperty>
            (this HtmlHelper<TModel> htmlHelper,
         Expression<Func<TModel, TProperty>> expression,
         IDictionary<string, object> htmlAttributes = null,
         string selectedName = null,
         bool exludeNoneValue = true)
            where TProperty : struct 
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            var names = GetEnumNames(metaData, exludeNoneValue);
            SelectListItem[] listItems =
                names.Select(name => new SelectListItem
                                         {
                                             Text = name.DisplayName, 
                                             Value = name.Name, 
                                             Selected = (selectedName == name.Name)
                                         })
                     .ToArray();

            if(String.IsNullOrEmpty(selectedName) && listItems.Any())
            {
                string propertyValue = expression.Compile()
                    .Invoke(htmlHelper.ViewData.Model)
                    .ToString();

                listItems.FirstOrDefault( item => item.Value == propertyValue)
                         .Do(item => item.Selected = true);
            }
            
            string dropDownHtml = htmlHelper.DropDownListFor(expression,listItems,htmlAttributes)
                                            .ToHtmlString();
    
            TagBuilder outerDiv = new TagBuilder("div");
            outerDiv.MergeAttribute("class", "form_input");

            outerDiv.InnerHtml = dropDownHtml;
            
            return new MvcHtmlString(outerDiv.ToString());
           
        }
     
       private static string[] GetValidationMessagesFrom<TModel, TValue>(HtmlHelper<TModel> html,
                                                                    Expression<Func<TModel, TValue>> expression)
       {

           IPropertyToValidate propertyToValidate = html.ViewData.Model as IPropertyToValidate;

           string propertyName = propertyToValidate.GetSafe(p => p.PropertyToValidateName) ?? 
                                  ModelMetadata.FromLambdaExpression(expression, html.ViewData).PropertyName;

           return GetValidationMessagesFrom(html, propertyName);
       }

       private static string[] GetValidationMessagesFrom<TModel>(HtmlHelper<TModel> html, string propertyName)
       {
           String prefix = html.ViewData.TemplateInfo.HtmlFieldPrefix;
           String propertyKey = String.IsNullOrEmpty(prefix) ? propertyName : String.Concat(prefix, ".", propertyName);

           var modelState = html.ViewData.ModelState;

           string[] errorMessages = modelState.ContainsKey(propertyKey) ?
                                    modelState[propertyKey].Errors
                                      .Select(error => error.ErrorMessage)
                                      .ToArray()
                                      :
                                    null;
           return errorMessages;
       }

    internal static MvcHtmlString ControlStyledFor<TModel, TValue>(HtmlHelper<TModel> html,
                                                            Func<Expression<Func<TModel, TValue>>,
                                                                IDictionary<string, object>,
                                                                MvcHtmlString> controlFunctor,   
                                                            Expression<Func<TModel, TValue>> expression,
                                                            object attributes)
    {
        return ControlStyledFor(html, controlFunctor, expression,
                                HtmlHelper.AnonymousObjectToHtmlAttributes(attributes));
    }

        internal static MvcHtmlString ControlStyledFor<TModel, TValue>(HtmlHelper<TModel> html,
                                                                      Func<Expression<Func<TModel, TValue>>,
                                                                           IDictionary<string, object>,
                                                                           MvcHtmlString> controlFunctor,   
                                                                      Expression<Func<TModel, TValue>> expression,
                                                                      IDictionary<string, object> attributes)
        {
            TagBuilder outerDiv = new TagBuilder("div");
            outerDiv.AddCssClass("form_input");

            attributes = attributes ?? new Dictionary<string, object>();

            string[] errorMessages = GetValidationMessagesFrom(html, expression);

            if (errorMessages != null)
            {                
                if (errorMessages.Any())
                {
                    attributes["original-title"] = String.Join(". ", errorMessages);
                    attributes["class"] = MergeCssClass(attributes, "in_error");
                    attributes["data-content"] = String.Join(". ", errorMessages);
                }
            }
            else
            {
                TValue value = expression.Compile()
                                         .Invoke(html.ViewData.Model);
                if (false == value.IsDefault())
                {
                    attributes["class"] = MergeCssClass(attributes, "in_submitted");
                }
            }

            outerDiv.InnerHtml = controlFunctor(expression,attributes).ToHtmlString();

            return new MvcHtmlString(outerDiv.ToString());
        }

        internal static MvcHtmlString ControlStyled<TModel>(HtmlHelper<TModel> html,
                                                                      Func<string,object,IDictionary<string, object>,MvcHtmlString> controlFunctor,
                                                                      string name,
                                                                      object value,
                                                                      IDictionary<string, object> attributes)
        {
            TagBuilder outerDiv = new TagBuilder("div");
            outerDiv.AddCssClass("form_input");

            attributes = attributes ?? new Dictionary<string, object>();

            string[] errorMessages = GetValidationMessagesFrom(html, name);

            if (errorMessages != null)
            {
                if (errorMessages.Any())
                {
                    attributes["original-title"] = String.Join(". ", errorMessages);
                    attributes["class"] = MergeCssClass(attributes, "in_error");
                    attributes["data-content"] = String.Join(". ", errorMessages);
                }
            }

            outerDiv.InnerHtml = controlFunctor(name,value, attributes).ToHtmlString();

            return new MvcHtmlString(outerDiv.ToString());
        }


        private static string MergeCssClass(IDictionary<string, object> attributes,string cssClass)
        {
            return attributes.ContainsKey("class") && attributes["class"] != null ?
                       String.Concat(attributes["class"], " ", cssClass): 
                       cssClass;
        }
        
        public static MvcHtmlString LocalizedTextFor<TModel,TResult>(this HtmlHelper<TModel> html, Expression<Func<TModel, TResult>> expression,params object[] args)
        {

            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            return metadata.DisplayName == null ? 
                        new MvcHtmlString(String.Empty) : 
                        new MvcHtmlString(String.Format(metadata.DisplayName, args));
        }
        public static MvcHtmlString Submit<TModel>(this HtmlHelper<TModel> html, string value)
        {
            TagBuilder tagBuilder = new TagBuilder("input");
            tagBuilder.MergeAttribute("type", "submit");
            tagBuilder.MergeAttribute("name", value.Trim(' ').ToLower());
            tagBuilder.MergeAttribute("value", value);

            return new MvcHtmlString(tagBuilder.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString UserProfileLink(this HtmlHelper helper,string userId,string userNames)
        {
            TagBuilder tagBuilder = new TagBuilder("a");
            tagBuilder.MergeAttribute("href","#");
            tagBuilder.SetInnerText(userNames);

            return new MvcHtmlString(tagBuilder.ToString());
        }
    }
}