﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Resources;

namespace TalentManagement.Web.Models.ReviewCycleCreation
{
    public class QuestionSetModel
    {
        private readonly IResourceProvider _resourceProvider;
        
        public QuestionSetModel()
        {
            Id = ObjectId.GenerateNewId();
            _resourceProvider = DependencyResolver.Current.GetService<IResourceProvider>();
        }

        public ObjectId Id { get; set; }
        internal static List<QuestionSetModel> DefaultSets
        {
            get
            {
                var resourceProvider = DependencyResolver.Current.GetService<IResourceProvider>();
                return new List<QuestionSetModel>
                                {
                                    new QuestionSetModel
                                        {
                                            SetType = QuestionSetType.Pears,
                                            Title = resourceProvider.ReviewCyclePearsQuestions
                                        },
                                    new QuestionSetModel
                                        {
                                            SetType = QuestionSetType.Self,
                                            Title = resourceProvider.ReviewCycleSelfQuestions
                                        },
                                    new QuestionSetModel
                                        {
                                            SetType = QuestionSetType.Summary,
                                            Title = resourceProvider.ReviewCycleSummaryQuestions
                                        }
                                };
            }
        }
        private List<QuestionModel> _questions = new List<QuestionModel>();
        
        public string Title { get; set; }
        public string QuestionNumberInfo
        {
            get
            {
                return Questions.Any() ? string.Format(_resourceProvider.ReviewCycleHasQuestions, Questions.Count()) : _resourceProvider.ReviewCycleNoQuestions;
            }
        }
        public QuestionSetType SetType { get; set; }
        public IEnumerable<QuestionModel> Questions
        {
            get { return _questions; }
            set { _questions = new List<QuestionModel>(value); }
        }

        public void AddQuestion(QuestionModel question)
        {
            question.Number = _questions.Count + 1;
            question.Id = ObjectId.GenerateNewId();
            question.SetType = SetType;
            _questions.Add(question);
        }

        public void RemoveQuestion(ObjectId questionId)
        {
            QuestionModel question = _questions.SingleOrDefault(q => q.Id == questionId);
            if (question != null)
            {
                _questions.Remove(question);
                for (int index = 0; index < _questions.Count; index++)
                {
                    _questions[index].Number = index + 1;
                }
            }
        }
    }
}