﻿using System.Linq;
using FluentValidation;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.MyFeedbacks.Validation
{
    internal class FeedbackCreateModelValidator  : AbstractValidator<FeedbackCreateModel>,
                                             TalentManagement.Infrastructure.Validation.IValidator<FeedbackCreateModel>
    {
        public FeedbackCreateModelValidator()
        {
            RuleFor(m => m.Question)
                .NotEmpty();

            RuleFor(m => m.Users)
                .Must(users => users != null && users.Any())
                .OverridePropertyName("CurrentNames")
                .WithLocalizedMessage(() => Resources.Errors.UserAtLeastOne);
        }

        public TalentManagement.Infrastructure.Validation.ValidationResult ValidateInstance(FeedbackCreateModel instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}