﻿using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TalentManagement.Domain;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Test.Infrastructure.Security
{
    [TestClass]
    public class AuthenticationServiceTestClass : MockedTestClass<IAuthenticationService>
    {
        private const string UserValid = "user_valid";
        private const string UserInvalid = "user_invalid";
        private const string PasswordValid = "password_valid";
        private const string PasswordValidHashed = "password_valid";
        private const string PasswordInvalid = "password_invalid";

        protected override void RegisterMocks(ContainerBuilder builder)
        {
            builder.RegisterMock<IUserRepository>(
                mock =>
                {
                    mock.Setup(repository => repository.GetById(UserValid)).Returns(new User(UserValid) {  HashedPassword = PasswordValidHashed });
                    mock.Setup(repository => repository.GetById(UserInvalid)).Returns((User)null).Verifiable();

                });

            builder.RegisterMock<IHashComputer>(
               mock =>
               {
                   mock.Setup(hasher => hasher.ComputeHash(PasswordInvalid)).Returns(PasswordInvalid);
                   mock.Setup(hasher => hasher.ComputeHash(PasswordValid)).Returns(PasswordValidHashed);
               });
        }

        [TestMethod]
        public void Login_should_call_IUserRepostitory_GetById()
        {
            IAuthenticationService target = CreateTarget();

            target.Login(UserValid,PasswordValid);

            var mock = Container.Resolve<Mock<IUserRepository>>();
             mock.Verify(m => m.GetById(UserValid),Times.Once());
        }
        
        [TestMethod]
        public void Login_should_return_InvalidUserOrPassword_if_user_not_founded()
        {
            IAuthenticationService target = CreateTarget();

            LoginResult result =  target.Login(UserInvalid,PasswordValid);

            Assert.AreEqual(LoginResult.InvalidUserOrPassword,result);
        }

        [TestMethod]
        public void Login_should_call_IHashComputer_ComputeHash_if_user_exists()
        {
            IAuthenticationService target = CreateTarget();

            target.Login(UserValid ,PasswordValid);

            var mock = Container.Resolve<Mock<IHashComputer>>();
            mock.Verify(m => m.ComputeHash(PasswordValid), Times.Once());
        }

        [TestMethod]
        public void Login_should_return_InvalidUserOrPassword_if_password_or_user_NULL()
        {
            IAuthenticationService target = CreateTarget();
            LoginResult result = target.Login(null ,PasswordValid);
            Assert.AreEqual(LoginResult.InvalidUserOrPassword, result);

            result = target.Login(UserValid, null);
            Assert.AreEqual(LoginResult.InvalidUserOrPassword, result);
        }

        [TestMethod]
        public void Login_should_return_InvalidUserOrPassword_if_password_not_matches()
        {
            int[] s1 = new int[] {1};
            int[] s2 = new int[] {1};

            int h1 = s1.GetHashCode();
            int h2 = s2.GetHashCode();
            IAuthenticationService target = CreateTarget();

            LoginResult result = target.Login(UserValid,PasswordInvalid);

            Assert.AreEqual(LoginResult.InvalidUserOrPassword, result);

        }

        [TestMethod]
        public void Login_should_return_Passed_if_password_matches()
        {
            IAuthenticationService target = CreateTarget();

            LoginResult result = target.Login(UserValid,PasswordValid);

            Assert.AreEqual(LoginResult.Ok, result);
        }
        
        
    }
}
