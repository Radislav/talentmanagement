﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TalentManagement.Domain;
using TalentManagement.Domain.Feeds;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Cache;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Web.Models;
using TalentManagement.Web.Models.Feeds;

namespace TalentManagement.Test.Mapping
{
    internal class CommentModelComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            CommentModel m1 = x as CommentModel;
            CommentModel m2 = y as CommentModel;

            if (m1 == null || m2 == null)
            {
                return -1;
            }

            return m1.Content == m2.Content && m1.CreatedBy == m2.CreatedBy ? 0 : -1;
        }
    }

    [TestClass]
    public class FeedMapperTest : MockedTestClass<IMapper<Feed,FeedModel>>
    {
        protected override void RegisterMocks(Autofac.ContainerBuilder containerBuilder)
        {
            base.RegisterMocks(containerBuilder);
            
            Mock<IDependencyResolver> resolverMock = new Mock<IDependencyResolver>();
            resolverMock.Setup(m => m.GetService(typeof(IGlobalCache))).Returns(new Mock<IGlobalCache>().Object);
            DependencyResolver.SetResolver(resolverMock.Object);
            containerBuilder.RegisterMock(new Mock<IGlobalCache>());

        }
        [TestMethod]
        public void Map_should_copy_base_properties()
        {
            var mapper = CreateTarget();
            Feed source = new Feed
                              {
                                 CreatedAt  = new DateTime(2000,1,1),
                                 CreatedBy = "me",
                                 FeedType = FeedType.Objective
                              };
            FeedModel result =  mapper.Map(source);

            Assert.AreEqual(result.CreatedAt,source.CreatedAt.ToString());
            Assert.AreEqual(result.CreatedBy,source.CreatedBy);
            Assert.AreEqual(result.FeedType,source.FeedType);
        }

        [TestMethod]
        public void Map_should_copy_FeedProperties()
        {
            var mapper = CreateTarget();
            Feed source = new Feed(null, new[]
                                             {
                                                 new FeedProperty {Name = "name1", Value = "value1"},
                                                 new FeedProperty {Name = "name2", Value = "value2"}
                                             });
            

            Dictionary<string, string> expected = new Dictionary<string, string>
                                                     {
                                                         {"name1","value1"},
                                                         {"name2","value2"}
                                                     };

            FeedModel result = mapper.Map(source);

            CollectionAssert.AreEqual(expected,result.Properties);    
        }

        [TestMethod]
        public void Map_should_copy_Comments()
        {
            var mapper = CreateTarget();
            Feed source = new Feed(new List<Comment>
                                       {
                                           new Comment
                                               {
                                                   Content = "content1",
                                                   CreatedBy = "user1",
                                                   CreatedAt = new DateTime(2001, 1, 1)
                                               },
                                           new Comment
                                               {
                                                   Content = "content2",
                                                   CreatedBy = "user2",
                                                   CreatedAt = new DateTime(2002, 2, 2)
                                               }
                                       }, null);
           

            CommentModel[] expected = new []
                                          {
                                              new CommentModel{Content = "content1",CreatedBy = "user1"}, 
                                              new CommentModel{Content = "content2",CreatedBy = "user2"}, 
                                          };

            FeedModel result = mapper.Map(source);

            CollectionAssert.AreEqual(expected, result.CommentSection.Comments,new CommentModelComparer());
        }
      
    }
}
