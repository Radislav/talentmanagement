﻿using System;

namespace TalentManagement.Infrastructure.Parsers
{
    public class InvalidFileException : ApplicationException
    {
        public InvalidFileException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public InvalidFileException(string message)
            : base(message)
        {
        }
    }
}
