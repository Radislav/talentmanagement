﻿using System;
using System.Diagnostics.Contracts;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Contracts
{
    [ContractClassFor(typeof(ISecurityLinkGenerator))]
    internal abstract class SecurityLinkGeneratorContract : ISecurityLinkGenerator
    {
        public string GenerateLink(int length)
        {
            Contract.Requires(length > 0);
            Contract.Ensures(false == String.IsNullOrEmpty(Contract.Result<string>()));

            return null;
        }
    }
}
