﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TalentManagement.Test
{
    public interface IHasAction
    {
        void Action(GenericParameterHelper arg);
    }

    [TestClass]
    public class FunctorExtensionsTest
    {
        [TestMethod]
        public void Map_should_not_fail_for_null_enumerable()
        {
            IEnumerable<GenericParameterHelper> enumerable = null;
            FunctorExtensions.Map(enumerable, item => { });
        }

        [TestMethod]
        public void Map_should_not_fail_for_null_functor()
        {
            IEnumerable<GenericParameterHelper> enumerable = new GenericParameterHelper[0];
            FunctorExtensions.Map(enumerable, null);
        }

        [TestMethod]
        public void Map_should_call_functor_for_each_entity()
        {
            IEnumerable<GenericParameterHelper> enumerable = new []
                                                                 {
                                                                     new GenericParameterHelper(10), 
                                                                     null
                                                                 };
            Mock<IHasAction> hasAction = new Mock<IHasAction>();
            
            FunctorExtensions.Map(enumerable,hasAction.Object.Action);

            hasAction.Verify(m => m.Action(It.IsAny<GenericParameterHelper>()), Times.Exactly(2));
        }

        [TestMethod]
        public void Do_should_not_execute_for_null_object()
        {
            Mock<IHasAction> hasAction = new Mock<IHasAction>();
            FunctorExtensions.Do((GenericParameterHelper)null,hasAction.Object.Action);

            hasAction.Verify(m => m.Action(It.IsAny<GenericParameterHelper>()),Times.Never());
        }

        [TestMethod]
        public void Do_should_execute_for_NOT_null_object()
        {
            Mock<IHasAction> hasAction = new Mock<IHasAction>();
            GenericParameterHelper arg = new GenericParameterHelper(10);
            FunctorExtensions.Do(arg, hasAction.Object.Action);

            hasAction.Verify(m => m.Action(arg), Times.Once());
        }

        [TestMethod]
        public void GetSafe_should_return_null_if_target_is_reference_null()
        {
            IHasAction target = null;
            var result = FunctorExtensions.GetSafe<IHasAction,Action<GenericParameterHelper>>(target, t => t.Action);
         
            Assert.IsNull(result);
        }

        [TestMethod]
        public void GetSafe_should_return_default_value_if_target_is_value_default()
        {
            DateTime target = new DateTime();

            int result = FunctorExtensions.GetSafe(target, dt => dt.Year);

            Assert.AreEqual(default(int),result);
        }

        [TestMethod]
        public void IsDefault_returns_true_for_null_reference_types()
        {
            Assert.IsTrue(FunctorExtensions.IsDefault((GenericParameterHelper)null));
        }

        [TestMethod]
        public void IsDefault_returns_true_for_default_value_types()
        {
            Assert.IsTrue(FunctorExtensions.IsDefault(default(DateTime)));
        }
    }
}
