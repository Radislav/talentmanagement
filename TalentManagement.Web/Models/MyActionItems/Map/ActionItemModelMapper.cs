﻿using System;
using MongoDB.Bson;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Resources;

namespace TalentManagement.Web.Models.MyActionItems.Map
{
    internal class ActionItemModelMapper : IActionItemModelMapper
    {
        private readonly IResourceProvider _resourceProvider;

        public ActionItemModelMapper(IResourceProvider resourceProvider)
        {
            _resourceProvider = resourceProvider;
        }
        
        public ActionItemModel Map(ObjectId objectiveId, ActionItem actionItem)
        {
            return new ActionItemModel
                       {
                           DueString = String.Format("{0} {1}", _resourceProvider.Due, actionItem.DueDate.AsLondDateString()),
                           Name = actionItem.Name,
                           ObjectiveId = objectiveId,
                           RelatedToObjective = actionItem.RelatedToObjective
                       };
        }
    }
}