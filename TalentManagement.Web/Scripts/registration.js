﻿$(function () {
    $("#tabs").tabs({
        disabled: [1],
        show: function (event, ui) {
            //Keep currently selected step
            $('#CurrentStep').val($('#tabs').tabs('option', 'selected'));
        }
    });

    setNextStep($('#InviteColleagues').is(':checked'));

    $('#InviteColleagues').change(function () {
        setNextStep($(this).is(':checked'));
    });

    function setNextStep(inviteOn) {

        $('#profile-finish').css('display', inviteOn ? 'none' : 'inline');
        $('#profile-next-step').css('display', inviteOn ? 'inline' : 'none');
    }
});

