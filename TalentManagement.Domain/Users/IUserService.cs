﻿using TalentManagement.Domain.Texts;

namespace TalentManagement.Domain.Users
{
    public interface IUserService
    {
        void ChangeUserPassword(string userId,string password);
        void InsertNotification(User sender, User receiver, TextType type,object formatterArgument = null);
    }
}
