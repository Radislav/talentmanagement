﻿using System.Web.Mvc;
using FluentValidation;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.MyPassword.Validation
{
    internal class IndexModelValidator : AbstractValidator<IndexModel>,
                                             TalentManagement.Infrastructure.Validation.IValidator<IndexModel>
    {
        private readonly IResourceProvider _resourceProvider;
        private readonly IHashComputer _hashComputer;

        public IndexModelValidator() : 
            this(DependencyResolver.Current.GetService<IResourceProvider>(),
                 DependencyResolver.Current.GetService<IHashComputer>())
        {
        }

        public IndexModelValidator(IResourceProvider resourceProvider,IHashComputer hashComputer)
        {
            _resourceProvider = resourceProvider;
            _hashComputer = hashComputer;

            RuleFor(m => m.OldPassword)
                .NotEmpty();

            RuleFor(m => m.OldPassword)
                .Must((model, oldPassword) => oldPassword != null && _hashComputer.ComputeHash(oldPassword) == model.OldPasswordHashed)
               .WithMessage(_resourceProvider.ErrorPasswordInvalid);

            RuleFor(m => m.NewPassword)
                .NotEmpty();

            RuleFor(m => m.ConfirmPassword)
                .NotEmpty();
            
            RuleFor(m => m.ConfirmPassword)
                .Equal(m => m.NewPassword)
                .WithMessage(_resourceProvider.ErrorPasswordsMustMatch);


        }

        public TalentManagement.Infrastructure.Validation.ValidationResult ValidateInstance(IndexModel instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}