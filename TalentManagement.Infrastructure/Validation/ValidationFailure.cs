﻿namespace TalentManagement.Infrastructure.Validation
{
    public class ValidationFailure
    {
        private readonly string _propertyName;
        private readonly string _error;
        private readonly object _attemptedValue;
        // Summary:
        //     Creates a new validation failure.
        public ValidationFailure(string propertyName, string error)
        {
            _propertyName = propertyName;
            _error = error;
        }

        //
        // Summary:
        //     Creates a new ValidationFailure.
        public ValidationFailure(string propertyName, string error, object attemptedValue): this(propertyName,error)
        {
            _attemptedValue = attemptedValue;
        }

        // Summary:
        //     The property value that caused the failure.
        public object AttemptedValue
        {
            get { return _attemptedValue; }
        }
        //
        // Summary:
        //     Custom state associated with the failure.
        public object CustomState { get; set; }
        //
        // Summary:
        //     The error message
        public string ErrorMessage
        {
            get { return _error; }
        }
        //
        // Summary:
        //     The name of the property.
        public string PropertyName
        {
            get { return _propertyName; }
        }
    }
}
