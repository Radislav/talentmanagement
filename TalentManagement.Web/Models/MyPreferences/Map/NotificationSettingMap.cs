﻿using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.MyPreferences.Map
{
    internal class NotificationSettingMap : IMap<NotificationSettingModel, NotificationSetting>
    {
        public void Configure(IMapperConfiguration<NotificationSettingModel, NotificationSetting> configuration)
        {
            configuration.ForMember(d => d.Id, s => s.SettingId);
            configuration.ForMember(d => d.IsEnabled, s => s.IsEnabled);
        }
    }
}