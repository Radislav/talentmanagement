﻿using System.Collections.Generic;
using TalentManagement.Domain.Users;

namespace TalentManagement.Web.Models.Teams.Map
{
    public interface ITeamModelMapper
    {
        TeamModel Map(Team team, IEnumerable<User> users,bool isExpanded);
    }
}