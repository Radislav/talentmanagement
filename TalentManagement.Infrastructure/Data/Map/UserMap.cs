﻿using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Data.Configuration;

namespace TalentManagement.Infrastructure.Data.Map
{
    class UserMap : EntityConfiguration<User>
    {
        public UserMap()
        {
            AsCollection("Users")
                .WithDataSource(DataSourceType.Shared)
                .UseSafeMode(true);

            Property(f => f.Names)
                .Ignore();

            Property(f => f.CompanyName)
                .Ignore();

            Property(p => p.FirstName)
                .Required();

            Property(p => p.SecondName)
                .Required();

            Property(p => p.Tenant)
                .Required();
        }
    }
}
