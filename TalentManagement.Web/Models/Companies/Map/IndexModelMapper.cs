﻿using System.Collections.Generic;
using TalentManagement.Domain.Companies;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Companies.Map
{
    class IndexModelMapper : IIndexModelMapper
    {
        private readonly IMapper<Company, GeneralTab> _generalTabMapper;
        private readonly IMapper<IEnumerable<Badge>, BadgesTab> _badgesTabMapper;
        private readonly IObjectiveFocusTabMapper _objectivesFocusMapper;

        public IndexModelMapper(IMapper<Company, GeneralTab> generalTabMapper, 
                                IMapper<IEnumerable<Badge>, BadgesTab> badgesTabMapper,
                                IObjectiveFocusTabMapper objectivesFocusMapper)
        {
            _generalTabMapper = generalTabMapper;
            _badgesTabMapper = badgesTabMapper;
            _objectivesFocusMapper = objectivesFocusMapper;
        }

       
        public IndexModel Map(Company company, IEnumerable<Badge> badges,IEnumerable<Objective> objectivesInFocus)
        {
            return new IndexModel
                       {
                           GeneralTab = _generalTabMapper.Map(company),
                           BadgesTab =  _badgesTabMapper.Map(badges),
                           ObjectivesFocusTab = _objectivesFocusMapper.Map(objectivesInFocus)
                       };
        }
    }
}