﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using TalentManagement.Web.Models.Questionaire;

namespace TalentManagement.Web.Models.PerformanceSummary
{
    public class IndexModel : IQuestionnarie
    {
        public IndexModel()
        {
            Answers = new List<AnswerModel>();
            PersonalAnswers = new List<AnswerModel>();
            PeersAnswers = new List<AnswerModel>();
            Objectives = new List<ObjectiveModel>();
            Kudos = new List<KudosModel>();
        }

        public IEnumerable<AnswerModel> Answers { get; set; }
        public string Respondent { get; set; }
        public string RespondentNames { get; set; }
        public string ReviewCreatedAt { get; set; }
        public string ReviewDue { get; set; }

        public IEnumerable<AnswerModel> PersonalAnswers { get; set; }
        public IEnumerable<AnswerModel> PeersAnswers { get; set; }
        public IEnumerable<ObjectiveModel> Objectives { get; set; }
        public IEnumerable<KudosModel> Kudos { get; set; }

        public IEnumerable<ObjectiveModel> ObjectivesAchieved{
            get { return Objectives.Where(o => o.Achieved); }
        }

        public IEnumerable<ObjectiveModel> ObjectivesNotAchieved
        {
            get { return Objectives.Where(o => false == o.Achieved); }
        }

        public ObjectId ReviewId { get; set; }
        public string CycleName { get; set; }
    }
}