﻿using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.ReviewCycleCreation.Map
{
    internal class QuestionMapper : IMapper<QuestionModel,Question>
    {
        public Question Map(QuestionModel source)
        {
            return new Question(source.Id,source.Text,source.Settings,source.AnswerType)
                {
                    OrderNumber = source.Number
                };
        }
    }
}