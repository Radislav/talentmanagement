﻿using TalentManagement.Domain.Events;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Services;

namespace TalentManagement.Infrastructure.Events
{
    class UserUpdatedHandler : IHandle<UserUpdatedArgs>
    {
        private readonly IUserRepository _userRepository;
        private readonly IRelationshipService _relationshipService;

        public UserUpdatedHandler(IUserRepository userRepository,IRelationshipService relationshipService)
        {
            _userRepository = userRepository;
            _relationshipService = relationshipService;
        }

        public void Handle(UserUpdatedArgs args)
        {
            User currentUser = args.UserCurrent;

            if (args.IsMarkedAsManager)
            {
                _relationshipService.MarkAsManager(currentUser);
            }

            if (args.IsMarkedAsNotManager)
            {
                _relationshipService.NotManager(currentUser);
            }

            User manager = _userRepository.GetById(currentUser.ManagerEmail);

            if (args.IsSubordinationAdded && manager != null)
            {
                 _relationshipService.AddToTeam(currentUser, manager);
            }

            if (args.IsSubordinationChanged && manager != null)
            {
                User previousManager = _userRepository.GetById(args.UserPrevious.ManagerEmail);
                _relationshipService.ChangeTeam(currentUser,previousManager,manager);
            }

            if (args.IsSubordinationDeleted)
            {
                User previousManager = _userRepository.GetById(args.UserPrevious.ManagerEmail);
                _relationshipService.RemoveFromTeams(currentUser,previousManager);
            }
        }
    }
}
