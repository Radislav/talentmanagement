﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Runtime.Caching;
using TalentManagement.Domain;
using TalentManagement.Domain.Texts;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Configuration;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Cache
{
    internal class GlobalCache : IGlobalCache
    {
        private const string NotificationsCount = "NotificationsCount";
        private const string UserName = "UserNames";
        //20 minutes
//        private readonly TimeSpan _expirationSpan;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IUserRepository _userRepository;
        private readonly ITextRepository _notificationRepository;

        public GlobalCache(IAppConfiguration configurationProvider,ITextRepository notificationRepository,IPrincipalProvider principalProvider,IUserRepository userRepository)
        {
            //TODO: Customizable cache will be later.
            //_expirationSpan = configurationProvider.GetCacheDuration();
            _notificationRepository = notificationRepository;
            _principalProvider = principalProvider;
            _userRepository = userRepository;
        }

        public TItem Get<TItem>(string key)
        {
            object tObject = MemoryCache.Default[key];

            return IsInCache<TItem>(tObject) ? (TItem)tObject : default(TItem);
        }

        public void Set<TItem>(TItem item, string key)
        {
            if (false == item.Equals(default(TItem)))
            {
                MemoryCache.Default.Set(key, item,new CacheItemPolicy{SlidingExpiration = new TimeSpan(0,20,0)});
            }
        }

        public void SetNoExpiration<TItem>(TItem item, string key)
        {
            if (false == item.Equals(default(TItem)))
            {
                MemoryCache.Default.Set(key, item, new CacheItemPolicy {Priority = CacheItemPriority.NotRemovable});
            }
        }

        public void Delete(string key)
        {
          
           MemoryCache.Default.Remove(key);
            
        }

        public void Delete<TItem>()
        {
          
           Delete(typeof(TItem).ToString());
            
        }

        public TItem Get<TItem>()
        {
            return Get<TItem>(typeof(TItem).ToString());
        }

        public void Set<TItem>(TItem item)
        {
            Set(item, typeof(TItem).ToString());
        }

        private bool IsInCache<TItem>(object tObject)
        {
            return tObject != null && (tObject is TItem);
        }

        public TItem GetOrRefresh<TItem>(string key, Func<TItem> refreshFunctor)
        {
            object tObject = MemoryCache.Default[key];
            TItem result;

            if (IsInCache<TItem>(tObject))
            {
                result = (TItem)tObject;
            }
            else
            {
                result = refreshFunctor();
                Set(result, key);
            }

            return result;
        }

        public TItem GetOrRefresh<TItem>(Func<TItem> refreshFunctor)
        {
            return GetOrRefresh(typeof(TItem).ToString(), refreshFunctor);
        }

        public Text[] Texts
        {
            get
            {
                Contract.Ensures(Contract.Result<Text[]>() != null);

                return GetOrRefresh(() => _notificationRepository.GetTexts().ToArray());
            }
        }
        
        public int GetNotificationsCountFor(string userId)
        {

            return GetOrRefresh(NotificationsCount + userId,
                                    () =>
                                        {
                                            User user = _userRepository.GetById(userId);
                                            return user.GetSafe(u => u.UserNotifications.Count());
                                        });
           
        }
        
        public void SetNotificationsCountFor(string userName,int notificationsCount)
        {
            Set(NotificationsCount + userName, Convert.ToString(notificationsCount));
        }

        public void DeleteNotificationsCountFor(string userName)
        {
            Delete(NotificationsCount + userName);
        }
        
        public string GetUserNames(string userId)
        {
            return GetOrRefresh(UserName + userId, () =>
                                                       {
                                                           User user = _userRepository.GetById(userId);
                                                           return user.GetSafe(u => u.Names);
                                                       });
        }
        
        public void DeleteUserNames(string userId)
        {
            Delete(UserName + userId);
        }
    }
}
