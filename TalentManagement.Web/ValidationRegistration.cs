﻿using System.ComponentModel;
using System.Linq;
using FluentValidation;
using FluentValidation.Mvc;
using TalentManagement.Web.Models;

namespace TalentManagement.Web
{
    public static class ValidationRegistration
    {
        public static void RegisterValidation()
        {
            ValidatorOptions.ResourceProviderType = typeof (Resources.Errors);
            ValidatorOptions.DisplayNameResolver = (type, member, arg) =>
            {
                if(member != null)
                {
                    var customAttributes = member.GetCustomAttributes(typeof (LocalizedDisplayNameAttribute), true);
                    return customAttributes.Any()
                               ? customAttributes.Cast<DisplayNameAttribute>().First().DisplayName
                               : member.Name;

                }

                return null;
            };
            FluentValidationModelValidatorProvider.Configure();
        }
    }
}