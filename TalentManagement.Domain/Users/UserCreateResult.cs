﻿namespace TalentManagement.Domain.Users
{
    public enum UserCreateResult
    {
        None,
        Duplicate,
        Invalid,
        Ok
    }
}
