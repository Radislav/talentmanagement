﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace TalentManagement.Web.Infrastructure.UI
{
    public static class CheckboxExtensions
    {
        public static MvcHtmlString CheckBoxStyledFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression)
        {
            TagBuilder label = new TagBuilder("label");
            label.AddCssClass("checkbox");

            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            label.MergeAttribute("for", metadata.PropertyName);

            label.InnerHtml = htmlHelper.CheckBoxFor(expression).ToHtmlString() + metadata.DisplayName;

            return new MvcHtmlString(label.ToString());
        }
    }
}