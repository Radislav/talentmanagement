﻿$.ajaxSetup({
    type : 'POST',
    contentType : 'application/json; charset=utf-8'
});

/* ------------------------------------------   SERIALIZER  ----------------------------------------------------  */
$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

/* ------------------------------------------   TYPEAHEAD  ----------------------------------------------------  */

function typeaheadInit() {
    
    //The user was chosen in user picker
    $('input[data-provide = "typeahead"]').off('change')
                                          .on('change', function () {

                                              var isElementPicked = (-1 != $.inArray($(this).val(), JSON.parse($(this).attr('data-source'))));

                                              if (isElementPicked) {
                                                  $(this).parents('.user-picker-control')
                                                       .find('#pickUserButton')
                                                       .trigger('click');
                                              }
                                          });

                                          $('.contributor button').off('click').on('click', function () {
                                             $(this).parents('.contributor').remove();
                                             
                                          });
}

/* ------------------------------------------   FILE UPLOAD  ----------------------------------------------------  */

function uploaderInit() {
    $('.file-upload a').off('click').on('click',(function () {
        $(this).parent().find('input[type="file"]').trigger('click');
    }));

    $('.file-upload input[type="file"]').off('change').on('change',(function (e) {
        if (e != null && e.target.files != null && e.target.files.length > 0) {
            $(this).parent().find('[name="filename"]').html(e.target.files[0].name);    
        }
    }));
}

/* ------------------------------------------   INPUTS  ----------------------------------------------------  */

//inputExtender

(function (jQuery) {

    jQuery.fn.inputExtender = function (options) {

        var opts = jQuery.extend({}, jQuery.fn.inputExtender.defaults, options);

        return this.each(function () {
            var $this = jQuery(this);

            if (opts.date) {
                $this.datepicker();
            }
            if (opts.decimal) {
                $this.jStepper({ decimalSeparator: '.' });
            }
        });
    };

    jQuery.fn.inputExtender.defaults = {
        date: false,
        decimal: false
    };


})(jQuery);


function customizeInputs() {
    if (typeof 'inputExtender' != 'undefined' && typeof 'datepicker' != 'undefined') {
        //Customize inputs
        $('input[data-type="date"]').inputExtender({ date: true });
        $('input[data-type="decimal"]').inputExtender({ decimal: true });    
    }
    
}

/* ------------------------------------------   ON DOCUMENT LOAD  ----------------------------------------------------  */

$(function () {
    typeaheadInit();
    uploaderInit();
    customizeInputs();

});


