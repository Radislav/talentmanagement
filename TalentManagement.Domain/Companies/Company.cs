﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;

namespace TalentManagement.Domain.Companies
{
    [AggregateRoot]
    public class Company : Entity , IOwnedByCompany
    {
        public static readonly ObjectId DefaultBadgeId = ObjectId.Empty;
        
        public Company()
        {
        }

        public Company(ObjectId companyId) :base(companyId)
        {
        }

        public Company(ObjectId companyId, IEnumerable<ObjectId> badgeIds) : this(companyId)
        {
            BadgeIds = badgeIds;
        }

        public Company(Company company,IEnumerable<ObjectId> badgeIds)
        {
            Id = company.Id;
            Name = company.Name;
            Logo = company.Logo;
            BadgeIds = badgeIds;
            CompanyName = company.CompanyName;
            Timezone = company.Timezone;
        }

        public string Name { get; set; }

        private Logo _logo;
        public Logo Logo 
        {
            get { return _logo; }
            set {
                _logo = value;
                if (_logo != null && _logo.Id == ObjectId.Empty)
                {
                    _logo.Id = ObjectId.GenerateNewId(DateTime.Now);
                }
            }
        }
        public string Timezone { get; set; }

        private List<ObjectId> _badgeIds = new List<ObjectId>();
        public IEnumerable<ObjectId> BadgeIds
        {
            get { return _badgeIds; }
            private set { _badgeIds = new List<ObjectId>(value); }
        }

        public void AddBadgeId(ObjectId badgeId)
        {
            if (false == _badgeIds.Contains(badgeId))
            {
                _badgeIds.Add(badgeId);
            }
        }

        public void RemoveBadgeId(ObjectId badgeId)
        {
            _badgeIds.Remove(badgeId);
        }

        public string CompanyName { get; set; }

        public bool IsLogoDefined
        {
            get { return Logo != null && Logo.Image != null && Logo.ImageType != null; }
        }
    }
}
