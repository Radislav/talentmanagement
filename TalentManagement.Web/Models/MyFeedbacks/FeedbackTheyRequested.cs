﻿namespace TalentManagement.Web.Models.MyFeedbacks
{
    public class FeedbackTheyRequested
    {
        public string Question { get; set; }
        public string QuestionAuthorId { get; set; }
        public string QuestionAuthorName { get; set; }
        public string QuestionDate { get; set; }
        public string Response { get; set; }
        public string FeedbackId { get; set; }
        public string ResponseAuthor { get; set; }
        public string ResponseDate { get; set; }
        public bool HasResponse
        {
            get { return false == string.IsNullOrEmpty(Response); }
        }
        public CommentSectionModel CommentSectionModel { get; set; }

    }
}