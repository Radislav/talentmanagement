﻿using TalentManagement.Domain.Users;

namespace TalentManagement.AppServices
{
    public interface IRegistrationFlow
    {
        ForgotPasswordResult ForgotPasswordFor(string email);
        void Register(User user,string[] invitationEmails);
        void Invite(string invitationEmail);
    }
}
