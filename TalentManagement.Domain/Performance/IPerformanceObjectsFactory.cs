﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using MongoDB.Bson;
using TalentManagement.Domain.Contracts;

namespace TalentManagement.Domain.Performance
{
    [ContractClass(typeof(PerformanceObjectsFactoryContract))]
    public interface IPerformanceObjectsFactory
    {
        ReviewCycleSummary CreateSummary(ObjectId reviewCycleId,string respondent);
        AnswersCollection CreateAnswersCollection(Review review);
        IEnumerable<Review> CreateReviews(ObjectId reviewCycleId, IEnumerable<string> participants, string owner, QuestionSet questionSet);
    }
}
