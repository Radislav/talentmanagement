﻿using System;
using Autofac;
using Moq;

namespace TalentManagement.Test
{
    public static class ContainerBuilderExtension
    {
        public static Mock<T> RegisterMock<T>(this ContainerBuilder builder, Action<Mock<T>> init) where T : class
        {
            var mock = new Mock<T>();
            init(mock);
            builder.RegisterInstance(mock.Object).As<T>().SingleInstance();
            builder.RegisterInstance(mock).As<Mock<T>>().SingleInstance();
            return mock;
        }

        public static Mock<T> RegisterMock<T>(this ContainerBuilder builder, Mock<T> mock) where T : class
        {
            builder.RegisterInstance(mock.Object).As<T>().SingleInstance();
            builder.RegisterInstance(mock).As<Mock<T>>().SingleInstance();
            return mock;
        }
    }
}
