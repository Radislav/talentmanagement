﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Collections.Generic;
using MongoDB.Bson;

namespace TalentManagement.Domain.Feeds
{
    [AggregateRoot]
    public class Feed : Entity , IOwnedByCompany , IOwned , IHasScope , IOwnedByTeam
    {
        private List<Comment> _comments = new List<Comment>();
        private FeedProperty[] _feedProperties = new FeedProperty[0];
        
        //This is for Db mapper and unit tests;
        internal Feed()
        {
        }

        //This is for Db mapper and unit tests;
        internal Feed(IEnumerable<Comment> comments, IEnumerable<FeedProperty> properties)
        {
            if (comments != null)
            {
                _comments = new List<Comment>(comments);    
            }
            if (properties != null)
            {
                _feedProperties = properties.ToArray();    
            }
        }
        
        public IEnumerable<Comment> Comments
        {
            get { return _comments; }
            private set
            {
                _comments = value.ListOrDefault();
            }
        }


        public IEnumerable<FeedProperty> Properties
        {
            get
            {
                return _feedProperties;
                
            }
            private set { _feedProperties = value.ArrayOrDefault(); }
        }

     
        public FeedType FeedType { get; set; }
        public string Title { get; set; }

        public string CompanyName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }

        public void AddComment(Comment comment)
        {
            Contract.Requires(comment != null);

            comment.Id = ObjectId.GenerateNewId(DateTime.Now);
            _comments = _comments ?? new List<Comment>();
            _comments.Add(comment);
        }

        public void SetPropertyValue(string name,object value)
        {
            _feedProperties = _feedProperties ?? new FeedProperty[0];
            
            String @string = value == null ? String.Empty : value.ToString();
            FeedProperty updatingProperty = _feedProperties.SingleOrDefault(p => p.Name == name);

            if (updatingProperty != null)
            {
                updatingProperty.Value = @string;    
            }
            else
            {
                FeedProperty[] newProperties = new FeedProperty[_feedProperties.Length + 1];
                _feedProperties.CopyTo(newProperties, 0);
                newProperties[newProperties.Length - 1] = new FeedProperty {Name = name, Value = @string,Id = ObjectId.GenerateNewId(DateTime.Now)};
                _feedProperties = newProperties;
            }
        }

        public string GetPropertyValue(string name)
        {
            Contract.Assume(_feedProperties != null);
            FeedProperty property = _feedProperties.SingleOrDefault(p => p.Name == name);
            return property.GetSafe(p => p.Value);
        }

        public ObjectId GetIdPropertyValue(string name)
        {
            ObjectId id;
            string value = GetPropertyValue(name);

            if (ObjectId.TryParse(value,out id))
            {
                return id;
            }

            return ObjectId.Empty;
        }

        public IScope Scope
        {
            get
            {
                string scopeName = GetPropertyValue("ScopeName");
                return scopeName == null
                           ? new Scope()
                           : new Scope(scopeName, GetPropertyValue("ScopeId"));
            }
            set
            {
                Contract.Requires(value != null);

                SetPropertyValue("ScopeId",value.ScopeId);
                SetPropertyValue("ScopeName", value.ScopeName);
                
            }
        }

        public string Team { get; set; }
    }
}
