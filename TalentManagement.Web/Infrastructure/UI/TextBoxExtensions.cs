﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;


namespace TalentManagement.Web.Infrastructure.UI
{
    public static class TextBoxExtensions
    {
        public static MvcHtmlString TextBoxStyledFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                     Expression<Func<TModel, TValue>> expression)
        {
            return TextBoxStyledFor(html, expression, (Dictionary<string, object>)null);
        }

        public static MvcHtmlString TextBoxStyledFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                             Expression<Func<TModel, TValue>> expression,
                                                             Dictionary<string, object> attributes)
        {
            return HtmlExtensions.ControlStyledFor(html, html.TextBoxFor, expression, attributes);
        }

        public static MvcHtmlString TextBoxStyledFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                           Expression<Func<TModel, TValue>> expression,
                                                           UiDataType uiDataType)
        {
            return TextBoxStyledFor(html, expression, new Dictionary<string, object> { { "data-type", (string)uiDataType } });
        }

        public static MvcHtmlString PasswordStyledFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                           Expression<Func<TModel, TValue>> expression)
        {
            return TextBoxStyledFor(html, expression, new Dictionary<string, object> { { "type", "password" } });
        }
    }
}