﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;

namespace TalentManagement.Web.Models
{
    public class UserPickedCollection : IEnumerable<UserPicked>
    {
        readonly JavaScriptSerializer _serializer = new JavaScriptSerializer();
        private List<UserPicked> _users = new List<UserPicked>();

        private string _prefix;
        public string Prefix
        {
            get { return _prefix; }
            set { _prefix = value;
            SetPrefixToUsers();}
        }
        
        public UserPickedCollection()
        {
        }

        private void SetPrefixToUsers()
        {
            _users.Map(u => u.Prefix = Prefix);
        }

        public UserPickedCollection(List<UserPicked> users) 
        {
            _users = users;
            SetPrefixToUsers();
        }

        public List<UserPicked> Users
        {
            get { return _users; }
            set
            {
                _users = value;
                SetPrefixToUsers();
                _usersJson = null;
                _userNames = null;
            }
        }

        private string _usersJson;
        public string UsersJson
        {
            get
            {
                if (null == _usersJson && _users != null)
                {
                    _usersJson = _serializer.Serialize(_users);
                }
                return _usersJson;
            }
            set
            {
                Users = _serializer.Deserialize<List<UserPicked>>(value);
                _usersJson = value;
            }
        }

        private string _userNames;
        public string UserNames
        {
            get
            {
                if (null == _userNames && _users != null)
                {
                    _userNames = _serializer.Serialize(_users.Select(u => u.ToString()).ToArray());
                }
                return _userNames;
            }
            set
            {
                _userNames = value;
            }
        }

        public UserPicked GetUserByNames(string firstSecondName)
        {
            return Users.FirstOrDefault(u => u.ToString() == firstSecondName);
        }

        public IEnumerator<UserPicked> GetEnumerator()
        {
            return _users.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _users.GetEnumerator();
        }
    }
}