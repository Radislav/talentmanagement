﻿using System;

namespace TalentManagement.Infrastructure.Security
{
    class SecurityTokenGenerator : IPasswordGenerator , ISecurityLinkGenerator
    {
        private const string PasswordChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-";
        private const string UrlChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";

        private string GenerateToken(int length,string allowedChars)
        {
            char[] chars = new char[length];
            Random rd = new Random(new object().GetHashCode() + DateTime.Now.Millisecond);

            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            return new string(chars);
        }
      
        public string GeneratePassword(int length)
        {
            return GenerateToken(length, PasswordChars);
        }

        public string GenerateLink(int length)
        {
            return GenerateToken(length, UrlChars);
        }
    }
}
