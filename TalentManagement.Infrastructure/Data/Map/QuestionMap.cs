﻿using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Data.Configuration;

namespace TalentManagement.Infrastructure.Data.Map
{
    public class QuestionMap : EntityConfiguration<Question>
    {
        public QuestionMap()
        {
            AsCollection("Questions")
                .WithDataSource(DataSourceType.Tenant);

            Property(p => p.Text)
                .Required();
        }
    }
}
