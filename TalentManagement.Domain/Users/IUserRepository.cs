﻿using System.Collections.Generic;
using System.Linq;

namespace TalentManagement.Domain.Users
{
    public interface IUserRepository : IRepositoryWithTypedId<User,string> 
    {
        string[] GetUserRoles(string userName);

        /// <summary>
        /// Gets users for provided tenant.
        /// </summary>
        /// <returns></returns>
        IEnumerable<User> GetUsersForTenant(string tenantName, IFilter filter = null);

        /// <summary>
        /// Gets users for provided tenant grouped by team.
        /// </summary>
        /// <returns></returns>
        IEnumerable<IGrouping<string, User>> GetGroupedUsersForTenant(string tenantName, IFilter filter = null);

        IEnumerable<User> GetUsers(IEnumerable<string> userNames);
        void UpdateSecondaryProperties(User user);
        void UpdateNotificationSettings(User user);
        UserCreateResult CreateUser(User user);
        IEnumerable<User> GetSubordinatesOf(IEnumerable<string> managersUserNames,IFilter filter);
        IEnumerable<User> GetSubordinatesOf(string manager, IFilter filter);
    }
}
