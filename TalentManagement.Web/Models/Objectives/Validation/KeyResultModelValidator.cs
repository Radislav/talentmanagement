﻿using FluentValidation;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.Objectives.Validation
{
    internal class KeyResultModelValidator : AbstractValidator<KeyResultModel>, 
                                             TalentManagement.Infrastructure.Validation.IValidator<KeyResultModel>
    {
        public KeyResultModelValidator()
        {
            RuleFor(m => m.Name)
                .NotEmpty();

            RuleFor(m => m.Due)
                .NotEmpty()
                .DateTime();
        }

        public TalentManagement.Infrastructure.Validation.ValidationResult ValidateInstance(KeyResultModel instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}