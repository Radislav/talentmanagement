﻿using System.Collections.Generic;
using Resource = TalentManagement.Resources.Models.Companies.BadgesTab;

namespace TalentManagement.Web.Models.Companies
{
    public class BadgesTab
    {
        public BadgesTab()
        {
            Badges = new List<BadgeModel>();
            NewBadge = new BadgeModel();
        }

        public List<BadgeModel> Badges { get; set; }
        public BadgeModel NewBadge { get; set; }
    }
}