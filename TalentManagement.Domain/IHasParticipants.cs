﻿using System.Collections.Generic;

namespace TalentManagement.Domain
{
    public interface IHasParticipants
    {
        IEnumerable<string> Participants { get; }
    }
}
