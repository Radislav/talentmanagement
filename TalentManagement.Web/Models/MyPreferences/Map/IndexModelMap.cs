﻿using System.Linq;
using TalentManagement.Domain;
using TalentManagement.Domain.Texts;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Cache;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.MyPreferences.Map
{
    public class IndexModelMap : IMap<User,IndexModel>
    {
        private readonly Text[] _texts;

        public IndexModelMap(IGlobalCache globalCache)
        {
            _texts = globalCache.Texts;
        }

        public void Configure(IMapperConfiguration<User, IndexModel> configuration)
        {
            configuration.ForMember(d => d.Settings,
                                    s =>
                                        {
                                            if (_texts == null)
                                            {
                                                return new NotificationSettingModel[0];
                                            }

                                            return _texts.Select(t =>
                                                                             {
                                                                                 NotificationSetting setting = s.GetNotificationSetting(t.Id);
                                                                                 bool isEnabled = setting == null ? false : setting.IsEnabled;
                                                                                 return
                                                                                     new NotificationSettingModel
                                                                                         {
                                                                                             SettingId = t.Id,
                                                                                             Text = t.Content,
                                                                                             IsEnabled = isEnabled
                                                                                         };
                                                                             }).ToArray();
                                        });
        }
    }
}