﻿using System.Collections.Generic;
using Resource = TalentManagement.Resources.Models.MyFeedbacks.FeedbackCreateModel;

namespace TalentManagement.Web.Models.MyFeedbacks
{
    public class FeedbackCreateModel : ICanPickUsers<FeedbackCreateModel>
    {
        [LocalizedDisplayName("Question",typeof(Resource))]
        public string Question { get; set; }

        [LocalizedDisplayName("WhoToAsk",typeof(Resource))]
        public string Participant { get; set; }

        public List<UserPicked> Users { get; set; }

        public System.Linq.Expressions.Expression<System.Func<FeedbackCreateModel, string>> TitleExpression
        {
            get { return model => model.Participant; }
        }
    }
}   