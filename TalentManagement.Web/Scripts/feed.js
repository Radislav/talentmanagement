﻿$(function () {

    Comments({
        containerSelector : '.feed',
        holderId: 'feedModel_Id',
        handlerUrl: globalUrls.addFeedCommentUrl()
    });

    //Refresh feeds panel on filter button clicked
    $('#quick_actions .btn').click(function (e) {

        e.preventDefault();
        $.ajax({
            url: $(this).attr('href'),
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                $('#feeds').html(response);
                Comments({
                    containerSelector: '.feed',
                    holderId: 'feedModel_Id',
                    handlerUrl: globalUrls.addFeedCommentUrl()
                });
            }
        });

    });


});


