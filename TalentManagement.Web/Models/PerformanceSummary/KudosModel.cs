﻿using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Web.Infrastructure.UI;

namespace TalentManagement.Web.Models.PerformanceSummary
{
    public class KudosModel
    {
        public string Sender { get; set; }
        public string SenderNames { get; set; }
        public string Receiver { get; set; }
        public string ReceiverNames { get;set; }
        public string BadgeName { get; set; }
        public ObjectId BadgeId { get; set; }
        public string CreatedAt { get; set; }
        public string Content { get; set; }

        public string BuildThankinkgMessage(HtmlHelper htmlHelper)
        {
            return string.Format(
                    DependencyResolver.Current.GetService<IResourceProvider>().PerformanceSummaryKudosThankingFormat,
                    htmlHelper.UserProfileLink(Sender,SenderNames), 
                    htmlHelper.UserProfileLink(Receiver,ReceiverNames), 
                    BadgeName);
            
        }

    }
}