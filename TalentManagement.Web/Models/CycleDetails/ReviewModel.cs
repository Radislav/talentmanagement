﻿using MongoDB.Bson;

namespace TalentManagement.Web.Models.CycleDetails
{
    public class ReviewModel
    {
        public string EmployeeName { get; set; }
        public string Status { get; set; }
        public ObjectId Id { get; set; }
    }
}