﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Cache;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Web.Infrastructure.Cache
{
    class UserCache : IUserCache
    {
        private readonly HttpSessionState _session;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IUserRepository _userRepository;

        public UserCache(IPrincipalProvider principalProvider,IUserRepository userRepository)
        {
            Contract.Requires(HttpContext.Current != null);
            Contract.Requires(HttpContext.Current.Session != null);
            Contract.Requires(principalProvider != null);
            Contract.Requires(userRepository != null);

            _session = HttpContext.Current.Session;
            _principalProvider = principalProvider;
            _userRepository = userRepository;
        }

        public TItem Get<TItem>(string key)
        {
            object tObject = _session[key];

            return IsInCache<TItem>(tObject) ? (TItem)tObject : default(TItem);
        }

        public void Set<TItem>(TItem item, string key)
        {
            if (false == item.Equals(default(TItem)))
            {
                _session.Add(key,item);
            }
        }

        public void Delete(string key)
        {
           _session.Remove(key);
        }

        public void Delete<TItem>()
        {
             Delete(typeof(TItem).ToString());
        }

        public TItem Get<TItem>()
        {
            return Get<TItem>(typeof(TItem).ToString());
        }

        public void Set<TItem>(TItem item)
        {
            Set(item, typeof(TItem).ToString());
        }

        private bool IsInCache<TItem>(object tObject)
        {
            return tObject != null && (tObject is TItem);
        }

        public TItem GetOrRefresh<TItem>(string key, Func<TItem> refreshFunctor)
        {
            object tObject = HttpRuntime.Cache[key];
            TItem result;

            if (IsInCache<TItem>(tObject))
            {
                result = (TItem)tObject;
            }
            else
            {
                result = refreshFunctor();
                Set(result, key);
            }

            return result;
        }

        public TItem GetOrRefresh<TItem>(Func<TItem> refreshFunctor)
        {
            return GetOrRefresh(typeof(TItem).ToString(), refreshFunctor);
        }

       
    }
}