﻿using System.Web.Mvc;
using System.Web.Security;
using TalentManagement.AppServices;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Web.Infrastructure.Security;
using TalentManagement.Web.Models;

namespace TalentManagement.Web.Controllers
{
    public class LoginController : Controller
    {
        private readonly IResourceProvider _resourceProvider;
        private readonly IRegistrationFlow _registrationFlow;
        private readonly IAuthenticationService _authenticationService;

        
        public LoginController(IResourceProvider resourceProvider, IRegistrationFlow registrationFlow, IAuthenticationService authenticationService)
        {
            _resourceProvider = resourceProvider;
            _registrationFlow = registrationFlow;
            _authenticationService = authenticationService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            Logout();
            return View();
        }
        
        [HttpPost]
        public ActionResult Index(LoginModel loginModel)
        {
            return View(loginModel);
        }

        [HttpGet]
        [OutputCache(CacheProfile = "Cache10Hours")]
        public ActionResult About()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Login(LoginModel loginModel)
        {
            loginModel.LoginResult = _authenticationService.Login(loginModel.Email, loginModel.Password);
            
            if (loginModel.LoginResult == LoginResult.Ok)
            {
                FormsAuthentication.SetAuthCookie(loginModel.Email,false);
                new SecurityTicket().AddCookieToContext(HttpContext);
                
                return RedirectToRoute(new {controller = "Feed"});
            }

            ModelState.AddModelError("Email", loginModel.Error);
            return View("Index", loginModel);
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            new SecurityTicket().Expire();
            
            return RedirectToAction("Index");
        }

        [HttpGet]
        //[OutputCache(CacheProfile = "Cache10Hours")]
        public ActionResult ForgotPassword()
        {
            return View(new EmailModel());
        }

        [HttpPost]
        public ActionResult ForgotPassword(EmailModel emailModel)
        {
            if (ModelState.IsValid)
            {
                ForgotPasswordResult userOperationResult = _registrationFlow.ForgotPasswordFor(emailModel.Email);

                if (userOperationResult != ForgotPasswordResult.Ok)
                {
                    ModelState.AddModelError("Email", _resourceProvider.ErrorEmailNotFounded);
                }    
            }
            
            return ModelState.IsValid
                       ? (ActionResult)RedirectToAction("About")
                       : View(emailModel);
        }
    }
}
