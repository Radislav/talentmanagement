﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using Moq;
using TalentManagement.Domain.Events;
using TalentManagement.Domain.Feedbacks;
using TalentManagement.Domain.Objectives;
using TalentManagement.Domain.Texts;
using TalentManagement.Infrastructure;
using TalentManagement.Infrastructure.Data;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Test.Web.Infrastructure
{
    [TestClass]
    public class NotificableArgsHandlerTest 
    {
        [TestMethod]
        public void Handle_should_be_called_when_objective_created()
        {
            //Setup
            ContainerBuilder builder = CreateBuilder();
            builder.RegisterMock(new Mock<IHandle<ObjectiveCreatedArgs>>());
            IContainer container = builder.Build();

            //Execute
            container.Resolve<IObjectiveRepository>()
                .Insert(new Objective(new KeyResult[0]));

            //Asserts
            var mock = container.Resolve<Mock<IHandle<ObjectiveCreatedArgs>>>();
            mock.Verify(m => m.Handle(It.IsAny<ObjectiveCreatedArgs>()), Times.Once());
        }

        [TestMethod]
        public void ObjectiveCreatedArgs_should_be_filled_when_objective_created()
        {
            //Setup
            ObjectiveCreatedArgs args = null;
            ContainerBuilder builder = CreateBuilder();
            var mock = new Mock<IHandle<ObjectiveCreatedArgs>>();
            mock.Setup(m => m.Handle(It.IsAny<ObjectiveCreatedArgs>()))
                .Callback<ObjectiveCreatedArgs>(objectiveArgs => args = objectiveArgs);
            builder.RegisterMock(mock);
            IContainer container = builder.Build();

            //Execute
            Objective objective = new Objective(new KeyResult[0]);
            objective.Contributors.AddRange(new []{"1","2"});
            container.Resolve<IObjectiveRepository>().Insert(objective);

            //Assert
            Assert.AreEqual(args.Entity,objective);
            Assert.AreEqual(args.NotificationTextType,TextType.NotificationObjectiveCreated);
            Assert.AreEqual(args.Objective,objective);
            CollectionAssert.AreEqual(args.ReceiversIds,objective.Contributors);
            Assert.AreEqual(args.SenderId, "CurrentUser");
            
            
        }

        [TestMethod]
        public void Handle_should_be_called_when_feedback_inserted()
        {
            //Setup
            ContainerBuilder builder = CreateBuilder();
            builder.RegisterMock(new Mock<IHandle<FeedbackRequestedArgs>>());
            IContainer container = builder.Build();

            //Execute
            container.Resolve<IFeedbackRepository>()
                .Insert(new Feedback
                            {
                                CreatedBy = "User",
                                CompanyName = "Company",
                                Responders = new []{"1","2"}
                            });

            //Asserts
            var mock = container.Resolve<Mock<IHandle<FeedbackRequestedArgs>>>();
            mock.Verify(m => m.Handle(It.IsAny<FeedbackRequestedArgs>()), Times.Once());
        }

        [TestMethod]
        public void Handle_should_be_called_when_feedback_upserted_and_feedback_IsNew()
        {
            //Setup
            ContainerBuilder builder = CreateBuilder();
            builder.RegisterMock(new Mock<IHandle<FeedbackRequestedArgs>>());
            IContainer container = builder.Build();

            //Execute
            container.Resolve<IFeedbackRepository>()
                .Upsert(new Feedback
                {
                    CreatedBy = "User",
                    CompanyName = "Company",
                    Responders = new[] { "1", "2" }
                });

            //Asserts
            var mock = container.Resolve<Mock<IHandle<FeedbackRequestedArgs>>>();
            mock.Verify(m => m.Handle(It.IsAny<FeedbackRequestedArgs>()), Times.Once());
        }

        [TestMethod]
        public void FeebackCreatedArgs_should_be_filled_when_feedback_created()
        {
            //Setup
            FeedbackRequestedArgs args = null;
            ContainerBuilder builder = CreateBuilder();
            var mock = new Mock<IHandle<FeedbackRequestedArgs>>();
            mock.Setup(m => m.Handle(It.IsAny<FeedbackRequestedArgs>()))
                .Callback<FeedbackRequestedArgs>(objectiveArgs => args = objectiveArgs);
            builder.RegisterMock(mock);
            IContainer container = builder.Build();

            //Execute
            Feedback feedback = new Feedback
                                    {
                                        CreatedBy = "PreviousUser",
                                        CompanyName = "Company",
                                        Responders = new[] {"1", "2"}
                                    };
            container.Resolve<IFeedbackRepository>().Insert(feedback);

            //Assert
            Assert.AreEqual(feedback, args.Entity);
            Assert.AreEqual(TextType.NotificationFeedbackRequested, args.NotificationTextType);
            Assert.AreEqual(feedback, args.Feedback);
            CollectionAssert.AreEqual(feedback.Responders, args.ReceiversIds);
            Assert.AreEqual("CurrentUser", args.SenderId);
        }

        [TestMethod]
        public void Handle_should_be_filled_when_feedback_upserted_and_feedback_IsNew()
        {
            //Setup
            FeedbackRequestedArgs args = null;
            ContainerBuilder builder = CreateBuilder();
            var mock = new Mock<IHandle<FeedbackRequestedArgs>>();
            mock.Setup(m => m.Handle(It.IsAny<FeedbackRequestedArgs>()))
                .Callback<FeedbackRequestedArgs>(objectiveArgs => args = objectiveArgs);
            builder.RegisterMock(mock);
            IContainer container = builder.Build();

            //Execute
            Feedback feedback = new Feedback
            {
                CreatedBy = "PreviousUser",
                CompanyName = "Company",
                Responders = new[] { "1", "2" }
            };
            container.Resolve<IFeedbackRepository>().Upsert(feedback);

            //Assert
            Assert.AreEqual(feedback, args.Entity);
            Assert.AreEqual(TextType.NotificationFeedbackRequested, args.NotificationTextType);
            Assert.AreEqual(feedback, args.Feedback);
            CollectionAssert.AreEqual(feedback.Responders, args.ReceiversIds);
            Assert.AreEqual("CurrentUser", args.SenderId);
        }

        [TestMethod]
        public void Handle_should_NOT_be_called_when_feedback_upserted_and_feedback_NotNew()
        {
            //Setup
            ContainerBuilder builder = CreateBuilder();
            builder.RegisterMock(new Mock<IHandle<FeedbackRequestedArgs>>());
            IContainer container = builder.Build();

            //Execute
            container.Resolve<IFeedbackRepository>()
                .Upsert(new Feedback
                {
                    CreatedBy = "User",
                    CompanyName = "Company",
                    Responders = new[] { "1", "2" },
                    Id = ObjectId.GenerateNewId()
                    
                });

            //Asserts
            var mock = container.Resolve<Mock<IHandle<FeedbackRequestedArgs>>>();
            mock.Verify(m => m.Handle(It.IsAny<FeedbackRequestedArgs>()), Times.Never());
        }

        [TestMethod]
        public void Handle_should_NOT_be_called_when_feedback_updated()
        {
            //Setup
            ContainerBuilder builder = CreateBuilder();
            builder.RegisterMock(new Mock<IHandle<FeedbackRequestedArgs>>());
            IContainer container = builder.Build();

            //Execute
            container.Resolve<IFeedbackRepository>()
                .Update(new Feedback
                {
                    CreatedBy = "User",
                    CompanyName = "Company",
                    Responders = new[] { "1", "2" },
                    Id = ObjectId.GenerateNewId()

                });

            //Asserts
            var mock = container.Resolve<Mock<IHandle<FeedbackRequestedArgs>>>();
            mock.Verify(m => m.Handle(It.IsAny<FeedbackRequestedArgs>()), Times.Never());
        }

        [TestMethod]
        public void Handle_should_be_called_when_objective_updated()
        {
            //Setup
            ContainerBuilder builder = CreateBuilder();
            builder.RegisterMock(new Mock<IHandle<ObjectiveUpdatedArgs>>());

            ObjectId objectiveId = ObjectId.GenerateNewId();
            Objective objective = new Objective(new KeyResult[0])
                                      {
                                          Id = objectiveId,
                                          CreatedBy = "User",
                                          CreatedAt = DateTime.Now.AddDays(-1)
                                      };

            var uowFake = new Mock<IUnitOfWork>();
            uowFake.Setup(u => u.GetById<Objective, ObjectId>(objectiveId)).Returns(objective);
            builder.RegisterMock(uowFake);
            IContainer container = builder.Build();
            

            //Execute
            container.Resolve<IObjectiveRepository>()
                .Update(objective);

            //Asserts
            var mock = container.Resolve<Mock<IHandle<ObjectiveUpdatedArgs>>>();
            mock.Verify(m => m.Handle(It.IsAny<ObjectiveUpdatedArgs>()), Times.Once());
        }


        
        private ContainerBuilder CreateBuilder()
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterModule(new InfrastructureRegistrationModule());
            builder.RegisterMock<IPrincipalProvider>(mock =>
                                                         {
                                                             mock.Setup(pp => pp.GetUserName()).Returns("CurrentUser");
                                                             mock.Setup(pp => pp.GetTenant()).Returns("CurrentTenant");
                                                         });
            builder.RegisterMock(new Mock<IUnitOfWork>());

            
            return builder;
        }
    }

}
