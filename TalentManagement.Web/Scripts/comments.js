﻿function Comments(args) {

    this._initialize = function (_args) {
  
        //Expand comment section on "comment" clicked
        $('.comment-link').off('click').on("click", function () {
            $(this).parents(_args.containerSelector)
                .find('.new-comment')
                .slideDown();
        });

        //Add comment on "POST" clicked
        $('.new-comment button').off('click').on("click", function () {
            postComment($(this), $(this).prev().val(),_args);
        });

        $('.new-comment input[type="text"]').off('keypress').on("keypress", function (e) {
            if (e.keyCode === 13) {
                postComment($(this), $(this).val(), _args);
            }
        });
    };

    function postComment(initiatorElement, commentContent, _args) {
        
        var data = JSON.stringify({
            comment:
                    {
                        Content: commentContent,
                        CreatedBy: ''
                    },
                    holderId: $(initiatorElement).parents(_args.containerSelector)
                    .find('#' + args.holderId)
                    .val()
        });

        $.ajax({
            url: _args.handlerUrl,
            data: data,
            success: function (response) {

                $(initiatorElement).parents('.comment-section')
                    .append(response)
                    .find('.new-comment input')
                    .val('');

            }
        });
    }

    _initialize(args);
}