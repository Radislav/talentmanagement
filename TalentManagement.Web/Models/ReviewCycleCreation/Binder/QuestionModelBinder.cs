﻿using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Domain.Performance;
using TalentManagement.Web.Models.Binder;

namespace TalentManagement.Web.Models.ReviewCycleCreation.Binder
{
    public class QuestionModelBinder : CustomModelBinder<QuestionModel>
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            BinderValueProvider valueProvider = new BinderValueProvider(bindingContext);
            return new QuestionModel
                {
                    AnswerType = valueProvider.GetValue<AnswerType>("AnswerType",Prefix,Index),
                    Id = valueProvider.GetValue<ObjectId>("Id",Prefix,Index),
                    Number = valueProvider.GetValue<int>("Number",Prefix,Index),
                    Settings = valueProvider.GetValue<QuestionSetting>("Settings",Prefix,Index),
                    SetType = valueProvider.GetValue<QuestionSetType>("SetType",Prefix,Index),
                    Text = valueProvider.GetValue<string>("Text",Prefix,Index)
                };
        }
    }
}