﻿using System.Collections.Generic;

namespace TalentManagement.Domain
{
    public class IdHolderComparer<TId> : IEqualityComparer<IHasId<TId>>
    {
        public bool Equals(IHasId<TId> x, IHasId<TId> y)
        {
            if (x == null || y == null)
            {
                return ReferenceEquals(x, y);
            }
            
            if (ReferenceEquals(y.Id, null) || 
                y.Id.Equals(default(TId))   || 
                ReferenceEquals(x.Id, null) || 
                y.Id.Equals(default(TId)))
            {
                return ReferenceEquals(x, y);
            }

            return x.Id.Equals(y.Id);
        }

        public int GetHashCode(IHasId<TId> obj)
        {
            return ReferenceEquals(obj.Id, null) || obj.Id.Equals(default(TId)) ? base.GetHashCode() : obj.Id.GetHashCode();
        }
    }
}
