﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Collections.Generic;
using TalentManagement.Domain.Companies;
using TalentManagement.Domain.Feeds;
using TalentManagement.Domain.Objectives;
using TalentManagement.Domain.Users;

namespace TalentManagement.Domain.Performance
{
    public class ReviewCycleSummary
    {
        public ReviewCycleSummary(ReviewCycle cycle,
                                IEnumerable<Review> reviews,
                                IEnumerable<AnswersCollection> answersCollections,
                                IEnumerable<Objective> respondentObjectives,
                                IEnumerable<Kudos> respondentKudos,
                                IEnumerable<Badge> respondentKudosBadges,
                                User summaryReviewRespondent)
        {
            Contract.Requires(cycle != null);
            Contract.Requires(cycle.IsNew == false);
            Contract.Requires(reviews != null);
            Contract.Requires(answersCollections != null);
            Contract.Requires(respondentObjectives != null);
            Contract.Requires(respondentKudos != null);
            Contract.Requires(respondentKudosBadges != null);
            Contract.Requires(summaryReviewRespondent != null);

            Cycle = cycle;
            Reviews = reviews;
            Objectives = respondentObjectives;
            AnswersCollections = answersCollections;
            SummaryReviewRespondent = summaryReviewRespondent;
            KudosWithBadges =
                respondentKudos.Select(
                    kudos =>
                    new Tuple<Kudos, Badge>(kudos,
                                            respondentKudosBadges.SingleOrDefault(badge => badge.Id == kudos.BadgeId)));

        }

        public ReviewCycle Cycle { get; private set; }
        public IEnumerable<Review> Reviews { get; private set; }
        public IEnumerable<Objective> Objectives { get; private set; }
        public IEnumerable<Tuple<Kudos, Badge>> KudosWithBadges { get; private set; }
        public IEnumerable<AnswersCollection> AnswersCollections { get; private set; }

        public Review GetReview(QuestionSetType setType)
        {
            return Reviews.SingleOrDefault(r => r.SetType == setType);
        }

        public AnswersCollection GetAnswers(QuestionSetType setType)
        {
            return AnswersCollections.SingleOrDefault(a => a.SetType == setType);
        }

        public Review SummaryReview
        {
            get { return GetReview(QuestionSetType.Summary); }
        }

        public User SummaryReviewRespondent { get; private set; }
    }
}
