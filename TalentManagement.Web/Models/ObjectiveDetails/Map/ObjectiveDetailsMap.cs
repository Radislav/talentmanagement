﻿using System.Linq;
using TalentManagement.Domain.Objectives;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;


namespace TalentManagement.Web.Models.ObjectiveDetails.Map
{
    public class ObjectiveDetailsMap : IMap<Objective,ObjectiveDetailsModel>
    {
        private readonly IMapper<KeyResult, KeyResultModel> _keyResultModelMapper;
        private readonly IMapper<User, UserPicked> _userMapper;
        private readonly IUserRepository _userRepository;


        public ObjectiveDetailsMap(IMapper<KeyResult, KeyResultModel> keyResultModelMapper, IMapper<User, UserPicked> userMapper, IUserRepository userRepository)
        {
            _keyResultModelMapper = keyResultModelMapper;
            _userMapper = userMapper;
            _userRepository = userRepository;
        }

        public void Configure(IMapperConfiguration<Objective, ObjectiveDetailsModel> configuration)
        {
          
            configuration.ForMember(d => d.Contributors, s => s.Contributors);
            configuration.ForMember(d => d.CreatedBy, s => s.CreatedBy);
            configuration.ForMember(d => d.Description, s => s.Description);
            configuration.ForMember(d => d.Id, s => s.Id);
            configuration.ForMember(d => d.KeyResults, s => s.KeyResults.Select(_keyResultModelMapper.Map).ToArray());
            configuration.ForMember(d => d.Name, s => s.Name);
            configuration.ForMember(d => d.Due, s => s.Due.AsLondDateString());
            configuration.ForMember(d => d.Visibility, s => s.Visibility);
            configuration.ForMember(d => d.Icon, s => s.Icon);
            configuration.ForMember(d => d.Progress, s => s.Progress);
            configuration.ForMember(d => d.ContributorsPicked,
                                    s => _userRepository.GetUsers(s.Contributors).Select(_userMapper.Map).ToList());
            
            configuration.AfterMap((source, dest) =>
                                       {
                                           dest.GetSafe(model => model.KeyResults)
                                               .SelectMany(kr => kr.ActionItems)
                                               .Map(actionItem => actionItem.Users = dest.ContributorsPicked);

                                           dest.GetSafe(model => model.KeyResults)
                                               .Map(kr => kr.NewActionItem = new ActionItemModel { Users = dest.ContributorsPicked });
                                       });
        }
    }
}