﻿using System.Collections.Generic;
using System.Linq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TalentManagement.Domain.Events;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Events;

namespace TalentManagement.Test.Infrastructure.Events
{
    [TestClass]
    public class UserUpdatedHandlerTest : MockedTestClass<IHandle<UserUpdatedArgs>>
    {
        private User _managerWithTeam;
        private Team _team;
        private User _managerWithoutTeam;

        [TestInitialize]
        public void Initialize()
        {
            _managerWithTeam = new User("manager1@gmail.com");
            _team = new Team(_managerWithTeam);
            _managerWithoutTeam = new User("manager2@gmail.com");
        }

        protected override IHandle<UserUpdatedArgs> CreateTarget(IContainer container)
        {
            return container.Resolve<IEnumerable<IHandle<UserUpdatedArgs>>>()
                            .SingleOrDefault(service => service is UserUpdatedHandler);

        }

        protected override void RegisterMocks(ContainerBuilder containerBuilder)
        {
            Mock<IUserRepository> userRepository = new Mock<IUserRepository>();
            userRepository.Setup(r => r.GetById("manager1@gmail.com")).Returns(_managerWithTeam);
            userRepository.Setup(r => r.GetById("manager2@gmail.com")).Returns(_managerWithoutTeam);
            containerBuilder.RegisterMock(userRepository);

            Mock<ITeamRepository> teamRepository = new Mock<ITeamRepository>();
            teamRepository.Setup(r => r.GetTeams(_managerWithTeam)).Returns(new Team[]{_team});
            teamRepository.Setup(r => r.GetTeams(_managerWithoutTeam)).Returns(new Team[0]);

            containerBuilder.RegisterMock(teamRepository);

            base.RegisterMocks(containerBuilder);
        }
        
        [TestMethod]
        public void Handle_Should_Add_user_to_managers_teams_When_manager_has_teams()
        {
            ContainerBuilder builder = GetContainerBuilder();
            RegisterMocks(builder);

            Team team = null;
            IContainer container = builder.Build();
            var teamRepository = container.Resolve<Mock<ITeamRepository>>();
            teamRepository.Setup(r => r.Update(It.IsAny<Team>())).Callback<Team>(t => team = t);
            ContainerBuilder updateBuilder = new ContainerBuilder();
            updateBuilder.RegisterMock(teamRepository);
            updateBuilder.Update(container);

            var target = CreateTarget(container);
            target.Handle(new UserUpdatedArgs(
                new User("user1@gmail.com") { ManagerEmail = "manager1@gmail.com" }, null));

            teamRepository.Verify(t => t.Update(_team),Times.Once());
            CollectionAssert.Contains(team.Members.ToArray(), "user1@gmail.com");
        }

        [TestMethod]
        public void Handle_Should_insert_new_team_and_add_user_to_it_When_manager_has_no_teams()
        {
            ContainerBuilder builder = GetContainerBuilder();
            RegisterMocks(builder);

            Team team = null;
            IContainer container = builder.Build();
            var teamRepository = container.Resolve<Mock<ITeamRepository>>();
            teamRepository.Setup(r => r.Insert(It.IsAny<Team>())).Callback<Team>(t => team = t);
            ContainerBuilder updateBuilder = new ContainerBuilder();
            updateBuilder.RegisterMock(teamRepository);
            updateBuilder.Update(container);

            var target = CreateTarget(container);
            target.Handle(new UserUpdatedArgs(
                new User("user1@gmail.com") { ManagerEmail = "manager2@gmail.com" }, null));

            teamRepository.Verify(t => t.Insert(It.IsAny<Team>()), Times.Once());
            CollectionAssert.Contains(team.Members.ToArray(), "user1@gmail.com");
        }


        [TestMethod]
        public void Handle_Should_Remove_user_from_team_When_manager_was_changed()
        {
            ContainerBuilder builder = GetContainerBuilder();
            RegisterMocks(builder);

            Team team = null;
            IContainer container = builder.Build();
            var teamRepository = container.Resolve<Mock<ITeamRepository>>();
            teamRepository.Setup(r => r.Insert(It.IsAny<Team>())).Callback<Team>(t => team = t);
            ContainerBuilder updateBuilder = new ContainerBuilder();
            updateBuilder.RegisterMock(teamRepository);
            updateBuilder.Update(container);

            var target = CreateTarget(container);
            target.Handle(new UserUpdatedArgs(
                new User("user1@gmail.com") { ManagerEmail = "manager2@gmail.com" }, null));

            teamRepository.Verify(t => t.Insert(It.IsAny<Team>()), Times.Once());
            CollectionAssert.Contains(team.Members.ToArray(), "user1@gmail.com");
        }
    }
}
