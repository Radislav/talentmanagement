﻿using MongoDB.Bson;

namespace TalentManagement.Domain
{
    public class Entity : EntityWithTypedId<ObjectId>
    {
        public Entity() 
        {
        }

        public Entity(ObjectId id) : base(id)
        {
        }
    }
}
