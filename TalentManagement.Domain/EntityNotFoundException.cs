﻿using System;

namespace TalentManagement.Domain
{
    public class EntityNotFoundException : EntityException
    {
        private readonly Type _entityType;
        private readonly object _entityId;

        public EntityNotFoundException()
        {
        }

        public EntityNotFoundException(string message)
            : base(message)
        {

        }

        public EntityNotFoundException(Type entityType, object entityId)
            : base(GenerateMessage(entityType, entityId))
        {
            _entityType = entityType;
            _entityId = entityId;
        }

        public static string GenerateMessage(Type entityType, object entityId)
        {
            return String.Format("Entity {0} with Id = {1} has been not found", entityType, entityId);
        }

        public Type EntityType
        {
            get { return _entityType; }
        }

        public Object EntityId
        {
            get { return _entityId; }
        }

    }
}
