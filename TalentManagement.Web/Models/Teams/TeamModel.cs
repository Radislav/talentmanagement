﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace TalentManagement.Web.Models.Teams
{
    public class TeamModel : ICanPickUsers<TeamModel>
    {
        public UserPicked Manager { get; set; }
        public string Name { get; set; }
        public ObjectId Id { get; set; }
        public bool IsUnassigned { get; set; }
        public List<UserPicked> Users { get; set; }
        public List<UserPicked> Members { get; set; }
        public bool IsExpanded { get; set; }

        public System.Linq.Expressions.Expression<System.Func<TeamModel, string>> TitleExpression
        {
            get { return model => Name; }
        }
    }
}