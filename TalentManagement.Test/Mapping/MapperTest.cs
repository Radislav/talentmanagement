﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TalentManagement.Infrastructure;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Test.Mapping
{
    [TestClass]
    public class MapperTest 
    {
        public class ForMap1
        {
            public string StringValue { get; set; }
        }

        public class ForMap2
        {
            public int IntValue { get; set; }
        }

        public class MapFrom1To2 : IMap<ForMap1, ForMap2>
        {
            public void Configure(IMapperConfiguration<ForMap1, ForMap2> configuration)
            {
                configuration.MapFrom(c => c.StringValue, d => d.IntValue);

            }
        }

        [TestMethod]
        public void Mapper_should_work()
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterModule(new InfrastructureRegistrationModule());

            builder.RegisterType<MapFrom1To2>()
                .As<IMap<ForMap1, ForMap2>>();
            IContainer container = builder.Build();

            var target = container.Resolve<IMapper<ForMap1, ForMap2>>();

            Assert.AreEqual(2222,target.Map(new ForMap1{StringValue = "2222"}).IntValue);
        }
    }
}
