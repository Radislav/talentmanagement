﻿using System.Collections.Generic;

namespace TalentManagement.Web.Infrastructure.Cache
{
    public interface IOutputCacheCleaner
    {
        void RemoveImageFromCache(string action, string controller, object id);
        void RemoveImageFromCache(string action, string controller, Dictionary<string, object> routes);
    }
}
