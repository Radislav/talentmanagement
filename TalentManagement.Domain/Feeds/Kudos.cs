﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using MongoDB.Bson;

namespace TalentManagement.Domain.Feeds
{
    public class Kudos
    {
        private readonly Feed _feed;

        public Kudos(Feed feed)
        {
            Contract.Requires(feed != null);
            _feed = feed;
        }

        public ObjectId BadgeId
        {
            get { return _feed.GetIdPropertyValue("BadgeId"); }
        }

        public IEnumerable<string> Receivers
        {
            get
            {
                string receivers = _feed.GetPropertyValue("Receivers");
                return receivers == null ? new string[0] : receivers.Split(',');
            }
        }

        public string Content
        {
            get { return _feed.GetPropertyValue("Content"); }
        }

        public Feed Feed
        {
            get { return _feed; }
        }
    }
}
