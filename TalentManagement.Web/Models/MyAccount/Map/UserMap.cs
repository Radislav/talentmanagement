﻿using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.MyAccount.Map
{
    internal class UserMap : IMap<IndexModel,User>
    {
        public void Configure(IMapperConfiguration<IndexModel, User> configuration)
        {
            configuration.ForMember(d => d.FirstName, s => s.FirstName);
            configuration.ForMember(d => d.ManagerEmail, s => s.ManagerEmail);
            configuration.ForMember(d => d.Position, s => s.Position);
            configuration.ForMember(d => d.SecondName, s => s.SecondName);
            configuration.ForMember(d => d.Id, s => s.UserId);
        }
    }
}