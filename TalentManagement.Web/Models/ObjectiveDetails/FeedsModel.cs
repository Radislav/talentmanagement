﻿using TalentManagement.Web.Models.Feeds;

namespace TalentManagement.Web.Models.ObjectiveDetails
{
    public class FeedsModel
    {
        public FeedModel[] Feeds { get; set; }

        [LocalizedDisplayName("NewFeed", typeof(Resources.Models.ObjectiveDetails.ObjectiveDetailsModel))]
        public string NewFeed { get; set; }
    }
}