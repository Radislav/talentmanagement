﻿using System;
using System.Linq;
using TalentManagement.Domain.Events;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data.Repositories
{
    class AvatarRepository : RepositoryWithTypedId<Avatar,string> , IAvatarRepository
    {
         private readonly Lazy<IDomainEvents> _domainEvents;

        public AvatarRepository(Lazy<IDomainEvents> domainEvents,IUnitOfWork unitOfWork,IPrincipalProvider principalProvider)
            : base(unitOfWork,principalProvider)
        {
            _domainEvents = domainEvents;
        }

        public Avatar GetOnlySmallImage(string userEmail)
        {
            Avatar avatar = UnitOfWork.AsQueryable<Avatar>()
                .Where(a => a.Id == userEmail)
                .Select(a => new Avatar(a.Id)
                                 {
                                     SmallImage = a.SmallImage,
                                     ImageType = a.ImageType
                                 })
                .SingleOrDefault();

            if (avatar != null)
            {
                avatar.Image = avatar.SmallImage;
            }

            return avatar;
        }

        public override void Update(Avatar entity)
        {
            base.Update(entity);
            _domainEvents.Value.Raise(new AvatarUpdatedArgs(entity.Id));
        }
    }
}
