﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TalentManagement.Domain.Objectives;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Infrastructure.Validation;
using TalentManagement.Web.Infrastructure.Cache;
using TalentManagement.Web.Infrastructure.Security;
using TalentManagement.Web.Models;
using TalentManagement.Web.Models.Objectives;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Controllers
{
    public class ObjectiveController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper<User, UserPicked> _userMapper;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IObjectiveRepository _objectiveRepository;
        private readonly IObjectiveIconRepository _objectiveIconRepository;
        private readonly IMapper<ObjectiveCreateModel, Objective> _objectiveMapper;
        private readonly IValidator<CreateStep4> _step4Validator;

        public ObjectiveController(IUserRepository userRepository,
                                   IMapper<User,UserPicked> userMapper, 
                                   IMapper<ObjectiveCreateModel, Objective> objectiveMapper,
                                   IPrincipalProvider principalProvider,
                                   IObjectiveRepository objectiveRepository,
                                   IObjectiveIconRepository objectiveIconRepository,
                                   IValidator<CreateStep4> step4Validator)
        {
            _userRepository = userRepository;
            _userMapper  = userMapper;
            _objectiveMapper = objectiveMapper;
            _principalProvider = principalProvider;
            _objectiveRepository = objectiveRepository;
            _objectiveIconRepository = objectiveIconRepository;
            _step4Validator = step4Validator;
        } 
       
        [HttpGet]
        [TenantAuthorization(Roles = SecurityConstants.ContributorRole)]
        public ActionResult Create()
        {
            List<UserPicked> users = _userRepository.GetUsersForTenant(_principalProvider.GetTenant())
                                                .Select(_userMapper.Map)
                                                .ToList();

            return View(new ObjectiveCreateModel(users, _objectiveIconRepository.GetIconIds()));
        }

        [HttpPost]
        [TenantAuthorization(Roles = SecurityConstants.ContributorRole)]
        public ActionResult Create(ObjectiveCreateModel model)
        {
            if (ModelState.IsValid)
            {
                Objective objective = _objectiveMapper.Map(model);
                _objectiveRepository.Insert(objective);
                return Json(new { RedirectTo = Url.Action("Index", "ObjectiveDetails", new { id = objective.Id }) });
            }

            return PartialView("_CreateWizard", model);
        }

        [HttpPost]
        [TenantAuthorization(Roles = SecurityConstants.ContributorRole)]
        public ActionResult CreateWizard(ObjectiveCreateModel model)
        {
            return PartialView("_CreateWizard", model);
        }

        [HttpPost]
        [TenantAuthorization(Roles = SecurityConstants.ContributorRole)]
        public ActionResult SetCurrentKeyResult(CreateStep2 step2Model)
        {
            step2Model.CurrentKeyResult = step2Model.CurrentKeyResult ?? new KeyResultModel();
        
            return PartialView("_CreateStep2", step2Model);
        }

        [HttpPost]
        [TenantAuthorization(Roles = SecurityConstants.ContributorRole)]
        public ActionResult AddCurrentKeyResult(CreateStep2 step2Model)
        {
            ValidationResult validationResult = ModelState.ValidateAndKeepState(step2Model.CurrentKeyResult, "CurrentKeyResult");
        
            if (validationResult.IsValid)
            {
                step2Model.UpsertKeyResult(step2Model.CurrentKeyResult);
                ModelState.Clear();
            }
            
            return PartialView("_CreateStep2", step2Model);
        }

        [HttpPost]
        [TenantAuthorization(Roles = SecurityConstants.ContributorRole)]
        public ActionResult DeleteKeyResult(CreateStep2 step2Model,int keyResultIndex)
        {
            step2Model.DeleteKeyResult(keyResultIndex);
            return PartialView("_CreateStep2", step2Model);
        }

        [HttpPost]
        [TenantAuthorization(Roles = SecurityConstants.ContributorRole)]
        public ActionResult AddContributor(CreateStep3 step3Model)
        {
            UserPicked currentUser = step3Model.UserPickerModel.CurrentUser;
            var pickedUsers = step3Model.UserPickerModel.UsersPicked;

            if (currentUser != null && false == pickedUsers.Contains(currentUser))
            {
                pickedUsers.Add(currentUser);
            }

            return PartialView("_CreateStep3", step3Model);
        }

        [HttpPost]
        [ModelImageCache]
        public ActionResult AddObjectiveIcon(CreateStep4 step4Model)
        {
            ValidationResult validationResult = _step4Validator.ValidateInstance(step4Model);
            if (validationResult.IsValid)
            {
                ObjectiveIcon objectiveIcon = new ObjectiveIcon
                {
                    Image = step4Model.IconBytes,
                    ImageType = step4Model.IconContentType
                };    
                _objectiveIconRepository.Insert(objectiveIcon);
                
                return new JsonResult
                {
                    Data = new {
                                validationResult.IsValid,
                                Errors = validationResult.Failures.Select(f => new { f.PropertyName, f.ErrorMessage }),
                                NewIconId = Convert.ToString(objectiveIcon.Id)
                            }
                };    
            }

            return validationResult.AsJson();

        }
    }
}
