﻿using System;
using TalentManagement.Infrastructure.Resources;

namespace TalentManagement.Web.Models.MyCompanyObjectives.Map
{
    class MonthYearTitleFormatter: ITitleFormatter<MonthYearPair>
    {
        private readonly IResourceProvider _resourceProvider;

        public MonthYearTitleFormatter(IResourceProvider resourceProvider)
        {
            _resourceProvider = resourceProvider;
        }

        public string FormatTitle(MonthYearPair groupKey)
        {
            return String.Format("{0} {1}", _resourceProvider.MyCompanyObjectivesDue,
                                            new DateTime(groupKey.Year, groupKey.Month, 1).AsMonthYearString());
        }
    }
}