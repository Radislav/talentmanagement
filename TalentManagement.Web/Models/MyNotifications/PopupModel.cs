﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace TalentManagement.Web.Models.MyNotifications
{
    public class PopupModel
    {
        public PopupModel()
        {
            Notifications = new NotificationPopupModel[0];
        }
        
        private const int NotificationsCountToShow = 5;
        public NotificationPopupModel[] Notifications { get; set; }

        [ContractInvariantMethod]
        private void Invariant()
        {
            Contract.Invariant(Notifications != null);
        }

        public IEnumerable<NotificationPopupModel> NotificationsToShow
        {
            get { return Notifications.Take(NotificationsCountToShow); }
        }

        public IEnumerable<NotificationPopupModel> NotificationsHidden
        {
            get { return Notifications.Skip(NotificationsCountToShow); }
        }

        public bool HasHiddenNotifications
        {
            get { return Notifications.Length > NotificationsCountToShow; }
        }
    }
}