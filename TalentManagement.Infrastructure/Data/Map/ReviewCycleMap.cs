﻿using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Data.Configuration;

namespace TalentManagement.Infrastructure.Data.Map
{
    public class ReviewCycleMap : EntityConfiguration<ReviewCycle>
    {
        public ReviewCycleMap()
        {
            AsCollection("ReviewCycles")
                .WithDataSource(DataSourceType.Tenant);

            Property(p => p.Name)
                .Required();

            Property(p => p.Participants)
                .Required();

            Property(p => p.QuestionSets)
                .Required();

            Property(p => p.CreatedBy)
                .Required();
            
        }
    }
}
