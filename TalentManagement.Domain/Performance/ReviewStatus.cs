﻿using Resource = TalentManagement.Resources.DomainResources;

namespace TalentManagement.Domain.Performance
{
    public enum ReviewStatus
    {
        [LocalizedDescription("None", typeof(Resource))]
        None,
        [LocalizedDescription("NotStarted",typeof(Resource))]
        NotStarted,
        [LocalizedDescription("InProgress", typeof(Resource))]
        InProgress,
        [LocalizedDescription("Completed", typeof(Resource))]
        Completed
    }
}
