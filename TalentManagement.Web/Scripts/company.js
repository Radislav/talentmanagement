﻿$(function () {

    function getEditorValue(editor, prefix, suffix) {
        return editor.find('#' + prefix + '_' + suffix).val();
    }

    function getBadgeContext(elementFromBadgeEditor) {

        var editor = $(elementFromBadgeEditor).parents(".modal").find('.badge-editor');
        var titleName = editor.find('input[name$="Title"]').attr('name');
        var prefix = titleName.substr(0, titleName.length - 6);
        var modalId = $(elementFromBadgeEditor).parents(".modal").attr('id');

        var data = {
            BadgeId: getEditorValue(editor, prefix, 'BadgeId'),
            Title: getEditorValue(editor, prefix, 'Title'),
            Description: getEditorValue(editor, prefix, 'Description'),
            IsEnabled: getEditorValue(editor, prefix, 'IsEnabled'),
            CompanyId: $('#CompanyId').val(),
            SubmitClicked: true
        };
        return {
            data: data,
            editor: editor,
            prefix: prefix,
            modalId: modalId
        };
    }

    function toggleObjectiveFocus(objectiveId, focusFlag) {
        var data = {
            objectiveId: objectiveId,
            focus: focusFlag,
            objectivesFocusTab : {
                filterByYear: $('#FilterByYear').val(),
                groupBy: $('#activity_stats .btn-group .active').attr('name')
            }
        };

        $.ajax({
            url: urls.toggleObjectiveFocus(),
            data: JSON.stringify(data),
            success: function (response) {
                $('#objectives-focus').html(response);
                var objectives = new MyCompanyObjectives(initObjectiveFocusTab);
                objectives.initialize();
                initObjectiveFocusTab();
            }
        });
    }

    function initObjectiveFocusTab() {

        var objectives = new MyCompanyObjectives(initObjectiveFocusTab);
        objectives.initialize();
        
        //Click on 'Add to focus'
        $('.add-to-focus a').off('click').on('click', function (e) {
            e.preventDefault();
            toggleObjectiveFocus($(this).data('objective'), true);
        });

        $('.objective-icon-action a').off('click').on('click', function (e) {
            e.preventDefault();
            toggleObjectiveFocus($(this).data('objective'), false);
        });
    }

    initObjectiveFocusTab();

    //Click on badge disable/enable
    $('.badge-enable').click(function (e) {
        e.preventDefault();

        var container = $(this).parents('.badge-view');
        var data = {
            companyId: $('#CompanyId').val(),
            badgeId: container.find('#badge_BadgeId').val(),
            enable: "FALSE" == container.find('#badge_IsEnabled').val().toUpperCase()
        };

        var enablingLink = $(this);


        $.ajax({
            url: $(enablingLink).attr('href'),
            data: JSON.stringify(data),
            success: function (response) {
                enablingLink.text(response.EnableAction);
                container.find('#badge_IsEnabled').val(response.IsEnabled);
            }
        });
    });

    //Click on badge thumbnail
    $('a.thumbnail').click(function () {
        $(this).parents('li')
               .find('a[href$="viewBadge"]')
               .click();
    });

    //Show active tab
    $('.nav-tabs a[href="#' + $('#ActiveButton').val() + '"]')
        .tab('show');

    //Initialize uploader
    $('.badge-editor input[type="file"]').fileupload({
        dataType: 'json',
        url: urls.addBadge(),
        add: function (e, data) {
            var file = data.files[0];
            if (file != null) {
                $(this).parents(".badge-editor")
                       .find('.form_input span')
                       .html(file.name);
            }
            var badgeContext = getBadgeContext(this);
            badgeContext.data.SubmitClicked = false;
            data.formData = badgeContext.data;
            data.submit();
        }
    });

    //Click on "Choose file" button
    $('.badge-editor .form_input a').click(function (e) {
        e.preventDefault();
        $(this).parents(".badge-editor")
               .find('input[type="file"]')
               .click();
    });

    //Click on 'Create badge button'
    $('a[name="upsert-badge"]').click(function (e) {
        e.preventDefault();

        var badgeContext = getBadgeContext(this);

        $.ajax({
            url: $(this).attr('href'),
            data: JSON.stringify(badgeContext.data),
            success: function (response) {
                if (response.IsValid) {
                    badgeContext.editor.find('#addBadge').modal('hide');
                    document.location = urls.badgesTab();
                } else {
                    var containerSelector = '#' + badgeContext.modalId + ' .modal-body';
                    validation.showJsonErrors(response, containerSelector, badgeContext.prefix);
                    validation.showSummaryErrors(response, containerSelector);
                }
            }
        });
    });
});

