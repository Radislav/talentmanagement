﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using MongoDB.Bson;
using TalentManagement.Domain.Performance;

namespace TalentManagement.Web.Models.ReviewCycleCreation
{
    public class ReviewCycleModel : WizardMaster
    {
        public ReviewCycleModel()
        {
            CreateStep1 = new CreateStep1();
            CreateStep2 = new CreateStep2();
            CreateStep3 = new CreateStep3();
        }

        public CreateStep1 CreateStep1 { get; set; }
        public CreateStep2 CreateStep2 { get; set; }
        public CreateStep3 CreateStep3 { get; set; }

        public IEnumerable<QuestionModel> GetQuestionModels()
        {
            return CreateStep2.QuestionSets.SelectMany(set => set.Questions);
        }

        public DateTime GetDueDateFor(QuestionSetType setType)
        {
            switch (setType)
            {
               case QuestionSetType.Pears:
                    return DateTime.Parse(CreateStep1.PearsDue);
                case QuestionSetType.Self:
                    return DateTime.Parse(CreateStep1.SelfDue);
                case QuestionSetType.Summary:
                    return DateTime.Parse(CreateStep1.SummaryDue);
                default:
                    throw new NotSupportedException();
            }
        }

        public override IEnumerable<WizardPage> Pages
        {
            get
            {
                return new []{ (WizardPage)CreateStep1,CreateStep2,CreateStep3 };
            }
        }
    }
}