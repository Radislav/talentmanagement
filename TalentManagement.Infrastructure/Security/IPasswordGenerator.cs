﻿using System.Diagnostics.Contracts;
using TalentManagement.Infrastructure.Contracts;

namespace TalentManagement.Infrastructure.Security
{
    [ContractClass(typeof(PasswordGeneratorContract))]
    public interface IPasswordGenerator
    {
        string GeneratePassword(int length);
    }
}
