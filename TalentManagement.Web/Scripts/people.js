﻿$(function () {

    $("#peopleForm").on("click", "button", function (e) {
        $("#peopleForm input[type=hidden]").prop("disabled", false);
        $('#peopleForm input[type=hidden][name="' + $(this).attr("name") + '"]').prop("disabled", "true");
    });

    function init() {

        uploaderInit();

        //Add user click
        $('button[form="addUserForm"]').click(function (e) {
            e.preventDefault();
            var form = $('#addUserContent form');
            var data = JSON.stringify(form.serializeObject());
            $.ajax({
                url: form.attr('action'),
                data: data,
                success: function (response, status, xhr) {
                    var ct = xhr.getResponseHeader("content-type") || "";
                    //Everything fine, let`s show question sets.
                    if (ct.indexOf('html') > -1) {
                        $('#addUserPopup').modal('hide');
                        validation.hideJsonErrors('#addUserContent');
                        $('.msgbar').show('slide');

                        setTimeout(function () {
                            $('.msgbar').hide('slide');
                        }, 3000);

                        init();
                    }
                    //Error(s) occured, display them
                    if (ct.indexOf('json') > -1) {
                        validation.showJsonErrors(response, $('#addUserContent'));
                    }
                }
            });
        });

        //Toggle manager click
        $('.toggle-manager-link').click(function (e) {
            e.preventDefault();
            var link = $(this);
            $.ajax({
                url: link.attr('href'),
                success: function (response) {
                    $(link).html(response);
                }
            });
        });

    }

    init();

});


