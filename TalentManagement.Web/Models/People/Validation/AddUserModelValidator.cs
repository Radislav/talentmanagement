﻿using FluentValidation;
using System.Web.Mvc;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Validation;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.People.Validation
{
    internal class AddUserModelValidator : AbstractValidator<AddUserModel>, TalentManagement.Infrastructure.Validation.IValidator<AddUserModel>
    {
        public AddUserModelValidator()
        {
            IResourceProvider resourceProvider = DependencyResolver.Current.GetService<IResourceProvider>();
            RuleFor(m => m.Email)
                .Matches(RegularExpressions.Email)
                .NotEmpty();

            RuleFor(m => m.Password)
                .NotEmpty();

            RuleFor(m => m.ManagerEmailLocal)
                .Matches(RegularExpressions.EmailUserName)
                .When(m => false == string.IsNullOrEmpty(m.ManagerEmailLocal))
                .WithMessage(resourceProvider.ErrorEmailInvalid);

            RuleFor(m => m.FirstName)
                .NotEmpty();

            RuleFor(m => m.SecondName)
                .NotEmpty();
        }

        public ValidationResult ValidateInstance(AddUserModel instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}