﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;

namespace TalentManagement.Web.Models.Objectives
{
    public class CreateStep2 : WizardPage
    {
        public CreateStep2()
        {
            CurrentKeyResult = new KeyResultModel();
        }

        public override int NextStep
        {
            get { return 2; }
        }

        public override int ThisStep
        {
            get { return 1; }
        }

        private List<KeyResultModel> _keyResults = new List<KeyResultModel>();
        public List<KeyResultModel> KeyResults
        {
            get { return _keyResults; }
            set { _keyResults = value;}
        }
        
        public KeyResultModel CurrentKeyResult { get; set; }

        public void DeleteKeyResult(int index)
        {
            _keyResults.RemoveAt(index);
            for (int idx = 0; idx < _keyResults.Count; idx++)
            {
                _keyResults[idx].Index = idx;
            }
        }

        public void UpsertKeyResult(KeyResultModel keyResult)
        {
            if (keyResult != null)
            {
                if (keyResult.IsNew || KeyResults.Count <= keyResult.Index)
                {
                    keyResult.Id = ObjectId.GenerateNewId(DateTime.Now);
                    keyResult.Index = KeyResults.Count;
                    KeyResults.Add(keyResult);
                }
                else
                {
                    KeyResults[keyResult.Index] = keyResult;
                }
            }
        }

       
    }
}