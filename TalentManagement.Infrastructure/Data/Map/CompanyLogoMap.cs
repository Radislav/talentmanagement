﻿using TalentManagement.Domain.Companies;
using TalentManagement.Infrastructure.Data.Configuration;

namespace TalentManagement.Infrastructure.Data.Map
{
    internal class CompanyLogoMap : EntityConfiguration<Logo>
    {
        public CompanyLogoMap()
        {
            AsCollection("CompaniesLogos")
                .WithDataSource(DataSourceType.Tenant);

            Property(p => p.Image)
                .Required();

            Property(p => p.ImageType)
                .Required();
        }
    }
}
