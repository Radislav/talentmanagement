﻿$(function () {

    var messageExpirationMs = 3000;

    Comments({
        containerSelector: '.feed',
        holderId: 'feedModel_Id',
        handlerUrl: globalUrls.addFeedCommentUrl()
    });

    initProgressSlider();

    initKeyResults();

    initObjectiveCompleteButton();

    initFeeds();

    function initProgressSlider() {
        $('#progress').slider({
            value: $('#progress-value').html(),
            range: "min",
            slide: function (event, ui) {
                $("#progress-value").html(ui.value);
            },
            stop: function (event, ui) {
                var data = {
                    objectiveId: getObjectiveId(),
                    progress: ui.value + ".0"
                };
                ajaxPost(urls.updateProgress(), data, function (response) {
                    $('#progress-updated-message').html(response.message);

                    if ($('#objective-completed').is(':visible')) {
                        $('#objective-completed').hide('slide');
                    }

                    $('#progress-updated').show('slide');

                    setTimeout(function () {
                        $('#progress-updated').hide('slide');
                    }, messageExpirationMs);
                });
            }
        });

        $('#progress-updated').click(function () {
            $(this).hide('slide');
        });
    }

    function initObjectiveCompleteButton() {
        $('#objective-complete-btn').click(function () {
            var data = {
                objectiveId: getObjectiveId()
            };
            ajaxPost(urls.completeObjective(), data, function (response) {
                $('#objective-completed-message').html(response.message);

                if ($('#progress-updated').is(':visible')) {
                    $('#progress-updated').hide('slide');
                }

                $('#objective-completed').show('slide');

                $('#progress').slider('value', response.progress);
                $('#progress-value').html(response.progress);

                setTimeout(function () {
                    $('#objective-completed').hide('slide');
                }, messageExpirationMs);

            });
        });
    }

    function initFeeds() {
        $('#post-feed .btn').off('click').on('click', function () {
            var data = {
                objectiveId: getObjectiveId(),
                feedText: $('#NewFeed').val()
            };
            ajaxPost(urls.postFeed(), data, function (response) {

                $('#feeds').html(response);
                initFeeds();
            });
        });
    }

    function initKeyResults() {

        //------------------------------      ADD ACTION ITEM ----------------------------------------------------------------------
        $('.action-item-edit-container button').off('click').on('click', function () {
            var actionContainer = $(this).parents('.action-item-edit-container');
            var data = {
                objectiveId: getObjectiveId(),
                keyResultDetailsModel: {
                    Id: actionContainer.attr('id'),
                    NewActionItem: {
                        Name: actionContainer.find('#NewActionItem_Name').val(),
                        Owner: getPickedUser(actionContainer),
                        Users: JSON.parse($('#NewActionItem_UsersJson').val())
                    }
                }
            };

            ajaxPost(urls.addActionItem(), data, function (response) {
                actionContainer.parent().html(response);
                $('#' + actionContainer.attr('id')).collapse('show');
                initKeyResults();
            });

        });

        //------------------------------      DELETE ACTION ITEM ----------------------------------------------------------------------
        $('.action-item .close').off('click').on('click', function () {
            var actionItemId = $(this).parents('.action-item').find('#actionItem_Id').val();
            var actionContainer = $(this).parents('.accordion-inner').find('.action-item-edit-container');
            var keyResultId = actionContainer.attr('id');
            var objectiveId = getObjectiveId();

            var data = {
                objectiveId: objectiveId,
                keyResultId: keyResultId,
                actionItemId: actionItemId
            };

            ajaxPost(urls.deleteActionItem(), data, function (response) {
                actionContainer.parent().html(response);
                $('#' + actionContainer.attr('id')).collapse('show');
                initKeyResults();
            });
        });
    }

    function getObjectiveId() {
        return $('#Id').val();
    }

    function getPickedUser(container) {
        return new UserPicker({
            container: container,
            prefix : "NewActionItem"
        }).getPickedUser();
    }

    function ajaxPost(url, data, success) {
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            success: success
        });
    }
});



