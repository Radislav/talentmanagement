﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace TalentManagement.Infrastructure.Validation
{
    public class ValidationResult : IEnumerable<ValidationFailure>
    {
        private readonly ValidationFailure[] _failures = new ValidationFailure[0];

        public ValidationResult()
        {
            _failures = new ValidationFailure[0];
        }

        public ValidationResult(string errorMessage) 
        {
            _failures = new []
                            {
                                new ValidationFailure("",errorMessage)
                            };
        }

        public ValidationResult(IEnumerable<ValidationFailure> failures)
        {
            Contract.Requires(failures != null);

            _failures = failures.ToArray();
        }

        public ValidationResult(IEnumerable<ValidationResult> results) : this(results.SelectMany(r =>r.Failures))
        {
            Contract.Requires(results != null);
        }

        public ValidationFailure[] Failures
        {
            get { return _failures; }
        }

        public IEnumerable<string> ErrorMessages
        {
            get { return _failures.Select(f => f.ErrorMessage); }
        }

        public bool IsValid
        {
            get { return _failures == null || _failures.Length == 0; }
        }


        public IEnumerator<ValidationFailure> GetEnumerator()
        {
            return (_failures as IEnumerable<ValidationFailure>).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (_failures as IEnumerable).GetEnumerator();
        }
    }
}
