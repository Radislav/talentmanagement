﻿using MongoDB.Bson;

namespace TalentManagement.Domain.Companies
{
    public class Logo : Entity , IPicture
    {
        public Logo()
        {
        }

        public Logo(ObjectId id) :base(id)
        {
        }

        public const int SizeInPixels = 100;
        public byte[] Image { get; set; }
        public string ImageType { get; set; }
    }
}
