﻿using MongoDB.Bson;

namespace TalentManagement.Domain
{
    public interface IRepository<TEntity> : IRepositoryWithTypedId<TEntity,ObjectId>
               where TEntity : EntityWithTypedId<ObjectId>
    {

    }
}
