﻿using Autofac;
using Autofac.Builder;
using TalentManagement.Infrastructure.Converter;

namespace TalentManagement.Infrastructure
{
    public static class AutofacExtensions
    {
        internal static IRegistrationBuilder<EnumLocalizationConverter<TEnum>, ConcreteReflectionActivatorData, SingleRegistrationStyle>
           RegisterConverterForEnum<TEnum>(this ContainerBuilder builder)
           where TEnum : struct
        {
            return builder.RegisterType<EnumLocalizationConverter<TEnum>>()
              .As<ITypeConverter<TEnum, string>>()
              .Keyed<ITypeConverter>(typeof(TEnum));
        }
    }
}
