﻿using System.Linq;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.ObjectiveDetails.Map
{
    public class KeyResultModelMap : IMap<KeyResult,KeyResultModel>
    {
        private readonly IMapper<ActionItem, ActionItemModel> _actionItemMapper;
        public KeyResultModelMap(IMapper<ActionItem, ActionItemModel> actionItemMapper)
        {
            _actionItemMapper = actionItemMapper;
        }

        public void Configure(IMapperConfiguration<KeyResult, KeyResultModel> configuration)
        {
            configuration.ForMember(d => d.ActionItems,
                    s => s.ActionItems.Select(ai => _actionItemMapper.Map(ai)).ToArray());
            configuration.ForMember(d => d.Id, s => s.Id);
            configuration.ForMember(d => d.Name, s => s.Name);
            
        }
    }
}