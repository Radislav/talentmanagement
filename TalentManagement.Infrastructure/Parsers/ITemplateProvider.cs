﻿namespace TalentManagement.Infrastructure.Parsers
{
    public interface ITemplateProvider<TContext>
    {
        string TemplateContentType { get; }
        string FileName { get; }
        byte[] GetTemplateContents(object argument);

    }
}
