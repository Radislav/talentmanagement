﻿using System;
using System.Linq.Expressions;
using AutoMapper;

namespace TalentManagement.Infrastructure.Mapping
{
    /// <summary>
    /// Proxy for the Automapper IMappingExpression
    /// </summary>
    /// <typeparam name="TSource"></typeparam>
    /// <typeparam name="TDestination"></typeparam>
    class MapperConfiguration<TSource, TDestination> : IMapperConfiguration<TSource,TDestination>
    {
        private readonly IMappingExpression<TSource, TDestination> _mappingExpression;
        
        public MapperConfiguration() : this(Mapper.CreateMap<TSource, TDestination>())
        {
           
        }

        public MapperConfiguration(IMappingExpression<TSource,TDestination> mappingExpression)
        {
            _mappingExpression = mappingExpression;
        }

        public IMapperConfiguration<TSource, TDestination> ForMember(string property, Func<TSource, object> resolver)
        {
            return new MapperConfiguration<TSource, TDestination>(_mappingExpression.ForMember(property, cfg => cfg.ResolveUsing(resolver)));
        }

        public IMapperConfiguration<TSource, TDestination> ForMember(Expression<Func<TDestination, object>> destinationMember, Func<TSource, object> resolver)
        {
            return new MapperConfiguration<TSource, TDestination>(_mappingExpression.ForMember(destinationMember, cfg => cfg.ResolveUsing(resolver)));
        }

        public IMapperConfiguration<TSource, TDestination> Ignore(Expression<Func<TDestination, object>> destinationMember)
        {
            return new MapperConfiguration<TSource, TDestination>(_mappingExpression.ForMember(destinationMember, cfg => cfg.Ignore()));
        }

        public IMapperConfiguration<TSource, TDestination> MapFrom<TMember>(Expression<Func<TSource, TMember>> sourceMember, Expression<Func<TDestination, object>> destinationMember)
        {
            return new MapperConfiguration<TSource, TDestination>(_mappingExpression.ForMember(destinationMember, cfg => cfg.MapFrom(sourceMember)));
        }
        
        public IMapperConfiguration<TSource, TDestination> UseValue<TValue>(Expression<Func<TDestination, object>> destinationMember, TValue value)
        {
            return new MapperConfiguration<TSource, TDestination>(_mappingExpression.ForMember(destinationMember, cfg => cfg.UseValue(value)));
        }

        public IMapperConfiguration<TSource, TDestination> BeforeMap(Action<TSource, TDestination> beforeFunction)
        {
            return new MapperConfiguration<TSource, TDestination>(_mappingExpression.BeforeMap(beforeFunction));
        }


        public IMapperConfiguration<TSource, TDestination> AfterMap(Action<TSource, TDestination> afterFunction)
        {
            return new MapperConfiguration<TSource, TDestination>(_mappingExpression.AfterMap(afterFunction));
        }
    }
}
