﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace TalentManagement.Domain.Performance
{
    public interface IQuestionRepository : IRepository<Question>
    {
        void InsertQuestions(IEnumerable<Question> questions);
        IEnumerable<Question> GetByIds(IEnumerable<ObjectId> questionIds);
    }
}
