﻿using MongoDB.Bson;
using System.Linq;

namespace TalentManagement.Infrastructure.Converter
{
    class ObjectIdArrayConverter : ITypeConverter<ObjectId[],string[]>
    {
        private readonly ITypeConverter<ObjectId, string> _itemConverter;

        public ObjectIdArrayConverter(ITypeConverter<ObjectId, string> itemConverter)
        {
            _itemConverter = itemConverter;
        }

        public ObjectId[] Convert(string[] to)
        {
            return to.Select(t => _itemConverter.Convert(t))
                      .ToArray();
        }

        public string[] ConvertBack(ObjectId[] from)
        {
            return from.Select(f => _itemConverter.ConvertBack(f))
                       .ToArray();
        }

        public object Convert(object to)
        {
            return Convert((string[]) to);
        }

        public object ConvertBack(object from)
        {
            return ConvertBack((ObjectId[])from);
        }

        public bool IsDefault
        {
            get { return false; }
        }
    }
}
