﻿using Resource = TalentManagement.Resources.DomainResources;

namespace TalentManagement.Domain.Performance
{
    public enum QuestionSetting
    {
        [LocalizedDescription("None",typeof(Resource))]
        None,
       [LocalizedDescription("QuestionSetting_Required", typeof(Resource))]
        Required,
        [LocalizedDescription("QuestionSetting_Optional", typeof(Resource))]
        Optional
    }
}
