﻿using FluentValidation.Attributes;
using TalentManagement.Domain.Objectives;
using TalentManagement.Web.Models.Objectives.Validation;
using CreateStep1Resource = TalentManagement.Resources.Models.Objectives.CreateStep1;

namespace TalentManagement.Web.Models.Objectives
{
    [Validator(typeof(CreateStep1Validator))]
    public class CreateStep1 : WizardPage
    {
        [LocalizedDisplayName("Visibility", typeof(CreateStep1Resource))]
        public ObjectiveVisibility Visibility { get; set; }

        [RequiredUi]
        [LocalizedDisplayName("Due", typeof(CreateStep1Resource))]
        public string Due { get; set; }

        [RequiredUi]
        [LocalizedDisplayName("Name", typeof(CreateStep1Resource))]
        public string Name { get; set; }

        [RequiredUi]
        [LocalizedDisplayName("Description", typeof(CreateStep1Resource))]
        public string Description { get; set; }

        public override int NextStep
        {
            get { return 1; }
        }

        public override int ThisStep
        {
            get { return 0; }
        }
    }
}