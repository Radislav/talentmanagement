﻿using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain;

namespace TalentManagement.Infrastructure.Data.Filter
{
    internal class CompositeFilter<TModel> : FilterBase<TModel>
    {
        private readonly IFilter<TModel>[] _filters = new IFilter<TModel>[0];
        private readonly int _prority;
        private IFilterContext _filterContext;

        public CompositeFilter(IEnumerable<IFilter> filters)
        {
            if (filters != null)
            {
                _filters = filters.Cast<IFilter<TModel>>()
                                   .OrderByDescending(f => f.Priority).ToArray();

                _prority = _filters.Max(f => f.Priority);
            }
        }

        public override int Priority
        {
            get { return _prority; }
        }

        public override IFilter<TModel> Filter(IFilterContext[] contexts)
        {
            if (_filters != null && _filters.Any())
            {
                _filters.Map(filter =>
                                 {
                                     if (filter == _filters.First())
                                     {
                                         filter.Init(Data);
                                         filter.Filter(contexts);
                                     }
                                     else
                                     {
                                         _filters.First().Chain(filter, contexts);
                                     }
                                 });
            }

            return this;
        }
        
        public override IFilterContext FilterContext
        {
            get
            {
                return _filterContext ?? new CompositeContext(_filters.GetSafe(filters => filters.Select(f => f.FilterContext)));
            }
            set { _filterContext = value; }
        }
    }
}
    