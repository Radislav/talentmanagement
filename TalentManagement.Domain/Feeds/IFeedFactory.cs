﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using MongoDB.Bson;
using TalentManagement.Domain.Contracts;
using TalentManagement.Domain.Objectives;

namespace TalentManagement.Domain.Feeds
{
    [ContractClass(typeof(FeedFactoryContract))]
    public interface IFeedFactory
    {
        Feed CreateEmptyFeed(FeedType feedType, OwnedDescriptor ownedDescriptor);
        Feed CreateKudos(string content, ObjectId badgeId, IEnumerable<string> receivers, OwnedDescriptor ownedDescriptor);
        Feed CreateUpdatingFeed(string text, IScope updatedItemScope, OwnedDescriptor ownedDescriptor);
        Feed CreateObjectiveFeed(Objective oldObjective, Objective newObjective, OwnedDescriptor ownedDescriptor);
    }
}
