﻿using System.Web.Mvc;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Web.Infrastructure.Security
{
    public static  class WebPageExtensions
    {
        public static TenantPrincipal GetPrincipal(this WebViewPage page)
        {
            return DependencyResolver.Current.GetService<IPrincipalProvider>()
                .GetPrincipal();
        }
    }
}