﻿using System;

namespace TalentManagement.Domain
{
    public interface IOwned
    {
        string CreatedBy { get; set; }
        DateTime CreatedAt { get; set; }
        string UpdatedBy { get; set; }
        DateTime UpdatedAt { get; set; }
    }
}
