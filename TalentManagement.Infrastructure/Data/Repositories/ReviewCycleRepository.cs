﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using MongoDB.Bson;
using TalentManagement.Domain.Events;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data.Repositories
{
    internal class ReviewCycleRepository : Repository<ReviewCycle> , IReviewCycleRepository
    {
        private readonly Lazy<IDomainEvents> _events;

        public ReviewCycleRepository(IUnitOfWork unitOfWork, IPrincipalProvider principalProvider,Lazy<IDomainEvents> events)
            : base(unitOfWork,principalProvider)
        {
            _events = events;
        }

        public IEnumerable<ReviewCycle> GetActiveCycles()
        {
            return UnitOfWork.AsQueryable<ReviewCycle>().Where(c => c.Status != ReviewCycleStatus.Completed)
                                                        .ToArray();
        }

        public IEnumerable<ReviewCycle> GetCompletedCycles()
        {
            return UnitOfWork.AsQueryable<ReviewCycle>().Where(c => c.Status == ReviewCycleStatus.Completed)
                                                        .ToArray();
        }


        public IEnumerable<ReviewCycleDescriptor> GetDescriptors()
        {
            return
                UnitOfWork.AsQueryable<ReviewCycle>()
                          .Select(cycle => new ReviewCycleDescriptor(cycle.Id, cycle.Name,cycle.Progress))
                          .ToArray();
        }

        public override void Insert(ReviewCycle entity)
        {
            base.Insert(entity);
            Contract.Assume(false == entity.IsNew);
            _events.Value.Raise(new ReviewCycleInsertedArgs(entity));
        }

    }
}
