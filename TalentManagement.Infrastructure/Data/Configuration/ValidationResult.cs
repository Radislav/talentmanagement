﻿using System.Linq;

namespace TalentManagement.Infrastructure.Data.Configuration
{
    public  class ValidationResult
    {
        internal ValidationResult()
        {
            ErrorMessages = new string[0];   
        }

        public bool IsValid
        {
            get { return ErrorMessages == null || ErrorMessages.Any() == false; }
        }

        public string[] ErrorMessages { get; set; }
    }
}
