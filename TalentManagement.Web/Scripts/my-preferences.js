﻿$(function () {

    function showUpdateMessage() {
        var messageExpirationMs = 3000;
        //Show/hide update message
        $('#updated').show('slide');
        setTimeout(function () {
            $('#updated').hide('slide');
        }, messageExpirationMs);
    }
    
    //Update button click
    $('button').click(function () {
        var settings = [];
        $('.row .span7 section').each(function () {
            settings.push({
                IsEnabled: $(this).find('input:checkbox').is(':checked'),
                SettingId: $(this).find('#setting_SettingId').val()
            });
        });

        $.ajax({
            url: document.location.pathname,
            data: JSON.stringify(settings),
            success: function () {
                showUpdateMessage();
            }
        });

    });
});


