﻿namespace TalentManagement.Infrastructure.Data.Configuration
{
    public interface IRegistrar
    {
        void Add<TEntity>(IEntityConfiguration<TEntity> configuration);
        IEntityReadConfiguration Get<TEntity>();
        object GetDatabase(IEntityReadConfiguration entityConfiguration);
        object GetWriteRules(IEntityReadConfiguration entityConfiguration);
        void ValidateEntity<TEntity>(TEntity entity);
        void RegisterIgnoredProperties<TEntity>(IEntityConfiguration<TEntity> configuration);
    }
}
