﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using MongoDB.Bson;

namespace TalentManagement.Domain.Users
{
    [AggregateRoot]
    public class User : EntityWithTypedId<string> , IOwnedByCompany , ICloneable
    {
        private List<NotificationSetting> _notificationSettings;
        private List<UserNotification> _userNotifications = new List<UserNotification>();

        public User() : this(String.Empty,new NotificationSetting[0])
        {
        }

        public User(string userId)
            : this(userId, new NotificationSetting[0])
        {
        }

        public User(string userId, IEnumerable<NotificationSetting> notificationSettings):base(userId)
        {
            _notificationSettings = notificationSettings.ListOrDefault();
        }
       
       
        public string HashedPassword { get; set; }
        public IEnumerable<string> Roles
        {
            get; private set;
        }
        public string HashedSecureAnswer { get; set; }
        public string Email { get { return Id; } }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Position { get; set; }
        public string ManagerEmail { get; set; }
        public string Team { get; internal set; }
        public DateTime LastSignIn { get; set; }
        public UserStatus Status { get; set; }

        private string _tenant;
        public string Tenant
        {
            get
            {
                _tenant = String.IsNullOrEmpty(_tenant) ? Email.EmailDomainShort() : _tenant;
                return _tenant;
            }
            set { _tenant = value; }
        }

        public string Names { get { return String.Format("{0}, {1}", SecondName, FirstName); }
        }

        public string CompanyName
        {
            get { return Tenant; }
            set { Tenant = value; }
        }

        public bool IsManager { get; set; }
        
        public IEnumerable<NotificationSetting> NotificationSettings
        {
            get { return _notificationSettings; }
            set { _notificationSettings = value.ListOrDefault(); }
        }
        
        public NotificationSetting GetNotificationSetting(ObjectId settingId)
        {
            return _notificationSettings.GetSafe(n => n.SingleOrDefault(setting => setting.Id == settingId));
        }

        public IEnumerable<UserNotification> UserNotifications
        {
            get { return _userNotifications; }
            private set { _userNotifications = value.ListOrDefault(); }
        }

        public void AddUserNotification(UserNotification userNotification)
        {
            Contract.Requires(userNotification != null);

            userNotification.Id = userNotification.IsNew ? ObjectId.GenerateNewId() : userNotification.Id;
            _userNotifications.Add(userNotification);
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
