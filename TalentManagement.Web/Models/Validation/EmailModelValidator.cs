﻿using FluentValidation;
using TalentManagement.Infrastructure.Validation;

namespace TalentManagement.Web.Models.Validation
{
    public class EmailModelValidator : AbstractValidator<EmailModel> , TalentManagement.Infrastructure.Validation.IValidator<EmailModel>
    {
        public EmailModelValidator()
        {
            RuleFor(model => model.Email)
                .NotEmpty();

            RuleFor(model => model.Email)
                .EmailAddress()
                .When(m => false == string.IsNullOrEmpty(m.Email));
        }

        public  ValidationResult ValidateInstance(EmailModel instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}