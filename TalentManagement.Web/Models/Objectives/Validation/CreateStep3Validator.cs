﻿using System.Linq;
using FluentValidation;
using TalentManagement.Web.Models.Validation;
using TalentManagementValidation = TalentManagement.Infrastructure.Validation;

namespace TalentManagement.Web.Models.Objectives.Validation
{
    internal class CreateStep3Validator : AbstractValidator<CreateStep3>, TalentManagementValidation.IValidator<CreateStep3>
    {
        public CreateStep3Validator()
        {
            RuleFor(step3 => step3.Invitation)
                .NotEmpty()
                .When(step3 => step3.IsPageSelected || step3.WasPageSubmitted);

            RuleFor(m => m.UsersPicked)
                .Must(up => up.Any())
                .When(step3 => step3.IsPageSelected || step3.WasPageSubmitted)
                .WithLocalizedMessage(() => Resources.Errors.UserAtLeastOne);
        }

        public TalentManagementValidation.ValidationResult ValidateInstance(CreateStep3 instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}