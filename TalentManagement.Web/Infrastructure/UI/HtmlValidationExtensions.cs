﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace TalentManagement.Web.Infrastructure.UI
{
    public static class HtmlValidationExtensions
    {
        public static MvcHtmlString ValidationMessageLinkedFor<TModel, TProperty>(this HtmlHelper<TModel> helper,
            Expression<Func<TModel, TProperty>> property)
        {
            return helper.ValidationMessageFor(property);
        }
    }
}