﻿using System;
using System.Diagnostics.Contracts;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Contracts
{
    [ContractClassFor(typeof(IHashComputer))]
    internal abstract class HashComputerContract : IHashComputer
    {
        public string ComputeHash(string source)
        {
            Contract.Requires<ArgumentNullException>(source != null);
            Contract.Ensures(Contract.Result<string>() != null);

            return null;
        }
    }
}
