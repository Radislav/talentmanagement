﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Resources;

namespace TalentManagement.Web.Models.MyNotifications.Map
{
    internal class IndexModelMapper : IMapper<User,IndexModel>
    {
        private readonly string _todayString;
        private readonly string _yesterdayString;

        public IndexModelMapper(IResourceProvider resourceProvider)
        {
            Contract.Requires(resourceProvider != null);

            _todayString = resourceProvider.Today;
            _yesterdayString = resourceProvider.Yesterday;
        }

        internal struct NotificationDate
        {
            internal int Year { get; set; }
            internal int Month { get; set; }
            internal int Day { get; set; }

            internal string TodayString { get; set; }
            internal string YesterDayString { get; set; }

            internal bool IsToday
            {
                get { return Year == DateTime.Today.Year && Month == DateTime.Today.Month && DateTime.Today.Day == Day; }
            }

            internal bool IsYesterday
            {
                get
                {
                    DateTime yesterday = DateTime.Today.AddDays(-1);
                    return Year == yesterday.Year && Month == yesterday.Month && yesterday.Day == Day;
                }
            }

            internal DateTime AsDateTime()
            {
                return new DateTime(Year,Month,Day);
            }

            public override string ToString()
            {
                if (IsToday)
                {
                    return TodayString;
                }
                if (IsYesterday)
                {
                    return YesterDayString;
                }

                return AsDateTime().AsLondDateString();
            }
        }

        public IndexModel Map(User source)
        {
            Contract.Requires(source != null);
            IndexModel indexModel = new IndexModel();

            var groupingNotifications = source.UserNotifications.GroupBy(u => new NotificationDate
                                                                               {
                                                                                   Year = u.CreatedAt.Year, 
                                                                                   Month = u.CreatedAt.Month, 
                                                                                   Day = u.CreatedAt.Day,
                                                                                   TodayString = _todayString,
                                                                                   YesterDayString = _yesterdayString
                                                                               });

            List<NotificationGroupModel> notificationGroups = new List<NotificationGroupModel>();
            foreach (var groupingNotification in groupingNotifications)
            {
                if (groupingNotification.Any())
                {
                    notificationGroups.Add(new NotificationGroupModel
                                               {
                                                   CreatedAt = groupingNotification.Key.ToString(),
                                                   Texts = groupingNotification.Select(n => n.Text).ToArray()
                                               });
                }
            }

            indexModel.NotificationGroups = notificationGroups.ToArray();
            return indexModel;

        }
    }
}