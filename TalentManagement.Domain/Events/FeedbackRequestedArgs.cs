﻿using System.Diagnostics.Contracts;
using TalentManagement.Domain.Feedbacks;
using TalentManagement.Domain.Texts;

namespace TalentManagement.Domain.Events
{
    public class FeedbackRequestedArgs : NotificableEventArgs
    {
        public FeedbackRequestedArgs(Feedback feedback, string senderId, params string[] receiversIds)
            : base(TextType.NotificationFeedbackRequested, senderId,feedback, receiversIds)
        {
            Contract.Requires(feedback != null);
            Contract.Requires(senderId != null);
            Contract.Requires(receiversIds != null);

            Feedback = feedback;
        }
        
        public Feedback Feedback { get; private set; }
    }
}
