﻿using TalentManagement.Domain.Events;
using TalentManagement.Web.Infrastructure.Cache;

namespace TalentManagement.Web.Infrastructure.DomainEventHandlers
{
    internal class CompanyLogoUpdatedHandler : IHandle<CompanyLogoUpdatedArgs>
    {
        private readonly IOutputCacheCleaner _cacheCleaner;

        public CompanyLogoUpdatedHandler(IOutputCacheCleaner cacheCleaner)
        {
            _cacheCleaner = cacheCleaner;
        }

        public void Handle(CompanyLogoUpdatedArgs args)
        {
            _cacheCleaner.RemoveImageFromCache("GetCompanyLogo", "Image", args.CompanyId);
            _cacheCleaner.RemoveImageFromCache("GetCompanyLogoForTenant", "Image", args.CompanyTenant);
        }
    }
}