﻿namespace TalentManagement.Web.Infrastructure.UI
{
    public class UiDataType
    {
        public static UiDataType Text = new UiDataType("text");
        public static UiDataType Date = new UiDataType("date");
        public static UiDataType Decimal = new UiDataType("decimal");

        private readonly string _description;

        private UiDataType(string description)
        {
            _description = description;
        }
        public static implicit operator string(UiDataType uiDataType)
        {
            return uiDataType._description;
        }
        
        public override string ToString()
        {
            return _description;
        }
    }
}