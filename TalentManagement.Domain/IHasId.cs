﻿namespace TalentManagement.Domain
{
    public interface IHasId<TId> 
    {
        TId Id
        {
            get;
        }
    }
}
