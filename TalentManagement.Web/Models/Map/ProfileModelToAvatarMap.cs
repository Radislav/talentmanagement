﻿using System.Web.Helpers;
using System.Web.Mvc;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Configuration;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Map
{
    public class ProfileModelToAvatarMap : IMap<ProfileModel,Avatar>
    {
        public void Configure(IMapperConfiguration<ProfileModel, Avatar> configuration)
        {
            IAppConfiguration appConfiguration = DependencyResolver.Current.GetService<IAppConfiguration>();

            configuration.ForMember(avatar => avatar.Id,
                                    profile =>profile.Email);
                
            
            configuration.ForMember(avatar => avatar.Image, 
                                    profile => profile.ProfileImageBytes);

            configuration.ForMember(avatar => avatar.SmallImage,
                                   profile =>
                                                new WebImage(profile.ProfileImageBytes)
                                                        .Resize(appConfiguration.AvatarWidth,appConfiguration.AvatarHeight)
                                                        .GetBytes());

            configuration.ForMember(avatar => avatar.ImageType, profile => profile.ProfileImageType);
        }
    }
}