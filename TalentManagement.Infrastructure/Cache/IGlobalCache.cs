﻿using TalentManagement.Domain;
using TalentManagement.Domain.Texts;

namespace TalentManagement.Infrastructure.Cache
{
    public interface IGlobalCache : ICache
    {
        Text[] Texts { get; }
        int GetNotificationsCountFor(string userId);
        void SetNotificationsCountFor(string userId,int notificationsCount);
        void DeleteNotificationsCountFor(string userId);
        string GetUserNames(string userId);
        void DeleteUserNames(string userId);
    }
}
