﻿$(function () {

    function setProgress() {
        var progressValue = 0;
        var selectedCycle = $('#ActiveCycle').val();
        $($('#ActiveCycle').data('progresses')).each(function () {
            var descriptor = $(this)[0];
            if (descriptor.Key == selectedCycle) {
                progressValue = descriptor.Value;
                return progressValue;
            }
            return progressValue;
        });
        $('.progress-value').html(progressValue);
    }

    $('#ActiveCycle').change(function () {
        $.ajax({
            url: urls.selectCycle($(this).val()),
            type: 'GET',
            success: function (response) {
                $('#reviews-table').html(response);
                setProgress();
            }
        });
    });

    setProgress();


});