﻿using System;
using System.Diagnostics.Contracts;

namespace TalentManagement.Domain
{
    public struct OwnedDescriptor
    {
        private readonly string _createdBy;
        private readonly DateTime _createdAt;
        private readonly string _company;
        private readonly string _team;

        public OwnedDescriptor(string createdBy,DateTime createdAt,string team,string company)
        {
            Contract.Requires(false == String.IsNullOrEmpty(createdBy));
            Contract.Requires(false == String.IsNullOrEmpty(company));
            Contract.Requires(false == string.IsNullOrEmpty(team));
            Contract.Requires(createdAt <= DateTime.Now);

            _createdBy = createdBy;
            _createdAt = createdAt;
            _company = company;
            _team = team;
        }
        
        public string CreatedBy { get { return _createdBy; }
        }
        public DateTime CreatedAt
        {
            get { return _createdAt; }
        }
        public string Company
        {
            get { return _company; }
        }

        public string Team
        {
            get { return _team; }
        }
    }
}
