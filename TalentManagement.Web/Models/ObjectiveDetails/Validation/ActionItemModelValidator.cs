﻿using System.Linq;
using FluentValidation;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.ObjectiveDetails.Validation
{
    public class ActionItemModelValidator : AbstractValidator<ActionItemModel>,
                                             TalentManagement.Infrastructure.Validation.IValidator<ActionItemModel>
    {
        public ActionItemModelValidator()
        {
            RuleFor(m => m.Name)
                .NotEmpty();

            RuleFor(m => m.Owner)
                .NotEmpty()
                .Owner()
                .Must((model, owner) => model.Users.Any(u => u.Id == owner))
                .WithLocalizedMessage(() => Resources.Errors.ErrorUserShouldBeObjectiveContributor);

        }

        public TalentManagement.Infrastructure.Validation.ValidationResult ValidateInstance(ActionItemModel instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}