﻿namespace TalentManagement.Web.Models.Binder
{
    public class UserPickedBinder : CustomModelBinder<UserPicked>
    {
        private BinderValueProvider _valueProvider;
        public override object BindModel(System.Web.Mvc.ControllerContext controllerContext, System.Web.Mvc.ModelBindingContext bindingContext)
        {
            _valueProvider = new BinderValueProvider(bindingContext);

            return new UserPicked
                       {
                           Prefix = _valueProvider.GetValue<string>("Prefix",Prefix,Index),
                           FirstName = _valueProvider.GetValue<string>("FirstName",Prefix,Index),
                           Id = _valueProvider.GetValue<string>("Id", Prefix, Index),
                           SecondName = _valueProvider.GetValue<string>("SecondName", Prefix, Index)
                       };
        }
    }
}