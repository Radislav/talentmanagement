﻿using System;
using MongoDB.Bson;
using TalentManagement.Domain.Companies;
using TalentManagement.Domain.Feeds;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Web.Models.PerformanceSummary.Map
{
    internal class KudosModelMap : IMap<Tuple<Kudos, Badge>,KudosModel>
    {
        private readonly string _receiver;
        private readonly IUserRepository _userRepository;

        public KudosModelMap(IPrincipalProvider principalProvider,IUserRepository userRepository)
        {
            _receiver = principalProvider.GetUserName();
            _userRepository = userRepository;
        }

        public KudosModel Map(Feed kudosFeed,string badgeName)
        {
            return new KudosModel
                {
                    BadgeId = ObjectId.Parse(kudosFeed.GetPropertyValue("BadgeId")),
                    BadgeName = badgeName,
                    Receiver = _receiver,
                    Sender = kudosFeed.CreatedBy
                };
        }



        public void Configure(IMapperConfiguration<Tuple<Kudos, Badge>, KudosModel> configuration)
        {
            configuration.ForMember(d => d.BadgeId, s => s.Item2.Id);
            configuration.ForMember(d => d.BadgeName, s => s.Item2.Title);
            configuration.ForMember(d => d.Receiver, s => _receiver);
            configuration.ForMember(d => d.Sender, s => s.Item1.Feed.CreatedBy);
            configuration.ForMember(d => d.SenderNames, s => _userRepository.GetById(s.Item1.Feed.CreatedBy).Names);
            configuration.ForMember(d => d.ReceiverNames, s => _userRepository.GetById(_receiver).Names);
            configuration.ForMember(d => d.CreatedAt, s => s.Item1.Feed.CreatedAt.AsLondDateString());
            configuration.ForMember(d => d.Content, s => s.Item1.Content);
        }
    }
}