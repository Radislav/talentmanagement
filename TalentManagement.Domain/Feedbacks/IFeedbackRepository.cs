﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace TalentManagement.Domain.Feedbacks
{
    public interface IFeedbackRepository : IRepository<Feedback>
    {
        IEnumerable<Feedback> GetFilteredFeedBacks(IFilter filter);
        IEnumerable<Feedback> GetRequiredAttentionFeedbacks();
        IEnumerable<Feedback> GetTheirRequestedFeedbacks();
        Feedback GetByResponseId(ObjectId responseId);
    }
}
