﻿using CsvHelper.Configuration;

namespace TalentManagement.Infrastructure.Parsers
{
    public class GroupRowParsedEventArgs : RowParsedEventArgs
    {
        public GroupRowParsedEventArgs(int row) : base(row)
        {
        }
       
        public string Email { get; set; }

        [CsvField(Name = "First Name")]
        public string FirstName { get; set; }

        [CsvField(Name = "Second Name")]
        public string SecondName { get; set; }
        public string Password { get; set; }
        public string Position { get; set; }
        public string Team { get; set; }

        [CsvField(Name = "Reporting manager's e-mail")]
        public string ReportingManagerEmailLocal { get; set; }

        [CsvField(Name = "@")]
        public string ReportingManagerEmailAppendix { get; set; }
    }
}
