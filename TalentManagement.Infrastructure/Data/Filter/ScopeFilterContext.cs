﻿using TalentManagement.Domain;

namespace TalentManagement.Infrastructure.Data.Filter
{
    public class ScopeFilterContext : IFilterContext
    {
        public ScopeFilterContext()
        {
        }

        public ScopeFilterContext(IScope scope)
        {
            ScopeId = scope.ScopeId;
            ScopeName = scope.ScopeName;
        }

        public string ScopeName { get; set; }
        public string ScopeId { get; set; }
    }
}
