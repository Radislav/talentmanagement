﻿using System.Diagnostics.Contracts;
using TalentManagement.Domain.Performance;

namespace TalentManagement.Domain.Events
{
    public class ReviewCycleInsertedArgs : IDomainEventArgs
    {
        public ReviewCycleInsertedArgs(ReviewCycle reviewCycle)
        {
            Contract.Requires(reviewCycle != null);
            Contract.Requires(false == reviewCycle.IsNew);

            ReviewCycle = reviewCycle;
        }

        public ReviewCycle ReviewCycle { get; private set; }
    }
}
