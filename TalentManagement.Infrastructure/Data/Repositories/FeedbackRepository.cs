﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using TalentManagement.Domain;
using TalentManagement.Domain.Events;
using TalentManagement.Domain.Feedbacks;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data.Repositories
{
    class FeedbackRepository : Repository<Feedback> , IFeedbackRepository
    {
        private readonly IFilter<IHasParticipants> _participantsFilter;
        private readonly IPrincipalProvider _principalProvider;
        private readonly Lazy<IDomainEvents> _domainEvents;
   
        public FeedbackRepository(IUnitOfWork unitOfWork,
            IFilter<IHasParticipants> participantsFilters,
            IPrincipalProvider principalProvider,
            Lazy<IDomainEvents> domainEvents)
            : base(unitOfWork,principalProvider)
        {
            _participantsFilter = participantsFilters;
            _principalProvider = principalProvider;
            _domainEvents = domainEvents;
        }

        public IEnumerable<Feedback> GetFilteredFeedBacks(IFilter filter)
        {
            return UnitOfWork.ApplyFilter<Feedback>(filter)
                             .ToArray();
        }

        public IEnumerable<Feedback> GetRequiredAttentionFeedbacks()
        {
            return GetFilteredFeedBacks(_participantsFilter)
                       .Where(f => null == f.GetResponseFor(_principalProvider.GetUserName()));
        }

        public IEnumerable<Feedback> GetTheirRequestedFeedbacks()
        {
            return GetFilteredFeedBacks(_participantsFilter)
                      .Where(f => null != f.GetResponseFor(_principalProvider.GetUserName()));
        }

        public Feedback GetByResponseId(ObjectId responseId)
        {
            return UnitOfWork.AsQueryable<Feedback>().SingleOrDefault(f => f.Responses != null && f.Responses.Any(r => r.Id == responseId));
        }

        public override void Update(Feedback entity)
        {
            base.Update(entity);
            RaiseFeedbackCommentedEvent(entity);
        }

        private void RaiseFeedbackCommentedEvent(Feedback entity)
        {
            entity.NewComments.Map(comment => _domainEvents.Value.Raise(new FeedbackCommentedArgs(entity, comment.CreatedBy, entity.Responders)));
        }

        public override void Insert(Feedback entity)
        {
            base.Insert(entity);
            _domainEvents.Value.Raise(new FeedbackRequestedArgs(entity, _principalProvider.GetUserName(), entity.Responders));
        }
    }
}
