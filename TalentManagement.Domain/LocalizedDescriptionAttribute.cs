﻿using System;
using System.Diagnostics.Contracts;
using System.Resources;

namespace TalentManagement.Domain
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Struct | AttributeTargets.Class,AllowMultiple = false,Inherited = true)]
    public class LocalizedDescriptionAttribute : Attribute
    {
         private readonly string _description;

         public LocalizedDescriptionAttribute(string resorceKey, Type resourceType)
        {
             Contract.Requires<ArgumentNullException>(resorceKey != null);
             Contract.Requires<ArgumentNullException>(resourceType != null);

            _description = new ResourceManager(resourceType).GetString(resorceKey);
        }

        public  string Description
        {
            get { return _description; }
        }
    }
}
