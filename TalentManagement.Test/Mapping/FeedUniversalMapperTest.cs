﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TalentManagement.Domain;
using TalentManagement.Domain.Feeds;
using TalentManagement.Web.Models.Feeds;
using TalentManagement.Web.Models.Feeds.Map;

namespace TalentManagement.Test.Mapping
{
    [TestClass]
    public class FeedUniversalMapperTest : MockedTestClass<IFeedUniversalMapper>
    {
        [TestMethod]
        public void FeedUniversalMapper_should_map_UdpatingFeeds_to_appropriate_model()
        {
            IFeedUniversalMapper target = CreateTarget();
            FeedModel result = target.Map(new Feed {FeedType = FeedType.Update, Scope = new Scope("Name", "Id")});

            Assert.IsInstanceOfType(result,typeof(UpdateFeedModel));
        }
    }
}
