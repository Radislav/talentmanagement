﻿using System.Configuration;

namespace TalentManagement.Web.Infrastructure.Bundles
{
    public class ContentElement : ConfigurationElement
    {
        [ConfigurationProperty("url",IsRequired = true,IsKey = true)]
        public string Url
        {
            get { return base["url"] as string; }
            set { base["url"] = value; }
        }
    }
}