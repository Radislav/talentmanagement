﻿using MongoDB.Bson;
using TalentManagement.Domain.Performance;

namespace TalentManagement.Web.Models.MyReviews
{
    public class ReviewModel
    {
        public string ReviewAbout { get; set; }
        public string DueDate { get; set; }
        public string CycleOwner { get; set; }
        public string Action { get; set; }
        public ObjectId Id { get; set; }
        public QuestionSetType QuestionSetType { get; set; }
        public ObjectId CycleId { get; set; }
    }
}