﻿using System;
using System.Linq;
using TalentManagement.Domain;
using TalentManagement.Domain.Feedbacks;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Web.Models.MyFeedbacks.Map
{
    internal class FeedbackTheyRequestedMap : IMap<Feedback,FeedbackTheyRequested>
    {
        private readonly IResourceProvider _resourceProvider;
        private readonly IUserRepository _userRepository;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IMapper<Comment, CommentModel> _commentModelMapper;

        public FeedbackTheyRequestedMap(IResourceProvider resourceProvider, IUserRepository userRepository, IPrincipalProvider principalProvider, IMapper<Comment, CommentModel> commentModelMapper)
        {
            _resourceProvider = resourceProvider;
            _userRepository = userRepository;
            _principalProvider = principalProvider;
            _commentModelMapper = commentModelMapper;
        }

        public void Configure(IMapperConfiguration<Feedback, FeedbackTheyRequested> configuration)
        {
            configuration.ForMember(d => d.Question, s => String.Format(_resourceProvider.FeedbackTheyRequestedTitle, _userRepository.GetById(s.CreatedBy).Names, s.Question));
            configuration.ForMember(d => d.QuestionAuthorId, s => s.CreatedBy);
            configuration.ForMember(d => d.QuestionDate, s => s.CreatedAt.AsLondDateString());
            configuration.ForMember(d => d.QuestionAuthorName, s => _userRepository.GetById(s.CreatedBy).Names);
            configuration.ForMember(d => d.FeedbackId, s => s.Id);
        
            configuration.AfterMap((s, d) =>
                                       {
                                           Response response = s.GetResponseFor(_principalProvider.GetUserName());
                                           if (response != null)
                                           {
                                               d.Response = String.Format(_resourceProvider.FeedbackYouProvidedFeedback, response.Answer);
                                               d.ResponseDate = response.CreatedAt.AsLondDateString();
                                               d.ResponseAuthor = _principalProvider.GetUserName();
                                               d.CommentSectionModel = new CommentSectionModel
                                                                           {
                                                                               HolderId = Convert.ToString(s.Id),
                                                                               Comments = response.Comments == null ? new CommentModel[0] :
                                                                                           response.Comments.Select(c => _commentModelMapper.Map(c))
                                                                                                            .ToArray()
                                                                           };
                                           }
                                       });

            



        }
     
    }
}