﻿namespace TalentManagement.Web.Models
{
    public abstract class WizardPage
    {
        public bool IsReadyToGo { get; set; }
        public int CurrentStep { get; set; }

        public abstract int NextStep { get; }
        public abstract int ThisStep { get; }

        public bool IsPageSelected
        {
            get { return ThisStep == CurrentStep; }
        }

        public bool WasPageSubmitted
        {
            get { return ThisStep < CurrentStep; }
        }

        public bool MustBeValidated
        {
            get { return IsPageSelected || WasPageSubmitted; }
        }
    }
}