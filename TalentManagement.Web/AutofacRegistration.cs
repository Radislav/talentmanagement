﻿using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using TalentManagement.AppServices;
using TalentManagement.Infrastructure;

namespace TalentManagement.Web
{
    public static class AutofacRegistration
    {
        public static IContainer RegisterAutofac()
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterModule<AutofacWebTypesModule>();
            builder.RegisterModule<InfrastructureRegistrationModule>();
            builder.RegisterModule<AppServiceRegistrationModule>();
            builder.RegisterModule<WebRegistrationModule>();

            IContainer container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            return container;
        }
    }
}