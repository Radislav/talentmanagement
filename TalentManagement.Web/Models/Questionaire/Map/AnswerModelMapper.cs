﻿using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Questionaire.Map
{
    public class AnswerModelMapper : IMapper<AnswersCollection,IEnumerable<AnswerModel>>
    {
        public IEnumerable<AnswerModel> Map(AnswersCollection source)
        {
            return source.Questions.Select(question =>
                {
                    Answer answer = source.Answers.SingleOrDefault(a => a.QuestionId == question.Id);
                    AnswerModel result = new AnswerModel
                        {
                            QuestionId = question.Id,
                            QuestionText = question.Text
                        };

                    if (answer != null)
                    {
                        result.Content = answer.Content;
                        result.Id = answer.Id;
                    }

                    return result;
                });
        }
    }
}