﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using TalentManagement.Domain.Feedbacks;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Resources;

namespace TalentManagement.Web.Models.MyFeedbacks.Map
{
    internal class IndexModelMapper : IIndexModelMapper
    {
        private readonly IResourceProvider _resourceProvider;
        private readonly IUsersPickedProvider _usersPickedProvider;
        private readonly IMapper<Feedback, FeedbackAttentionRequired> _feedbackAttentionRequiredMapper;
        private readonly IMapper<Feedback, FeedbackYouRequested> _feedbackYouRequestedMapper;
        private readonly IMapper<Feedback, FeedbackTheyRequested> _feedbackTheyRequestedMapper;
        private readonly IMapper<Feedback, DetailsModel> _detailsMapper;
        
        public IndexModelMapper(  IResourceProvider resourceProvider,
                IUsersPickedProvider usersPickedProvider,
                IMapper<Feedback, FeedbackAttentionRequired> feedbackAttentionRequiredMapper,
                IMapper<Feedback, FeedbackYouRequested> feedbackYouRequestedMapper,
                IMapper<Feedback, FeedbackTheyRequested> feedbackTheyRequestedMapper,
             IMapper<Feedback, DetailsModel> detailsMapper)
        {
            _feedbackAttentionRequiredMapper = feedbackAttentionRequiredMapper;
            _feedbackTheyRequestedMapper = feedbackTheyRequestedMapper;
            _feedbackYouRequestedMapper = feedbackYouRequestedMapper;
            _usersPickedProvider = usersPickedProvider;
            _resourceProvider = resourceProvider;
            _detailsMapper = detailsMapper;
        }

        public IndexModel Map(string activeButton,Feedback[] currentFeedbacks,ObjectId[] badgeIds)
        {
            List<UserPicked> usersToPick = _usersPickedProvider.GetUsersToPick().ToList();

            IndexModel model = new IndexModel
            {
                ActiveButton = activeButton,
                FeedbackCreateModel = new FeedbackCreateModel { Users = usersToPick },
                KudosCreateModel = new KudosCreateModel { Users = usersToPick,BadgeIds = badgeIds}
            };

            if (activeButton == _resourceProvider.FeedbackAttentionRequired)
            {
                model.FeedbacksAttentionRequired = MapFeedbacks(currentFeedbacks,_feedbackAttentionRequiredMapper);

            }
            if (activeButton == _resourceProvider.FeedbackYouRequested)
            {
                model.FeedbacksYouRequested = MapFeedbacks(currentFeedbacks,
                                                           _feedbackYouRequestedMapper);
            }
            if (activeButton == _resourceProvider.FeedbackTheyRequested)
            {
                model.FeedbacksTheyRequested = MapFeedbacks(currentFeedbacks,_feedbackTheyRequestedMapper);
            }

            if (activeButton == _resourceProvider.FeedbackDetails)
            {
                model.Details = _detailsMapper.Map(currentFeedbacks.First());
            }

            return model;
        }

        private TViewModel[] MapFeedbacks<TViewModel>(IEnumerable<Feedback> feedbacks, IMapper<Feedback, TViewModel> mapper)
        {
            return feedbacks.Select(mapper.Map).ToArray();
        }
    }
}