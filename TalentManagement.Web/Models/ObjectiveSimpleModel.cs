﻿using MongoDB.Bson;

namespace TalentManagement.Web.Models
{
    public class ObjectiveSimpleModel
    {
        public string Name { get; set; }
        public string Due { get; set; }
        public ObjectId IconId { get; set; }
        public ObjectId Id { get; set; }
        public bool ShowAddToFocus { get; set; }
    }
}