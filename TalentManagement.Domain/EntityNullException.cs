﻿namespace TalentManagement.Domain
{
    public class EntityNullException : EntityException
    {
        public EntityNullException()
        {

        }

        public EntityNullException(string message)
            : base(message)
        {
          
        }
    }
}
