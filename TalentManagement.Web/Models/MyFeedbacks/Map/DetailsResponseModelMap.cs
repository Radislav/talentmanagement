﻿using System;
using System.Linq;
using TalentManagement.Domain;
using TalentManagement.Domain.Feedbacks;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.MyFeedbacks.Map
{
    internal class DetailsResponseModelMap : IMap<Response,DetailsResponseModel>
    {
        private readonly IMapper<Comment, CommentModel> _commentModelMapper;

        public DetailsResponseModelMap(IMapper<Comment,CommentModel> commentModelMapper)
        {
            _commentModelMapper = commentModelMapper;
        }

        public void Configure(IMapperConfiguration<Response, DetailsResponseModel> configuration)
        {
            configuration.ForMember(d => d.ResponseId, s => s.Id);
            configuration.ForMember(d => d.CommentSectionModel, s => new CommentSectionModel
                                                                         {
                                                                             HolderId = Convert.ToString(s.Id),
                                                                             Comments =s.Comments.Select(c => _commentModelMapper.Map(c)).ToArray()
                                                                         });
            configuration.ForMember(d => d.CreatedAt, s => s.CreatedAt.AsLondDateString());
            configuration.ForMember(d => d.Response, s => s.Answer);
        }
    }
}