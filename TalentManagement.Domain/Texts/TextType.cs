﻿using System;

namespace TalentManagement.Domain.Texts
{
    [Flags]
    public enum TextType
    {
        None,                         
        Setting,                      
        NotificationFeedbackRequested,
        NotificationFeedbackCommented,
        NotificationObjectiveCreated,
        NotificationObjectiveUpdated
    }
}
