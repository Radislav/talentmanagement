﻿using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Web.Models.PerformanceSummary;


namespace TalentManagement.Web.Controllers
{
    public class PerformanceSummaryController : Controller
    {
        private readonly IMapper<ReviewCycleSummary, IndexModel> _indexModelMapper;
        private readonly IPerformanceObjectsFactory _reviewCycleService;

        public PerformanceSummaryController(IMapper<ReviewCycleSummary, IndexModel> indexModelMapper,
                                            IPerformanceObjectsFactory reviewCycleService)
        {
            _indexModelMapper = indexModelMapper;
            _reviewCycleService = reviewCycleService;
        }

        [HttpGet]
        public ActionResult Index(ObjectId cycleId,string respondent)
        {
            return View(_indexModelMapper.Map(_reviewCycleService.CreateSummary(cycleId,respondent)));
          
        }

    }
}
