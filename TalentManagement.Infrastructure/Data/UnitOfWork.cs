﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using TalentManagement.Domain;
using TalentManagement.Infrastructure.Data.Configuration;

namespace TalentManagement.Infrastructure.Data
{
    class UnitOfWork : IUnitOfWork
    {
        private readonly IRegistrar _registrar;
        private WriteConcern _writeConcern = new WriteConcern();
        private MongoDatabase _db;
        
        public UnitOfWork(IRegistrar registrar)
        {
            _registrar = registrar;
        }

        protected MongoCollection GetCollection<TEntity>()
        {
            IEntityReadConfiguration entityConfiguration = _registrar.Get<TEntity>();
            if (entityConfiguration == null)
            {
                throw new EntityException(String.Format("Type {0} is not registered in Registrar.",typeof(TEntity)));
            }

            _db = (MongoDatabase)_registrar.GetDatabase(entityConfiguration);
            _writeConcern = (WriteConcern)_registrar.GetWriteRules(entityConfiguration);
            
            return _db.GetCollection<TEntity>(entityConfiguration.CollectionName);
        }
        
        public void Insert<TEntity>(TEntity entity)
             where TEntity : class
        {
            _registrar.ValidateEntity(entity);

            GetCollection<TEntity>()
                .Insert(entity,_writeConcern);
        }
        
        public void DeleteById<TEntity,TId>(TId id)
             where TEntity : class
        {
            var query = new QueryDocument("_id", BsonValue.Create(id));
            GetCollection<TEntity>().Remove(query, _writeConcern);
        }

        public void Update<TEntity>(TEntity entity)
             where TEntity : class
        {
            _registrar.ValidateEntity(entity);
            GetCollection<TEntity>().Save(entity, _writeConcern);
        }

        public TEntity GetById<TEntity, TId>(TId id)
             where TEntity : class
        {
            return GetCollection<TEntity>().FindOneByIdAs<TEntity>(BsonValue.Create(id));
        }
        
        public IQueryable<TEntity> AsQueryable<TEntity>() where TEntity : class
        {
            return GetCollection<TEntity>().AsQueryable<TEntity>();
        }

        public void InsertBatch<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            var enumerable = entities as TEntity[] ?? entities.ToArray();
            enumerable.Map(entity => _registrar.ValidateEntity(entity));

            GetCollection<TEntity>().InsertBatch(enumerable, _writeConcern);
        }


        public IQueryable<TEntity> Find<TEntity>(object command) where TEntity : class
        {
            IMongoQuery query = command as IMongoQuery;
            if (query == null)
            {
                throw new NotSupportedException("IMongoQuery only supported.");
            }
            return GetCollection<TEntity>().FindAs<TEntity>(query).AsQueryable();
        }
    }
}
