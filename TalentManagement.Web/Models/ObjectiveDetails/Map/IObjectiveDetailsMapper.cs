﻿using System.Collections.Generic;
using TalentManagement.Domain.Feeds;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.ObjectiveDetails.Map
{
    public interface IObjectiveDetailsMapper : IMapper<Objective,ObjectiveDetailsModel>
    {
        ObjectiveDetailsModel Map(Objective objective, IEnumerable<Feed> feeds);
    }
}
