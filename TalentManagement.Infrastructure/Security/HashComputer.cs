﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace TalentManagement.Infrastructure.Security
{
    internal class HashComputer : IHashComputer
    {
        public string ComputeHash(string source)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            Byte[] originalBytes = Encoding.Default.GetBytes(source);
            Byte[] encodedBytes = md5.ComputeHash(originalBytes);

            return BitConverter.ToString(encodedBytes);
        }
    }
}
