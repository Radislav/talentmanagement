﻿using System.IO;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using TalentManagement.Infrastructure.Configuration;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Serialization;
using TalentManagement.Infrastructure.Session;

namespace TalentManagement.Infrastructure.Email
{
    class EmailSender : IEmailSender
    {
        private readonly ISessionFactory _sessionFactory;

        public EmailSender(ISessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;
        }
        
        public void Send<TEmailArgument>(string to, EmailType emailType, TEmailArgument argument)
        {
            using(var session = _sessionFactory.Create())
            {
                EmailSettings emailSettings = session.Instance<IAppConfiguration>().EmailSettings;
                IResourceProvider resources = session.Instance<IResourceProvider>();
                IXmlSerializer<TEmailArgument> serializer = session.Instance<IXmlSerializer<TEmailArgument>>();

                MailMessage mailMessage = new MailMessage(emailSettings.From,to)
                                              {
                                                  Body = CreateEmailBody(emailType, argument, resources, serializer),
                                                  IsBodyHtml = true,
                                                  Subject = resources.GetEmailSubject(emailType),
                                              };

                SmtpClient smtpClient = new SmtpClient(emailSettings.Host, emailSettings.Port);
                smtpClient.Send(mailMessage);
            }
        }

        public virtual string CreateEmailBody<TEmailArgument>(EmailType emailType, TEmailArgument argument, IResourceProvider resources, IXmlSerializer<TEmailArgument> serializer)
        {
            string emailBody = resources.GetEmailBody(emailType);

            if (typeof(TEmailArgument) == typeof(EmptyArgument))
            {
                return emailBody;
            }

            XmlDocument template = new XmlDocument();
            template.LoadXml(emailBody);

            XslCompiledTransform xslTransform = new XslCompiledTransform();
            xslTransform.Load(template);

            StringBuilder buffer = new StringBuilder();
            XmlWriter writer = new XmlTextWriter(new StringWriter(buffer));
            xslTransform.Transform(serializer.Serialize(argument).CreateReader(), writer);

            return buffer.ToString();
        }

        public void Send(string to, EmailType emailType)
        {
            Send(to, emailType, EmptyArgument.Value);
        }
    }
}
