﻿using MongoDB.Bson;
using TalentManagement.Domain.Objectives;

namespace TalentManagement.Web.Models.MyActionItems.Map
{
    public interface IActionItemModelMapper
    {
        ActionItemModel Map(ObjectId objectiveId, ActionItem actionItem);
    }
}
