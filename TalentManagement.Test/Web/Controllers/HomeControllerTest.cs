﻿using System.Linq;
using System.Web.Mvc;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TalentManagement.AppServices;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Infrastructure.Validation;
using TalentManagement.Web.Controllers;
using TalentManagement.Web.Models;

namespace TalentManagement.Test.Web.Controllers
{
    [TestClass]
    public class HomeControllerTest : MockedTestClass<LoginController>
    {
        private const string ValidEmail = "valid_email@email.com";
        private const string NonExistingEmail = "valid_but_not_existing_email@email.com";
        private const string InvalidEmail = "";
        private const string ValidPassword = "password";

        protected override void RegisterMocks(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterMock(MockResourceProvider());
            containerBuilder.RegisterMock(MockLoginService());
            containerBuilder.RegisterMock(MockAuthenticationService());
            containerBuilder.RegisterMock(MockBasicValidator());
            
            base.RegisterMocks(containerBuilder);
        }

        private static Mock<IAuthenticationService> MockAuthenticationService()
        {
            Mock<IAuthenticationService> authenticationService = new Mock<IAuthenticationService>();
            authenticationService.Setup(m => m.Login(ValidEmail, ValidPassword))
            .Verifiable();
            return authenticationService;
        }

        private static Mock<IRegistrationFlow> MockLoginService()
        {
            Mock<IRegistrationFlow> loginService = new Mock<IRegistrationFlow>();
            loginService.Setup(m => m.ForgotPasswordFor(null))
                .Returns(ForgotPasswordResult.UserNotFounded);
            loginService.Setup(m => m.ForgotPasswordFor(ValidEmail))
                     .Returns(ForgotPasswordResult.Ok);
            return loginService;
        }

        private static Mock<IResourceProvider> MockResourceProvider()
        {
            Mock<IResourceProvider> resourceProvider = new Mock<IResourceProvider>();
            resourceProvider.Setup(r => r.ErrorEmailNotFounded).Returns("Error: email not founded");
            resourceProvider.Setup(r => r.ErrorEmailInvalid).Returns("Error: email invalid");
            return resourceProvider;
        }
        
        private static Mock<IBasicValidator> MockBasicValidator()
        {
            Mock<IBasicValidator> basicValidator = new Mock<IBasicValidator>();
            basicValidator.Setup(m => m.ValidateEmail(ValidEmail)).Returns(new ValidationResult());
            basicValidator.Setup(m => m.ValidateEmail(NonExistingEmail)).Returns(new ValidationResult());
            basicValidator.Setup(m => m.ValidateEmail(InvalidEmail)).Returns(
                new ValidationResult(new []{new ValidationFailure(null,"Error: email invalid")}));

            return basicValidator;
        }


        protected override LoginController CreateTarget(IContainer container)
        {
            return new LoginController(
                Container.Resolve<IResourceProvider>(),
                Container.Resolve<IRegistrationFlow>(),
                Container.Resolve<IAuthenticationService>()
                );
        }
     
        [TestMethod]
        public void ForgotPassword_should_return_ViewResult_if_invalid_email_provided()
        {
            LoginController target = CreateTarget();
            ViewResult result = (ViewResult)target.ForgotPassword(new EmailModel { Email = InvalidEmail });
        }

        [TestMethod]
        public void ForgotPassword_should_return_EmailModel_if_invalid_email_provided()
        {
            LoginController target = CreateTarget();
            EmailModel result = ((ViewResult)target.ForgotPassword(new EmailModel { Email = InvalidEmail })).Model as EmailModel;

            Assert.AreEqual(InvalidEmail, result.Email);
        }

        [TestMethod]
        public void ForgotPassword_should_return_ErrorEmailNotFounded_error_if_non_existing_email_provided()
        {
            LoginController target = CreateTarget();
            target.ForgotPassword(new EmailModel { Email = InvalidEmail });

            IResourceProvider resourceProvider = Container.Resolve<IResourceProvider>();

            Assert.AreEqual(1, target.ModelState.Count);
            bool hasErrorMessage =
                target.ModelState.SelectMany(m => m.Value.Errors).Any(
                    me => me.ErrorMessage == resourceProvider.ErrorEmailNotFounded);
                                                    
            Assert.IsTrue(hasErrorMessage);
        }

        [TestMethod]
        public void ForgotPassword_should_return_error_if_ModelStateInvalid()
        {
            LoginController target = CreateTarget();
            target.ModelState.AddModelError("Email", "Error");
            target.ForgotPassword(new EmailModel { Email = InvalidEmail });
            
            Assert.AreEqual(1, target.ModelState.Count);
            bool hasErrorMessage =
                target.ModelState.SelectMany(m => m.Value.Errors).Any(me => me.ErrorMessage == "Error");

            Assert.IsTrue(hasErrorMessage);
        }

        [TestMethod]
        public void ForgotPassword_should_not_call_RegistrationFlow_if_Model_invalid()
        {
            LoginController target = CreateTarget();
            target.ModelState.AddModelError("Email", "Error");
            target.ForgotPassword(new EmailModel { Email = InvalidEmail });
            

            Container.Resolve<Mock<IRegistrationFlow>>()
                .Verify(m => m.ForgotPasswordFor(InvalidEmail), Times.Never());
        }
        
        [TestMethod]
        public void Login_should_call_AuthenticationService()
        {
            LoginController target = CreateTarget();
            target.Login(new LoginModel
                             {
                                 Email = ValidEmail,
                                 Password = ValidPassword
                             });
            Container.Resolve<Mock<IAuthenticationService>>()
                .Verify(s => s.Login(ValidEmail,ValidPassword),Times.Once());
        }

        [TestMethod]
        public void Login_should_return_IndexView_with_model_if_invalid_email()
        {
            LoginController target = CreateTarget();
            LoginModel model = new LoginModel();
            ViewResult result = (ViewResult)target.Login(model);

            Assert.AreEqual("Index", result.ViewName);
            Assert.AreEqual(model,result.Model);
        }
    }
}
