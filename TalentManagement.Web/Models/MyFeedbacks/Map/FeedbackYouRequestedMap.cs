﻿using System;
using System.Linq;
using TalentManagement.Domain.Feedbacks;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Resources;

namespace TalentManagement.Web.Models.MyFeedbacks.Map
{
    internal class FeedbackYouRequestedMap : IMap<Feedback,FeedbackYouRequested>
    {
        private readonly IResourceProvider _resourceProvider;
        private readonly IUserRepository _userRepository;

        public FeedbackYouRequestedMap(IResourceProvider resourceProvider, IUserRepository userRepository)
        {
            _resourceProvider = resourceProvider;
            _userRepository = userRepository;
        }

        public void Configure(IMapperConfiguration<Feedback, FeedbackYouRequested> configuration)
        {
            configuration.ForMember(d => d.FeedbackId, s => s.Id);
            configuration.ForMember(d => d.CreationDate, s => s.CreatedAt.AsLondDateString());
            configuration.ForMember(d => d.ResponsesText, 
                s => String.Format(_resourceProvider.FeedbackYourRequestedResponse,
                                    s.GetSafe(fb => fb.Responses)
                                    .GetSafe(responses => responses.Count())));

            configuration.ForMember(d => d.Title, 
                s =>{
                       string participants = ExtractParticipants(s);
                        return String.Format(_resourceProvider.FeedbackYourRequestedTitle,
                                             participants, 
                                             s.Question);
                    });

        }

        private string ExtractParticipants(Feedback s)
        {
            return String.Join(",", 
                s.GetSafe(fb => fb.Participants)
                  .Select(participant =>
                        {
                            User user = _userRepository.GetById(participant);
                            return string.Format("{0} {1}", user.SecondName ?? String.Empty,
                                                    user.FirstName ?? String.Empty);
                        }));
        }
    }
}