﻿using FluentValidation.Attributes;
using System.Web.Mvc;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Web.Models.People.Validation;
using Resource = TalentManagement.Resources.Models.People.SendInvitationModel;

namespace TalentManagement.Web.Models.People
{
    [Validator(typeof(SendInvitationModelValidator))]
    public class SendInvitationModel
    {
        private string _emailAppendix;

        public SendInvitationModel()
        {
            var principalProvider = DependencyResolver.Current.GetService<IPrincipalProvider>();
            _emailAppendix = principalProvider.GetUserName().EmailDomainLong();
        }

        [LocalizedDisplayName("Email", typeof(Resource))]
        public string EmailLocal { get; set; }

        public string EmailAppendix { get { return "@" + _emailAppendix; } }

        public string Email { get { return EmailLocal + EmailAppendix; } }
    }
}