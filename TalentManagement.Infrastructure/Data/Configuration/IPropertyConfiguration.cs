﻿namespace TalentManagement.Infrastructure.Data.Configuration
{
    public interface IPropertyConfiguration<TEntity,TProperty> : IPropertyReadConfiguration<TEntity>
    {
        IPropertyConfiguration<TEntity, TProperty> Ignore();
        IPropertyConfiguration<TEntity, TProperty> Required();
    }
}
