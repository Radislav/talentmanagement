﻿using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Web.Models.People.Map
{
    public class AddUserModelToUserMap : IMap<AddUserModel, User>
    {
        private IHashComputer _hashComputer;

        public AddUserModelToUserMap(IHashComputer hashComputer)
        {
            _hashComputer = hashComputer;
        }

        public void Configure(IMapperConfiguration<AddUserModel, User> configuration)
        {
            configuration.ForMember(x => x.Id, x => x.Email);
            configuration.ForMember(x => x.HashedPassword, x => _hashComputer.ComputeHash(x.Password));
        }
    }
}