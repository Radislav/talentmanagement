﻿using TalentManagement.Domain.Companies;
using TalentManagement.Infrastructure.Data.Configuration;

namespace TalentManagement.Infrastructure.Data.Map
{
    class CompanyMap : EntityConfiguration<Company>
    {
        public CompanyMap()
        {
            AsCollection("Companies").
                WithDataSource(DataSourceType.Tenant);

            Property(p => p.Name)
                .Required();

            Property(p => p.CompanyName)
                .Required();
        }
    }
}
