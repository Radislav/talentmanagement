﻿using System;
using System.Diagnostics.Contracts;
using System.Linq.Expressions;
using TalentManagement.Infrastructure.Validation;

namespace TalentManagement.Infrastructure.Data.Configuration
{
    public class PropertyConfiguration<TEntity,TProperty> : IPropertyConfiguration<TEntity,TProperty>
    {
        private readonly Expression<Func<TEntity, TProperty>> _property;
        private bool _isIgnored;
        private bool _isRequired;

        public PropertyConfiguration(Expression<Func<TEntity, TProperty>> property)
        {
            _property = property;
        }

        public IPropertyConfiguration<TEntity, TProperty> Ignore()
        {
            _isIgnored = true;
            return this;
        }

        public bool IsIgnored
        {
            get { return _isIgnored; }
        }

        public bool IsRequired
        {
            get { return _isRequired; }
        }

        public string Name
        {
            get
            {
                Contract.Ensures( _property.Body as MemberExpression != null);

                MemberExpression expression = (MemberExpression) _property.Body;
                return expression.Member.Name;
            }
        }
        
        public IPropertyConfiguration<TEntity, TProperty> Required()
        {
            _isRequired = true;
            return this;
        }
        
        public ValidationResult ValidateInstance(TEntity entity)
        {
            object propertyValue = typeof (TEntity).GetProperty(Name)
                .GetValue(entity, null);

            if (_isRequired && propertyValue == null)
            {
                return
                    new ValidationResult(new []
                                             {
                                                 new ValidationFailure(Name,String.Format("Entity {0} must have not null property {1}",entity, Name), null)
                                             });
            }
            
            return new ValidationResult();
        }
    }
}
