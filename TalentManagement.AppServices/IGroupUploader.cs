﻿using System.Collections.Generic;
using System.IO;
using TalentManagement.Infrastructure.Parsers;

namespace TalentManagement.AppServices
{
    public interface IGroupUploader : ITemplateProvider<GroupRowParsedEventArgs>
    {
        /// <summary>
        /// Reads and creates users from file.
        /// </summary>
        /// <param name="contentsStream">file contents stream</param>
        /// <returns>List of warnings</returns>
        ICollection<string> Upload(Stream contentsStream);

    }
}
