﻿using System.Diagnostics.Contracts;
using TalentManagement.Infrastructure.Contracts;

namespace TalentManagement.Infrastructure.Security
{
    [ContractClass(typeof(SecurityLinkGeneratorContract))]
    public interface ISecurityLinkGenerator
    {
        string GenerateLink(int length);
    }
}
