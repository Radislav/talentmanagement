﻿using System;
using System.Diagnostics.Contracts;
using MongoDB.Bson;

namespace TalentManagement.Domain.Events
{
    public class CompanyLogoUpdatedArgs : IDomainEventArgs
    {
        public CompanyLogoUpdatedArgs(ObjectId companyId,string companyTenant)
        {
            Contract.Requires(companyId != ObjectId.Empty);
            Contract.Requires(false == String.IsNullOrEmpty(companyTenant));

            CompanyId = companyId;
            CompanyTenant = companyTenant;
        }

        public ObjectId CompanyId { get; set; }
        public string CompanyTenant { get; set; }
    }
}
