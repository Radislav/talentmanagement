﻿using System;
using System.Net.Configuration;
using System.Text;
using Autofac;
using Moq;
using TalentManagement.Infrastructure;
using TalentManagement.Infrastructure.Configuration;
using TalentManagement.Infrastructure.Email;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Serialization;
using TalentManagement.Infrastructure.Session;

namespace TalentManagement.Test.Infrastructure.Email
{
    
    
    /// <summary>
    ///This is a test class for EmailSenderTest and is intended
    ///to contain all EmailSenderTest Unit Tests
    ///</summary>
    [TestClass()]
    public class EmailSenderTest 
    {
        public class EmailArgumentTest
        {
            public DateTime Date { get; set; }
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion
        
        /// <summary>
        ///A test for Send
        ///</summary>
        [TestMethod()]
        public void CreateEmailBody_should_take_it_from_resources_if_EmptyArgument_provided()
        {
            EmailSender target = new EmailSender(null);
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterModule(new InfrastructureRegistrationModule());
            RegisterMocks(builder);
            var container = builder.Build();

            string actual = target.CreateEmailBody(EmailType.None,
                                                   EmptyArgument.Value,
                                                   container.Resolve<IResourceProvider>(),
                                                   container.Resolve<IXmlSerializer<EmptyArgument>>());

            Assert.AreEqual("none_email_body",actual);
        }

        [TestMethod()]
        public void CreateEmailBody_should_use_Xslt_transformation()
        {
            EmailSender target = new EmailSender(null);
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterModule(new InfrastructureRegistrationModule());
            RegisterMocks(builder);
            var container = builder.Build();

            string actual = target.CreateEmailBody(EmailType.ForgotPassword,
                                                   new EmailArgumentTest
                                                       {
                                                           Date = new DateTime(2000,1,1)
                                                       },
                                                   container.Resolve<IResourceProvider>(),
                                                   container.Resolve<IXmlSerializer<EmailArgumentTest>>());

            Assert.AreEqual("2000-01-01T00:00:00", actual);
        }

        private void RegisterMocks(ContainerBuilder containerBuilder)
        {
            var resoruceProviderMock =  containerBuilder.RegisterMock<IResourceProvider>(
                mock =>
                    {
                        mock.Setup(m => m.GetEmailBody(EmailType.None)).Returns("none_email_body");
                        mock.Setup(m => m.GetEmailSubject(EmailType.None)).Returns("none_email_subject");

                        StringBuilder template = new StringBuilder();
                        template.Append(@"<?xml version=""1.0"" encoding=""UTF-8"" ?>");
                        template.Append(@"<xsl:stylesheet version=""1.0"" xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"">");
                        template.Append(@"<xsl:template match=""/"">");
                        template.Append(@" <xsl:value-of select=""EmailArgumentTest/Date""  disable-output-escaping=""yes""/>");
                        template.Append(@"</xsl:template>");
                        template.Append(@"</xsl:stylesheet>");

                        mock.Setup(m => m.GetEmailBody(EmailType.ForgotPassword)).Returns(template.ToString);
                        mock.Setup(m => m.GetEmailSubject(EmailType.ForgotPassword)).Returns("forgot_password_subject");
                    }
                );

            var configurationMock = containerBuilder.RegisterMock<IAppConfiguration>(mock => mock.Setup(appCfg => appCfg.EmailSettings).Returns(new EmailSettings()));
            

            var sessionMock =  containerBuilder.RegisterMock<ISession>(mock =>
                                                        {
                                                            mock.Setup(m => m.Instance<IResourceProvider>()).Returns(resoruceProviderMock.Object);
                                                            mock.Setup(m => m.Instance<IAppConfiguration>()).Returns(configurationMock.Object);
                                                        });

            containerBuilder.RegisterMock<ISessionFactory>(mock => mock.Setup(m => m.Create())
                                                                       .Returns(sessionMock.Object));
        }
    }
}
