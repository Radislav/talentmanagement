﻿using TalentManagement.Infrastructure.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Linq;

namespace TalentManagement.Test.Infrastructure.Serialization
{
    
    
    /// <summary>
    ///This is a test class for XmlSerializerTest and is intended
    ///to contain all XmlSerializerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class XmlSerializerTest
    {

        public class ReferenceType
        {
            public int Data { get; set; }
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        [TestMethod]
        public void Serialize_should_work_with_value_types()
        {
            XmlSerializer<int> target = new XmlSerializer<int>();
            XElement actual =   target.Serialize(10);

            XElement expected = new XElement("int")
                                    {
                                        Value = "10"
                                    };

            Assert.IsTrue(XNode.DeepEquals(expected, actual));
        }

        [TestMethod]
        public void Serialize_should_work_with_reference_types()
        {
            XmlSerializer<ReferenceType> target = new XmlSerializer<ReferenceType>();
            ReferenceType obj = new ReferenceType {Data = 20};
            XElement actual = target.Serialize(obj);
            XElement expected = new XElement("ReferenceType", new XElement("Data")
                                                                           {
                                                                               Value = "20"
                                                                           });

            Assert.IsTrue(XNode.DeepEquals(expected, actual));
        }
        
        [TestMethod]
        public void Deserialize_should_work_with_value_types()
        {
            XmlSerializer<int> target = new XmlSerializer<int>();
            int actual = target.Deserialize(new XElement("int")
            {
                Value = "10"
            });

            Assert.AreEqual(10,actual);
        }

        [TestMethod]
        public void Deserialize_should_work_with_reference_types()
        {
            XmlSerializer<ReferenceType> target = new XmlSerializer<ReferenceType>();
            ReferenceType actual = target.Deserialize(new XElement("ReferenceType", new XElement("Data")
                                                                                        {
                                                                                            Value = "20"
                                                                                        }));
            Assert.AreEqual(20,actual.Data);
        }
    }
}
