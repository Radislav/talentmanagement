﻿namespace TalentManagement.Web.Models.MyCompanyObjectives
{
    public class ObjectiveGroupModel
    {
        public string Title { get; set; }
        public ObjectiveSimpleModel[] Objectives { get; set; }
    }
}