﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using MongoDB.Bson;
using TalentManagement.Domain.Performance;

namespace TalentManagement.Domain.Contracts
{
    [ContractClassFor(typeof(IPerformanceObjectsFactory))]
    internal  abstract class PerformanceObjectsFactoryContract : IPerformanceObjectsFactory
    {
        public IEnumerable<Review> CreateReviews(ObjectId reviewCycleId, IEnumerable<string> participants, string owner, QuestionSet questionSet)
        {
            Contract.Requires(participants != null);
            Contract.Requires(owner != null);
            Contract.Requires(questionSet != null);
            Contract.Ensures(Contract.Result<IEnumerable<Review>>() != null);
            return null;
        }

        public ReviewCycleSummary CreateSummary(ObjectId reviewCycleId,string respondent)
        {
            Contract.Requires(reviewCycleId != ObjectId.Empty);
            Contract.Requires(respondent != null);
            Contract.Ensures(Contract.Result<ReviewCycleSummary>() != null);
            return null;
        }

        public AnswersCollection CreateAnswersCollection(Review review)
        {
            Contract.Requires(review != null);
            Contract.Ensures(Contract.Result<AnswersCollection>() != null);

            return null;
        }
    }
}
