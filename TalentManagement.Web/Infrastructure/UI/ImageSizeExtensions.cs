﻿using System.Collections.Generic;

namespace TalentManagement.Web.Infrastructure.UI
{
    public static class ImageSizeExtensions
    {
        private static readonly Dictionary<ImageSize,string> CssMap = new Dictionary<ImageSize, string>
                                                                           {
                                                                               {ImageSize.Small, "image-small"},
                                                                               {ImageSize.Smaller, "image-smaller"},
                                                                               {ImageSize.Smallest, "image-smallest"},
                                                                               {ImageSize.Medium, "image-medium"}

                                                                           };
        public static string GetCss(this ImageSize avatarSize)
        {
            if (avatarSize == ImageSize.Actual)
            {
                return string.Empty;
            }

            return CssMap.ContainsKey(avatarSize) ? CssMap[avatarSize] : CssMap[ImageSize.Small];
        }
    }
}