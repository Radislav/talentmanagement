﻿using TalentManagement.Domain;

namespace TalentManagement.Infrastructure.Data.Filter
{
    public class OwnedByCompanyFilterContext : IFilterContext
    {
        public OwnedByCompanyFilterContext()
        {
        }

        public OwnedByCompanyFilterContext(string company)
        {
            Company = company;
        }

        public string Company { get; set; }
    }
}
