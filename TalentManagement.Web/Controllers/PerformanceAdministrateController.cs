﻿using System.Collections.Generic;
using System.Web.Mvc;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Web.Models.PerformanceAdministrate;
using Resource = TalentManagement.Resources.Models.PerformanceAdministrate.IndexModel;

namespace TalentManagement.Web.Controllers
{
    public class PerformanceAdministrateController : Controller
    {
        private readonly IReviewCycleRepository _reviewCycleRepository;
        private readonly IMapper<IEnumerable<ReviewCycle>, IndexModel> _indexModelMapper;

        public PerformanceAdministrateController(IReviewCycleRepository reviewCycleRepository, IMapper<IEnumerable<ReviewCycle>, IndexModel> indexModelMapper)
        {
            _reviewCycleRepository = reviewCycleRepository;
            _indexModelMapper = indexModelMapper;
        }

        public ActionResult Index(string id)
        {
            IEnumerable<ReviewCycle> cycles = (id == Resource.Completed) ?
                                                    _reviewCycleRepository.GetCompletedCycles() :  
                                                    _reviewCycleRepository.GetActiveCycles();
            IndexModel indexModel = _indexModelMapper.Map(cycles);
            indexModel.ActiveButton = id ?? indexModel.ActiveButton;
            if (Request.IsAjaxRequest())
            {
                return PartialView("_ReviewCycleTable", indexModel);
            }
            return View(indexModel);
        }

    }
}
