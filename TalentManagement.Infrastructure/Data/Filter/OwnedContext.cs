﻿using TalentManagement.Domain;

namespace TalentManagement.Infrastructure.Data.Filter
{
    public class OwnedFilterContext : IFilterContext
    {
        public OwnedFilterContext()
        {
        }

        public OwnedFilterContext(string createdBy)
        {
            CreatedBy = createdBy;
        }

        public string CreatedBy { get; set; }
    }
}
