﻿using System.Collections.Generic;
using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Web.Models.Questionaire;
using TalentManagement.Web.Models.Questionaire.Map;

namespace TalentManagement.Web.Controllers
{
    public class QuestionnaireController : Controller
    {
        private readonly IReviewRepository _reviewRepository;
        private readonly IIndexModelMapper _indexModelMapper;
        private readonly IQuestionRepository _questionRepository;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IMapper<IndexModel, IEnumerable<Answer>> _answersMapper;
        private readonly IResourceProvider _resourceProvider;
      
        
        public QuestionnaireController(IReviewRepository reviewRepository,
            IIndexModelMapper indexModelMapper,
            IQuestionRepository questionRepository,
            IPrincipalProvider principalProvider,
            IMapper<IndexModel, IEnumerable<Answer>> answersMapper,
            IResourceProvider resourceProvider)
        {
            _reviewRepository = reviewRepository;
            _indexModelMapper = indexModelMapper;
            _questionRepository = questionRepository;
            _principalProvider = principalProvider;
            _answersMapper = answersMapper;
            _resourceProvider = resourceProvider;
        }

        [HttpGet]
        public ActionResult Index(ObjectId id)
        {
            Review review = _reviewRepository.GetById(id);
            AnswersCollection answers = new AnswersCollection(review.Answers, _questionRepository.GetByIds(review.QuestionIds),review.SetType);
            return View(_indexModelMapper.Map(answers,review.Id));
        }

        [HttpPost]
        public ActionResult Index(IndexModel indexModel,ReviewStatus reviewStatus)
        {
            Review review = _reviewRepository.GetById(indexModel.ReviewId);

            _answersMapper.Map(indexModel)
                          .Map(answer => review.UpsertAnswer(answer,_principalProvider.GetUserName()));
          
            _reviewRepository.Update(review);

            return Json(new { message = _resourceProvider.QuestionnaireAnswersSaved });
        }

    }
}
