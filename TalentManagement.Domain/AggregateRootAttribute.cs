﻿using System;

namespace TalentManagement.Domain
{
    [AttributeUsage(AttributeTargets.Class,AllowMultiple = false,Inherited = true)]
    public class AggregateRootAttribute : Attribute
    {
    }
}
