﻿using System;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Resources;

namespace TalentManagement.Web.Models.PerformanceSummary.Map
{
    internal class ObjectiveModelMap : IMap<Objective,ObjectiveModel>
    {
        private readonly string _due;
        public ObjectiveModelMap(IResourceProvider resourceProvider)
        {
            _due = resourceProvider.Due;
        }

        public void Configure(IMapperConfiguration<Objective, ObjectiveModel> configuration)
        {
            configuration.ForMember(d => d.Achieved, s => s.Status == ObjectiveStatus.Completed);
            configuration.ForMember(d => d.Name, s => s.Name);
            configuration.ForMember(d => d.ObjectiveDue, s => String.Format("{0} {1}", _due, s.Due.AsLondDateString()));
            configuration.ForMember(d => d.Progress, s => s.Progress);
            configuration.ForMember(d => d.IconId, s => s.Icon);
            configuration.ForMember(d => d.Id, s => s.Id);
        }
    }
}