﻿using System.Diagnostics.Contracts;
using TalentManagement.Infrastructure.Configuration;

namespace TalentManagement.Infrastructure.Contracts
{
    [ContractClassFor(typeof(IAppConfiguration))]
    internal abstract class AppConfigurationContract : IAppConfiguration
    {
        public string GetConfiguration(string key)
        {
            Contract.Requires(false == string.IsNullOrEmpty(key));
            return default(string);
        }

        public string GetConnectionString(string name)
        {
            Contract.Requires(false == string.IsNullOrEmpty(name));
            Contract.Ensures(false == string.IsNullOrEmpty(Contract.Result<string>()));
            return default(string);
        }

        public string GetTenantName()
        {
            Contract.Ensures(false == string.IsNullOrEmpty(Contract.Result<string>()));
            return default(string);
        }

        public string GetDbName(DataSourceType dataSourceType)
        {
            Contract.Ensures(false == string.IsNullOrEmpty(Contract.Result<string>()));
            return default(string);
        }

        public int MinPasswordLength
        {
            get
            {
                Contract.Ensures(Contract.Result<int>() > 0);
                return default(int);
            }
        }

        public EmailSettings EmailSettings
        {
            get
            {
                Contract.Ensures(null != Contract.Result<EmailSettings>());
                return default(EmailSettings);
            }
        }

        public int AvatarWidth
        {
            get
            {
                Contract.Ensures(Contract.Result<int>() > 0);
                return default(int);
            }
        }

        public int AvatarHeight
        {
            get
            {
                Contract.Ensures(Contract.Result<int>() > 0);
                return default(int);
            }
        }

        public string AvatarImageType
        {
            get
            {
                Contract.Ensures(false == string.IsNullOrEmpty(Contract.Result<string>()));
                return default(string);
            }
        }
    }
}
