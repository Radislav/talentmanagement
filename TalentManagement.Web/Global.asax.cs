﻿using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Web.Infrastructure;
using TalentManagement.Web.Infrastructure.Bundles;
using TalentManagement.Web.Infrastructure.Security;

namespace TalentManagement.Web
{
    public class MvcApplication : HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
        
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            //Improve performance
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
            
            RegisterGlobalFilters(GlobalFilters.Filters);
            RoutesRegistration.RegisterRoutes(RouteTable.Routes);
            var container = AutofacRegistration.RegisterAutofac();
            container.AddModelBinders(ModelBinders.Binders);
            
            BundlesRegistration.RegisterBundles(BundleTable.Bundles);
            ValidationRegistration.RegisterValidation();

            /*  Uncomment this to reace routes  */
            //RouteDebug.RouteDebugger.RewriteRoutesForTesting(RouteTable.Routes);

            /* Uncomment to view how bundles will work on Release mode */
            //BundleTable.EnableOptimizations = true;

        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
        }
        
        protected void Application_PostAuthenticateRequest(object sender, EventArgs eventArgs)
        {
            HttpContext context = HttpContext.Current;

            SecurityTicket securityTicket = new SecurityTicket(context);

            if (context.User != null && context.User.Identity.IsAuthenticated && context.User.Identity is FormsIdentity)
            {
                TenantPrincipal principal = securityTicket.GetPrincipal();

                if (principal == null)
                {
                    principal = DependencyResolver.Current.GetService<IAuthorizationService>().LoadPrincipal(context.User.Identity.Name);   
                    securityTicket.SetPrincipal(principal);
                }

                TenantPrincipal currentPrincipal = securityTicket.GetPrincipal();

                if (currentPrincipal == null)
                {
                    FormsAuthentication.SignOut();
                }
                else
                {
                    Thread.CurrentPrincipal = context.User = currentPrincipal;
                }

                if(false == securityTicket.DoesRequestHasCookie)
                {
                    securityTicket.AddCookieToContext(context);
                }
            }
        }
    }
}