﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace TalentManagement.Infrastructure
{
    public interface IUrlProvider
    {
        string HelpUrl { get; }
        string LoginUrl { get; }
        string FeedbacksUrl { get; }
        string GetObjectiveDetailsUrl(ObjectId objectiveId);

        string GetActivationUrl(string code);

        IUrlProvider Generate(string action, string controller, IDictionary<string, object> routes);
        IUrlProvider Generate(string action, string controller);
        string GetUrl();
        IUrlProvider ToVirtualPath();
        IUrlProvider Decode();


    }
}
