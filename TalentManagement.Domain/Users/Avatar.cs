﻿namespace TalentManagement.Domain.Users
{
    public class Avatar : EntityWithTypedId<string> , IPicture
    {
        public Avatar()
        {
        }

        public Avatar(string userId) :base(userId)
        {
        }

        public byte[] Image { get; set; }
        public byte[] SmallImage { get; set; }
        public string ImageType { get; set; }
    }
}
