﻿namespace TalentManagement.Web.Models.Feeds
{
    public class AlertFeedModel : FeedModel
    {
        public string Text { get; set; }
    }
}