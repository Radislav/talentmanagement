﻿using System;
using System.Diagnostics.Contracts;
using MongoDB.Bson;

namespace TalentManagement.Domain.Performance
{
    public class Answer : Entity , IOwned
    {
        private Answer() 
        {

        }

        public Answer(ObjectId answerId,ObjectId questionId, string content) : base(answerId)
        {
            Contract.Requires(questionId != ObjectId.Empty);

            Content = content;
            Id = answerId;
            QuestionId = questionId;

        }
        
        public ObjectId QuestionId { get; private set; }

        public string Content { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedAt { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}
