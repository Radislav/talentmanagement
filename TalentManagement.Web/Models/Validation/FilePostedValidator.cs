﻿using System.Web;
using FluentValidation.Validators;

namespace TalentManagement.Web.Models.Validation
{
    internal class FilePostedValidator : PropertyValidator
    {
        public FilePostedValidator()
            : base("FilePostedInvalid", typeof(Resources.Errors))
        {

        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            HttpPostedFileBase value = context.PropertyValue as HttpPostedFileBase;
            if (false == new HttpPostedFileWrapper(value).IsReady)
            {
                return false;
            }
            return true;
        }
    }
}