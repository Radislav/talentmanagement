﻿using System.Collections.Generic;
using System.Linq;
using MvcSiteMapProvider.Web.Html.Models;
using TalentManagement.Infrastructure;

namespace TalentManagement.Web.Infrastructure.UI
{
    public static class SiteMapExtensions
    {
        private static readonly Dictionary<string,string> ViewsMap = new Dictionary<string, string>
                                                                {
                                                                    {"css","~/Views/Shared/DisplayTemplates/SiteMapNodes/SiteMapCssNodeModel.cshtml"},
                                                                    {"icon","~/Views/Shared/DisplayTemplates/SiteMapNodes/SiteMapIconNodeModel.cshtml"},
                                                                };

        public static string GetMetaValue(this SiteMapNodeModel node, string metaKey)
        {
            return node.MetaAttributes.SingleOrDefault(ma => ma.Key == metaKey).GetSafe(ma => ma.Value);
        }

        private static bool HasKey(SiteMapNodeModel node, string metaKey)
        {
            return node.MetaAttributes.Any(ma => ma.Key == metaKey);
        }

        public static string GetNodeModelView(this SiteMapNodeModel node)
        {
            foreach (var viewMap in ViewsMap)
            {
                if (HasKey(node,viewMap.Key))
                {
                    return viewMap.Value;
                }
            }

            return "~/Views/Shared/DisplayTemplates/SiteMapNodes/SiteMapDefaultNodeModel.cshtml";
        }
    }
}