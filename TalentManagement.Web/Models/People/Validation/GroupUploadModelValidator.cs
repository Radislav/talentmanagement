﻿using FluentValidation;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.People.Validation
{
    internal class GroupUploadModelValidator : AbstractValidator<GroupUploadModel>, TalentManagement.Infrastructure.Validation.IValidator<GroupUploadModel>
    {
        public GroupUploadModelValidator()
        {
            RuleFor(m => m.Template)
                .NotEmpty();
        }

        public TalentManagement.Infrastructure.Validation.ValidationResult ValidateInstance(GroupUploadModel instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}