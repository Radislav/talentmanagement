﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;
using TalentManagement.Domain.Contracts;

namespace TalentManagement.Domain.Objectives
{
    [ContractClass(typeof(ObjectiveRepositoryContract))]
    public interface IObjectiveRepository : IRepository<Objective>
    {
        IEnumerable<IGrouping<TProperty, Objective>> GetObjectivesGroupedBy<TProperty>
           (Expression<Func<Objective, TProperty>> groupingExpression);

        IEnumerable<IGrouping<TProperty, Objective>> GetObjectivesGroupedBy<TProperty>
            (Expression<Func<Objective, TProperty>> groupingExpression, int dueYear);
        IEnumerable<Objective> GetFocusObjectives();
    }
}
