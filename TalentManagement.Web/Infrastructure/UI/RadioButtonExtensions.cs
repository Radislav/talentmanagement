﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace TalentManagement.Web.Infrastructure.UI
{
    public static class RadioButtonExtensions
    {
        public static MvcHtmlString RadioButtonBootstrapForEnum<TModel, TProperty>(
        this HtmlHelper<TModel> htmlHelper,
        Expression<Func<TModel, TProperty>> expression,
        object htmlAttributes = null,
        string selectedName = null,
        bool exludeNoneValue = true)
         where TProperty : struct
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            
            TagBuilder container = new TagBuilder("div");
            container.AddCssClass("btn-group");
            container.MergeAttribute("data-toggle","buttons-radio");
            container.MergeAttribute("data-property",metaData.PropertyName);

            if (htmlAttributes != null)
            {
                container.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));    
            }

            
            IEnumerable<EnumNamesPair> namePairs = HtmlExtensions.GetEnumNames(metaData,exludeNoneValue);
            
            StringBuilder sb = new StringBuilder();
            bool isDefaultRadioSelected = false;
            foreach (var pair in namePairs)
            {
                //Skip None enum.
                if (pair.AreAnyNameEqualTo(TalentManagement.Resources.DomainResources.None))
                {
                    continue;
                }
                TagBuilder button = new TagBuilder("button");
                button.MergeAttribute("type","button");
                button.MergeAttribute("id", string.Format("{0}_{1}", metaData.PropertyName, pair.Name));
                button.MergeAttribute("data-value",pair.Name);
                button.AddCssClass("btn");

                if (false == isDefaultRadioSelected && (pair.AreAnyNameEqualTo(selectedName) || selectedName == null))
                {
                    button.AddCssClass("active");
                    isDefaultRadioSelected = true;
                }
                button.SetInnerText(pair.DisplayName);

                sb.Append(button.ToString(TagRenderMode.Normal));
            }
            container.InnerHtml = sb.ToString();

            return new MvcHtmlString(container.ToString());
        }

        public static MvcHtmlString RadioButtonForEnum<TModel, TProperty>(
         this HtmlHelper<TModel> htmlHelper,
         Expression<Func<TModel, TProperty>> expression,
         IDictionary<string, object> htmlAttributes = null,
         string selectedName = null,
         bool exludeNoneValue = true)
          where TProperty : struct
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            IEnumerable<EnumNamesPair> namePairs = HtmlExtensions.GetEnumNames(metaData,exludeNoneValue);

            StringBuilder sb = new StringBuilder();
            bool isDefaultRadioSelected = false;
            foreach (var pair in namePairs)
            {
                //Skip None enum.
                if (pair.AreAnyNameEqualTo(TalentManagement.Resources.DomainResources.None))
                {
                    continue;
                }

                var id = string.Format("{0}_{1}", metaData.PropertyName, pair.Name);

                htmlAttributes = htmlAttributes ?? new Dictionary<string, object>();
                htmlAttributes["id"] = id;

                if (false == isDefaultRadioSelected && (pair.AreAnyNameEqualTo(selectedName) || selectedName == null))
                {
                    htmlAttributes.Add("selected", "selected");
                    isDefaultRadioSelected = true;
                }

                string radioHtml = htmlHelper.RadioButtonFor(expression, pair.Name, htmlAttributes)
                                             .ToHtmlString();

                TagBuilder label = new TagBuilder("label");
                label.MergeAttribute("for", id);
                label.AddCssClass("radio");
                label.InnerHtml = radioHtml + HttpUtility.HtmlEncode(pair.DisplayName);


                TagBuilder outerDiv = new TagBuilder("div");
                outerDiv.MergeAttribute("class", "form_input");

                outerDiv.InnerHtml = label.ToString(TagRenderMode.Normal);
                sb.Append(outerDiv.ToString(TagRenderMode.Normal));
            }
            return MvcHtmlString.Create(sb.ToString());
        }
    }
}