﻿using TalentManagement.Domain;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Web.Models.Map
{
    class CommentMap : IMap<CommentModel,Comment>
    {
        private readonly IPrincipalProvider _principalProvider;

        public CommentMap(IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
        }

        public void Configure(IMapperConfiguration<CommentModel, Comment> configuration)
        {
            configuration.ForMember(dest => dest.CreatedBy, src => _principalProvider.GetUserName());

        }
    }
}