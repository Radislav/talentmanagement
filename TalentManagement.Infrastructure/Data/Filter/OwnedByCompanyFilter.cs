﻿using System.Linq;
using TalentManagement.Domain;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data.Filter
{
    internal class OwnedByCompanyFilter : FilterBase<IOwnedByCompany>
    {
        private readonly IPrincipalProvider _principalProvider;
        private IFilterContext _filterContext;

        public OwnedByCompanyFilter(IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
        }

        public override IFilter<IOwnedByCompany> Filter(IFilterContext[] contexts)
        {
            OwnedByCompanyFilterContext ownedFilterContext = GetContextOrFail<OwnedByCompanyFilterContext>(contexts);

            if (ownedFilterContext != null && false == string.IsNullOrEmpty(ownedFilterContext.Company))
            {
                Data = Data.Where(o => o.CompanyName == ownedFilterContext.Company);        
            }
            return this;
        }

        public override IFilterContext FilterContext
        {
            get
            {
                return _filterContext ?? new OwnedByCompanyFilterContext(_principalProvider.GetTenant());
            }
            set { _filterContext = value; }
        }
    }
}
