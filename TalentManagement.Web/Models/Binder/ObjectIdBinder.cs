﻿using System.Web.Mvc;
using MongoDB.Bson;
namespace TalentManagement.Web.Models.Binder
{
    public class ObjectIdBinder : CustomModelBinder<ObjectId>
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (value == null)
            {
                return new ObjectId();
            }
            return string.IsNullOrEmpty(value.AttemptedValue) ? new ObjectId() 
                                                              : new ObjectId(value.AttemptedValue);
        }
    }
}