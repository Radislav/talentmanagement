﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;

namespace TalentManagement.Infrastructure.Parsers
{
    class UserGroupParserCsv : IRowsParser<string[], GroupRowParsedEventArgs>,
                               ITemplateProvider<GroupRowParsedEventArgs>
    {
        private const int RecordsCnt = 50;
        private int _rowCount;

        public GroupRowParsedEventArgs ParseRow(string[] source)
        {
            return new GroupRowParsedEventArgs(_rowCount)
                       {
                           Email = source[0],
                           Password = source[1],
                           FirstName = source[2],
                           SecondName = source[3],
                           Position = source[4],
                           Team = source[5],
                           ReportingManagerEmailLocal = source[6],
                           ReportingManagerEmailAppendix = source[7]
                       };
        }

        public event EventHandler<GroupRowParsedEventArgs> RowParsed;

        public object Parse(Stream content)
        {
            _rowCount = 0;
            List<GroupRowParsedEventArgs> rows = new List<GroupRowParsedEventArgs>();
            using(var csv = new CsvReader(new StreamReader(content)))
            {
                while (csv.Read())
                {
                    if (false == IsRecordEmpty(csv.CurrentRecord))
                    {
                        GroupRowParsedEventArgs row = ParseRow(csv.CurrentRecord);
                        rows.Add(row);
                        _rowCount++;

                        if (RowParsed != null)
                        {
                            RowParsed(this, row);
                        }
                    }
                }
            }
            return rows;
        }

        public string TemplateContentType
        {
            get { return "Content-type: text/csv"; }
        }

        private bool IsRecordEmpty(IEnumerable<string> csvRecord)
        {
            if (csvRecord == null)
            {
                return true;
            }

            return csvRecord.Count(r => false == string.IsNullOrEmpty(r)) <= 1;
        }

        public byte[] GetTemplateContents(object argument)
        {
            List<GroupRowParsedEventArgs> records = new List<GroupRowParsedEventArgs>(RecordsCnt);
            for (int idx = 0; idx < RecordsCnt; idx++)
            {
                records.Add(new GroupRowParsedEventArgs(idx)
                                {
                                    ReportingManagerEmailAppendix = "@" + (string)argument
                                });
            }
            MemoryStream memoryStream = new MemoryStream();
            using (var csv = new CsvWriter(new StreamWriter(memoryStream)))
            {
                csv.WriteRecords(records);
            }
            return memoryStream.ToArray();
        }


        public string FileName
        {
            get { return "TalentManagement.UploadUsers.csv"; }
        }
    }
}
