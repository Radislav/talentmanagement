﻿using System.Web.Mvc;
using FluentValidation;
using TalentManagement.Infrastructure.Resources;

namespace TalentManagement.Web.Models.Validation
{
    public class LoginModelValidator : AbstractValidator<LoginModel>
    {
        public LoginModelValidator()
        {
            IResourceProvider resourceProvider = DependencyResolver.Current.GetService<IResourceProvider>();

            RuleFor(m => m.Email)
                .NotEmpty();

            RuleFor(m => m.Email)
                .EmailAddress();

            RuleFor(m => m.Password)
                .NotEmpty();
        }
    }
}