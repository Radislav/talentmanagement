﻿using TalentManagement.Domain.Feeds;
using TalentManagement.Infrastructure.Data.Configuration;

namespace TalentManagement.Infrastructure.Data.Map
{
    public class FeedMap : EntityConfiguration<Feed>
    {
        public FeedMap()
        {
            AsCollection("Feeds")
                .WithDataSource(DataSourceType.Tenant);

            Property(f => f.Scope)
                .Ignore();

            Property(p => p.CompanyName)
             .Required();

            Property(p => p.CreatedBy)
                .Required();
            
        }
    }
}
