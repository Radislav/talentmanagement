﻿using TalentManagement.Domain.Companies;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Companies.Map
{
    internal class CompanyMap : IMap<GeneralTab,Company>
    {
        public void Configure(IMapperConfiguration<GeneralTab, Company> configuration)
        {
            configuration.Ignore(d => d.CompanyName);
            configuration.ForMember(d => d.Name, c => c.CompanyName);
            configuration.ForMember(d => d.Id, c => c.CompanyId);
            configuration.ForMember(d => d.Timezone, c => c.SelectedTimezone);
            configuration.ForMember(d => d.Logo, c =>
                                                     {
                                                         HttpPostedFileWrapper wrapper = new HttpPostedFileWrapper(c.CompanyLogo);
                                                         return wrapper.IsReady
                                                                    ? new Logo(c.LogoId)
                                                                          {
                                                                              Image = wrapper.ResizeToSquare(Logo.SizeInPixels),
                                                                              ImageType = wrapper.ContentType
                                                                          }
                                                                    : new Logo(c.LogoId);
                                                     });
        }
    }
}