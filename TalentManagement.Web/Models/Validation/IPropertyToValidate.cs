﻿namespace TalentManagement.Web.Models.Validation
{
    public interface IPropertyToValidate
    {
        string PropertyToValidateName { get; set; }
    }
}