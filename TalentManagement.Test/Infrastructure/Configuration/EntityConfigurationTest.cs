﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TalentManagement.Domain;
using TalentManagement.Infrastructure;
using TalentManagement.Infrastructure.Data.Configuration;
using TalentManagement.Infrastructure.Validation;
using TalentManagement.Test;
using TalentManagement.Test.Infrastructure.Configuration;

namespace TalentManagement.Test.Infrastructure.Configuration
{
    public class MyRequiredEntity : Entity
    {
        public string RequiredField { get; set; }
        public string OptionalField { get; set; }
    }

    public class MyRequiredEntityMap : EntityConfiguration<MyRequiredEntity>
    {
        public MyRequiredEntityMap()
        {
            AsCollection("MyEntity")
                .WithDataSource(DataSourceType.Shared);

            Property(e => e.RequiredField)
                .Required();
        }
    }
}

    [TestClass]
    public class EntityConfigurationTest : MockedTestClass<EntityConfiguration<MyRequiredEntity>>
    {
        [TestMethod]
        public void ValidateEntity_should_return_IsValid_TRUE_for_NOT_NULL_required_field()
        {
            ValidationResult result = ((IValidator<object>) new MyRequiredEntityMap()).ValidateInstance(new MyRequiredEntity{RequiredField = "Value"});

            Assert.IsTrue(result.IsValid);

        }

        [TestMethod]
        public void ValidateEntity_should_return_IsValid_FALSE_for__NULL_required_field()
        {
            ValidationResult result = ((IValidator<object>)new MyRequiredEntityMap()).ValidateInstance(new MyRequiredEntity());

            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.ErrorMessages.Contains("Entity TalentManagement.Test.Infrastructure.Configuration.MyRequiredEntity must have not null property RequiredField"));

        }

        [TestMethod]
        public void ValidateEntity_should_return_IsValid_TRUE_for_NULL_optional_field()
        {
            ValidationResult result = ((IValidator<object>)new MyRequiredEntityMap()).ValidateInstance(new MyRequiredEntity { RequiredField = "Value", OptionalField = null });

            Assert.IsTrue(result.IsValid);

        }
}
