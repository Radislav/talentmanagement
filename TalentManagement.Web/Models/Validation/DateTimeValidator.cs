﻿using System;
using FluentValidation.Validators;

namespace TalentManagement.Web.Models.Validation
{
    internal class DateTimeValidator : PropertyValidator 
    {
        public DateTimeValidator()
            : base("DateTimeFormatInvalid",typeof(Resources.Errors))
        {

        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            string value = context.PropertyValue as string;
            if (false == String.IsNullOrEmpty(value))
            {
                DateTime dateTime;
                return DateTime.TryParse(value, out dateTime);
            }
            return true;
        }
    }
}