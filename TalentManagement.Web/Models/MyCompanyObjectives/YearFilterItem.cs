﻿using System;
using System.Web.Mvc;
using TalentManagement.Infrastructure.Resources;

namespace TalentManagement.Web.Models.MyCompanyObjectives
{
    public class YearFilterItem
    {
        private static readonly IResourceProvider ResourceProvider = DependencyResolver.Current.GetService<IResourceProvider>();
        
        public string Title { get; set; }
        public int Year { get; set; }

        public static YearFilterItem[] CreateDropdownItems()
        {
            return new[]
                       {
                           new YearFilterItem
                               {
                                   Title = ResourceProvider.MyCompanyObjectiveCurrentYear,
                                   Year = DateTime.Now.Year
                               },
                           new YearFilterItem
                               {
                                   Title = ResourceProvider.MyCompanyObjectivePreviousYear,
                                   Year = DateTime.Now.Year - 1
                               },
                           new YearFilterItem
                               {
                                   Title = ResourceProvider.MyCompanyObjectiveAllYears,
                                   Year = 0
                               }
                       };
        }
    }
}