﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using MongoDB.Bson;
using TalentManagement.Domain;
using TalentManagement.Domain.Companies;
using TalentManagement.Domain.Events;
using TalentManagement.Domain.Feedbacks;
using TalentManagement.Domain.Feeds;
using TalentManagement.Domain.Objectives;
using TalentManagement.Domain.Performance;
using TalentManagement.Domain.Texts;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure;
using TalentManagement.Infrastructure.Cache;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Infrastructure.Serialization;
using TalentManagement.Infrastructure.Validation;
using TalentManagement.Web.Controllers;
using TalentManagement.Web.Infrastructure;
using TalentManagement.Web.Infrastructure.Cache;
using TalentManagement.Web.Infrastructure.DomainEventHandlers;
using TalentManagement.Web.Infrastructure.Resources;
using TalentManagement.Web.Infrastructure.Security;
using TalentManagement.Web.Infrastructure.Texts;
using TalentManagement.Web.Models;
using TalentManagement.Web.Models.Binder;
using TalentManagement.Web.Models.Companies;
using TalentManagement.Web.Models.Companies.Map;
using TalentManagement.Web.Models.Companies.Validation;
using TalentManagement.Web.Models.Feeds;
using TalentManagement.Web.Models.Feeds.Map;
using TalentManagement.Web.Models.Map;
using TalentManagement.Web.Models.MyCompanyObjectives;
using TalentManagement.Web.Models.MyCompanyObjectives.Map;
using TalentManagement.Web.Models.MyFeedbacks;
using TalentManagement.Web.Models.MyFeedbacks.Map;
using TalentManagement.Web.Models.MyFeedbacks.Validation;
using TalentManagement.Web.Models.MyPreferences.Map;
using TalentManagement.Web.Models.ObjectiveDetails;
using TalentManagement.Web.Models.ObjectiveDetails.Map;
using TalentManagement.Web.Models.ObjectiveDetails.Validation;
using TalentManagement.Web.Models.Objectives;
using TalentManagement.Web.Models.Objectives.Binder;
using TalentManagement.Web.Models.Objectives.Map;
using TalentManagement.Web.Models.Objectives.Validation;
using TalentManagement.Web.Models.People;
using TalentManagement.Web.Models.People.Map;
using TalentManagement.Web.Models.People.Validation;
using TalentManagement.Web.Models.PerformanceAdministrate;
using TalentManagement.Web.Models.Validation;
using Companies = TalentManagement.Web.Models.Companies;
using MyAccount = TalentManagement.Web.Models.MyAccount;
using MyFeedbacks = TalentManagement.Web.Models.MyFeedbacks;
using MyNotifications = TalentManagement.Web.Models.MyNotifications;
using MyPassword = TalentManagement.Web.Models.MyPassword;
using MyPreferences = TalentManagement.Web.Models.MyPreferences;
using MyActionItems = TalentManagement.Web.Models.MyActionItems;
using PerformanceAdministrate = TalentManagement.Web.Models.PerformanceAdministrate;
using ReviewCycleCreation = TalentManagement.Web.Models.ReviewCycleCreation;
using MyReviews = TalentManagement.Web.Models.MyReviews;
using CycleDetails = TalentManagement.Web.Models.CycleDetails;
using Questionaire = TalentManagement.Web.Models.Questionaire;
using PerformanceSummary = TalentManagement.Web.Models.PerformanceSummary;
using Teams = TalentManagement.Web.Models.Teams;

namespace TalentManagement.Web
{
    public class WebRegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            #region Infrastructure
            
            builder.RegisterType<ApplicationResourceProvider>()
               .As<IResourceProvider>()
               .SingleInstance();

            builder.RegisterGeneric(typeof (JsonSerializer<>))
                .As(typeof(IJsonSerializer<>));

            builder.RegisterType<UrlProvider>()
                .As<IUrlProvider>();

            builder.RegisterType<UserCache>()
                .As<IUserCache>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UsersPickedProvider>()
                .As<IUsersPickedProvider>();

            //Handlers
            builder.RegisterType<CompanyLogoUpdatedHandler>()
                .As<IHandle<CompanyLogoUpdatedArgs>>();

            builder.RegisterType<BadgeUpdatedHandler>()
                .As<IHandle<BadgeUpdatedArgs>>();

            builder.RegisterType<AvatarUpdatedHandler>()
                .As<IHandle<AvatarUpdatedArgs>>();

            builder.RegisterType<UserUpdatedHandler>()
                .As<IHandle<UserUpdatedArgs>>();

            builder.RegisterType<ObjectiveUpdatedHandler>()
                .As<IHandle<ObjectiveUpdatedArgs>>();

            builder.RegisterType<NotificableArgsHandler<FeedbackCommentedArgs>>()
                .As<IHandle<FeedbackCommentedArgs>>();

            builder.RegisterType<NotificableArgsHandler<FeedbackRequestedArgs>>()
                .As<IHandle<FeedbackRequestedArgs>>();

            builder.RegisterType<NotificableArgsHandler<ObjectiveCreatedArgs>>()
              .As<IHandle<ObjectiveCreatedArgs>>();

            builder.RegisterType<NotificableArgsHandler<ObjectiveUpdatedArgs>>()
              .As<IHandle<ObjectiveUpdatedArgs>>();

            //Cache
            builder.RegisterType<OutputCacheCleaner>()
                .As<IOutputCacheCleaner>();
            
            //Text formatters
            builder.RegisterType<EntityTextFormatter>()
                .As<ITextFormatter<NotificableEventArgs>>()
                .As<ITextFormatter>();

            builder.RegisterType<FeedbackTextFormatter>()
                .Keyed<ITextFormatter>(TextType.NotificationFeedbackRequested);

            builder.RegisterType<FeedbackCommentedTextFormatter>()
                .Keyed<ITextFormatter>(TextType.NotificationFeedbackCommented);

            builder.RegisterType<ObjectiveTextFormatter>()
                .Keyed<ITextFormatter>(TextType.NotificationObjectiveCreated);

            builder.RegisterType<ObjectiveUpdatedTextFormatter>()
                .Keyed<ITextFormatter>(TextType.NotificationObjectiveUpdated);

            #endregion

            #region Shared

            //Maps
            builder.RegisterType<ObjectiveSimpleModelMap>()
                .As<IMap<Objective, ObjectiveSimpleModel>>();

            builder.RegisterType<CommentMap>()
                .As<IMap<CommentModel, Comment>>();

            builder.RegisterType<CommentModelMap>()
                    .As<IMap<Comment, CommentModel>>();

            //Binders
            builder.RegisterGeneric(typeof (EnumerableBinder<>))
                   .As(typeof (IEnumerableBinder<>));

            builder.RegisterModelBinderFor<UserPickedBinder, UserPicked>(true);
            builder.RegisterModelBinderFor<ObjectIdBinder, ObjectId>(false);
            builder.RegisterModelBinderForEnum<FilterType>();
           
            #endregion

            #region Users

            //Validators

            builder.RegisterType<ProfileModelVaildator>()
                .As<IValidator<ProfileModel>>();

            builder.RegisterType<InvitationModelValidator>()
                .As<IValidator<InvitationModel>>();
                

            builder.RegisterType<AddUserModelValidator>()
                .As<IValidator<AddUserModel>>();

            builder.RegisterType<GroupUploadModelValidator>()
                .As<IValidator<GroupUploadModel>>();

            //Maps

            builder.RegisterType<ProfileModelToUserMap>()
                .As<IMap<ProfileModel, User>>();

            builder.RegisterType<ProfileModelToAvatarMap>()
                .As<IMap<ProfileModel, Avatar>>();

            builder.RegisterType<AddUserModelToUserMap>()
                .As<IMap<AddUserModel, User>>();

            builder.RegisterType<GroupingUserToTeamModelRowMap>()
                .As<IMap<IGrouping<string, User>, TeamRowModel>>();

            builder.RegisterType<UserInfoModelMap>()
                .As<IMap<TenantIdentity, UserInfoModel>>();

            //Binders

            builder.RegisterModelBinderFor<RegistrationModelBinder, RegistrationModel>(false);

            #endregion

            #region Feeds

            //Maps
            builder.RegisterType<FeedModelMap>()
             .As<IMap<Feed, FeedModel>>();

            builder.RegisterType<AlertFeedModelMap>()
                .As<IMap<Feed, AlertFeedModel>>();

            builder.RegisterType<ObjectiveFeedModelMap>()
                .As<IMap<Feed, ObjectiveFeedModel>>();

            builder.Register(ctx => ctx.Resolve<IMapper<Feed, AlertFeedModel>>())
                .Keyed<IMapper<Feed, FeedModel>>(FeedType.Alert);

            builder.Register(ctx => ctx.Resolve<IMapper<Feed, ObjectiveFeedModel>>())
                .Keyed<IMapper<Feed, FeedModel>>(FeedType.Objective);

            builder.Register(ctx => ctx.Resolve<IMapper<Feed, UpdateFeedModel>>())
                .Keyed<IMapper<Feed, FeedModel>>(FeedType.Update);

            builder.RegisterType<KudosModelMapper>()
                .Keyed<IMapper<Feed, FeedModel>>(FeedType.Kudos);

            builder.RegisterType<FeedUniversalMapper>()
                .As<IFeedUniversalMapper>();
            
            builder.RegisterType<UpdateFeedModelMap>()
                .As<IMap<Feed, UpdateFeedModel>>();

            #endregion

            #region Objective Create
            //Maps 
            builder.RegisterType<ObjectiveCreateModelToObjective>()
                .As<IMap<ObjectiveCreateModel, Objective>>();

            builder.RegisterType<ObjectiveCreateModelToObjectiveIcon>()
            .As<IMap<ObjectiveCreateModel, ObjectiveIcon>>();

            //Validators
            builder.RegisterType<CreateStep1Validator>()
                .As<IValidator<CreateStep1>>();

            builder.RegisterType<CreateStep2Validator>()
                .As<IValidator<CreateStep2>>();

            builder.RegisterType<CreateStep3Validator>()
                .As<IValidator<CreateStep3>>();

            builder.RegisterType<CreateStep4Validator>()
                .As<IValidator<CreateStep4>>();

            //Binders
            builder.RegisterModelBinderFor<ObjectiveCreateModelBinder, ObjectiveCreateModel>(false);
            builder.RegisterModelBinderFor<KeyResultModelBinder, Models.Objectives.KeyResultModel>(true);

            #endregion

            #region Objective Details

            //Maps
            builder.RegisterType<ObjectiveDetailsMap>()
                .As<IMap<Objective, ObjectiveDetailsModel>>();

            builder.RegisterType<KeyResultModelMap>()
                .As<IMap<KeyResult, Models.ObjectiveDetails.KeyResultModel>>();

            builder.RegisterType<ObjectiveDetailsMapper>()
                .As<IObjectiveDetailsMapper>();

            //Validators
            builder.RegisterType<KeyResultModelValidator>()
                .As<IValidator<Models.Objectives.KeyResultModel>>();

            builder.RegisterType<ActionItemModelValidator>()
                .As<IValidator<ActionItemModel>>();

            //binders
            builder.RegisterModelBinderForEnum<KeyResultUnit>();
            builder.RegisterModelBinderForEnum<ActionItemStatus>();

            #endregion

            #region My Company objectives
         
            builder.RegisterType<PageModelMapper<MonthYearPair>>()
                .Keyed<IPageModelMapper>(Resources.Views.MyCompanyObjectives.Index.DateRange);

            builder.RegisterType<PageModelMapper<string>>()
                .Keyed<IPageModelMapper>(Resources.Views.MyCompanyObjectives.Index.Person);

            builder.RegisterType<PageModelMapper<ObjectiveStatus>>()
                .Keyed<IPageModelMapper>(Resources.Views.MyCompanyObjectives.Index.Status);
                
            builder.RegisterGeneric(typeof (DefaultTitleFormatter<>))
                .As(typeof (ITitleFormatter<>));

            builder.RegisterType<MonthYearTitleFormatter>()
                .As<ITitleFormatter<MonthYearPair>>();

            #endregion

            #region My Feedbacks
            //Maps
            builder.RegisterType<FeedbackAttentionRequiredMap>()
                .As<IMap<Feedback, FeedbackAttentionRequired>>();

            builder.RegisterType<FeedbackYouRequestedMap>()
                .As<IMap<Feedback, FeedbackYouRequested>>();

            builder.RegisterType<FeedbackTheyRequestedMap>()
                .As<IMap<Feedback, FeedbackTheyRequested>>();

            builder.RegisterType<DetailsResponseModelMap>()
                .As<IMap<Response, DetailsResponseModel>>();

            builder.RegisterType<DetailsModelMap>()
                .As<IMap<Feedback, DetailsModel>>();

            builder.RegisterType<MyFeedbacks.Map.IndexModelMapper>()
                .As<MyFeedbacks.Map.IIndexModelMapper>();
            
            builder.RegisterType<FeedbackMap>()
                .As<IMap<FeedbackCreateModel, Feedback>>();

            builder.RegisterType<KudosToFeedMapper>()
                .As<IMapper<KudosCreateModel, Feed>>();

            //Validators
            builder.RegisterType<FeedbackCreateModelValidator>()
                .As<IValidator<FeedbackCreateModel>>();

            builder.RegisterType<FeedbackAttentionRequiredValidator>()
                .As<IValidator<FeedbackAttentionRequired>>();

            builder.RegisterType<KudosCreateModelValidator>()
                .As<IValidator<KudosCreateModel>>();

            #endregion 

            #region Company

            //Maps
            builder.RegisterType<GeneralTabMap>()
                .As<IMap<Company, GeneralTab>>();

            builder.RegisterType<Companies.Map.IndexModelMapper>()
                .As<Companies.Map.IIndexModelMapper>();

            builder.RegisterType<BadgeModelMapper>()
                .As<IBadgeModelMapper>();

            builder.RegisterType<BadgesTabMap>()
                .As<IMap<IEnumerable<Badge>, BadgesTab>>();

            builder.RegisterType<CompanyMap>()
                .As<IMap<GeneralTab, Company>>();

            builder.RegisterType<BadgeMap>()
                .As<IMap<BadgeModel, Badge>>();

            builder.RegisterType<ObjectivesFocusTabMapper>()
                .As<IObjectiveFocusTabMapper>();

            //Validators
            builder.RegisterType<GeneralTabValidator>()
                .As<IValidator<GeneralTab>>();

            builder.RegisterType<BadgeModelValidator>()
                .As<IValidator<BadgeModel>>();

            #endregion

            #region My Account
            //Maps
            builder.RegisterType<MyAccount.Map.IndexModelMap>()
                .As<IMap<User, MyAccount.IndexModel>>();

            builder.RegisterType<MyAccount.Map.UserMap>()
                .As<IMap<MyAccount.IndexModel, User>>();

            builder.RegisterType<MyAccount.Map.AvatarMap>()
                .As<IMap<MyAccount.IndexModel, Avatar>>();

            //Validation
            builder.RegisterType<MyAccount.Validation.IndexModelValidator>()
                .As<IValidator<MyAccount.IndexModel>>();

            #endregion

            #region My Preferences

            builder.RegisterType<MyPreferences.Map.IndexModelMap>()
             .As<IMap<User, MyPreferences.IndexModel>>();

            builder.RegisterType<NotificationSettingMap>()
                .As<IMap<MyPreferences.NotificationSettingModel, NotificationSetting>>();

            #endregion

            #region My Notifications

            builder.RegisterType<MyNotifications.Map.IndexModelMapper>()
                .As<IMapper<User, MyNotifications.IndexModel>>();

            builder.RegisterType<MyNotifications.Map.PopupModelMapper>()
                .As<IMapper<User, MyNotifications.PopupModel>>();

            #endregion

            #region My password

            builder.RegisterType<MyPassword.Validation.IndexModelValidator>()
                .As<IValidator<MyPassword.IndexModel>>();

            #endregion

            #region My Action items
            //Map
            builder.RegisterType<MyActionItems.Map.ActionItemModelMapper>()
                .As<MyActionItems.Map.IActionItemModelMapper>();

            builder.RegisterType<MyActionItems.Map.IndexModelMapper>()
                .As<MyActionItems.Map.IIndexModelMapper>();

            builder.RegisterType<MyActionItems.Map.ActionItemMapper>()
                .As<IMapper<MyActionItems.ActionItemModel, ActionItem>>();

            //Validation
            builder.RegisterType<MyActionItems.Validation.ActionItemModelValidator>()
                .As<IValidator<MyActionItems.ActionItemModel>>();

            #endregion 

            #region Performance Administrate
            //Maps
            builder.RegisterType<ReviewCycleModelMap>()
                .As<IMap<ReviewCycle, ReviewCycleModel>>();

            builder.RegisterType<PerformanceAdministrate.IndexModelMap>()
                .As<IMap<IEnumerable<ReviewCycle>, PerformanceAdministrate.IndexModel>>();
            #endregion

            #region Review Cycle creation

            //Map
            builder.RegisterType<ReviewCycleCreation.Map.QuestionMapper>()
                   .As<IMapper<ReviewCycleCreation.QuestionModel, Question>>();

            builder.RegisterType<ReviewCycleCreation.Map.QuestionSetsMapper>()
                   .As<IMapper<ReviewCycleCreation.ReviewCycleModel, IEnumerable<QuestionSet>>>();

            builder.RegisterType<ReviewCycleCreation.Map.ReviewCycleMapper>()
                   .As<IMapper<ReviewCycleCreation.ReviewCycleModel, ReviewCycle>>();

            //Validation
            builder.RegisterType<ReviewCycleCreation.Validation.CreateStep1Validator>()
                   .As<IValidator<ReviewCycleCreation.CreateStep1>>();

            builder.RegisterType<ReviewCycleCreation.Validation.QuestionModelValidator>()
                   .As<IValidator<ReviewCycleCreation.QuestionModel>>();

            builder.RegisterType<ReviewCycleCreation.Validation.CreateStep3Validator>()
                .As<IValidator<ReviewCycleCreation.CreateStep3>>();

            //Binders
            builder.RegisterModelBinderFor<ReviewCycleCreation.Binder.ReviewCycleModelBinder,ReviewCycleCreation.ReviewCycleModel>(false);
            builder.RegisterModelBinderFor
                <ReviewCycleCreation.Binder.QuestionSetsModelBinder, IEnumerable<ReviewCycleCreation.QuestionSetModel>>(
                    true);
                
            builder.RegisterModelBinderFor<ReviewCycleCreation.Binder.QuestionModelBinder, ReviewCycleCreation.QuestionModel>(true);

            builder.RegisterModelBinderForEnum<AnswerType>();
            builder.RegisterModelBinderForEnum<QuestionSetType>();
            builder.RegisterModelBinderForEnum<QuestionSetting>();
            builder.RegisterModelBinderForEnum<ReviewCycleVisibility>();

            #endregion

            #region My Reviews
            //Maps
            builder.RegisterType<MyReviews.Map.IndexModelMap>()
                   .As<IMap<IEnumerable<Review>, MyReviews.IndexModel>>();

            #endregion

            #region Cycle Details

            builder.RegisterType<CycleDetails.Map.IndexModelMapper>()
                   .As<CycleDetails.Map.IIndexModelMapper>();

            #endregion 

            #region Questionaire

            builder.RegisterType<Questionaire.Map.IndexModelMapper>()
                   .As<Questionaire.Map.IIndexModelMapper>();

            builder.RegisterType<Questionaire.Map.AnswersMapper>()
                   .As<IMapper<Questionaire.IndexModel, IEnumerable<Answer>>>();

            builder.RegisterType<Questionaire.Map.AnswerModelMapper>()
                   .As<IMapper<AnswersCollection, IEnumerable<AnswerModel>>>();

            #endregion

            #region Performance Summary

            builder.RegisterType<PerformanceSummary.Map.IndexModelMap>()
                   .As<IMap<ReviewCycleSummary,PerformanceSummary.IndexModel>>();

            builder.RegisterType<PerformanceSummary.Map.KudosModelMap>()
                   .As<IMap<Tuple<Kudos, Badge>, PerformanceSummary.KudosModel>>();

            builder.RegisterType<PerformanceSummary.Map.ObjectiveModelMap>()
                   .As<IMap<Objective, PerformanceSummary.ObjectiveModel>>();

            #endregion

            #region Teams

            builder.RegisterType<Teams.Map.TeamModelMapper>()
                   .As<Teams.Map.ITeamModelMapper>();

            #endregion

            #region Security

            builder.RegisterType<PrincipalProvider>()
                .As<IPrincipalProvider>()
                .SingleInstance();

            #endregion
            
            #region Controllers

            builder.Register((ctx, p) => new MyObjectivesController(
                                             ctx.ResolveKeyed<IObjectiveRepository>(FilterType.OwnedByCompany),
                                             ctx.Resolve<IMapper<Objective, ObjectiveSimpleModel>>(),
                                             ctx.Resolve<IResourceProvider>(),
                                             ctx.Resolve<IFilter<IOwnedByTeam>>(),
                                             ctx.Resolve<IFilter<IOwned>>(),
                                             ctx.Resolve<IFilter<IHasParticipants>>()));
                            

            #endregion 

            base.Load(builder);
        }
    }
}