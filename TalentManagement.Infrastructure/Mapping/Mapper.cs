﻿namespace TalentManagement.Infrastructure.Mapping
{
    class Mapper<TSource,TDestination> : IMapper<TSource,TDestination>
    {
        public Mapper(IMap<TSource,TDestination> map)
        {
            map.Configure(new MapperConfiguration<TSource, TDestination>());
        }

        public TDestination Map(TSource source)
        {
            return AutoMapper.Mapper.Map<TSource, TDestination>(source);
        }
    }
}
