﻿using System.ComponentModel;

namespace TalentManagement.Infrastructure.Converter
{
    class DefaultTypeConverter<TFrom,TTo> : ITypeConverter<TFrom,TTo>
    {
        public TFrom Convert(TTo to)
        {
            return (TFrom)TypeDescriptor.GetConverter(to).ConvertTo(to, typeof (TFrom));
        }

        public TTo ConvertBack(TFrom from)
        {
            return (TTo)TypeDescriptor.GetConverter(from).ConvertTo(from, typeof(TTo));
        }


        public bool IsDefault
        {
            get { return true; }
        }

        public object Convert(object to)
        {
            return Convert((TTo)to);
        }

        public object ConvertBack(object from)
        {
            return ConvertBack((TFrom) from);
        }
    }
}
