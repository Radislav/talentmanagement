﻿using System.Web.Mvc;
using System.Linq;
using MongoDB.Bson;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Validation;
using TalentManagement.Web.Models.ReviewCycleCreation;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Controllers
{
    public class ReviewCycleCreationController : Controller
    {
        private readonly IValidator<QuestionModel> _questionModelValidator;
        private readonly IReviewCycleRepository _reviewCycleRepository;
        private readonly IMapper<ReviewCycleModel, ReviewCycle> _reviewCycleMapper;
        private readonly IQuestionRepository _questionRepository;
        private readonly IMapper<QuestionModel, Question> _questionMapper;
        
        public ReviewCycleCreationController(IValidator<QuestionModel> questionModelValidator,
            IReviewCycleRepository reviewCycleRepository,
            IMapper<ReviewCycleModel,ReviewCycle> reviewCycleMapper,
            IQuestionRepository questionRepository,
            IMapper<QuestionModel,Question> questionMapper)
        {
            _questionModelValidator = questionModelValidator;
            _reviewCycleRepository = reviewCycleRepository;
            _reviewCycleMapper = reviewCycleMapper;
            _questionRepository = questionRepository;
            _questionMapper = questionMapper;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(new ReviewCycleModel());
        }

        [HttpPost]
        public ActionResult Index(ReviewCycleModel reviewCycleModel)
        {
            if (ModelState.IsValid && reviewCycleModel.IsFlowFinished)
            {
                var questions = reviewCycleModel.GetQuestionModels()
                                                .Select(qmodel => _questionMapper.Map(qmodel));

                _questionRepository.InsertQuestions(questions);
                
                _reviewCycleRepository.Insert(_reviewCycleMapper.Map(reviewCycleModel));
                return RedirectToAction("Index", "PerformanceAdministrate");
            }

            return PartialView("_CreateWizard",reviewCycleModel);
        }

        [HttpPost]
        public ActionResult AddQuestion(CreateStep2 step2, QuestionSetType setType)
        {
            ValidationResult validationResult = _questionModelValidator.ValidateInstance(step2.QuestionNew);
            if (validationResult.IsValid)
            {
                step2.AddQuestion(step2.QuestionNew, setType);    
            }
            return validationResult.IsValid ?  (ActionResult)PartialView("_CreateStep2", step2) : validationResult.AsJson();
        }

        [HttpPost]
        public ActionResult EditQuestion(CreateStep2 step2, QuestionSetType setType)
        {
            ValidationResult validationResult = _questionModelValidator.ValidateInstance(step2.QuestionEdit);
            if (validationResult.IsValid)
            {
                step2.EditQuestion(step2.QuestionEdit, setType);
            }
            return validationResult.IsValid ? (ActionResult)PartialView("_CreateStep2", step2) : validationResult.AsJson();
        }

        [HttpPost]
        public ActionResult DeleteQuestion(CreateStep2 step2, QuestionSetType setType, ObjectId questionId)
        {
            step2.DeleteQuestion(questionId, setType);
            return PartialView("_CreateStep2", step2);
        }

        [HttpPost]
        public ActionResult ShowEditModal(CreateStep2 step2, QuestionSetType setType, ObjectId questionId)
        {
            step2.QuestionEdit = step2.GetQuestion(setType, questionId);
            return PartialView("_CreateStep2", step2);
        }

        [HttpPost]
        public ActionResult AddIndividual(CreateStep3 step3Model)
        {
            step3Model.IndividualsPicker.AddCurrentUserToUsersPicked();
            return PartialView("_CreateStep3", step3Model);
        }

        [HttpPost]
        public ActionResult AddManager(CreateStep3 step3Model)
        {
            step3Model.ManagersPicker.AddCurrentUserToUsersPicked();
            return PartialView("_CreateStep3", step3Model);
        }
    }
}
