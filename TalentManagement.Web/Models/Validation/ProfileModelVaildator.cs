﻿using FluentValidation;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Validation;

namespace TalentManagement.Web.Models.Validation
{
    internal class ProfileModelVaildator : AbstractValidator<ProfileModel> , TalentManagement.Infrastructure.Validation.IValidator<ProfileModel>
    {
        public ProfileModelVaildator(IResourceProvider resourceProvider,IUserRepository userRepository)
        {
            RuleFor(m => m.Email)
                .NotEmpty();

            RuleFor(m => m.Email)
                .EmailAddress()
                .When(m => false == string.IsNullOrEmpty(m.Email));
            
            RuleFor(model => model.Email)
                .Must(email => null == userRepository.GetById(email))
                .When(m => false == string.IsNullOrEmpty(m.Email))
                .WithMessage(resourceProvider.ErrorEmailBusy);

            RuleFor(m => m.Password)
                .NotEmpty()
                .When(m => m.IsPageSelected);

            RuleFor(m => m.ConfirmPassword)
                .NotEmpty()
                .When(m => m.IsPageSelected);

            RuleFor(m => m.ConfirmPassword)
                .Equal(m => m.Password)
                .When(m => m.IsPageSelected)
                .WithMessage(resourceProvider.ErrorPasswordsMustMatch);

            RuleFor(m => m.ManagerEmail)
                .EmailAddress()
                .When(m => false == string.IsNullOrEmpty(m.ManagerEmail));

            RuleFor(m => m.FirstName)
                .NotEmpty();

        }

        public ValidationResult ValidateInstance(ProfileModel instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}