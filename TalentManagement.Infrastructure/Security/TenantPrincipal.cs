﻿using System.Linq;
using System.Security.Principal;

namespace TalentManagement.Infrastructure.Security
{
    public class TenantPrincipal : IPrincipal
    {
        private readonly TenantIdentity _identity;
        private readonly string[] _roles;

        public TenantPrincipal(TenantIdentity identity,string[] roles)
        {
            _identity = identity;
            _identity.IsAuthenticated = true;
            _roles = roles;
        }
        
        public IIdentity Identity
        {
            get { return _identity; }
        }

        public bool IsInRole(string role)
        {
            return _roles != null && _roles.Contains(role);
        }

        public TenantPrincipalModel AsModel()
        {
            return new TenantPrincipalModel
                       {
                           FirstName = _identity.FirstName,
                           IsAuthenticated = _identity.IsAuthenticated,
                           LastSignIn = _identity.LastSignIn,
                           Roles = _roles,
                           SecondName = _identity.SecondName,
                           UserName = _identity.Name
                       };
        }
    }
}
