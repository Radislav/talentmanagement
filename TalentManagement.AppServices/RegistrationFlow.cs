﻿using System;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure;
using TalentManagement.Infrastructure.Configuration;
using TalentManagement.Infrastructure.Email;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.AppServices
{
    internal class RegistrationFlow : IRegistrationFlow
    {
        private readonly IUserRepository _userRepository;
        private readonly IEmailSender _emailSender;
        private readonly IPasswordGenerator _passwordGenerator;
        private readonly IHashComputer _hashComputer;
        private readonly IAppConfiguration _appConfiguration;
        private readonly IUrlProvider _urlProvider;

        public RegistrationFlow(IUserRepository userRepository,
                            IEmailSender emailSender,
                            IPasswordGenerator passwordGenerator,
                            IHashComputer hashComputer,
                            IAppConfiguration appConfiguration,
                            IUrlProvider urlProvider)
        {
            _userRepository = userRepository;
            _emailSender = emailSender;
            _passwordGenerator = passwordGenerator;
            _hashComputer = hashComputer;
            _appConfiguration = appConfiguration;
            _urlProvider = urlProvider;
        }

        public ForgotPasswordResult ForgotPasswordFor(string email)
        {
            User user = _userRepository.GetById(email);

            if (user == null)
            {
                return ForgotPasswordResult.UserNotFounded;
            }

            string password = _passwordGenerator.GeneratePassword(_appConfiguration.MinPasswordLength);

            user.HashedPassword = _hashComputer.ComputeHash(password);

            _userRepository.Update(user);

            _emailSender.Send(user.Email, EmailType.ForgotPassword, new ForgotPasswordArgument
            {
                Email = user.Id,
                Password = password,
                LoginUrl = _urlProvider.LoginUrl
            });

            return ForgotPasswordResult.Ok;
        }

        public void Register(User user, string[] invitationEmails)
        {
            if (user == null)
            {
                throw new RegistrationException("Registration fails due the NULL user");
            }

            UserCreateResult userCreateResult = _userRepository.CreateUser(user);
            if (userCreateResult != UserCreateResult.Ok)
            {
                throw new RegistrationException(String.Format("Registration fails becuase email {0} is busy.",user.Email));
            }


            invitationEmails.Map(Invite);

        }
        

        public void Invite(string invitationEmail)
        {
            _emailSender.Send(invitationEmail, EmailType.Invitation, new InvitationArgument { InvitationUrl = _urlProvider.LoginUrl, HelpUrl = _urlProvider.HelpUrl });
        }
    }
}
