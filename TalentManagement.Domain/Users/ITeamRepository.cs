﻿using System.Collections.Generic;

namespace TalentManagement.Domain.Users
{
    public interface ITeamRepository : IRepository<Team>
    {
        IEnumerable<Team> GetTeams(User manager);
        IEnumerable<Team> GetTeams(string managerId, string tenant);
        IEnumerable<Team> GetTeams(string tenant,bool includeUnassigned);
        bool HasTeams(User manager);
        Team GetUnassignedTeam(string tenant);
    }
}
