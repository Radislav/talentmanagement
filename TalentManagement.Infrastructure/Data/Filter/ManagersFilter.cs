﻿using System;
using System.Linq;
using TalentManagement.Domain.Users;


namespace TalentManagement.Infrastructure.Data.Filter
{
    class ManagersFilter : FilterBase<User>
    {
        public override Domain.IFilterContext FilterContext
        {
            get { return new EmptyFilterContext(); }
            set { }
        }

        public override Domain.IFilter<User> Filter(Domain.IFilterContext[] filterContexts)
        {
             var possibleManagers = Data.Where(user => false == String.IsNullOrEmpty(user.ManagerEmail))
                                        .Select(user => user.ManagerEmail)
                                        .ToArray();

            Data = Data.Where(user => possibleManagers.Contains(user.Id));
            return this;
        }
    }
}
