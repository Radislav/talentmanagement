﻿using FluentValidation;
using TalentManagement.Web.Models.Validation;
using TalentManagementValidation = TalentManagement.Infrastructure.Validation;

namespace TalentManagement.Web.Models.Objectives.Validation
{
    internal class CreateStep4Validator : AbstractValidator<CreateStep4>, TalentManagementValidation.IValidator<CreateStep4>
    {
        public CreateStep4Validator()
        {
            RuleFor(m => m.Image)
                .ImageFile()
                .FileSize(Constants.Megabyte);

            
        }

        public TalentManagementValidation.ValidationResult ValidateInstance(CreateStep4 instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}