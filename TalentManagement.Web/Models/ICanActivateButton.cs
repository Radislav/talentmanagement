﻿namespace TalentManagement.Web.Models
{
    public interface ICanActivateButton
    {
        string ActiveButton { get; set; }
    }
}