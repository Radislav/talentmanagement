﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using TalentManagement.Domain;
using TalentManagement.Infrastructure.Configuration;
using TalentManagement.Infrastructure.Validation;

namespace TalentManagement.Infrastructure.Data.Configuration
{
    class Registrar : IRegistrar
    {
        private static MongoServer _mongoServer;
        private static Dictionary<string, MongoDatabase> _databases;
        readonly List<IEntityReadConfiguration> _configurations = new List<IEntityReadConfiguration>();
        private readonly IAppConfiguration _appConfiguration;

        public Registrar(IAppConfiguration appConfiguration)
        {
            _databases = new Dictionary<string, MongoDatabase>();
            _appConfiguration = appConfiguration;
            _mongoServer = new MongoClient(appConfiguration.GetConnectionString("mongoServer")).GetServer();
        }

     
        public void Add<TEntity>(IEntityConfiguration<TEntity> configuration)
        {
            configuration.ValidateConfiguration();

            if (_configurations.Any(c => c.IsOfType(typeof(TEntity))))
            {
              throw new EntityException(String.Format("The configuration for type {0} is already added.",typeof(TEntity)));   
            }


            if (configuration.PropertyConfigurations.Any())
            {
                RegisterIgnoredProperties(configuration);
            }

            _configurations.Add(configuration);
        }

        public void RegisterIgnoredProperties<TEntity>(IEntityConfiguration<TEntity> configuration)
        {
            string[] ignoredProperties = configuration.PropertyConfigurations.Where(pcfg => pcfg.IsIgnored)
                                                                            .Select(pcfg => pcfg.Name)
                                                                            .ToArray();
            BsonClassMap.RegisterClassMap<TEntity>(cm =>
            {
                cm.AutoMap();
                ignoredProperties.Map(cm.UnmapProperty);
            });
        }

        public IEntityReadConfiguration Get<TEntity>()
        {
            return _configurations.SingleOrDefault(config => config.IsOfType(typeof (TEntity)));
        }

        public object GetDatabase(IEntityReadConfiguration entityConfiguration)
        {
            string dbName = _appConfiguration.GetDbName(entityConfiguration.DataSourceType);
            if (false == _databases.ContainsKey(dbName))
            {
                _databases[dbName] = _mongoServer.GetDatabase(dbName);
            }

            return _databases[dbName];
        }

        public object GetWriteRules(IEntityReadConfiguration entityConfiguration)
        {
            return entityConfiguration.SafeMode ? new WriteConcern {Journal = true} : new WriteConcern();
        }


        public void ValidateEntity<TEntity>(TEntity entity)
        {
            var readConfiguration = Get<TEntity>();
            if (readConfiguration == null)
            {
                throw new EntityException(String.Format("Data confgiration for type {0} was not founded",typeof(TEntity)));
            }
            ValidationResult validationResult = readConfiguration.ValidateInstance(entity);
            if (false == validationResult.IsValid)
            {
                throw new EntityException(String.Format("Entity of type {0} does not satisfy the rules defined in enity data map",typeof(TEntity)),
                    validationResult.ErrorMessages);   
            }
        }
    }
}
