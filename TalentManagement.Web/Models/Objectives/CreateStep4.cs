﻿using System;
using System.Web;
using System.Linq;
using System.Web.Script.Serialization;
using MongoDB.Bson;
using Resource = TalentManagement.Resources.Models.Objectives.CreateStep4;

namespace TalentManagement.Web.Models.Objectives
{
    public class CreateStep4 : WizardPage , IHasCachedImage
    {
        private HttpPostedFileBase _image;
        private HttpPostedFileWrapper _fileWrapper;

        public ObjectId CurrentIconId { get; set; }

        [LocalizedDisplayName("ObjectiveIcon",typeof(Resource))]
        public HttpPostedFileBase Image
        {
            get { return _image; }
            set
            {
                _image = value;
                _fileWrapper = new HttpPostedFileWrapper(value);
            }
        }
        
        public byte[] IconBytes
        {
            get { return _fileWrapper.GetSafe(w => w.Bytes); }
        }

        public string IconContentType
        {
            get { return _fileWrapper.GetSafe(w => w.ContentType); }
        }
       
        private ObjectId[] _icons;

        public ObjectId[] Icons
        {
            get { return _icons ?? new ObjectId[0]; }
            set
            {
                _icons = value;
                _iconsJson = new JavaScriptSerializer().Serialize(value.Select(s => Convert.ToString(s)).ToArray());
            }
        }

        private string _iconsJson;
        public string IconsJson
        {
            get { return _iconsJson; }
            set
            {
                _iconsJson = value;
                if (value != null)
                {
                    _icons = new JavaScriptSerializer().Deserialize<string[]>(value).Select(str => new ObjectId(str)).ToArray();    
                }
            }
        }

        public override int NextStep
        {
            get { return 3; }
        }

        public override int ThisStep
        {
            get { return 3; }
        }

        public string GenerateCacheKey()
        {
            return "ObjectiveCreateNewIcon";
        }
    }
}