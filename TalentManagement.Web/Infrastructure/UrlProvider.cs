﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MongoDB.Bson;
using TalentManagement.Infrastructure;

namespace TalentManagement.Web.Infrastructure
{
    internal class UrlProvider : IUrlProvider
    {
        private string _url;
        
        protected string ActivationUrl
        {
            get { return Generate("", "Activation").GetUrl(); }
        }
        
        public string HelpUrl
        {
            get { return Generate("About", "Login").GetUrl(); }
        }


        public string LoginUrl
        {
            get { return Generate("Index", "Login").GetUrl(); }
        }

        public string FeedbacksUrl
        {
            get { return Generate("Index", "MyFeedbacks").GetUrl(); }
        }


        public string GetActivationUrl(string code)
        {
            return string.Format("{0}/{1}", ActivationUrl, code);
        }

        public string Decode(string url)
        {
            return HttpUtility.UrlDecode(url);
        }
     
        public IUrlProvider Generate(string action, string controller, IDictionary<string, object> routes)
        {
            _url =  new UrlHelper(HttpContext.Current.Request.RequestContext)
                             .Action(action, controller, new RouteValueDictionary(routes), "http", HttpContext.Current.Request.Url.DnsSafeHost);
            return this;
        }

        public IUrlProvider Generate(string action, string controller)
        {
            Generate(action, controller,new Dictionary<string, object>());
            return this;
        }

        public string GetUrl()
        {
            return _url;
        }

        public IUrlProvider ToVirtualPath()
        {
            _url = new Uri(_url).AbsolutePath;
            return this;
        }

        public IUrlProvider Decode()
        {
            _url = HttpUtility.UrlDecode(_url);
            return this;
        }


        public string GetObjectiveDetailsUrl(ObjectId objectiveId)
        {
            return Generate("Index", "ObjectiveDetails", new Dictionary<string, object> {{"id", objectiveId}})
                        .GetUrl();
        }
    }
}