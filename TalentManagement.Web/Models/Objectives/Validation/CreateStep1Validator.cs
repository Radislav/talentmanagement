﻿using FluentValidation;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.Objectives.Validation
{
    internal class CreateStep1Validator : AbstractValidator<CreateStep1>, TalentManagement.Infrastructure.Validation.IValidator<CreateStep1>
    {
        public CreateStep1Validator()
        {
            RuleFor(m => m.Name)
                .NotEmpty();

            RuleFor(m => m.Description)
                .NotEmpty();

            RuleFor(m => m.Due)
                .NotEmpty()
                .DateTime();
        }

        public TalentManagement.Infrastructure.Validation.ValidationResult ValidateInstance(CreateStep1 instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}