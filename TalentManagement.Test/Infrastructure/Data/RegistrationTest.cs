﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TalentManagement.Domain;
using TalentManagement.Infrastructure.Data;

namespace TalentManagement.Test.Infrastructure.Data
{
    [TestClass]
    public class RegistrationTest : MockedTestClass<IUnitOfWork>
    {
        [TestMethod]
        [ExpectedException(typeof(EntityException))]
        public void Resolve_UnitOfWork_should_fail_if_called_for_not_registered_entity()
        {
            Container.Resolve<IUnitOfWork>().AsQueryable<Comment>().ToArray();
        }
    }
}
