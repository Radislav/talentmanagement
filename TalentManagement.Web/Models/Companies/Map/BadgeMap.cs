﻿using TalentManagement.Domain.Companies;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Companies.Map
{
    internal class BadgeMap : IMap<BadgeModel,Badge>
    {
        public void Configure(IMapperConfiguration<BadgeModel, Badge> configuration)
        {
            configuration.ForMember(d => d.Description, s => s.Description);
            configuration.ForMember(d => d.Image, s => s.ImageBytes);
            configuration.ForMember(d => d.ImageType, s => s.ImageType);
            configuration.ForMember(d => d.Id, s => s.BadgeId);
            configuration.ForMember(d => d.IsEnabled, s => s.IsEnabled);
        }
    }
}