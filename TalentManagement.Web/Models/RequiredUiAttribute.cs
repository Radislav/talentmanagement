﻿using System;

namespace TalentManagement.Web.Models
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property)]
    public class RequiredUiAttribute : Attribute
    {
    }
}