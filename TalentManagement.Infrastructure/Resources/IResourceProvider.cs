﻿using TalentManagement.Infrastructure.Email;

namespace TalentManagement.Infrastructure.Resources
{
    public interface IResourceProvider
    {
        string GetEmailBody(EmailType emailType);
        string GetEmailSubject(EmailType emailType);

        string GroupUploadFileNotValid { get; }
        string GroupUploadRowWarning(int rowIndex, string warning);

        string ErrorEmailNotFounded { get; }
        string ErrorEmailInvalid { get; }
        string ErrorEmailBusy { get; }
        string ErrorInvalidUserOrPassword { get; }
        string ErrorLoginsAttemptReached { get; }
        string ErrorPasswordRequired { get; }
        string ErrorPasswordsMustMatch { get; }
        string ErrorUserShouldBeObjectiveContributor { get; }
        string ErrorAtLeastOneUser { get; }
        string ErrorPasswordInvalid { get; }
        string ErrorDateStartAfterDateEnd(string dateStartTitle, string dateEndTitle);
     
        string UiAppendixEmail { get; }
        string UiNextStep { get; }

        string GetMimeType(string fileType);

        string ObjectiveDetailsProgressUpdated(string objectiveName, decimal progress);
        string ObjectiveDetailsCompleted { get; }

        string MyCompanyObjectivesDue { get; }
        string MyCompanyObjectiveGoto { get; }
        string MyCompanyObjectiveDateRange { get; }
        string MyCompanyObjectivePerson { get; }
        string MyCompanyObjectiveStatus { get; }
        string MyCompanyObjectiveCurrentYear { get; }
        string MyCompanyObjectivePreviousYear { get; }
        string MyCompanyObjectiveAllYears { get; }

        string MyObjectivesTop { get; }
        string MyObjectivesTeam { get; }
        string MyObjectivesPersonal { get; }
        string MyObjectivesContribute { get; }

        string MyObjectivesNoTop { get; }
        string MyObjectivesNoTeam { get; }
        string MyObjectivesNoPersonal { get; }
        string MyObjectivesNoContribute { get; }

        /// <summary>
        /// {0} is asking you to provide feedback on: "{1}"
        /// </summary>
        string FeedbackTheyRequestedTitle { get; }
        string FeedbackActiveButton { get; }
        /// <summary>
        /// You are asking {0} to provide feedback on: "{1}"
        /// </summary>
        string FeedbackYourRequestedTitle { get; }
        string FeedbackYourRequestedResponse { get; }
        string FeedbackAttentionRequired { get; }
        string FeedbackYouRequested { get; }
        string FeedbackTheyRequested { get; }
        string FeedbackDetails { get; }
        /// <summary>
        /// You provided feedback on: "{0}"
        /// </summary>
        string FeedbackYouProvidedFeedback { get; }

        string MyActionItemsRelated { get; }
        string MyActionItemsNotRelated { get; }

        string ReviewCycleDateStart { get; }
        string ReviewCycleDateEnd { get; }
        string ReviewCyclePearsQuestions { get; }
        string ReviewCycleSelfQuestions { get; }
        string ReviewCycleSummaryQuestions { get; }
        string ReviewCycleQuestion { get; }
        string ReviewCycleAddInvidivual { get; }
        string ReviewCycleAddManager { get; }
        string ReviewCycleHasQuestions { get; }
        string ReviewCycleNoQuestions { get; }

        string MyReviewsRequiredAttention { get; }
        string MyReviewsYouRequested { get; }
        string MyReviewsTheyRequested { get; }

        string CycleDetailsSelectCycle { get; }
        string CycleDetailsProgress { get; }

        string QuestionnaireAnswersSaved { get; }

        string PerformanceSummaryKudosThankingFormat { get; }

        string PeopleNotManager { get; }
        string PeopleMarkAsManager { get; }


        string Enable { get; }
        string Disable { get; }
        string Today { get; }
        string Yesterday { get; }
        string Due { get; }
        string Created { get; }
    }
}
