﻿using System.Web.Mvc;
using System.Web.Mvc.Html;
using MongoDB.Bson;

namespace TalentManagement.Web.Infrastructure.Navigation
{
    public static class UrlExtensions
    {
        public static string ObjectiveDetails(this UrlHelper urlHelper,ObjectId objectiveId)
        {
            return urlHelper.Action("Index", "ObjectiveDetails", new {id = objectiveId});
        }

        public static MvcHtmlString ObjectiveDetailsLink(this HtmlHelper html,string linkText, ObjectId objectiveId)
        {
            return ObjectiveDetailsLink(html, linkText, objectiveId, null);
        }

        public static MvcHtmlString ObjectiveDetailsLink(this HtmlHelper html, string linkText, ObjectId objectiveId, object htmlAttributes)
        {
            return html.ActionLink(linkText, "Index", "ObjectiveDetails", new { id = objectiveId },htmlAttributes);
        }
    }
}