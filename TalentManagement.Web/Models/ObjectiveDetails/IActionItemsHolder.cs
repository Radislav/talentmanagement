﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace TalentManagement.Web.Models.ObjectiveDetails
{
    public interface IActionItemsHolder
    {
        ObjectId Id { get; set; }
        ActionItemModel NewActionItem { get; set; }
        List<ActionItemModel> ActionItems { get; set; }
    }
}
