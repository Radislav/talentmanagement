﻿using System.Configuration;

namespace TalentManagement.Web.Infrastructure.Bundles
{
    public class ContentCollection : ConfigurationElementCollection
    {
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        public ContentElement this[int index]
        {
            get { return (ContentElement)BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                    BaseRemoveAt(index);
                BaseAdd(index, value);
            }
        }

        public void Add(ContentElement element)
        {
            BaseAdd(element);
        }

        public void Clear()
        {
            BaseClear();
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ContentElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ContentElement)element).Url;
        }

        public void Remove(ContentElement element)
        {
            BaseRemove(element.Url);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }
    }
}