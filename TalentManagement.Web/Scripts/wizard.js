﻿var wizard =
    {
        initialize: function (wizardContext) {

            $("#tabs").tabs({
                disabled: [1, 2, 3, 4, 5],
                show: function (event, ui) {
                    $('#CurrentStep').val(ui.index);
                }
            });

            var currentStep = wizardContext.CurrentStep;
            var wizardPages = wizardContext.Pages;

            var previousStep = 0;
            //enable previous steps
            while (previousStep <= currentStep) {
                $("#tabs").tabs("enable", previousStep);
                previousStep = previousStep + 1;
            }
            $("#tabs").tabs("select", currentStep);

            $.each(wizardPages, function (idx, page) {
                //enable next step if needed
                if (currentStep == page.ThisStep && page.IsReadyToGo == true) {
                    $("#tabs").tabs("enable", page.NextStep);
                    $("#tabs").tabs("select", page.NextStep);
                }
            });

            $('[name="previous-step"]').off('click').on('click', function () {
                $("#tabs").tabs("select", $('#CurrentStep').val() - 1);
            });
        }
    };
