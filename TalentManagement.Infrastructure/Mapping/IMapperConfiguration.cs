﻿using System;
using System.Linq.Expressions;

namespace TalentManagement.Infrastructure.Mapping
{
    public interface IMapperConfiguration<TSource, TDestination>
    {
        IMapperConfiguration<TSource, TDestination> ForMember(string property, Func<TSource, object> resolver);

        IMapperConfiguration<TSource, TDestination> ForMember(Expression<Func<TDestination, object>> destinationMember, Func<TSource, object> resolver);

        IMapperConfiguration<TSource, TDestination> Ignore(Expression<Func<TDestination, object>> destinationMember);

        IMapperConfiguration<TSource, TDestination> MapFrom<TMember>(Expression<Func<TSource, TMember>> sourceMember, Expression<Func<TDestination, object>> destinationMember);

        IMapperConfiguration<TSource, TDestination> UseValue<TValue>(Expression<Func<TDestination, object>> destinationMember,TValue value);

        IMapperConfiguration<TSource, TDestination> BeforeMap(Action<TSource, TDestination> beforeFunction);

        IMapperConfiguration<TSource, TDestination> AfterMap(Action<TSource, TDestination> afterFunction);
    }
}
