﻿using System.Collections.Generic;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Companies.Map
{
    public interface IObjectiveFocusTabMapper : IMapper<IEnumerable<Objective>,ObjectivesFocusTab>
    {
        ObjectivesFocusTab Map(IEnumerable<Objective> focusedObjectives, int filterByYear, string groupBy);
    }
}
