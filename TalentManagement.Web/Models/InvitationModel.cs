﻿using System.Linq;
using ModelResources = TalentManagement.Resources.Models.InvitationModel;

namespace TalentManagement.Web.Models
{
    public class InvitationModel : WizardPage 
    {
        private EmailAppendixModel _emailAppendixModel;

        public InvitationModel()
        {
            _emailAppendixModel = new EmailAppendixModel(_userEmail);
        }

        private string _userEmail;
        public string UserEmail
        {
            get { return _userEmail; }
            set
            {
                _userEmail = value;
                _emailAppendixModel = new EmailAppendixModel(_userEmail);
             }
        }

        [LocalizedDisplayName("Email1", typeof(ModelResources))]
        [Watermark("Colleague",typeof(ModelResources))]
        public string Email1 { get; set; }

        [LocalizedDisplayName("Email2", typeof(ModelResources))]
        [Watermark("Colleague", typeof(ModelResources))]
        public string Email2 { get; set; }

        [LocalizedDisplayName("Email3", typeof(ModelResources))]
        [Watermark("Colleague", typeof(ModelResources))]
        public string Email3 { get; set; }

        [LocalizedDisplayName("Email4", typeof(ModelResources))]
        [Watermark("Colleague", typeof(ModelResources))]
        public string Email4 { get; set; }

        [LocalizedDisplayName("Email5", typeof(ModelResources))]
        [Watermark("Colleague", typeof(ModelResources))]
        public string Email5 { get; set; }

        public string[] Emails
        {
            get
            {
                string[] emails = new[]
                                      {
                                          Email1, Email2, Email3, Email4, Email5
                                      };
                return emails.Where(emailLocal => false == string.IsNullOrEmpty(emailLocal))
                             .Select(emailLocal => _emailAppendixModel.CreateEmail(emailLocal))
                             .ToArray();
            }
        }

        public string EmailAppendix { get { return _emailAppendixModel.EmailAppendix; }
        }

        public override  int NextStep
        {
            get { return (int)RegistrationStep.Finish; }
        }

        public override  int ThisStep
        {
            get { return (int)RegistrationStep.Invitation; }
        }
    }
}