﻿using System.Diagnostics.Contracts;
using MongoDB.Bson.Serialization.Attributes;

namespace TalentManagement.Domain
{
    public class EntityWithTypedId<TId> : IHasId<TId>
    {
        private readonly IdHolderComparer<TId> _holderComparer;
   
        [ContractInvariantMethod]
        private void Invariant()
        {
            Contract.Invariant(_holderComparer != null);
        }
     
        public EntityWithTypedId() 
        {
            _holderComparer = new IdHolderComparer<TId>();
        }

        public EntityWithTypedId(TId id) 
        {
            _holderComparer = new IdHolderComparer<TId>();
            Id = id;
        }

        [BsonId]
        public  TId Id
        {
            get;
            internal set;
        }
        
        public override bool Equals(object @object)
        {
            return _holderComparer.Equals(this, @object as EntityWithTypedId<TId>);
         
        }

        public override int GetHashCode()
        {
            return _holderComparer.GetHashCode(this);
        }
        
        public static bool operator ==(EntityWithTypedId<TId> x, EntityWithTypedId<TId> y)
        {
            return ReferenceEquals(x, null) ? ReferenceEquals(y, null) : x.Equals(y);
        }

        public static bool operator !=(EntityWithTypedId<TId> x, EntityWithTypedId<TId> y)
        {
            return !(x == y);
        }

        public bool IsNew
        {
            get { return Id.IsDefault(); }
        }
    }
}
