﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TalentManagement.Domain;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Web.Infrastructure;
using TalentManagement.Infrastructure.Data;

namespace TalentManagement.Web.Models.ReviewCycleCreation
{
    public class CreateStep3 : WizardPage
    {
        public CreateStep3()
        {
            IUsersPickedProvider usersPickedProvider = DependencyResolver.Current.GetService<IUsersPickedProvider>();
            IResourceProvider resourceProvider = DependencyResolver.Current.GetService<IResourceProvider>();
            IUserRepository userRepository = DependencyResolver.Current.GetService<IUserRepository>();
            IFilter companyFilter = DependencyResolver.Current.GetService<IFilter>(FilterType.OwnedByCompany);
            IFilter managersFilter = DependencyResolver.Current.GetService<IFilter>(FilterType.Managers);

            IEnumerable<User> managers = userRepository.ApplyFilter(companyFilter)
                                                       .ApplyFilter(managersFilter); 
                
            IndividualsPicker = new UserPickerControl
            {
                Users = usersPickedProvider.GetUsersToPick().ToList(),
                Title = resourceProvider.ReviewCycleAddInvidivual,
                Prefix = "IndividualsPicker"
            };
            ManagersPicker = new UserPickerControl
            {
                Users = usersPickedProvider.GetUsersToPick(managers,"Managers").ToList(),
                Title = resourceProvider.ReviewCycleAddManager,
                Prefix = "ManagersPicker"
            };
        }

        public UserPickerControl IndividualsPicker { get; set; }
        public UserPickerControl ManagersPicker { get; set; }

        [LocalizedDisplayName("ReviewEveryone",typeof(Resources.Models.ReviewCycleCreation.CreateStep3))]
        public bool ReviewEveryone { get; set; }
        
        public override int NextStep
        {
            get { return 2; }
        }

        public override int ThisStep
        {
            get { return 2; }
        }
    }
}