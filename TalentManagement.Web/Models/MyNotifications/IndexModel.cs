﻿namespace TalentManagement.Web.Models.MyNotifications
{
    public class IndexModel
    {
        public NotificationGroupModel[] NotificationGroups { get; set; }
    }
}