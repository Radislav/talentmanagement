﻿using System;
using System.Collections.Generic;
using Autofac;

namespace TalentManagement.Infrastructure.Session
{
    class Session : ISession
    {
        private readonly ILifetimeScope _currentScope;
        private bool _disposed;

        public Session(ILifetimeScope container)
        {
            _currentScope = container;
            _disposed = false;
        }

        public T Instance<T>()
        {
            return _currentScope.Resolve<T>();
        }

        public T Instance<T>(object key)
        {
            return _currentScope.ResolveKeyed<T>(key);
        }

        public IEnumerable<TService> Instances<TService>()
        {
            return _currentScope.Resolve<IEnumerable<TService>>();
        }

        public T InstanceOptional<T>(object key)
            where T : class
        {
            return _currentScope.ResolveOptionalKeyed<T>(key);
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                _currentScope.Dispose();
                _disposed = true;
            }
        }
      
    }
}
