﻿using TalentManagement.Domain.Feeds;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Feeds.Map
{
    class AlertFeedModelMap :  IMap<Feed,AlertFeedModel>
    {
        private readonly IMapper<Feed, FeedModel> _baseMapper;

        public AlertFeedModelMap(IMapper<Feed, FeedModel> baseMapper)
        {
            _baseMapper = baseMapper;
        }

        public void Configure(IMapperConfiguration<Feed, AlertFeedModel> configuration)
        {
            FeedModel feedModel = null;

            configuration.BeforeMap((feed, model) => feedModel = _baseMapper.Map(feed));
            configuration.ForMember(destination => destination.Properties,source => feedModel.Properties);
            configuration.ForMember(destination => destination.CommentSection, source => feedModel.CommentSection);
            configuration.ForMember(destination => destination.Text,source => feedModel.GetProperty("text"));
        }
    }
}