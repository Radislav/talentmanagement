﻿$(function () {
    validation.show();
});

var validation = {
    show: function() {
        $('.tipsy').remove();
        $('.input-validation-error, .in_error').each(function() {
            $($(this)).tipsy({
                gravity: 'w'
            });

        });

        $('.field-validation-error[data-content]').each(function() {
            $($(this)).tipsy({
                trigger: 'manual',
                gravity: 'w'
            }).tipsy('show');
        });
    },
    hideJsonErrors: function (containerSelector) {
        $(containerSelector).find('input,textarea')
            .removeClass('in_error')
            .removeClass('in_submitted')
            .removeAttr('original-title');
    },
showJsonErrors: function (data, containerSelector, prefix) {
        $(containerSelector).find('input,textarea')
            .removeClass('in_error')
            .removeClass('in_submitted')
            .removeAttr('original-title');

        $.each(data.Errors, function () {
            var elementToVerify = $(containerSelector).find('#' + this.PropertyName).length == 0 ?
                                                        $(containerSelector).find('#' + prefix + '_' + this.PropertyName) :
                                                        $(containerSelector).find('#' + this.PropertyName);
            elementToVerify.addClass('in_error')
                                       .removeClass('in_submitted')
                                       .attr('original-title', this.ErrorMessage);

            validation.show();
        });
    },
    showSummaryErrors: function (data, containerSelector) {  
        var container = $(containerSelector).find("[data-valmsg-summary=true]"),
            list = container.find("ul");

        if (list && list.length && data.Errors.length) {
            list.empty();
            container.addClass("validation-summary-errors").removeClass("validation-summary-valid");

            $.each(data.Errors, function () {
                $("<li />").html(this.ErrorMessage).appendTo(list);
            });
        }
    },
    hideSummaryErrors: function (containerSelector) { 
        var container = $(containerSelector);

        if (container.length) {
            container.addClass("field-validation-valid").removeClass("field-validation-error");
            container.find('ul').empty();
        }
    }
};
