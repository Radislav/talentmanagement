﻿namespace TalentManagement.Infrastructure.Security
{
    public enum LoginResult
    {
        None,
        InvalidUserOrPassword,
        LoginsAttemptReached,
        Ok
    }
}
