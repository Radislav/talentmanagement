﻿using System;
using System.Collections.Generic;

namespace TalentManagement.Infrastructure.Session
{
    public interface ISession : IDisposable
    {
        TService Instance<TService>();
        TService Instance<TService>(object key);
        IEnumerable<TService> Instances<TService>();
        T InstanceOptional<T>(object key) where T : class;
    }
}
