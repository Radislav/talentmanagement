﻿using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Cache;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Web.Models
{
    internal class UsersPickedProvider : IUsersPickedProvider
    {
        private readonly IMapper<User, UserPicked> _userPickedMapper;
        private readonly IUserRepository _userRepository;
        private readonly IFilter<IOwnedByCompany> _companyFilter;
        private readonly IGlobalCache _globalCache;
        private readonly string _cacheKey;

        public UsersPickedProvider(IMapper<User, UserPicked> userPickedMapper,
                                  IUserRepository userRepository,
                                  IFilter<IOwnedByCompany> companyFilter,
                                  IGlobalCache globalCache,
                                  IPrincipalProvider principalProvider)
        {
            _userPickedMapper = userPickedMapper;
            _userRepository = userRepository;
            _companyFilter = companyFilter;
            _globalCache = globalCache;
            _cacheKey = string.Format("GetUsersToPick_{0}", principalProvider.GetTenant());
        }

        public IEnumerable<UserPicked> GetUsersToPick()
        {
            return _globalCache.GetOrRefresh(_cacheKey, () => _userRepository.ApplyFilter(_companyFilter)
                                                     .Select(u => _userPickedMapper.Map(u)));

        }

          public IEnumerable<UserPicked> GetUsersToPick(IFilter customFilter)
        {
            return _globalCache.GetOrRefresh(string.Format("GetUsersToPick_{0}", customFilter), () => _userRepository.ApplyFilter(customFilter)
                                                     .Select(u => _userPickedMapper.Map(u)));

        }


          public IEnumerable<UserPicked> GetUsersToPick(IEnumerable<User> customSource,string key)
          {
              return _globalCache.GetOrRefresh(string.Format("GetUsersToPick_{0}", key), () => customSource.Select(u => _userPickedMapper.Map(u)));
          }
    }
}