﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18034
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TalentManagement.Resources.Models.ReviewCycleCreation {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class CreateStep2 {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CreateStep2() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("TalentManagement.Resources.Models.ReviewCycleCreation.CreateStep2", typeof(CreateStep2).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create question.
        /// </summary>
        public static string CreateQuestion {
            get {
                return ResourceManager.GetString("CreateQuestion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit questino.
        /// </summary>
        public static string EditQuestion {
            get {
                return ResourceManager.GetString("EditQuestion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Has {0} questions.
        /// </summary>
        public static string HasQuestions {
            get {
                return ResourceManager.GetString("HasQuestions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No questions created.
        /// </summary>
        public static string NoQuestions {
            get {
                return ResourceManager.GetString("NoQuestions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pears questions.
        /// </summary>
        public static string PearsQuestions {
            get {
                return ResourceManager.GetString("PearsQuestions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Question.
        /// </summary>
        public static string Question {
            get {
                return ResourceManager.GetString("Question", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Self questions.
        /// </summary>
        public static string SelfQuestions {
            get {
                return ResourceManager.GetString("SelfQuestions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Summary questions.
        /// </summary>
        public static string SummaryQuestions {
            get {
                return ResourceManager.GetString("SummaryQuestions", resourceCulture);
            }
        }
    }
}
