﻿namespace TalentManagement.Web.Models.Companies
{
    public class IndexModel : ICanActivateButton
    {
        public IndexModel()
        {
            GeneralTab = new GeneralTab();
            BadgesTab = new BadgesTab();
            ObjectivesFocusTab = new ObjectivesFocusTab();
        }

        public string ActiveButton { get; set; }
        public GeneralTab GeneralTab { get; set; }
        public BadgesTab BadgesTab { get; set; }
        public ObjectivesFocusTab ObjectivesFocusTab { get; set; }

        [LocalizedDisplayName("Title",typeof(Resources.Models.Companies.IndexModel))]
        public string Title { get; set; }
    }
}