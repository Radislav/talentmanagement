﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Domain.Objectives;
using Resource = TalentManagement.Resources.Models.ObjectiveDetails.ObjectiveDetailsModel;

namespace TalentManagement.Web.Models.ObjectiveDetails
{
    public class ObjectiveDetailsModel
    {
        public ObjectiveDetailsModel()
        {
            FeedsModel = new FeedsModel();
        }

        private const int MaxContributorsToShow = 4;

        public ObjectId Id { get; set; }
        public string Name { get; set; }

        [LocalizedDisplayName("Description", typeof(Resource))]
        public string Description { get; set; }

        [LocalizedDisplayName("Contributors", typeof(Resource))]
        public string[] Contributors { get; set; }

        public bool ExceedContributorsCount
        {
            get { return Contributors.GetSafe(c => c.Length) > MaxContributorsToShow; }
        }
        public string[] ContributorsToShow
        {
            get { return Contributors == null ? new string[0] : Contributors.Take(MaxContributorsToShow).ToArray(); }
        }

        private KeyResultModel[] _keyResults;
        public KeyResultModel[] KeyResults
        {
            get { return _keyResults; }
            set
            {
                _keyResults = value;
                if (_keyResults != null)
                {
                    for (int idx = 0; idx < _keyResults.Length; idx++)
                    {
                        _keyResults[idx].Index = idx;
                    }    
                }
            }
        }

        public MvcHtmlString[] KeyResultsTitles
        {
            get
            {
                if (KeyResults == null)
                {
                    return new MvcHtmlString[0];
                }
                return KeyResults.Select(kr => new MvcHtmlString(kr.Name))
                                 .ToArray();
            }
        }
        
        [LocalizedDisplayName("Due",typeof(Resource))]
        public string Due { get; set; }

        [LocalizedDisplayName("Progress", typeof(Resource))]
        public decimal Progress { get; set; }

        [LocalizedDisplayName("CreatedBy", typeof(Resource))]
        public string CreatedBy { get; set; }

        [LocalizedDisplayName("Visibility",typeof(Resource))]
        public ObjectiveVisibility Visibility { get; set; }
        
        public ObjectId Icon { get; set; }

        public FeedsModel FeedsModel { get; set; }

        public List<UserPicked> ContributorsPicked { get; set; }
       
    }
}