﻿using System;
using System.Diagnostics.Contracts;
using TalentManagement.Domain;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure
{
    public static class PrincipalProviderExtensions
    {
        public static IPrincipalProvider FillOWned(this IPrincipalProvider principalProvider, IOwned owned)
        {
            Contract.Requires<ArgumentNullException>(principalProvider != null);
            Contract.Requires<ArgumentNullException>(owned != null);
            Contract.Ensures(Contract.Result<IPrincipalProvider>() != null);

            owned.CreatedAt = DateTime.Now;
            owned.CreatedBy = principalProvider.GetUserName();
            owned.UpdatedAt = DateTime.Now;
            owned.UpdatedBy = principalProvider.GetUserName();

            return principalProvider;
        }

        public static IPrincipalProvider FillOWnedByCompany(this IPrincipalProvider principalProvider, IOwnedByCompany owned)
        {
            Contract.Requires<ArgumentNullException>(principalProvider != null);
            Contract.Requires<ArgumentNullException>(owned != null);
            Contract.Ensures(Contract.Result<IPrincipalProvider>() != null);

            owned.CompanyName = principalProvider.GetTenant();
            return principalProvider;
        }

        public static bool IsDefined(this IPicture picture)
        {
            return picture != null && picture.Image != null && picture.ImageType != null;
        }
    }
}
