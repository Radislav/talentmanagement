﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Web.Mvc;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Security;
using Resource = TalentManagement.Resources.Models.Objectives.CreateStep3;

namespace TalentManagement.Web.Models.Objectives
{
    public class CreateStep3 : WizardPage
    {
        public CreateStep3()
        {
            UserPickerModel = new UserPickerControl();
            AddMe = true;
        }

        [RequiredUi]
        [LocalizedDisplayName("Invitation",typeof(Resource))]
        public string Invitation { get; set; }

        [LocalizedDisplayName("AddMe", typeof(Resource))]
        public bool AddMe { get; set; }

        [LocalizedDisplayName("MyTeam", typeof(Resource))]
        public bool MyTeam { get; set; }

        public UserPickerControl UserPickerModel { get; set; }

        public IEnumerable<string> UsersPicked
        {
            get
            {
                Contract.Ensures(Contract.Result<IEnumerable<string>>() != null);

                List<string> usersPicked = new List<string>();
                IPrincipalProvider principalProvider = DependencyResolver.Current.GetService<IPrincipalProvider>();

                if (AddMe)
                {
                    usersPicked.Add(principalProvider.GetUserName());
    
                }
                if (MyTeam)
                {
                    IUserRepository userRepository = DependencyResolver.Current.GetService<IUserRepository>();
                    usersPicked.AddRange(userRepository.GetUsersForTenant(principalProvider.GetTenant())
                                                    .Select(u => u.Id));
                }

                usersPicked.AddRange(UserPickerModel.UsersPicked.Select(u => u.Id));    

                return usersPicked.Distinct();
            }
        }

        public override int NextStep
        {
            get { return 3; }
        }

        public override int ThisStep
        {
            get { return 2; }
        }
    }
}