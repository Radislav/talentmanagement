﻿using System.Xml.Linq;

namespace TalentManagement.Infrastructure.Serialization
{
    public interface IXmlSerializer<TEntity> : ISerializer<TEntity,XElement>
    {

    }
}
