﻿using TalentManagement.Domain.Feeds;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Feeds.Map
{
    public class UpdateFeedModelMap : IMap<Feed,UpdateFeedModel>
    {
        private readonly IMapper<Feed, FeedModel> _baseMapper;

        public UpdateFeedModelMap(IMapper<Feed, FeedModel> baseMapper)
        {
            _baseMapper = baseMapper;
        }
        public void Configure(IMapperConfiguration<Feed, UpdateFeedModel> configuration)
        {
            FeedModel feedModel = null;
            configuration.BeforeMap((feed, model) => feedModel = _baseMapper.Map(feed));
            configuration.ForMember(d => d.Properties, s =>feedModel.Properties);
            configuration.ForMember(d => d.Title, s => s.Title);
            configuration.ForMember(d => d.CommentSection, s => feedModel.CommentSection);
            
            configuration.ForMember(d => d.ScopeName, s => feedModel.GetProperty("ScopeName"));
            configuration.ForMember(d => d.ScopeId, s => feedModel.GetProperty("ScopeId"));
            configuration.ForMember(d => d.Text, s => feedModel.GetProperty("Text"));
        }
    }
}