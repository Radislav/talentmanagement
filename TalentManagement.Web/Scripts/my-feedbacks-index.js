﻿$(function () {

    initHeader();

    function initHeader() {
        Comments({
            containerSelector: '.feedback',
            holderId: 'feedbackTheyRequested_FeedbackId',
            handlerUrl: urls.addComment()
        });

        initUserPicker();
        initSend();
    }
    
    $('#activity_stats [data-toggle="buttons-radio"] .btn')
        .click(function () {
            $.ajax({
                url: urls.feedbacks() + '/' + $(this).attr('name'),
                type: 'GET',
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    $('#feedbacks').html(response);

                    initHeader();
                }
            });
        });
});

