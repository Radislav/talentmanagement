﻿using System.Web;
using System.Web.Mvc;
using FluentValidation.Attributes;
using MongoDB.Bson;
using TalentManagement.Web.Models.Companies.Validation;
using Resource = TalentManagement.Resources.Models.Companies.GeneralTab;

namespace TalentManagement.Web.Models.Companies
{
    [Validator(typeof(GeneralTabValidator))]
    public class GeneralTab
    {
        [LocalizedDisplayName("CompanyName", typeof(Resource))]
        public string CompanyName { get; set; }

        [LocalizedDisplayName("CompanyLogo", typeof(Resource))]
        public HttpPostedFileBase CompanyLogo { get; set; }
        
        [LocalizedDisplayName("Timezone", typeof(Resource))]
        public string SelectedTimezone { get; set; }

        public SelectListItem[] Timezones { get; set; }
        public ObjectId LogoId { get; set; }
        public ObjectId CompanyId { get; set; }
        public bool Show { get; set; }
    }
}