﻿using MongoDB.Bson;
using TalentManagement.Domain.Objectives;
using Resource = TalentManagement.Resources.Models.Objectives.KeyResultModel;

namespace TalentManagement.Web.Models.Objectives
{
    public class KeyResultModel
    {
        public KeyResultModel()
        {
            Index = -1;
        }

        private int _index = -1;
        public int Index
        {
            get { return _index; }
            set { _index = value; }
        }

        public bool IsNew
        {
            get { return Index < 0 && Id == default(ObjectId); }
        }

        public ObjectId Id { get; set; }

        [RequiredUi]
        [LocalizedDisplayName("name",typeof(Resource))]
        public string Name { get; set; }

        [RequiredUi]
        [LocalizedDisplayName("Due",typeof(Resource))]
        public string Due { get; set; }

        [LocalizedDisplayName("Unit",typeof(Resource))]
        public KeyResultUnit Unit { get; set; }

        [LocalizedDisplayName("Current",typeof(Resource))]
        public decimal CurrentValue { get; set; }

        [LocalizedDisplayName("Target",typeof(Resource))]
        public decimal TargetValue { get; set; }

        public string AddButtonTitle
        {
            get { return IsNew ? Resource.Add : Resource.Update; }
        }
    }
}