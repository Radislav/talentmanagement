﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using TalentManagement.Infrastructure.Cache;
using TalentManagement.Web.Models;

namespace TalentManagement.Web.Infrastructure.Cache
{
    /// <summary>
    /// The filtering attribute for the controller action. Searchs <seealso cref="TalentManagement.Web.Models.IHasCachedImage"/> instances in HttpRequest and saves HttpRequest file in IUserCache or tries to load image from cache to IHasCachedImage if HttpRequest has no files. 
    /// </summary>
    public class ModelImageCacheAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            IHasCachedImage hasCachedImage = filterContext.ActionParameters.Values.OfType<IHasCachedImage>().FirstOrDefault();
            if (hasCachedImage != null)
            {
                IUserCache cache = DependencyResolver.Current.GetService<IUserCache>();
                var request = filterContext.HttpContext.Request;
                if (request.Files.Count > 0)
                {
                    HttpPostedFileBase image = request.Files[0];
                    cache.Set(image,hasCachedImage.GenerateCacheKey());
                }
                else
                {
                    hasCachedImage.Image = cache.Get<HttpPostedFileBase>(hasCachedImage.GenerateCacheKey());
                }
            }
            base.OnActionExecuting(filterContext);
        }
    }
}