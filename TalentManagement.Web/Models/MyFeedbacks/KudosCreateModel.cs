﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Resource = TalentManagement.Resources.Models.MyFeedbacks.KudosCreateModel;

namespace TalentManagement.Web.Models.MyFeedbacks
{
    public class KudosCreateModel : ICanPickUsers<KudosCreateModel>
    {
        [LocalizedDisplayName("Participant", typeof(Resource))]
        public string Participant { get; set; }

        [LocalizedDisplayName("WhoToPrise", typeof(Resource))]
        public string WhoToPrise { get; set; }
        
        [LocalizedDisplayName("Kudos",typeof(Resource))]
        public string Kudos { get; set; }
        
        public List<UserPicked> Users { get; set; }

        public string CurrentUser { get; set; }

        public ObjectId CurrentBadgeId { get; set; }

        public System.Linq.Expressions.Expression<Func<KudosCreateModel, string>> TitleExpression
        {
            get { return model => model.WhoToPrise; }
        }

        public ObjectId[] BadgeIds { get; set; }
    }
}