﻿using System;
using Autofac;
using Autofac.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TalentManagement.AppServices;
using TalentManagement.Domain;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure;
using TalentManagement.Infrastructure.Configuration;
using TalentManagement.Infrastructure.Email;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Infrastructure.Validation;

namespace TalentManagement.Test.AppServices
{
    [TestClass]
    public class LoginServiceTest : MockedTestClass<IRegistrationFlow>
    {
        private const string ValidEmail = "validEmail@email.com";
        private const string InvalidEmail = "invalid@email.com";
        private const string ExceptionThrowingEmail = "exception@exception.com";

        private static readonly User ValidUser = new User(ValidEmail);
        private static readonly User DuplicateUser = new User();

        private const int PasswordLength = 10;

        protected override void RegisterMocks(ContainerBuilder containerBuilder)
        {
            Mock<IUserRepository> userRepository = StubUserRepository();
            containerBuilder.RegisterMock(userRepository);

            containerBuilder.RegisterMock(new Mock<IEmailSender>());
            containerBuilder.RegisterMock(new Mock<IRepositoryWithTypedId<Avatar, string>>());
            containerBuilder.RegisterMock(new Mock<IUrlProvider>());
            
            containerBuilder.RegisterMock(MockAppConfiguration());
            containerBuilder.RegisterMock(MockBasicValidator());

            base.RegisterMocks(containerBuilder);
        }
        
        private Mock<IBasicValidator> MockBasicValidator()
        {
            Mock<IBasicValidator> validator = new Mock<IBasicValidator>();
            validator.Setup(m => m.ValidateEmail(ValidEmail)).Returns(new ValidationResult());
            validator.Setup(m => m.ValidateEmail(InvalidEmail)).Returns(new ValidationResult(new [] {new ValidationFailure(null,"Error")}));
            validator.Setup(m => m.ValidateEmail(ExceptionThrowingEmail)).Returns(new ValidationResult());

            return validator;
        }

        private  Mock<IAppConfiguration> MockAppConfiguration()
        {
            Mock<IAppConfiguration> appConfiguration = new Mock<IAppConfiguration>();
            appConfiguration.Setup(m => m.MinPasswordLength).Returns(PasswordLength);
            return appConfiguration;
        }

        private Mock<IUserRepository> StubUserRepository()
        {
            Mock<IUserRepository> userService = new Mock<IUserRepository>();
            userService.Setup(m => m.GetById(String.Empty)).Returns((User)null);
            userService.Setup(m => m.GetById(ValidEmail)).Returns(ValidUser);
            userService.Setup(m => m.CreateUser(DuplicateUser)).Returns(UserCreateResult.Duplicate);
            userService.Setup(m => m.CreateUser(ValidUser)).Returns(UserCreateResult.Ok);
            return userService;
        }
        
        [TestMethod]
        public void PerformForgotPasswordTest_should_return_NotFounded_if_no_user()
        {
            IRegistrationFlow target = CreateTarget();
            ForgotPasswordResult result = target.ForgotPasswordFor(null);

            Assert.AreEqual(ForgotPasswordResult.UserNotFounded, result);
        }

        [TestMethod]
        public void PerformForgotPasswordTest_should_update_user_with_new_password()
        {
            #region Setup
            ContainerBuilder builder = GetContainerBuilder();   
            RegisterMocks(builder);

            const string expectedPassword = "new password";

            string actualPasswordHashed = null;
            Mock<IUserRepository> stubUserRepository = StubUserRepository();
            stubUserRepository.Setup(m => m.Update(It.IsAny<User>()))
                .Callback<User>(user => actualPasswordHashed = user.HashedPassword);
            builder.RegisterMock(stubUserRepository);

            Mock<IPasswordGenerator> pwdMock = new Mock<IPasswordGenerator>();
            pwdMock.Setup(m => m.GeneratePassword(PasswordLength)).Returns(expectedPassword);
            builder.RegisterMock(pwdMock);

            #endregion

            IContainer container = builder.Build();
            IRegistrationFlow target = CreateTarget(container);
             target.ForgotPasswordFor(ValidEmail);

            IHashComputer hashComputer = container.Resolve<IHashComputer>();
            Assert.AreEqual(hashComputer.ComputeHash(expectedPassword), actualPasswordHashed);
            stubUserRepository.Verify(m => m.Update(ValidUser),Times.Once());
        }


        [TestMethod]
        public void PerformForgotPasswordTest_should_send_email_if_email_valid()
        {
            #region Setup
            ContainerBuilder builder = GetContainerBuilder();
            RegisterMocks(builder);

            const string expectedPassword = "new password";
            ForgotPasswordArgument actualArgument = null;

            Mock<IEmailSender> emailSender = new Mock<IEmailSender>();
            emailSender.Setup(m => m.Send(ValidEmail, EmailType.ForgotPassword, It.IsAny<ForgotPasswordArgument>()))
                .Callback<string, EmailType, ForgotPasswordArgument>((email, password, arg) =>
                                                    {
                                                        actualArgument = new ForgotPasswordArgument
                                                                            {
                                                                                Password = arg.Password,
                                                                                Email = arg.Email
                                                                            };
                                                    }
                );
            builder.RegisterMock(emailSender);

            Mock<IPasswordGenerator> pwdMock = new Mock<IPasswordGenerator>();
            pwdMock.Setup(m => m.GeneratePassword(PasswordLength)).Returns(expectedPassword);
            builder.RegisterMock(pwdMock);

            #endregion

            IContainer container = builder.Build();
            IRegistrationFlow target = CreateTarget(container);
            target.ForgotPasswordFor(ValidEmail);

            emailSender.Verify(m => m.Send(ValidEmail,EmailType.ForgotPassword,It.IsAny<ForgotPasswordArgument>()),Times.Once());
            Assert.AreEqual(expectedPassword, actualArgument.Password);
        }


        [TestMethod]
        public void PerformForgotPasswordTest_should_NOT_send_email_if_email_NOT_valid()
        {
         
            ContainerBuilder builder = GetContainerBuilder();
            RegisterMocks(builder);
            Mock<IEmailSender> emailSender = new Mock<IEmailSender>();
            emailSender.Setup(m => m.Send(ValidEmail, EmailType.ForgotPassword, It.IsAny<ForgotPasswordArgument>()));
            builder.RegisterMock(emailSender);
          
            IContainer container = builder.Build();
            IRegistrationFlow target = CreateTarget(container);
            target.ForgotPasswordFor(null);

            emailSender.Verify(m => m.Send(It.IsAny<string>(), It.IsAny<EmailType>(), It.IsAny<ForgotPasswordArgument>()), Times.Never());
        }


        [TestMethod]
        [ExpectedException(typeof(RegistrationException))]
        public void Register_should_throw_exception_if_user_null()
        {
            IRegistrationFlow target = CreateTarget();
            target.Register(null,null);
        }

        [TestMethod]
        [ExpectedException(typeof(RegistrationException))]
        public void Register_should_throw_exception_if_user_creation_fails()
        {
            IRegistrationFlow target = CreateTarget();
            target.Register(DuplicateUser, null);
        }

        [TestMethod]
        public void Register_should_call_UserService_Create()
        {
            IRegistrationFlow target = CreateTarget();
            target.Register(ValidUser,  null);

            Container.Resolve<Mock<IUserRepository>>()
                .Verify(m => m.CreateUser(ValidUser),Times.Once());
        }
        [TestMethod]
        public void Register_should_send_invitation_emails()
        {
            #region Setup
            var builder = GetContainerBuilder();

            builder.RegisterMock(new Mock<IAppConfiguration>());
            builder.RegisterMock(new Mock<IUrlProvider>());

            Mock<IBasicValidator> basicValidator = new Mock<IBasicValidator>();
            basicValidator.Setup(b => b.ValidateEmail(It.IsAny<string>())).Returns(new ValidationResult());
            builder.RegisterMock(basicValidator);

            Mock<IUserRepository> userRepository = new Mock<IUserRepository>();
            userRepository.Setup(s => s.CreateUser(It.IsAny<User>())).Returns(UserCreateResult.Ok);
            builder.RegisterMock(userRepository);

            InvitationArgument actualArgument = null;
            Mock<IEmailSender> emailSender = new Mock<IEmailSender>();
            emailSender.Setup(m => m.Send(It.IsAny<string>(), EmailType.Invitation, It.IsAny<InvitationArgument>()))
                .Callback<string, EmailType, InvitationArgument>((email, password, arg) =>
                                                                     {
                                                                         actualArgument =
                                                                             new InvitationArgument { InvitationUrl = "invitation url", HelpUrl = "help url" };

                                                                     }
                );
            builder.RegisterMock(emailSender);
            #endregion 

            IRegistrationFlow target = CreateTarget(builder.Build());

            target.Register(ValidUser,  new[] { "email1", "email2" });

            
            Assert.AreEqual("invitation url",actualArgument.InvitationUrl);
            Assert.AreEqual("help url", actualArgument.HelpUrl);
        }
    }
}
