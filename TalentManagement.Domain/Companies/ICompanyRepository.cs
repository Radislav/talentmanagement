﻿using System.Diagnostics.Contracts;
using MongoDB.Bson;
using TalentManagement.Domain.Contracts;

namespace TalentManagement.Domain.Companies
{
    [ContractClass(typeof(CompanyRepositoryContract))]
    public interface ICompanyRepository : IRepository<Company>
    {
        Company GetWithoutPictures(ObjectId companyId);
        Company UpsertIgnoreBadges(Company company);
        Company UpsertBadgesOnly(Company company);
        Company GetByTenantWithoutPictures(string tenantName);
        Company GetByTenant(string tenantName);
        Logo GetLogo(ObjectId companyId);
    }
}
