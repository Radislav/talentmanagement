﻿using MongoDB.Bson;

namespace TalentManagement.Web.Models.Feeds
{
    public class KudosModel : FeedModel
    {
        public string[] Receivers { get; set; }
        public string Content { get; set; }
        public ObjectId BadgeId { get; set; }
    }
}