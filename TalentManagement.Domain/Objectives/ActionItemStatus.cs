﻿using TalentManagement.Resources;

namespace TalentManagement.Domain.Objectives
{
    public enum ActionItemStatus
    {
        None,
        [LocalizedDescription("NotStarted", typeof(DomainResources))]
        NotStarted,
        [LocalizedDescription("InProgress", typeof(DomainResources))]
        InProgress,
        [LocalizedDescription("Completed", typeof(DomainResources))]
        Completed
    }
}
