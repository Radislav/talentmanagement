﻿using System;
using System.Text.RegularExpressions;

namespace TalentManagement
{
    public static class StringExtensions
    {
        public static  string EmailDomainShort(this string email)
        {
            return EmailParts(email, RegularExpressions.EmailDomainShort,"domain");
        }

        public static string EmailDomainLong(this string email)
        {
            return EmailParts(email, RegularExpressions.EmailDomainFull,"domain");
        }

        public static string EmailUserName(this string email)
        {
            return EmailParts(email, RegularExpressions.EmailUserNameGroup, "username");
        }

        private static string EmailParts(string email,string pattern,string group)
        {
            if (String.IsNullOrEmpty(email))
            {
                return String.Empty;
            }

            Regex regex = new Regex(pattern);
            Match match = regex.Match(email);

            return match.Success ? match.Groups[group].Value : String.Empty;
        }
    }
}
