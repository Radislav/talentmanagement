﻿namespace TalentManagement.Infrastructure.Serialization
{
    public interface ISerializer<TEntity,TSerializedEntity>
    {
        TSerializedEntity Serialize(TEntity entity);
        TEntity Deserialize(TSerializedEntity serializedEntity);
    }

}
