﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace TalentManagement.Web.Models
{
    public interface ICanPickUsers<TModel>
    {
        List<UserPicked> Users { get; set; }
        Expression<Func<TModel, string>> TitleExpression { get; }
    }
}