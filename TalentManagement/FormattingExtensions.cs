﻿using System;
using System.Threading;

namespace TalentManagement
{
    public static class FormattingExtensions
    {
        private const string LongDateFormat= "MMMM dd, yyyy";
        private const string ShortDateFormat = "MM/dd/yyyy";
        private const string MonthYearFormat = "MMMM yyyy";
        
        private static string FormatDateTime(this DateTime dateTime,string format)
        {
            return dateTime.ToString(format, Thread.CurrentThread.CurrentUICulture.DateTimeFormat);
        }

        /// <summary>
        /// Format "MMMM dd, yyyy" or May 12, 2012
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static string AsLondDateString(this DateTime dateTime)
        {
            return dateTime.FormatDateTime(LongDateFormat);
        }

        /// <summary>
        /// Format "MM/dd/yyyy" or 12/05/2012
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static string AsShortDateString(this DateTime dateTime)
        {
            return dateTime.FormatDateTime(ShortDateFormat);
        }

        /// <summary>
        /// Format "MMMM yyyy" or  May 2012
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static string AsMonthYearString(this DateTime dateTime)
        {
            return dateTime.FormatDateTime(MonthYearFormat);
        }
        
    }
}