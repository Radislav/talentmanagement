﻿using System.Collections.Generic;

namespace TalentManagement.Web.Models.MyReviews
{
    public class IndexModel : ICanActivateButton
    {
        public IEnumerable<ReviewModel> Reviews { get; set; }

        public string ActiveButton { get; set; }
    }
}