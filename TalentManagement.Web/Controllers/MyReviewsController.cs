﻿using System.Collections.Generic;
using System.Web.Mvc;
using Autofac.Features.Indexed;
using TalentManagement.Domain;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Web.Models.MyReviews;

namespace TalentManagement.Web.Controllers
{
    public class MyReviewsController : Controller
    {
        private readonly IMapper<IEnumerable<Review>, IndexModel> _indexModelMapper;
        private readonly IReviewRepository _reviewRepository;
        private readonly Dictionary<string,IFilter> _filters = new Dictionary<string, IFilter>();
        private readonly IResourceProvider _resourceProvider;


        public MyReviewsController(IReviewRepository reviewRepository,
            IMapper<IEnumerable<Review>, IndexModel> indexModelMapper, 
            IIndex<FilterType,IFilter> filters,
            IResourceProvider resourceProvider)
        {
            _reviewRepository = reviewRepository;
            _indexModelMapper = indexModelMapper;
            _resourceProvider = resourceProvider;

            _filters.Add(resourceProvider.MyReviewsRequiredAttention,filters[FilterType.ReviewRequiredAttention]);
            _filters.Add(resourceProvider.MyReviewsYouRequested,filters[FilterType.Owned]);
            _filters.Add(resourceProvider.MyReviewsTheyRequested,filters[FilterType.Participants]);

        }

        public ActionResult Index(string id)
        {
            id = id ?? _resourceProvider.MyReviewsRequiredAttention;
            IEnumerable<Review> reviews = _reviewRepository.ApplyFilter(_filters[id]);
            IndexModel indexModel = _indexModelMapper.Map(reviews);
            indexModel.ActiveButton = id;

            if (Request.IsAjaxRequest())
            {
                return PartialView("_ReviewsTable", indexModel);
            }
            return View(indexModel);
        }

    }
}
