﻿$(function () {

    /********************************************* DECLARE START  ***************************/

    function getIndividualsPicker() {
        return new UserPicker({ container: $('.individuals-picker'), prefix : 'IndividualsPicker' });
    }
    
    function getManagersPicker() {
        return new UserPicker({ container: $('.managers-picker'),prefix : 'ManagersPicker' });
    }

    function submitForm(form, url) {
        var data = form.serializeObject();
        
        //Needs to separate data from 2 user pickers. 
        data.FirstName = data.SecondName = data.Id = null;
        data.Individuals = getIndividualsPicker().getFormModel();
        data.Managers = getManagersPicker().getFormModel();

        $.ajax({
            url: url,
            data: JSON.stringify(data),
            success: function (response) {
                $('#wizard').html('');
                $('#wizard').html(response);
                init(true);
            }
        });
    }

    function getQuestionProperty(questionContainer, property, prefix) {
        return $(questionContainer).find('[name="' + prefix + '.' + property + '"]').val();
    }

    function getQuestionRadio(questionContainer, property) {
        return $(questionContainer).find('[data-property="' + property + '"] .active').data('value');
    }

    function getQuestionDisplayModel(questionContainer) {
        return {
            Number: getQuestionProperty(questionContainer, 'Number', 'question'),
            Settings: getQuestionProperty(questionContainer, 'Settings', 'question'),
            Id: getQuestionProperty(questionContainer, 'Id', 'question'),
            AnswerType: getQuestionProperty(questionContainer, 'AnswerType', 'question'),
            SetType: getQuestionProperty(questionContainer, 'SetType', 'question'),
            Text: $(questionContainer).find('.question-text').html()
        };
    }

    function getQuestionEditModel(questionContainer, prefix) {
        return {
            Number: getQuestionProperty(questionContainer, 'Number', prefix),
            Id: getQuestionProperty(questionContainer, 'Id', prefix),
            Text: getQuestionProperty(questionContainer, 'Text', prefix),
            Settings: getQuestionRadio(questionContainer, 'Settings', prefix),
            AnswerType: getQuestionRadio(questionContainer, 'AnswerType', prefix),
            SetType: getQuestionProperty(questionContainer, 'SetType', prefix)
        };
    }

    function getQuestionSet(setContainer) {
        var questions = [];
        $(setContainer).find('.question').each(function () {
            questions.push(getQuestionDisplayModel($(this)));
        });

        return {
            Title: $(setContainer).find('.title').html(),
            Id : $(setContainer).find('#Id').val(),
            SetType: $(setContainer).find('.create-question-link').data('set-type'),
            Questions: questions
        };
    }

    function getStep2Model(questionNew, questionEdit) {
        var sets = [];
        $('#step2-container .question-set').each(function () {
            sets.push(getQuestionSet($(this)));
        });
        return {
            QuestionSets: sets,
            QuestionNew: questionNew,
            QuestionEdit: questionEdit,
            SelectedSetType: $('#SelectedSetType').val()
        };
    }

    function getQuestionSetType(setContainer) {
        var setType = $(setContainer).find('.create-question-link').data('set-type');
        return setType == null ? $('#SelectedSetType').val() : setType;
    }

    function syncPreview(modalBody, text) {
        $(modalBody).find('#preview-content').html(text);
    }

    function step2() {
        
        //Expand latest question set accordion
        $('#step2 .collapse').each(function() {
            var setType = getQuestionSetType($(this));
            if ($('#SelectedSetType').val() == setType) {
                $(this).addClass('in');
            }
        });

        //Typing in question text
        $('#QuestionNew_Text,#QuestionEdit_Text').off('keyup').on('keyup', function () {
            syncPreview($(this).parents('.modal-body'), $(this).val());
        });

        //Click on 'Create' or 'Edit' or 'Deletel link. Saves current set type.
        $('.create-question-link').off('click').on('click', function () {
            $('#SelectedSetType').val($(this).data('set-type'));
        });

        //Click on 'Save' button in 'Create question' dialog
        $('#create-question-modal .modal-footer button').off('click').on('click', function () {
            var data = {
                step2: getStep2Model(getQuestionEditModel($('#create-question-modal'), 'QuestionNew')),
                setType: getQuestionSetType($(this).parents('.question-set'))
            };
            $.ajax({
                url: $(this).data('href'),
                data: JSON.stringify(data),
                success: function (response, status, xhr) {
                    var ct = xhr.getResponseHeader("content-type") || "";
                    //Everything fine, let`s show question sets.
                    if (ct.indexOf('html') > -1) {
                        $('#create-question-modal').modal('hide');
                        $('#step2-container').html(response);
                        init(true);

                    }
                    //Error(s) occured, display them
                    if (ct.indexOf('json') > -1) {
                        validation.showJsonErrors(response, $('#create-question-modal'), 'QuestionNew');
                    }
                }
            });
        });

        //Click on 'Save' button in 'Edit question' dialog
        $('#edit-question-modal .modal-footer button').off('click').on('click', function () {
            var data = {
                step2: getStep2Model(null, getQuestionEditModel($('#edit-question-modal'), 'QuestionEdit')),
                setType: getQuestionSetType($(this).parents('.question-set'))
            };
            $.ajax({
                url: $(this).data('href'),
                data: JSON.stringify(data),
                success: function (response, status, xhr) {
                    var ct = xhr.getResponseHeader("content-type") || "";
                    //Everything fine, let`s show question sets.
                    if (ct.indexOf('html') > -1) {
                        $('#edit-question-modal').modal('hide');
                        $('#step2-container').html(response);
                        init(true);
                    }
                    //Error(s) occured, display them
                    if (ct.indexOf('json') > -1) {
                        $('#edit-question-modal').modal('show');
                        validation.showJsonErrors(response, $('#edit-question-modal'), 'QuestionEdit');
                    }
                }
            });
        });

        //Click on 'Edit' or 'Delete' link in question view 
        $('.question-actions .question-edit,.question-actions .question-delete').off('click').on('click', function (e) {
            e.preventDefault();

            //Set questionSet value for modals
            var setType = getQuestionSetType($(this).parents('.question-set'));
            $('#SelectedSetType').val(setType);

            var data = {
                step2: getStep2Model(null, null),
                setType: getQuestionSetType($(this).parents('.question-set')),
                questionId: getQuestionProperty($(this).parents('.question'), 'Id', 'question')
            };

            var editMode = $(this).hasClass('question-edit');
            $.ajax({
                url: $(this).attr('href'),
                data: JSON.stringify(data),
                success: function (response) {
                    $('#step2-container').html(response);
                    init(true);
                    if (editMode) {
                        $('#edit-question-modal').modal('show');
                        var modalBody = $('#edit-question-modal .modal-body');
                        var text = getQuestionProperty($('#edit-question-modal'), 'Text', 'QuestionEdit');
                        syncPreview(modalBody, text);

                    }
                }
            });
        });
    }

    function  getStep3Model() {
        return {
                IndividualsPicker: getIndividualsPicker().getPickerModel(),
                ManagersPicker : getManagersPicker().getPickerModel(),
            };
    }

    function addUserHandler(containerSelector,serverActionUrl) {
          $('#step3 ' + containerSelector +' #pickUserButton').off('click').on('click', function () {
              var data = getStep3Model();
            $.ajax({
                url: serverActionUrl,
                data: JSON.stringify(data),
                success: function (response, status, xhr) {
                    var ct = xhr.getResponseHeader("content-type") || "";
                    if (ct.indexOf('html') > -1) {
                        $('#step3-container').html(response);
                        init();
                    }
                    if (ct.indexOf('json') > -1) {
                        validation.showJsonErrors(response, $(containerSelector));
                    }
                }
            });
        });
    }

    function step3() {
        addUserHandler('.individuals-picker', urls.addIndividual());
        addUserHandler('.managers-picker', urls.addManager());

        setReviewEveryoneStatus($('.review-everyone'));
        
          //ReviewEveryone checked changed
        $('.review-everyone').off('click').on('click', function() {
            setReviewEveryoneStatus($(this),true);
        });
    }

    function setReviewEveryoneStatus(element,revert) {
        var active = $(element).hasClass('active');
        var shouldDisable = revert ? !active : active;
        
          if (shouldDisable) {
                getIndividualsPicker().disable();
                getManagersPicker().disable();
                $('#ReviewEveryone').val(true);
               $('[data-id="CurrentNames"]').removeClass('input-validation-error').removeClass('in_error');
               $('#step3 .validation-summary-errors').css('display', 'none');

          } 
           else {
                getIndividualsPicker().enable();
                getManagersPicker().enable();
                $('#ReviewEveryone').val(false);
            }
    }

    function init() {

        customizeInputs();
        typeaheadInit();
        validation.show();
        //Click on "Next Step" button
        $('#wizard_actions button[name="next-step"],#wizard_actions button[name="finish"]').off('click').on('click', function () {
            submitForm($(this).parents('form'), $(this).data('href'));
        });

        step2();
        step3();
    }

    /********************************************* DECLARE END  ***************************/

    init();

});


