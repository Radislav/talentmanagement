﻿using FluentValidation;
using MongoDB.Bson;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.Companies.Validation
{
    internal class GeneralTabValidator  : AbstractValidator<GeneralTab>,
                                        TalentManagement.Infrastructure.Validation.IValidator<GeneralTab>
    {

        public GeneralTabValidator()
        {
            RuleFor(m => m.CompanyName)
                .NotEmpty();

            RuleFor(m => m.CompanyLogo)
                .FilePosted()
                .When(m => m.CompanyId == default(ObjectId));

            RuleFor(m => m.CompanyLogo)
                .ImageFile()
                .FileSize(Constants.Megabyte);

        }

        public TalentManagement.Infrastructure.Validation.ValidationResult ValidateInstance(GeneralTab instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}