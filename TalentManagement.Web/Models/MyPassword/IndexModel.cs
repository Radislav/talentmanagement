﻿using Resource = TalentManagement.Resources.Models.MyPassword.IndexModel;

namespace TalentManagement.Web.Models.MyPassword
{
    [LocalizedDisplayName("Title", typeof(Resource))]
    public class IndexModel
    {
        [LocalizedDisplayName("OldPassword", typeof(Resource))]
        public string OldPassword { get; set; }

        [LocalizedDisplayName("NewPassword", typeof(Resource))]
        public string NewPassword { get; set; }

        [LocalizedDisplayName("ConfirmPassword", typeof(Resource))]
        public string ConfirmPassword { get; set; }

        public string OldPasswordHashed { get; set; }
        public bool ValidationOk { get; set; }
    }
}