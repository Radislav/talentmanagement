﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace TalentManagement.Web.Infrastructure.UI
{
    public static class ButtonExtensions
    {
        public static MvcHtmlString ButtonFor<TModel, TResult>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TResult>> expression, string text, TResult value)
        {
            return htmlHelper.ButtonFor(expression, text, value, null);
        }

        public static MvcHtmlString ButtonFor<TModel, TResult>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TResult>> expression, string text, TResult value, object htmlAttributes)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var tagBuilder = new TagBuilder("button");
            tagBuilder.Attributes.Add("name", metadata.PropertyName);
            tagBuilder.AddCssClass("btn");
            tagBuilder.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes), true);
            if (value != null)
            {
                tagBuilder.Attributes.Add("value", value.ToString());
            }

            if ((value == null && metadata.Model == null)
                || (value != null && metadata.Model != null && value.ToString() == metadata.Model.ToString()))
            {
                tagBuilder.AddCssClass("active");
            }

            tagBuilder.SetInnerText(text);
            return new MvcHtmlString(tagBuilder.ToString());
        }

        public static MvcHtmlString Button<TModel>(this HtmlHelper<TModel> html, string text, object htmlAttributes)
        {
            TagBuilder tagBuilder = new TagBuilder("button");
            if (htmlAttributes != null)
            {
                tagBuilder.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
            }
            tagBuilder.AddCssClass("btn");
            tagBuilder.InnerHtml = text;

            return new MvcHtmlString(tagBuilder.ToString());
        }
        
    }
}