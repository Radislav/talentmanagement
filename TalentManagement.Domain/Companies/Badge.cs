﻿using System.Diagnostics.Contracts;
using MongoDB.Bson;

namespace TalentManagement.Domain.Companies
{
    public class Badge : Entity , IPicture
    {
        private Badge() : base()
        {

        }

        public Badge(ObjectId badgeId,ObjectId companyId) : base(badgeId)
        {
            Contract.Requires(companyId != ObjectId.Empty);
            CompanyId = companyId;
        }

        public ObjectId CompanyId { get; private set; }
        public byte[] Image { get; set; }
        public string ImageType { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsEnabled { get; set; }

        
    }
}
