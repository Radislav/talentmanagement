﻿using System;
using FluentValidation.Validators;

namespace TalentManagement.Web.Models.Validation
{
    internal class DueDateValidator : PropertyValidator
    {
        public DueDateValidator()
            : base("DateShouldBeAfterToday", typeof(Resources.Errors))
        {

        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            string value = context.PropertyValue as string;
            DateTime dateTime;
            if (DateTime.TryParse(value, out dateTime))
            {
                return dateTime >= DateTime.Today;
            }
            return true;
        }
    }
}