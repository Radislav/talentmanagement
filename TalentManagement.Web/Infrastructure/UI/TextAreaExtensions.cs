﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace TalentManagement.Web.Infrastructure.UI
{
    public static class TextAreaExtensions
    {
        public static MvcHtmlString TextAreaStyledFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                          Expression<Func<TModel, TValue>> expression,
                                                          IDictionary<string, object> htmlAttributes, bool showPlaceholder = false)
        {
            htmlAttributes = htmlAttributes ?? new Dictionary<string, object>();
            if (showPlaceholder)
            {
                ModelMetadata metaData = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
                htmlAttributes.Add("placeholder", metaData.DisplayName);

            }
            return HtmlExtensions.ControlStyledFor(html, html.TextAreaFor, expression, htmlAttributes);
        }

        public static MvcHtmlString TextAreaStyledFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                         Expression<Func<TModel, TValue>> expression,
                                                         object htmlAttributes, bool showPlaceholder = false)
        {
            return TextAreaStyledFor(html, expression, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes),
                                     showPlaceholder);
        }

        public static MvcHtmlString TextAreaStyledFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                           Expression<Func<TModel, TValue>> expression)
        {
            return TextAreaStyledFor(html, expression, null);
        }
        
    }
}