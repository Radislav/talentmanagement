﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TalentManagement.Web.Models.People
{
    public class TeamModel : PeopleModel
    {
        public TeamRowModel[] Teams { get; set; }
    }
}