﻿using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.MyAccount.Map
{
    internal class IndexModelMap : IMap<User, IndexModel>
    {
        public void Configure(IMapperConfiguration<User, IndexModel> configuration)
        {
            configuration.ForMember(d => d.FirstName, s => s.FirstName);
            configuration.ForMember(d => d.ManagerEmailUserName, s => s.ManagerEmail.EmailUserName());
            configuration.ForMember(d => d.Position, s => s.Position);
            configuration.ForMember(d => d.SecondName, s => s.SecondName);
            configuration.ForMember(d => d.UserId, s => s.Id);
        }
    }
}