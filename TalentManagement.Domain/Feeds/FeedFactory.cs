﻿using System;
using System.Linq;
using System.Collections.Generic;
using MongoDB.Bson;
using TalentManagement.Domain.Objectives;
using TalentManagement.Resources;

namespace TalentManagement.Domain.Feeds
{
    class FeedFactory : IFeedFactory 
    {
        public Feed CreateEmptyFeed(FeedType feedType, OwnedDescriptor descriptor)
        {
            return new Feed
            {
                FeedType = feedType,
                CreatedBy = descriptor.CreatedBy,
                CreatedAt = descriptor.CreatedAt,
                CompanyName = descriptor.Company,
                Team = descriptor.Team
            };
        }

        public Feed CreateKudos(string content, ObjectId badgeId, IEnumerable<string> receivers, OwnedDescriptor descriptor)
        {
            Feed feed = CreateEmptyFeed(FeedType.Kudos, descriptor);
       
            feed.Title = String.Format(DomainResources.Feed_KudosTitle,descriptor.CreatedBy);

            feed.SetPropertyValue("Content", content);
            feed.SetPropertyValue("BadgeId", badgeId);
            feed.SetPropertyValue("Receivers", string.Join(",", receivers));

            return feed;
        }

        public Feed CreateUpdatingFeed(string text, IScope updatedItemScope, OwnedDescriptor creationDescriptor)
        {
            Feed feed = CreateEmptyFeed(FeedType.Update, creationDescriptor);
            feed.Scope = updatedItemScope;
            feed.SetPropertyValue("Text", text);

            return feed;
        }

        public Feed CreateObjectiveFeed(Objective oldObjective, Objective newObjective, OwnedDescriptor creationDescriptor)
        {
            Feed feed = CreateEmptyFeed(FeedType.Objective, creationDescriptor);
            feed.Scope = new Scope("Objective", Convert.ToString(newObjective.Id));

            feed.SetPropertyValue("Status",newObjective.Status);
            feed.SetPropertyValue("Progress",newObjective.Progress);
            feed.SetPropertyValue("Name",newObjective.Name);
            feed.SetPropertyValue("Id",newObjective.Id);

            List<string> texts = new List<string>();
            if (oldObjective.Status != newObjective.Status)
            {
                texts.Add(String.Format(DomainResources.Feed_ObjectiveStatusUpdated,Convert.ToString(oldObjective.Status),Convert.ToString(newObjective.Status)));
            }

            if (oldObjective.Progress != newObjective.Progress)
            {
                texts.Add(String.Format(DomainResources.Feed_ObjectiveProgressUpdated,oldObjective.Progress,newObjective.Progress));
            }

            feed.SetPropertyValue("Text",texts.Any() ? texts.Aggregate((s1,s2) => String.Concat(s1,";",s2)) : DomainResources.Feed_ObjectiveUpdated);
            
            return feed;
        }
    }
}
