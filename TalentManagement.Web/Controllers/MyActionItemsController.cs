﻿using System.Web.Mvc;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Infrastructure.Validation;
using TalentManagement.Web.Models.MyActionItems;
using TalentManagement.Web.Models.MyActionItems.Map;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Controllers
{
    public class MyActionItemsController : Controller
    {
        private readonly IIndexModelMapper _indexModelMapper;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IActionItemRepository _actionItemRepository;
        private readonly IMapper<ActionItemModel, ActionItem> _actionItemMapper;
        private readonly IValidator<ActionItemModel> _actionItemValidator;

        public MyActionItemsController(
            IIndexModelMapper indexModelMapper, 
            IPrincipalProvider principalProvider, 
            IActionItemRepository actionItemRepository, 
            IMapper<ActionItemModel, ActionItem> actionItemMapper,
            IValidator<ActionItemModel> actionItemValidator)
        {
            _indexModelMapper = indexModelMapper;
            _principalProvider = principalProvider;
            _actionItemRepository = actionItemRepository;
            _actionItemMapper = actionItemMapper;
            _actionItemValidator = actionItemValidator;
        }

        [HttpGet]
        public ActionResult Index(ActionItemStatus? id)
        {
            string owner = _principalProvider.GetUserName();
            var relatedItems = _actionItemRepository.GetRelated(owner, id);
            var notRelatedItems = _actionItemRepository.GetNotRelated(owner, id);

            IndexModel indexModel = _indexModelMapper.Map(relatedItems, notRelatedItems);

            if (Request.IsAjaxRequest())
            {
                return PartialView("_Containers", indexModel);
            }
            
            return View(indexModel);    
        }

        [HttpPost]
        public ActionResult CreateActionItem(ActionItemModel actionItemModel)
        {
            ValidationResult validationResult = _actionItemValidator.ValidateInstance(actionItemModel);
            if (validationResult.IsValid)
            {
                _actionItemRepository.Insert(_actionItemMapper.Map(actionItemModel));
            }

            return validationResult.AsJson();
        }
    }
}
