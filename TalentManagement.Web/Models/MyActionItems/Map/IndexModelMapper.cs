﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Resources;

namespace TalentManagement.Web.Models.MyActionItems.Map
{
    internal class IndexModelMapper :  IIndexModelMapper
    {
        private readonly IResourceProvider _resourceProvider;
        private readonly IActionItemModelMapper _actionItemModelMapper;

        public IndexModelMapper(IResourceProvider resourceProvider, IActionItemModelMapper actionItemModelMapper)
        {
            _resourceProvider = resourceProvider;
            _actionItemModelMapper = actionItemModelMapper;
        }

      
        private ActionItemsContainer CreateContainer(IEnumerable<ActionItem> actionItems,string title)
        {
            return new ActionItemsContainer
                       {
                           ActionItems = actionItems.Select(ai => _actionItemModelMapper.Map(ObjectId.Empty,ai)).ToArray(),
                           Title = title
                       };
        }

        private ActionItemsContainer CreateContainer(ActionItemsDictionary actionItems, string title)
        {
            var actionItemTuples = actionItems.ToTuples();
            
            return new ActionItemsContainer
            {
                ActionItems = actionItemTuples.Select(tuple => _actionItemModelMapper.Map(tuple.Item1,tuple.Item2)).ToArray(),
                Title = title
            };
        }

        public IndexModel Map(ActionItemsDictionary relatedActionItems, IEnumerable<ActionItem> notRelatedActionItem)
        {
            return new IndexModel
            {
                NotRelated = CreateContainer(notRelatedActionItem, _resourceProvider.MyActionItemsNotRelated),
                Related = CreateContainer(relatedActionItems, _resourceProvider.MyActionItemsRelated)
            };
        }
    }
}