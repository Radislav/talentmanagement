﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace TalentManagement.Web.Infrastructure.Security
{
    public interface ITicketManager
    {
        void IsTicketIssued();
        void RemoveCurrentTicket();
        

    }
}
