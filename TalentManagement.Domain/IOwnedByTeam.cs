﻿namespace TalentManagement.Domain
{
    public interface IOwnedByTeam
    {
        string Team { get; set; }
    }
}
