﻿using System;
using System.Linq;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;


namespace TalentManagement.Web.Models.Objectives.Map
{
    class ObjectiveCreateModelToObjective : IMap<ObjectiveCreateModel,Objective>
    {
        private readonly IPrincipalProvider _principalProvider;
        private readonly IMapper<KeyResultModel, KeyResult> _keyResultModelMapper;

        public ObjectiveCreateModelToObjective(IPrincipalProvider principalProvider, IMapper<KeyResultModel, KeyResult> keyResultModelMapper)
        {
            _principalProvider = principalProvider;
            _keyResultModelMapper = keyResultModelMapper;
        }

        public void Configure(IMapperConfiguration<ObjectiveCreateModel, Objective> configuration)
        {
            configuration.ForMember(d => d.Contributors,
                                    s => s.CreateStep3.UsersPicked.ToArray());
            configuration.ForMember(d => d.Visibility, s => s.CreateStep1.Visibility);
            configuration.ForMember(d => d.Description, s => s.CreateStep1.Description);
            configuration.ForMember(d => d.Due, s => s.CreateStep1.Due);
            configuration.ForMember(d => d.KeyResults,
                                    s => s.CreateStep2.KeyResults.Select(_keyResultModelMapper.Map).ToList());
            configuration.ForMember(d => d.Name, s => s.CreateStep1.Name);
            configuration.ForMember(d => d.Icon, s => s.CreateStep4.CurrentIconId);

            configuration.UseValue(d => d.Start, DateTime.Now);
            configuration.UseValue(d => d.UpdatedAt, DateTime.Now);
            configuration.UseValue(d => d.UpdatedBy, _principalProvider.GetUserName());
            configuration.UseValue(d => d.CreatedAt, DateTime.Now);
            configuration.UseValue(d => d.CreatedBy, _principalProvider.GetUserName());
            
            
            configuration.UseValue(d => d.CompanyName, _principalProvider.GetTenant());
            

        }
    }
}