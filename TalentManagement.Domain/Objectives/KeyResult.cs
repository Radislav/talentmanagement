﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using MongoDB.Bson;

namespace TalentManagement.Domain.Objectives
{
    [AggregateRoot]
    public class KeyResult : Entity
    {
        private List<ActionItem> _actionItems;

        public KeyResult() :this(ObjectId.Empty,new List<ActionItem>())
        {
        }

        public KeyResult(ObjectId id)
            : this(id,new List<ActionItem>())
        {

        }

        public KeyResult(ObjectId id,IEnumerable<ActionItem> actionItems) :base(id)
        {
            _actionItems = actionItems.ListOrDefault();
        }
        
        public string Name { get; set; }
        public KeyResultUnit Unit { get; set; }
        public decimal CurrentValue { get; set; }
        public decimal TargetValue { get; set; }
        public DateTime DueDate { get; set; }

        public IEnumerable<ActionItem> ActionItems
        {
            get { return _actionItems; }
            private set
            {
                _actionItems = value.ListOrDefault();
            }
        }

        public int GetIndexOfActionItem(ActionItem actionItem)
        {
            return _actionItems.IndexOf(actionItem);
        }

        public void AddActionItem(ActionItem actionItem)
        {
            Contract.Requires(actionItem != null);
            actionItem.DueDate = DueDate;
            actionItem.RelatedToObjective = true;
            _actionItems.Add(actionItem);
        }

        public void InsertActionItem(ActionItem actionItem,int index)
        {
            Contract.Requires(actionItem != null);
            Contract.Requires(index >= 0);
            Contract.Assume(index <_actionItems.Count );

            actionItem.DueDate = DueDate;
            actionItem.RelatedToObjective = true;
            _actionItems[index] = actionItem;
        }

        public void RemoveActionItem(ObjectId actionItemId)
        {
            if (_actionItems != null)
            {
                _actionItems.RemoveAll(ai => ai.Id == actionItemId);    
            }
        }
    }
}
