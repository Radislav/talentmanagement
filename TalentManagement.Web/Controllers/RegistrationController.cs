﻿using System.Web.Mvc;
using System.Web.Security;
using TalentManagement.AppServices;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;
using TalentManagement.Web.Infrastructure.Security;
using TalentManagement.Web.Models;

namespace TalentManagement.Web.Controllers
{
    public class RegistrationController : Controller
    {
        private readonly IMapper<ProfileModel, User> _profileToUserMapper;
        private readonly IMapper<ProfileModel, Avatar> _profileToAvatarMapper;
        private readonly IRegistrationFlow _registrationFlow;
        private readonly IAvatarRepository _avatarRepository;
        private readonly IAuthenticationService _authenticationService;

        public RegistrationController(IMapper<ProfileModel, User> profileToUserMapper,
                                      IMapper<ProfileModel, Avatar> profileToAvatarMapper,
                                      IRegistrationFlow registrationFlow,
                                      IAvatarRepository avatarRepository,
                                      IAuthenticationService authenticationService)
        {
            _profileToUserMapper = profileToUserMapper;
            _registrationFlow = registrationFlow;
            _profileToAvatarMapper = profileToAvatarMapper;
            _avatarRepository = avatarRepository;
            _authenticationService = authenticationService;
        }
      
        [HttpGet]
        public ActionResult Register()
        {
            return View(new RegistrationModel());
        }

        [HttpPost]
        public ActionResult Register(RegistrationModel registration)
        {
            if (registration.Profile.IsProfileImageShouldBeSaved)
            {
                Avatar avatar = _profileToAvatarMapper.Map(registration.Profile);
                _avatarRepository.Update(avatar);
            }

            if (registration.IsFlowFinished)
            {
                User user = _profileToUserMapper.Map(registration.Profile);
                _registrationFlow.Register(user,registration.Invitation.Emails);

                LoginResult loginResult = _authenticationService.Login(registration.Profile.Email,
                                                                       registration.Profile.Password);
                if (loginResult == LoginResult.Ok)
                {
                    FormsAuthentication.SetAuthCookie(registration.Profile.Email, false);
                    new SecurityTicket().AddCookieToContext(HttpContext);

                    return RedirectToRoute(new { controller = "Feed" });
                }
                //This case should no occure, but anyway...
                return RedirectToAction("Login", "Login");

            }
            
            return View(registration);
        }
    }
}
