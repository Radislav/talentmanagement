﻿using FluentValidation;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.MyFeedbacks.Validation
{
    public class FeedbackAttentionRequiredValidator : AbstractValidator<FeedbackAttentionRequired>,
                                             TalentManagement.Infrastructure.Validation.IValidator<FeedbackAttentionRequired>
    {
        public FeedbackAttentionRequiredValidator()
        {
            RuleFor(m => m.Response)
                .NotEmpty();
        }

        public TalentManagement.Infrastructure.Validation.ValidationResult ValidateInstance(FeedbackAttentionRequired instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}