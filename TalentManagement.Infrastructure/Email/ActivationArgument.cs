﻿namespace TalentManagement.Infrastructure.Email
{
    public class ActivationArgument
    {
        public string ActivationUrl { get; set; }
    }
}
