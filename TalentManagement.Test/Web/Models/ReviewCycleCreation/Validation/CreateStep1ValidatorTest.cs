﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TalentManagement.Infrastructure.Validation;
using TalentManagement.Web.Models.ReviewCycleCreation;

namespace TalentManagement.Test.Web.Models.ReviewCycleCreation.Validation
{
    [TestClass]
    public class CreateStep1ValidatorTest : MockedTestClass<IValidator<CreateStep1>>
    {
        [TestMethod]
        public void Should_fail_if_Name_empty()
        {
            var target = CreateTarget();
            CreateStep1 step1 = new CreateStep1();
            ValidationResult result = target.ValidateInstance(step1);

            Assert.IsTrue(result.HasMessage( "'Name' should not be empty.", "Name"));
        }

        [TestMethod]
        public void Should_fail_if_ReviewStart_empty()
        {
            var target = CreateTarget();
            CreateStep1 step1 = new CreateStep1();
            ValidationResult result = target.ValidateInstance(step1);

            Assert.IsTrue(result.HasMessage("'Review Start' should not be empty.", "ReviewStart"));
        }

        [TestMethod]
        public void Should_not_fail_due_ReviewStart_empty_if_ReviewStart_not_datetime()
        {
            var target = CreateTarget();
            CreateStep1 step1 = new CreateStep1
                {
                    ReviewStart = "NOT DATE TIME"
                };
            ValidationResult result = target.ValidateInstance(step1);

            Assert.IsFalse(result.HasMessage("'Review Start' should not be empty.", "ReviewStart"));
        }

        [TestMethod]
        public void Should_fail_if_ReviewStart_later_than_ReviewEnd()
        {
            var target = CreateTarget();
            CreateStep1 step1 = new CreateStep1
                {
                    ReviewStart = "02/02/2012",
                    ReviewEnd = "01/01/2012"
                };

            ValidationResult result = target.ValidateInstance(step1);

            Assert.IsTrue(result.HasMessage("'Review start date' must be before 'Review end date'.", "ReviewStart"));
        }

        [TestMethod]
        public void Should_not_fail_due_the_ReviewStart_later_than_ReviewEnd_when_ReviewStart_not_date()
        {
            var target = CreateTarget();
            CreateStep1 step1 = new CreateStep1
            {
                ReviewStart = null,
                ReviewEnd = "01/01/2012"
            };

            ValidationResult result = target.ValidateInstance(step1);

            Assert.IsFalse(result.HasMessage("'Review start date' must be before 'Review end date'.", "ReviewStart"));
        }

        [TestMethod]
        public void Should_not_fail_due_the_ReviewStart_later_than_ReviewEnd_when_ReviewEnd_not_date()
        {
            var target = CreateTarget();
            CreateStep1 step1 = new CreateStep1
            {
                ReviewStart = "02/02/2012",
                ReviewEnd = "NOT DATE"
            };

            ValidationResult result = target.ValidateInstance(step1);

            Assert.IsFalse(result.HasMessage("'Review start date' must be before 'Review end date'.", "ReviewStart"));
        }

        [TestMethod]
        public void Should_fail_if_SummaryDate_before_Today()
        {
            var target = CreateTarget();
            CreateStep1 step1 = new CreateStep1
            {
                SummaryDue = DateTime.Today.AddDays(-1).ToShortDateString()
            };

            ValidationResult result = target.ValidateInstance(step1);

            Assert.IsTrue(result.HasMessage("Summary Due should be after today`s date.","SummaryDue"));
        }

        [TestMethod]
        public void Should_not_fail_if_SummaryDate_is_Today()
        {
            var target = CreateTarget();
            CreateStep1 step1 = new CreateStep1
            {
                SummaryDue = DateTime.Today.ToShortDateString()
            };

            ValidationResult result = target.ValidateInstance(step1);

            Assert.IsFalse(result.HasMessage("Summary Due should be after today`s date.", "SummaryDue"));
        }

    }
}
