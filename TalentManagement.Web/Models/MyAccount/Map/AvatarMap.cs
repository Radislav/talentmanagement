﻿using System.Web.Helpers;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Configuration;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.MyAccount.Map
{
    internal class AvatarMap : IMap<IndexModel,Avatar>
    {
        private readonly IAppConfiguration _appConfiguration; 

        public AvatarMap(IAppConfiguration appConfiguration)
        {
            _appConfiguration = appConfiguration;
        }

        public void Configure(IMapperConfiguration<IndexModel, Avatar> configuration)
        {
            configuration.ForMember(d => d.Id, s => s.UserId);

            configuration.ForMember(d => d.Image,
                                      s => s.ProfileImageBytes);

            configuration.ForMember(d => d.SmallImage,
                                   s => new WebImage(s.ProfileImageBytes)
                                                .Resize(_appConfiguration.AvatarWidth, _appConfiguration.AvatarHeight)
                                                .GetBytes());

            configuration.ForMember(d => d.ImageType, s => s.ProfileImageType);
        }
    }
}