﻿using System;
using System.Diagnostics.Contracts;
using MongoDB.Bson;
using TalentManagement.Domain;
using TalentManagement.Domain.Companies;
using System.Linq;
using TalentManagement.Domain.Events;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data.Repositories
{
    internal class CompanyRepository : Repository<Company> , ICompanyRepository
    {
        private readonly Lazy<IDomainEvents> _domainEvents;

        public CompanyRepository(IUnitOfWork unitOfWork, IPrincipalProvider principalProvider, Lazy<IDomainEvents> domainEvents)
            :base(unitOfWork,principalProvider)
        {
            _domainEvents = domainEvents;
        }

        private IQueryable<Company> WithoutPictures(IQueryable<Company> source)
        {
            Contract.Requires(source != null);
            Contract.Ensures(Contract.Result<IQueryable<Company>>() != null);

            return source.Select(c => new Company( c.Id,c.BadgeIds)
                                          {
                                              Name = c.Name,
                                              Timezone = c.Timezone,
                                              Logo = c.Logo == null ? (Logo)null : new Logo(c.Logo.Id) 
                                          });
        }

        public Company GetWithoutPictures(ObjectId companyId)
        {
            return WithoutPictures(UnitOfWork.AsQueryable<Company>())
                .SingleOrDefault(c => c.Id == companyId);
        }
        
        public Company UpsertIgnoreBadges(Company company)
        {
            Company result = UpsertCompany(company, (c1, c2) =>
                                       {
                                           c1.Name = c2.Name;
                                           c1.Logo = c2.Logo.IsDefined() ? c2.Logo : c1.Logo;
                                           c1.Timezone = c2.Timezone;

                                           return c1;
                                       });

            _domainEvents.Value.Raise(new CompanyLogoUpdatedArgs(company.Id, company.CompanyName));

            return result;
        }

        public Company GetByTenantWithoutPictures(string tenantName)
        {
            var companies = UnitOfWork.AsQueryable<Company>()
                .Where(c => c.CompanyName == tenantName);

            return WithoutPictures(companies)
                .SingleOrDefault();
        }

        public Company GetByTenant(string tenantName)
        {
            return UnitOfWork.AsQueryable<Company>()
                .SingleOrDefault(c => c.CompanyName == tenantName);
        }
        
        public Logo GetLogo(ObjectId companyId)
        {
            return UnitOfWork.AsQueryable<Company>()
                .Where(c => c.Id == companyId)
                .Select(c => c.Logo)
                .SingleOrDefault();
        }
       
        public Company UpsertBadgesOnly(Company company)
        {
            return UpsertCompany(company, (toUpdate, initial) => new Company(toUpdate,initial.BadgeIds));
        }

        private Company UpsertCompany(Company company, Func<Company, Company, Company> updator)
        {
            Contract.Requires<EntityNullException>(company != null);
            Contract.Ensures(Contract.Result<Company>() != null);

            Company result;

            if (company.IsNew)
            {
                Insert(company);
                result = company;
            }
            else
            {
                Company companyToUpdate = GetById(company.Id);

                if (companyToUpdate == null)
                {
                    throw new EntityNotFoundException(typeof(Company),company.Id);
                }

                companyToUpdate = updator(companyToUpdate, company);

                Update(companyToUpdate);
                result = companyToUpdate;
            }

            return result;
        }
    }
}
