﻿using MongoDB.Bson;
using Resource = TalentManagement.Resources.Models.MyActionItems.ActionItemModel;

namespace TalentManagement.Web.Models.MyActionItems
{
    public class ActionItemModel
    {
        [LocalizedDisplayName("Name",typeof(Resource))]
        public string Name { get; set; }
        
        [LocalizedDisplayName("Due", typeof(Resource))]
        public string DueString { get; set; }

        public bool RelatedToObjective { get; set; }
        public ObjectId ObjectiveId { get; set; }
    }
}