﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;

namespace TalentManagement.Web.Models.Objectives
{
    public class ObjectiveCreateModel : WizardMaster
    {
        public ObjectiveCreateModel(List<UserPicked> users, ObjectId[] objectiveIconIds)
        {
            CreateStep1 = new CreateStep1();
            CreateStep2 = new CreateStep2();
            CreateStep3 = new CreateStep3
            {
                UserPickerModel = new UserPickerControl { Users = users }
            };
            CreateStep4 = new CreateStep4 { Icons = objectiveIconIds };
        }

        public ObjectiveCreateModel()
        {
            CreateStep1 = new CreateStep1();
            CreateStep2 = new CreateStep2();
            CreateStep3 = new CreateStep3();
            CreateStep4 = new CreateStep4();
        }

        public CreateStep1 CreateStep1 { get; set; }
        public CreateStep2 CreateStep2 { get; set; }
        public CreateStep3 CreateStep3 { get; set; }
        public CreateStep4 CreateStep4 { get; set; }

        public override IEnumerable<WizardPage> Pages
        {
            get { return new WizardPage[] { CreateStep1, CreateStep2, CreateStep3, CreateStep4 }; }
        }
    }
}