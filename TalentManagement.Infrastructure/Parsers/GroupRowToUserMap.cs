﻿using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Parsers
{
    internal class GroupRowToUser : IMap<GroupRowParsedEventArgs, User>
    {
        private readonly IHashComputer _hashComputer;

        public GroupRowToUser(IHashComputer hashComputer)
        {
            _hashComputer = hashComputer;
        }

        public void Configure(IMapperConfiguration<GroupRowParsedEventArgs, User> configuration)
        {
            configuration.ForMember(x => x.Id, x => x.Email);
            configuration.ForMember(x => x.HashedPassword, x => _hashComputer.ComputeHash(x.Password));
            configuration.ForMember(x => x.ManagerEmail, x => string.Concat(x.ReportingManagerEmailLocal, x.ReportingManagerEmailAppendix));
        }
    }
}
