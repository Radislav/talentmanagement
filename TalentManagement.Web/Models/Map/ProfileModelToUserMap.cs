﻿using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Web.Models.Map
{
    class ProfileModelToUserMap : IMap<ProfileModel,User>
    {
        private readonly IHashComputer _hashComputer;

        public ProfileModelToUserMap(IHashComputer hashComputer)
        {
            _hashComputer = hashComputer;
        }

        public void Configure(IMapperConfiguration<ProfileModel, User> configuration)
        {
            configuration.ForMember(user => user.HashedPassword, 
                                    profile => _hashComputer.ComputeHash(profile.Password));

            configuration.ForMember(user => user.Id,
                                    profile => profile.Email);

        }
    }
}