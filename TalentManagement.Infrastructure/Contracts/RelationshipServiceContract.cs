﻿using System.Diagnostics.Contracts;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Services;

namespace TalentManagement.Infrastructure.Contracts
{
    [ContractClassFor(typeof (IRelationshipService))]
    internal abstract class RelationshipServiceContract : IRelationshipService
    {
        public void MarkAsManager(User manager)
        {
            Contract.Requires(manager != null);
            Contract.Requires(manager.Id != null);
        }

        public void NotManager(User manager)
        {
            Contract.Requires(manager != null);
            Contract.Requires(manager.Id != null);
        }

        public void ChangeTeam(User user, User previousManager, User newManager)
        {
            Contract.Requires(user != null);
            Contract.Requires(user.Id != null);
            Contract.Requires(newManager != null);
            Contract.Requires(newManager.Id != null);
        }

        public void AddToTeam(User user, User newManager)
        {
            Contract.Requires(user != null);
            Contract.Requires(user.Id != null);
            Contract.Requires(newManager != null);
            Contract.Requires(newManager.Id != null);
        }

        public void RemoveFromTeams(User user, User previousManager)
        {
            Contract.Requires(user != null);
            Contract.Requires(user.Id != null); ;
        }
    }
}