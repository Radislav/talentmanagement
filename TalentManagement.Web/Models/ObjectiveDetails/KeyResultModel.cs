﻿using System.Collections.Generic;
using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.ObjectiveDetails
{
    public class KeyResultModel : IActionItemsHolder
    {
        public KeyResultModel()
        {
            NewActionItem = new ActionItemModel();
        }

        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public List<ActionItemModel> ActionItems { get; set; }
        public ActionItemModel NewActionItem { get; set; }
        public int Index { get; set; }

        public KeyResultModel PersistPickedContributors(Objective objective,KeyResult keyResult, bool isDomainValid)
        {
            var keyResultMapper = DependencyResolver.Current.GetService<IMapper<KeyResult, KeyResultModel>>();
            var objectiveMapper = DependencyResolver.Current.GetService<IMapper<Objective, ObjectiveDetailsModel>>();

            List<UserPicked> contributorsPicked = objectiveMapper.Map(objective).ContributorsPicked;

            KeyResultModel result = keyResultMapper.Map(keyResult);

            if (false == isDomainValid)
            {
                result.NewActionItem = NewActionItem;
            }

            result.NewActionItem.Users = contributorsPicked;

            return result;
        }
    }
}