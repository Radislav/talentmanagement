﻿using System;
using System.Web.Mvc;
using TalentManagement.Domain;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Session;
using TalentManagement.Web.Models.MyCompanyObjectives;
using TalentManagement.Web.Models.MyCompanyObjectives.Map;
using Resource = TalentManagement.Resources.Views.MyCompanyObjectives.Index;

namespace TalentManagement.Web.Controllers
{
    public class MyCompanyObjectivesController : Controller
    {
        private readonly IObjectiveRepository _objectiveRepository;
        private readonly ISessionFactory _sessionFactory;
        
        public MyCompanyObjectivesController(ISessionFactory sessionFactory)
        {
            _objectiveRepository = sessionFactory.Create().Instance<IObjectiveRepository>(FilterType.OwnedByCompany);
            _sessionFactory = sessionFactory;
        }

        private PageModel GetPageModel(string groupBy, int filterByYear,bool showAddToFocus)
        {
            var objectives = _objectiveRepository.GetObjectivesGroupedBy(PageModel.GetGroupingExpression(groupBy), filterByYear);
            using (var session = _sessionFactory.Create())
            {
                IPageModelMapper mapper = session.Instance<IPageModelMapper>(groupBy);
                PageModel pageModel = mapper.Map(objectives,showAddToFocus);
                pageModel.ActiveButton = groupBy;
                pageModel.FilterByYear = filterByYear;
                return pageModel;
            }
        }
        
        /// <summary>
        /// Gets the Company Objectives View
        /// </summary>
        /// <param name="id">If true, then link "Add to focus" will be shown for each objective.</param>
        /// <returns>Company Objectives View</returns>
        [HttpGet]
        public ActionResult Index(bool? id)
        {
            return View(GetPageModel(Resource.DateRange,DateTime.Today.Year,id.GetValueOrDefault(false)));
        }
        
        public ActionResult Filter(int filterByYear,string groupBy,bool showAddToFocus)
        {
            return PartialView("_Objectives", GetPageModel(groupBy, filterByYear, showAddToFocus));
        }
       
    }
}