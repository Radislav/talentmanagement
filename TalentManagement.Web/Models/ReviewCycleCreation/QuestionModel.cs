﻿using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Resources;
using Resource = TalentManagement.Resources.Models.ReviewCycleCreation.QuestionModel;

namespace TalentManagement.Web.Models.ReviewCycleCreation
{
    public class QuestionModel
    {
        public QuestionModel()
        {
            Settings = QuestionSetting.Required;
            AnswerType = AnswerType.Text;
        }

        [LocalizedDisplayName("Text",typeof(Resource))]
        public string Text { get; set; }

        [LocalizedDisplayName("Settings",typeof(Resource))]
        public QuestionSetting Settings { get; set; }

        [LocalizedDisplayName("AnswerType", typeof(Resource))]
        public AnswerType AnswerType { get; set; }

        public int Number { get; set; }

        public ObjectId Id { get; set; }

        public bool IsEditMode
        {
            get { return Id != ObjectId.Empty; }
        }

        public QuestionSetType SetType { get; set; }

        private string _numberAsText;
        public string NumberAsText
        {
            get
            {
                _numberAsText = _numberAsText ?? string.Format("{0} {1}:", DependencyResolver.Current.GetService<IResourceProvider>().ReviewCycleQuestion, Number);
                return _numberAsText;
            }
        }



    }
}