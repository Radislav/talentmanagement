﻿using System.Linq;
using TalentManagement.Domain;
using TalentManagement.Domain.Feedbacks;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.MyFeedbacks.Map
{
    internal class FeedbackCreateModelMap : CanPickUsersMap<Feedback, FeedbackCreateModel>
    {
        private readonly IMapper<User, UserPicked> _userPickedMapper;
        private readonly IUserRepository _userRepository;
        private readonly IFilter<IOwnedByCompany> _companyFilter;

        public FeedbackCreateModelMap(IMapper<User, UserPicked> userPickedMapper,
                              IUserRepository userRepository,
                              IFilter<IOwnedByCompany> companyFilter)
        {
            _userPickedMapper = userPickedMapper;
            _userRepository = userRepository;
            _companyFilter = companyFilter;
        }

        public void Configure(IMapperConfiguration<Feedback, FeedbackCreateModel> configuration)
        {
            configuration.ForMember(d => d.Users,
                                    s => _userRepository.GetFiltered(_companyFilter)
                                                        .Select(u => _userPickedMapper.Map(u))
                                                        .ToArray()); 
        }
    }
}