﻿using System.Collections.Generic;
using TalentManagement.Domain.Events;
using TalentManagement.Web.Infrastructure.Cache;

namespace TalentManagement.Web.Infrastructure.DomainEventHandlers
{
    internal class BadgeUpdatedHandler : IHandle<BadgeUpdatedArgs>
    {
        private readonly IOutputCacheCleaner _cacheCleaner;

        public BadgeUpdatedHandler(IOutputCacheCleaner cacheCleaner)
        {
            _cacheCleaner = cacheCleaner;
        }

        public void Handle(BadgeUpdatedArgs args)
        {
            _cacheCleaner.RemoveImageFromCache("GetBadge", "Image", new Dictionary<string, object>
                                                                                    {
                                                                                        {"companyId",args.CompanyId},
                                                                                        {"badgeId",args.BadgeId}
                                                                                    });
        }
    }
}