﻿namespace TalentManagement.Web.Models.MyNotifications
{
    public class NotificationGroupModel
    {
        public string CreatedAt { get; set; }
        public string[] Texts { get; set; }
    }
}