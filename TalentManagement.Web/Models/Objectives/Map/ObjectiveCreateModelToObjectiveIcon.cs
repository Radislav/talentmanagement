﻿using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Objectives.Map
{
    public class ObjectiveCreateModelToObjectiveIcon : IMap<ObjectiveCreateModel,ObjectiveIcon>
    {
        public void Configure(IMapperConfiguration<ObjectiveCreateModel, ObjectiveIcon> configuration)
        {
            configuration.ForMember(d => d.Image, s => s.CreateStep4.IconBytes);
            configuration.ForMember(d => d.ImageType, s => s.CreateStep4.IconContentType);
        }
    }
}