﻿namespace TalentManagement.Domain
{
    public interface IPicture
    {
        byte[] Image { get;  }
        string ImageType { get;  }
    }
}
