﻿namespace TalentManagement.Web.Models.Companies
{
    public class ObjectivesFocusTab
    {
        public ObjectivesFocusTab()
        {
            ObjectivesInFocus = new ObjectiveIconModel[0];
            FilterByYear = 0;
            GroupBy = Resources.Views.MyCompanyObjectives.Index.DateRange;
        }

        public ObjectiveIconModel[] ObjectivesInFocus { get; set; }
        public int FilterByYear { get; set; }
        public string GroupBy { get; set; }
    }
}