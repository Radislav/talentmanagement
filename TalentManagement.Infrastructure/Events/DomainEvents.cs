﻿using System.Collections.Generic;
using Autofac;
using TalentManagement.Domain.Events;

namespace TalentManagement.Infrastructure.Events
{
    internal class DomainEvents : IDomainEvents
    {
        private readonly ILifetimeScope _lifetimeScope;

        public DomainEvents(ILifetimeScope lifetimeScope)
        {
            _lifetimeScope = lifetimeScope;
        }

        public void Raise<T>(T args) where T : IDomainEventArgs
        {
            foreach (var handler in _lifetimeScope
                .Resolve<IEnumerable<IHandle<T>>>())
            {
                handler.Handle(args);
            }
        }
    }
}
