﻿using System.Collections.Generic;
using MongoDB.Bson;
using TalentManagement.Domain;
using TalentManagement.Domain.Feeds;
using System.Linq;

namespace TalentManagement.Infrastructure.Data.Repositories
{
    class MyFeedRepository : DecoratedRepository<Feed,ObjectId> , IMyFeedRepository
    {
        public MyFeedRepository(IRepositoryWithTypedId<Feed,ObjectId> decorated ,IFilter<Feed> feedFilter)
            : base(decorated,feedFilter)
        {
           
        }
  
        public IEnumerable<Feed> GetFeeds(IFilter filter)
        {
            return AsQueryable()
                 .ApplyFilter(filter)
                .OrderByDescending(f => f.CreatedAt);

        }
    }
}
