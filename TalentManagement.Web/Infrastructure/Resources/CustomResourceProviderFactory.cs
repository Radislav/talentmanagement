﻿using System;
using System.Web.Compilation;

namespace TalentManagement.Web.Infrastructure.Resources
{
    public class CustomResourceProviderFactory : ResourceProviderFactory
    {
        public override IResourceProvider CreateGlobalResourceProvider(string classKey)
        {
            return new FrameworkResourceProvider(classKey);
        }

        public override IResourceProvider CreateLocalResourceProvider(string virtualPath)
        {
            throw new NotSupportedException(String.Format("{0} does not support local resources.",GetType().Name));
        }
    }
}