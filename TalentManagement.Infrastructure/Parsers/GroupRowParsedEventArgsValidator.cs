﻿using FluentValidation;
using System.Linq;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Validation;

namespace TalentManagement.Infrastructure.Parsers
{
    internal class GroupRowParsedEventArgsValidator : AbstractValidator<GroupRowParsedEventArgs>, TalentManagement.Infrastructure.Validation.IValidator<GroupRowParsedEventArgs>
    {
        private readonly IResourceProvider _resourceProvider;

        public GroupRowParsedEventArgsValidator(IResourceProvider resourceProvider)
        {
            _resourceProvider = resourceProvider;
            RuleFor(m => m.Email)
                .NotEmpty()
                .Matches(RegularExpressions.Email);

            RuleFor(m => m.Password)
                .NotEmpty();

            RuleFor(m => m.FirstName)
                .NotEmpty();

            RuleFor(m => m.ReportingManagerEmailLocal)
                .Matches(RegularExpressions.EmailUserName)
                .When(m => false == string.IsNullOrEmpty(m.ReportingManagerEmailLocal))
                .WithMessage(_resourceProvider.ErrorEmailInvalid);

            RuleFor(m => m.ReportingManagerEmailAppendix)
                .Matches(RegularExpressions.EmailDomainFull)
                .When(m => false == string.IsNullOrEmpty(m.ReportingManagerEmailLocal))
                .WithMessage(_resourceProvider.ErrorEmailInvalid);
        }

        public ValidationResult ValidateInstance(GroupRowParsedEventArgs instance)
        {
            return new ValidationResult(Validate(instance).Errors.Select(e => new ValidationFailure(e.PropertyName, e.ErrorMessage, e.AttemptedValue)));
        }
    }
}
