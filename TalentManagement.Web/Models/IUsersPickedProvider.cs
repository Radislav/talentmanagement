﻿using System.Collections.Generic;
using TalentManagement.Domain.Users;

namespace TalentManagement.Web.Models
{
    public interface IUsersPickedProvider
    {
        IEnumerable<UserPicked> GetUsersToPick();
        IEnumerable<UserPicked> GetUsersToPick(IEnumerable<User> customSource,string key);
    }
}
