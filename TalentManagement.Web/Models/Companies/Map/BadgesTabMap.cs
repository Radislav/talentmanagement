﻿using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain.Companies;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Companies.Map
{
    internal class BadgesTabMap : IMap<IEnumerable<Badge>, BadgesTab>
    {
        private readonly IBadgeModelMapper _badgeModelMapper;

        public BadgesTabMap(IBadgeModelMapper badgeModelMapper)
        {
            _badgeModelMapper = badgeModelMapper;
        }

        public void Configure(IMapperConfiguration<IEnumerable<Badge>, BadgesTab> configuration)
        {
            configuration.ForMember(d => d.Badges,
                s => s.Select(badge => _badgeModelMapper.Map(badge.CompanyId, badge)));

        }
    }
}