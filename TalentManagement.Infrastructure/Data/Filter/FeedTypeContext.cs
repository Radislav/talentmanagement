﻿using TalentManagement.Domain;
using TalentManagement.Domain.Feeds;

namespace TalentManagement.Infrastructure.Data.Filter
{
    public class FeedTypeContext : IFilterContext
    {
        public FeedTypeContext(FeedType feedType)
        {
            FeedType = feedType;
        }

        public FeedType FeedType { get; set; }
    }
}
