﻿using TalentManagement.Domain.Companies;
using TalentManagement.Domain.Events;

namespace TalentManagement.Infrastructure.Events
{
    internal class BadgeUpdatedHandler : IHandle<BadgeUpdatedArgs>
    {
        private readonly ICompanyRepository _companyRepository;

        public BadgeUpdatedHandler(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        public void Handle(BadgeUpdatedArgs args)
        {
            Company company = _companyRepository.GetById(args.CompanyId);
            if (company != null)
            {
                if (args.IsEnabled)
                {
                    company.AddBadgeId(args.BadgeId);     
                }
                else
                {
                    company.RemoveBadgeId(args.BadgeId);
                }
                _companyRepository.UpsertBadgesOnly(company);   
            }
            
        }
    }
}
