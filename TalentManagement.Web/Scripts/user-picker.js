﻿function UserPicker(args) {
    
    this._args = args == null ? new Object() : args;

    if (this._args.prefix == null || this._args.prefix == "") {
        this._args.prefix = "#";
    } else {
        this._args.prefix = "#" + this._args.prefix + "_";
    }

    this._args.container = this._args.container == null ? $("html") : this._args.container;
    
    var _args = this._args;
    var picker = this;
    
    this._args.container.find('input[data-provide = "typeahead"]')
        .off('change')
        .on('change', function () {
            
            var isElementPicked = (-1 != $.inArray($(this).val(), JSON.parse($(this).attr('data-source'))));

            if (isElementPicked) {
                $(this).parents('.user-picker-control')
                                        .find('#pickUserButton')
                                        .trigger('click');
            }
        });

    this._args.container.find('.contributor button')
                .off('click')
                .on('click', function () {
                    var contributorElement = $(this).parents('.contributor');
                    var contributor = createContributor(contributorElement, _args.prefix);
                    contributorElement.remove();

                    $(_args.container).trigger('userRemoved', [picker.getPickerModel(), contributor.Id]);
                });

    
    
    this._args.container.find('#pickUserButton')
        .off('click')
        .on('click', function () {
            $(_args.container).trigger('userPicked', [picker.getPickerModel(),picker.getPickedUser()]);
        });

    this.bindUserPicked = function (handler) {
        $(_args.container).on('userPicked', handler);
    };
    this.bindUserRemoved = function (handler) {
        $(_args.container).on('userRemoved', handler);
    };
    this.getPickedUser = function () {
        var prefix = this._args.prefix;
        var container = this._args.container;
        var selectedNames = $(container).find(prefix + "CurrentNames").val().split(',');
        if (selectedNames == null || selectedNames.length < 2) {
            return "invalid_user_id";
        }
        var secondName = selectedNames[0].trim();
        var firstName = selectedNames[1].trim();
        var users = JSON.parse($(container).find(prefix + "UsersJson").val());

        var id = "invalid_user_id";
        $.each(users, function () {
            if (this.FirstName == firstName && this.SecondName == secondName) {
                id = this.Id;
                return;
            }
        });
        return id;
    };

    this.getPickerModel = function () {
        var users = [];
        var prefix = this._args.prefix;
        var container = $(this._args.container);
        container.find('.contributor').each(function () {
            users.push(createContributor($(this), prefix));
        });

        return {
            CurrentNames: container.find(prefix + 'CurrentNames').val(),
            UserNames: container.find(prefix + 'UserNames').val(),
            UsersJson: container.find(prefix + 'UsersJson').val(),
            UsersPicked: users,
            Title: container.find(prefix + 'user-picker-title').html(),
            Prefix: container.find('.user-picker-control').data('prefix')
        };
    };

    this.getFormModel = function () {
        var firstNames = this.getProperties('FirstName');
        var secondNames = this.getProperties('SecondName');
        var ids = this.getProperties('Id');
        var prefixes = this.getProperties('Prefix');

        if (ids.length == 0) {
            return null;
        }

        return {
            FirstName: arrayOrNull(firstNames),
            SecondName: arrayOrNull(secondNames),
            Id: arrayOrNull(ids),
            Prefix : arrayOrNull(prefixes)
        };
    };

    function  arrayOrNull(arr) {
        return arr.length > 0 ? arr : null;
    }

    this.getProperties = function (propName) {
        var properties = [];
        var prefix = this._args.prefix;
        var container = $(this._args.container);
        container.find('.contributor').each(function () {
            var prop = getProp($(this), prefix, propName);
            if (prop != null && prop != '') {
                properties.push(getProp($(this), prefix, propName));    
            }
        });
        return properties;
    };

    this.enable = function () {
        var container = this._args.container;
        var title = $(container).find('#user-picker-title');
        var prevColor = title.data('prev-color');

        title.css('color', prevColor);
        $(container).find('input').removeAttr('disabled');
        $(container).find('button').removeAttr('disabled');
    };

    this.disable = function () {
        var container = this._args.container;
        var title = $(container).find('#user-picker-title');
        title.data('prev-color', title.css('color'));
        title.css('color', 'gray');
        $(container).find('input').attr('disabled', 'disabled');
        $(container).find('button').attr('disabled', '');
    };
    function createContributor(contributor,prefix) {
        return {
            Id: getProp(contributor,prefix,'Id'),
            FirstName: getProp(contributor,prefix,'FirstName'),
            SecondName: getProp(contributor, prefix, 'SecondName'),
            Prefix : $(contributor).data('prefix')
        };
    }

    function getProp(contributor, prefix,propName) {
        return $(contributor).find(prefix + propName).val();
    }
}