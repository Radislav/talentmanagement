﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using TalentManagement.Domain.Feeds;

namespace TalentManagement.Web.Models.Feeds
{
    public class FeedModel
    {
        public FeedModel()
        {
            CommentSection = new CommentSectionModel();
        }

        public FeedType FeedType { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedAt { get; set; }
        public string Title { get; set; }
        
        public ObjectId Id { get; set; }

        public CommentSectionModel CommentSection { get; set; }
        
        public Dictionary<string,string> Properties { get; set; }

        public string GetProperty(string key)
        {
            return Properties.GetSafe(pr => pr.ContainsKey(key) ? Properties[key] : String.Empty);
        }
        
        public bool HasBottomLine { get; set; }
    }
}