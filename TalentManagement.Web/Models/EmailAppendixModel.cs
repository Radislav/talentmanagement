﻿using System;
using System.Web.Mvc;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Web.Models
{
    internal class EmailAppendixModel
    {
        private string _email;

        public EmailAppendixModel(string email)
        {
            _email = email;
        }

        private string Email
        {
            get
            {
                if (string.IsNullOrEmpty(_email))
                {
                    _email = DependencyResolver.Current.GetService<IPrincipalProvider>().GetUserName();
                }
                return _email;
            }
        }
        
        public string EmailAppendix
        {
            get { return String.Concat("@", Email.EmailDomainLong()); }
        }
        
        public string CreateEmail(string emailUserName)
        {
            return String.IsNullOrEmpty(emailUserName) ? null : String.Concat(emailUserName, EmailAppendix);
        }
    }
}