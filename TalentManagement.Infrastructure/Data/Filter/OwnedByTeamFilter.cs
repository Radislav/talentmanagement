﻿using System.Linq;
using TalentManagement.Domain;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data.Filter
{
    class OwnedByTeamFilter : FilterBase<IOwnedByTeam>
    {
           private readonly IPrincipalProvider _principalProvider;
        private IFilterContext _filterContext;

        public OwnedByTeamFilter(IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
        }

        public override IFilter<IOwnedByTeam> Filter(IFilterContext[] contexts)
        {
            OwnedByTeamContext context = GetContextOrFail<OwnedByTeamContext>(contexts);

            if (context != null && false == string.IsNullOrEmpty(context.Team))
            {
                Data = Data.Where(o => o.Team == context.Team);        
            }
            return this;
        }

        public override IFilterContext FilterContext
        {
            get
            {
                return _filterContext ?? new OwnedByTeamContext(_principalProvider.GetTeam());
            }
            set { _filterContext = value; }
        }
    }
}
