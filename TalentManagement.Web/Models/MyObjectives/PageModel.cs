﻿namespace TalentManagement.Web.Models.MyObjectives
{
    public class PageModel
    {
        public PageModel()
        {
            ObjectiveContainers = new ObjectiveContainer[0];
        }

        public ObjectiveContainer[] ObjectiveContainers { get; set; }
    }
}