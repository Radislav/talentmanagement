﻿using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.MyCompanyObjectives.Map
{
    class PageModelMapper<TGroupKey> : IPageModelMapper<TGroupKey> 
    {
        private readonly IMapper<Objective, ObjectiveSimpleModel> _objectiveSimpleModelMapper;
        private readonly ITitleFormatter<TGroupKey> _titleFormatter;

        public PageModelMapper(IMapper<Objective, ObjectiveSimpleModel> objectiveSimpleModelMapper, ITitleFormatter<TGroupKey> titleFormatter)
        {
            _objectiveSimpleModelMapper = objectiveSimpleModelMapper;
            _titleFormatter = titleFormatter;
        }

        public PageModel Map(IEnumerable<IGrouping<object, Objective>> source,bool showAddToFocus)
        {
            return new PageModel
                       {
                           ShowAddToFocus = showAddToFocus,
                           ObjectiveGroups =
                               source.Select(group => new ObjectiveGroupModel
                                                          {
                                                              Title = _titleFormatter.FormatTitle((TGroupKey)group.Key),
                                                              Objectives = group.Select(objective =>
                                                                                            {
                                                                                                ObjectiveSimpleModel model = _objectiveSimpleModelMapper.Map(objective);
                                                                                                model.ShowAddToFocus = showAddToFocus;
                                                                                                return model;
                                                                                            })
                                                                                .ToArray()        
                                                          }).ToArray()
                       };
        }
    }
}