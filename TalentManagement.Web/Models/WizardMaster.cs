﻿using System.Collections.Generic;
using System.Linq;

namespace TalentManagement.Web.Models
{
    public abstract class WizardMaster
    {
        public int CurrentStep { get; set; }
        public abstract IEnumerable<WizardPage> Pages { get; }
        public virtual bool IsFlowFinished
        {
            get
            {
                if (Pages == null || Pages.Any(p => p == null))
                {
                    return false;
                }

                return Pages.All(page => page.IsReadyToGo && CurrentStep == Pages.Max(p => p.ThisStep));
            }
        }
    }
}