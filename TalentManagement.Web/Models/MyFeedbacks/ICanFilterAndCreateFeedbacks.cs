﻿namespace TalentManagement.Web.Models.MyFeedbacks
{
    public interface ICanFilterAndCreateFeedbacks : ICanActivateButton
    {
        FeedbackCreateModel FeedbackCreateModel { get; set; }
        KudosCreateModel KudosCreateModel { get; set; }
    }
}
