﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Resources;
using ResourceModel = TalentManagement.Resources.Models.MyCompanyObjectives.PageModel;

namespace TalentManagement.Web.Models.MyCompanyObjectives
{
    public class PageModel  : ICanActivateButton
    {
        private  static readonly IResourceProvider ResourceProvider = DependencyResolver.Current.GetService<IResourceProvider>();
      
        private GotoLinkModel[] _gotoLinkModels;
        private ObjectiveGroupModel[] _groupModels;
        private readonly YearFilterItem[] _yearFilterItems;

        public PageModel()
        {
            _groupModels = new ObjectiveGroupModel[0];
            ActiveButton = ResourceProvider.MyCompanyObjectiveDateRange;
            _yearFilterItems = YearFilterItem.CreateDropdownItems();
            FilterByYear = 0; //All years by default.
        }

        public string ActiveButton { get; set; }
        public bool ShowAddToFocus { get; set; }

        public ObjectiveGroupModel[] ObjectiveGroups
        {
            get { return _groupModels; }
            set
            {
                _groupModels = value;
                _gotoLinkModels = null;
            }
        }
        
        public GotoLinkModel[] GotoLinks
        {
            get
            {
                _gotoLinkModels = _gotoLinkModels ?? 
                                   ObjectiveGroups.Select(group => 
                                                              new GotoLinkModel
                                                                  {
                                                                      Text = string.Format("{0} {1}", ResourceProvider.MyCompanyObjectiveGoto, group.Title),
                                                                      Reference = group.Title
                                                                  }
                    )
                                                 .ToArray();
                return _gotoLinkModels;
            }
        }
        
        public YearFilterItem[] YearFilterItems
        {
            get { return _yearFilterItems; }
        }

        [LocalizedDisplayName("FilterByYear", typeof(ResourceModel))]
        public int FilterByYear { get; set; }
        
        public static Expression<Func<Objective, object>> GetGroupingExpression(string groupBy)
        {
            if (groupBy == ResourceProvider.MyCompanyObjectiveDateRange)
            {
                return objective => new MonthYearPair(objective.Due.Month, objective.Due.Year);
            }

            if (groupBy == ResourceProvider.MyCompanyObjectivePerson)
            {
                return objective => objective.CreatedBy;
            }

            if (groupBy == ResourceProvider.MyCompanyObjectiveStatus)
            {
                return objective => objective.Status;
            }
           
            throw new NotSupportedException(String.Format("Specified grouping parameter '{0}' has not appropriate expression",groupBy));
           
        }
        
    }
}