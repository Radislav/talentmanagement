﻿using TalentManagement.Domain.Companies;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Companies.Map
{
    internal class IndexModelMap : IMap<Company,IndexModel>
    {
        private readonly IMapper<Company, GeneralTab> _generalTabMapper;
        private readonly IMapper<Company, BadgesTab> _badgesTabMapper;

        public IndexModelMap(IMapper<Company, GeneralTab> generalTabMapper,IMapper<Company,BadgesTab> badgesTabMapper)
        {
            _generalTabMapper = generalTabMapper;
            _badgesTabMapper = badgesTabMapper;
        }

        public void Configure(IMapperConfiguration<Company, IndexModel> configuration)
        {
            configuration.ForMember(d => d.GeneralTab, s => _generalTabMapper.Map(s));
            configuration.ForMember(d => d.BadgesTab, s => _badgesTabMapper.Map(s));
        }
    }
}