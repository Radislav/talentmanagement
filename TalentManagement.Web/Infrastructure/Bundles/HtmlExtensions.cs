﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Optimization;

namespace TalentManagement.Web.Infrastructure.Bundles
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString Bundles<TModel>(this HtmlHelper<TModel> html,string name)
        {
            var bundles = BundlesRegistration.GetBundlesForName(name);
            IEnumerable<string> result = RenderBundleUrls(bundles);
            
            return new MvcHtmlString(String.Join(String.Empty, result));
        }

        private static IEnumerable<string> RenderBundleUrls(IEnumerable<BundleElement> bundles)
        {
            List<string> result = new List<string>();
            foreach (BundleElement bundleElement in bundles)
            {
                switch (bundleElement.BundleType)
                {
                    case BundleType.css:
                        result.Add(Styles.Render(bundleElement.Name).ToHtmlString());
                        break;
                    case BundleType.js:
                        result.Add(Scripts.Render(bundleElement.Name).ToHtmlString());
                        break;
                }
            }
            return result;
        }

        public static MvcHtmlString Bundles<TModel>(this HtmlHelper<TModel> html)
        {
            IEnumerable<string> result = new List<string>();
            var view = html.ViewContext.View as BuildManagerCompiledView;
            if (view != null)
            {
                var bundles = BundlesRegistration.GetBundlesForView(view.ViewPath);
                result = RenderBundleUrls(bundles);
            }
            return new MvcHtmlString(String.Join(String.Empty, result));
        }
    }
}