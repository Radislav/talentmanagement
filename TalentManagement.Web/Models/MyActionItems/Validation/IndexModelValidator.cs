﻿using System;
using FluentValidation;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.MyActionItems.Validation
{
    internal class ActionItemModelValidator : AbstractValidator<ActionItemModel>,
                                        TalentManagement.Infrastructure.Validation.IValidator<ActionItemModel>
    {
        public ActionItemModelValidator()
        {

            RuleFor(m => m.Name)
                .NotEmpty();

            RuleFor(m => m.DueString)
                .NotEmpty();

            RuleFor(m => m.DueString)
                .DateTime()
                .When(m => false == string.IsNullOrEmpty(m.DueString));

            RuleFor(m => m.DueString)
                .Must(due => DateTime.Parse(due) >= DateTime.Today)
                .When(m =>
                          {
                              DateTime dateTime;
                              return DateTime.TryParse(m.DueString, out dateTime);
                          })
                .WithLocalizedMessage(() => Resources.Errors.DateMoreThenToday);
        }

        public TalentManagement.Infrastructure.Validation.ValidationResult ValidateInstance(ActionItemModel instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}