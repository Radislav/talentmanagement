﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using TalentManagement.Domain;

namespace TalentManagement.Infrastructure.Data.Filter
{
    public class CompositeContext : IFilterContext
    {
        private readonly IFilterContext[] _contexts;

        public CompositeContext(IEnumerable<IFilterContext> contexts)
        {
            Contract.Requires(contexts != null);

            _contexts = contexts.ToArray();
        }

        public IEnumerable<IFilterContext> Contexts
        {
            get { return _contexts; }
        }
    }
}
