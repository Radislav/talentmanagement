﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace TalentManagement.Domain.Objectives
{
    public interface IActionItemRepository : IRepository<ActionItem>
    {
        /// <summary>
        /// Get Action Items related to Objective. 
        /// </summary>
        /// <param name="owner">Owner of the Action Item.</param>
        /// <param name="status">Status of the Action Item. If null then any statuses will be returned.</param>
        /// <returns>Dictionary where key is Objective Id, value is IEnumerable of ActionsItems.</returns>
        ActionItemsDictionary GetRelated(string owner, ActionItemStatus? status);

        /// <summary>
        /// Get Action Items not related to Objective. 
        /// </summary>
        /// <param name="owner">Owner of the Action Item.</param>
        /// <param name="status">Status of the Action Item. If null then any statuses will be returned.</param>
        /// <returns>IEnumerable of ActionItems</returns>
        IEnumerable<ActionItem> GetNotRelated(string owner, ActionItemStatus? status);
    }
}
