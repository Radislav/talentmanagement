﻿using System.Linq;
using MongoDB.Bson;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data.Repositories
{
    internal class ObjectiveIconRepository : Repository<ObjectiveIcon> , IObjectiveIconRepository
    {
        public ObjectiveIconRepository(IUnitOfWork unitOfWork,IPrincipalProvider principalProvider)
            : base(unitOfWork,principalProvider)
        {
        }
        
        public ObjectId[] GetIconIds()
        {
            return UnitOfWork.AsQueryable<ObjectiveIcon>()
                             .Select(icon => icon.Id)
                             .ToArray();
        }
    }
}
