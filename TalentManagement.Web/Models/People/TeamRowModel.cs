﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TalentManagement.Web.Models.People
{
    public class TeamRowModel
    {
        public string Name { get; set; }

        public UserRowModel[] Users { get; set; }
    }
}