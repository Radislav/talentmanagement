﻿using System;
using System.Web;
using TalentManagement.Domain;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Web.Infrastructure.Security
{
    internal class PrincipalProvider : IPrincipalProvider
    {
        public TenantPrincipal GetPrincipal()
        {
            return HttpContext.Current.User as TenantPrincipal;
        }
        
        public TenantIdentity GetIdentity()
        {
            TenantPrincipal principal = GetPrincipal();
            return principal.GetSafe(pr => pr.Identity as TenantIdentity);
        }
        
        public string GetUserName()
        {
            return GetIdentity().GetSafe(identity => identity.Name);
        }


        public string GetTenant()
        {
            return GetIdentity().GetSafe(identity => identity.Tenant);
        }


        public string GetTenantFor(string email)
        {
            return TenantIdentity.GetTenantFor(email);
        }
        
        public string GetTeam()
        {
            //TODO: this is a fake
            return GetTenant();
        }
        
        public OwnedDescriptor GetOwnderDescriptor()
        {
            return new OwnedDescriptor(GetUserName(),DateTime.Now,GetTeam(),GetTenant());
        }
    }
}