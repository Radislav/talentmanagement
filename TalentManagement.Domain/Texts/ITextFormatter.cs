﻿namespace TalentManagement.Domain.Texts
{
    public interface ITextFormatter
    {
        string Format(string format, object argument);
    }

    public interface ITextFormatter<in TArgument> : ITextFormatter
    {
        string Format(string format, TArgument argument);
    }
}
