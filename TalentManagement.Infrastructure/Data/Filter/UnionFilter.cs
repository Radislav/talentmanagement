﻿using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain;

namespace TalentManagement.Infrastructure.Data.Filter
{
    internal class UnionFilter<TModel> : FilterBase<TModel>
    {
        private readonly IFilter[] _filters = new IFilter[0];
        private readonly int _prority;
        private IFilterContext _filterContext;

        public UnionFilter(IEnumerable<IFilter> filters)
        {
            if (filters != null)
            {
                _filters = filters.ToArray();

                _prority = _filters.Max(f => f.Priority);
            }
        }

        public override int Priority
        {
            get { return _prority; }
        }

        public override IFilter<TModel> Filter(IFilterContext[] contexts)
        {
            IEnumerable<TModel> buffer = new TModel[0];

            _filters.Map(filter =>
                                {
                                    buffer = buffer.Union(filter.Init(Data)
                                        .Filter(contexts)
                                        .AsEnumerable()
                                        .Cast<TModel>());
                                });
            Data = buffer;
            return this;
        }
        
        public override IFilterContext FilterContext
        {
            get
            {
                return _filterContext ?? new CompositeContext(_filters.GetSafe(filters => filters.Select(f => f.FilterContext)));
            }
            set { _filterContext = value; }
        }
    }
}
    