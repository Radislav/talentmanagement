﻿using System.Diagnostics.Contracts;
using System.Linq;
using TalentManagement.Domain.Texts;

namespace TalentManagement.Domain.Users
{
    public class NotificationSetting : Entity
    {
        public bool IsEnabled { get; set; }

        public string GetText(params Text[] notifications)
        {
            Contract.Requires<EntityNullException>(notifications != null);

            return notifications.GetSafe(n1 => n1.SingleOrDefault(n2 => n2.Id == Id))
                                .GetSafe(n3 => n3.Content);
        }
    }
}
