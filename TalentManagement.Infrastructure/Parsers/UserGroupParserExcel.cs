﻿using ClosedXML.Excel;
using System.IO;

namespace TalentManagement.Infrastructure.Parsers
{
    internal class UserGroupParserExcel : ExcelRowParser<GroupRowParsedEventArgs>, 
                                     ITemplateProvider<GroupRowParsedEventArgs>
    {
        private const string SheetName = "Upload Data";
        private const int HeaderRowIndex = 1;
        private const string EmailColumnLetter = "A";
        private const string PasswordColumnLetter = "B";
        private const string FirstNameColumnLetter = "C";
        private const string SecondNameColumnLetter = "D";
        private const string PositionColumnLetter = "E";
        private const string TeamColumnLetter = "F";
        private const string ReportingManagerEmailLocalColumnLetter = "G";
        private const string ReportingManagerEmailAppendixColumnLetter = "H";
        private const string InvalidFileMessage = "contents is not a stream with a valid excel file.";

        public UserGroupParserExcel()
            :base(SheetName,HeaderRowIndex,InvalidFileMessage)
        {
        }

        public override GroupRowParsedEventArgs ParseRow(IXLRow row)
        {
            bool rowsEmpty = true;
            var email = SetIsEmpty(row.Cell(EmailColumnLetter).GetValue<string>(), ref rowsEmpty);
            var password = SetIsEmpty(row.Cell(PasswordColumnLetter).GetValue<string>(), ref rowsEmpty);
            var firstName = SetIsEmpty(row.Cell(FirstNameColumnLetter).GetValue<string>(), ref rowsEmpty);
            var secondName = SetIsEmpty(row.Cell(SecondNameColumnLetter).GetValue<string>(), ref rowsEmpty);
            var position = SetIsEmpty(row.Cell(PositionColumnLetter).GetValue<string>(), ref rowsEmpty);
            var team = SetIsEmpty(row.Cell(TeamColumnLetter).GetValue<string>(), ref rowsEmpty);
            var managersEmailLocal = SetIsEmpty(row.Cell(ReportingManagerEmailLocalColumnLetter).GetValue<string>(), ref rowsEmpty);

            //rowsEmpty is not set for email appendix as it is always in the template
            var managersEmailAppendix = row.Cell(ReportingManagerEmailAppendixColumnLetter).GetValue<string>();
            if (!rowsEmpty)
            {
                var rowParsedArgs = new GroupRowParsedEventArgs(row.RowNumber())
                {
                    Email = email,
                    Password = password,
                    FirstName = firstName,
                    SecondName = secondName,
                    Position = position,
                    Team = team,
                    ReportingManagerEmailLocal = managersEmailLocal,
                    ReportingManagerEmailAppendix = managersEmailAppendix
                };

                return rowParsedArgs;
            }

            return null;
        }

        private string SetIsEmpty(string value, ref bool isEmpty)
        {
            if (!string.IsNullOrEmpty(value))
            {
                isEmpty = false;
            }

            return value;
        }

        public byte[] GetTemplateContents(object reportingManagerDomainLong)
        {
            var workbook = new XLWorkbook();
            var worksheet = workbook.AddWorksheet(SheetName);
            worksheet.Cell(HeaderRowIndex, EmailColumnLetter).Value = "Email";
            worksheet.Cell(HeaderRowIndex, PasswordColumnLetter).Value = "Password";
            worksheet.Cell(HeaderRowIndex, FirstNameColumnLetter).Value = "First Name";
            worksheet.Cell(HeaderRowIndex, SecondNameColumnLetter).Value = "Second Name";
            worksheet.Cell(HeaderRowIndex, PositionColumnLetter).Value = "Position";
            worksheet.Cell(HeaderRowIndex, TeamColumnLetter).Value = "Team";
            worksheet.Cell(HeaderRowIndex, ReportingManagerEmailLocalColumnLetter).Value = "Reporting manager's e-mail";
            for (int i = 2; i <= 50; i++)
            {
                worksheet.Cell(i, ReportingManagerEmailAppendixColumnLetter).Value = "@" + reportingManagerDomainLong;
            }

            using (var memoryStream = new MemoryStream())
            {
                workbook.SaveAs(memoryStream);
                return memoryStream.ToArray();
            }
        }

        public string TemplateContentType
        {
            get { return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; }
        }

        public string FileName
        {
            get { return "TalentManagement.UploadUsers.xlsx"; }
        }
    }
}
