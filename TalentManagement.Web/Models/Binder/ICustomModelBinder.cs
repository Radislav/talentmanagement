﻿using System.Web.Mvc;

namespace TalentManagement.Web.Models.Binder
{
    public interface ICustomModelBinder<T> : IModelBinder
    {
        ICustomModelBinder<T> SetPrefix(string prefix);
        ICustomModelBinder<T> SetIndex(int index);

    }
}