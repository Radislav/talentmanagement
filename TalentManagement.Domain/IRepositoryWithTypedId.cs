﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using TalentManagement.Domain.Contracts;

namespace TalentManagement.Domain
{
    [ContractClass(typeof(RepositoryWithTypedIdContract<,>))]
    public interface IRepositoryWithTypedId<TEntity, in TId>
        where TEntity : EntityWithTypedId<TId>  
    {
        void Insert(TEntity entity);
        void DeleteById(TId id);
        void Update(TEntity entity);
        IQueryable<TEntity> AsQueryable();
        TEntity GetById(TId id);
        TEntity Upsert(TEntity entity);
        IEnumerable<TEntity> ApplyFilter(IFilter filter);
    }
}