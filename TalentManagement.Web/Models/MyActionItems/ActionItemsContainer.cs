﻿namespace TalentManagement.Web.Models.MyActionItems
{
    public class ActionItemsContainer
    {
        public string Title { get; set; }
        public ActionItemModel[] ActionItems { get; set; }
    }
}