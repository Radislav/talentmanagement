﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using MongoDB.Bson;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Infrastructure.Serialization;
using TalentManagement.Web.Infrastructure;

namespace TalentManagement.Web.Models.CycleDetails
{
    public class IndexModel
    {
        public IndexModel()
        {
            Reviews = new ReviewModel[0];
            Cycles = new ReviewCycleDescriptor[0];
            ActiveCycle = ObjectId.Empty;
        }

        public IEnumerable<ReviewModel> Reviews { get; set; }
        public string CycleProgressText { get; set; }
        public decimal Progress { get; set; }
        public IEnumerable<ReviewCycleDescriptor> Cycles { get; set; }
        public ObjectId ActiveCycle { get; set; }

        public IEnumerable<SelectListItem> CyclesSelectItems
        {
            get
            {
                var cyclesWithDefault = Cycles.ToList();
                string selectCycleText =
                    DependencyResolver.Current.GetService<IResourceProvider>().CycleDetailsSelectCycle;
                cyclesWithDefault.Add(new ReviewCycleDescriptor(ObjectId.Empty, selectCycleText,0));

                return cyclesWithDefault.Select(cycle => new SelectListItem
                    {
                        Selected = cycle.Id == ActiveCycle,
                        Text = cycle.Name,
                        Value = cycle.Id.ToString()
                    });
            }
        }

        public string CyclesProgress
        {
            get
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var pairs = Cycles.Select(c => new KeyValuePair<string, decimal>(c.Id.ToString(), c.Progress));
                return serializer.Serialize(pairs);
            }
        }
    }
}