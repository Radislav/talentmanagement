﻿using System.Diagnostics.Contracts;
using TalentManagement.Infrastructure.Contracts;

namespace TalentManagement.Infrastructure.Security
{
    [ContractClass(typeof(HashComputerContract))]
    public interface IHashComputer
    {
        string ComputeHash(string source);
    }
}
