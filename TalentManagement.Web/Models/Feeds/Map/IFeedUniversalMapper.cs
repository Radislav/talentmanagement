﻿using TalentManagement.Domain.Feeds;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Feeds.Map
{
    public interface IFeedUniversalMapper : IMapper<Feed, FeedModel>
    {
    }
}
