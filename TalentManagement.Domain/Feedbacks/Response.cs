﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using MongoDB.Bson;

namespace TalentManagement.Domain.Feedbacks
{
    public class Response : Entity , IOwned
    {
        private List<Comment> _comments;
        private readonly List<Comment> _newComments;
   
        public Response()
        {
            _comments = new List<Comment>();
            _newComments = new List<Comment>();
        }

        public string Answer { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedAt { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedAt { get; set; }

        public IEnumerable<Comment> Comments
        {
            get { return _comments; }
            private set { _comments = value.ListOrDefault(); }
        }

        internal IEnumerable<Comment> NewComments
        {
            get { return _newComments; }
        }

        public void AddComment(Comment comment)
        {
            Contract.Requires<EntityNullException>(comment != null);
            
            comment.Id = ObjectId.GenerateNewId(DateTime.Now);
            _comments = _comments ?? new List<Comment>();
            _comments.Add(comment);

            _newComments.Add(comment);
        }
    }
}
