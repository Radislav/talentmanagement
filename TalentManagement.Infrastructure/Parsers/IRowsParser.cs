﻿using System;

namespace TalentManagement.Infrastructure.Parsers
{
    public interface IRowsParser<in TRow, TRowParsedEventArgs> : IParser
        where TRowParsedEventArgs : RowParsedEventArgs
    {
        TRowParsedEventArgs ParseRow(TRow source);
        event EventHandler<TRowParsedEventArgs> RowParsed;
    }
}
