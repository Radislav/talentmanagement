﻿using Resource = TalentManagement.Resources.Models.MyFeedbacks.DetailsModel;

namespace TalentManagement.Web.Models.MyFeedbacks
{
    public class DetailsModel 
    {
        private const string NumberPrefix = "#";
        
        [LocalizedDisplayName("Feedback", typeof(Resource))]
        public string Feedback { get; set; }

        [LocalizedDisplayName("Title", typeof(Resource))]
        public string Title { get; set; }
        
        public int ParticipantsCount { get; set; }
        public string FeedbackDate { get; set; }
        public int ResponsesCount { get; set; }

        [LocalizedDisplayName("ResponseSummary",typeof(Resource))]
        public string ResponseSummary { get; set; }

        private DetailsResponseModel[] _responses;
        public DetailsResponseModel[] Responses
        {
            get { return _responses; }
            set
            {
                _responses = value;
                for (int idx = 0; idx < _responses.Length; idx++)
                {
                    _responses[idx].NumberString = string.Format("{0}{1}", NumberPrefix, idx);
                }
            }
        }
    }
}