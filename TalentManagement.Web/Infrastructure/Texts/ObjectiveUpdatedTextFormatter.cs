﻿using TalentManagement.Infrastructure;

namespace TalentManagement.Web.Infrastructure.Texts
{
    internal class ObjectiveUpdatedTextFormatter : ObjectiveTextFormatter
    {
        public ObjectiveUpdatedTextFormatter(IUrlProvider urlProvider)
            : base(urlProvider)
        {

        }
    }
}
