﻿namespace TalentManagement.Web.Models.MyFeedbacks
{
    public class FeedbackYouRequested
    {
        public string Title { get; set; }
        public string CreationDate {get; set; }
        public string ResponsesText { get; set; }
        public string FeedbackId { get; set; }

    }
}