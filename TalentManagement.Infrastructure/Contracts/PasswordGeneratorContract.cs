﻿using System;
using System.Diagnostics.Contracts;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Contracts
{
    [ContractClassFor(typeof(IPasswordGenerator))]
    internal  abstract class PasswordGeneratorContract : IPasswordGenerator
    {
        public string GeneratePassword(int length)
        {
            Contract.Requires(length > 0);
            Contract.Ensures(false == String.IsNullOrEmpty(Contract.Result<string>()));

            return null;
        }
    }
}
