﻿using System.Web;
using ModelResources = TalentManagement.Resources.Models.ProfileModel;

namespace TalentManagement.Web.Models
{
    public class ProfileModel : WizardPage 
    {
        public bool ShowSubmit { get; set; }

        [LocalizedDisplayName("Email",typeof(ModelResources))]
        public string Email { get; set; }

        [LocalizedDisplayName("FirstName", typeof(ModelResources))]
        public string FirstName { get; set; }

        [LocalizedDisplayName("SecondName", typeof(ModelResources))]
        public string SecondName { get; set; }

        [LocalizedDisplayName("Password", typeof(ModelResources))]
        public string Password { get; set; }

        [LocalizedDisplayName("ConfirmPassword", typeof(ModelResources))]
        public string ConfirmPassword { get; set; }

        [LocalizedDisplayName("Position", typeof(ModelResources))]
        public string Position { get; set; }

        [LocalizedDisplayName("ManagerEmail", typeof(ModelResources))]
        public string ManagerEmail { get; set; }

        [LocalizedDisplayName("InviteColleagues",typeof(ModelResources))]
        public bool InviteColleagues { get; set; }
        
        private HttpPostedFileBase _profileImage;

        [LocalizedDisplayName("ProfileImage", typeof(ModelResources))]
        public HttpPostedFileBase ProfileImage
        {
            get { return _profileImage; }
            set 
            {
                _profileImage = value;
                 HttpPostedFileWrapper wrapper = new HttpPostedFileWrapper(value);
                _profileImageType = wrapper.ContentType;
                _profileImageBytes = wrapper.Bytes;
            }
        }

        private byte[] _profileImageBytes;
        public byte[] ProfileImageBytes
        {
            get { return _profileImageBytes; }
        }

        private string _profileImageType;
        public string ProfileImageType
        {
            get { return _profileImageType; }
        }
        
        public bool IsProfileImageShouldBeSaved
        {
            get
            {
                return IsReadyToGo &&
                       CurrentStep == ThisStep &&
                       ProfileImage != null &&
                       ProfileImage.InputStream != null &&
                       ProfileImage.InputStream.CanRead;
            }
        }

        public override int NextStep
        {
            get { return (int)RegistrationStep.Invitation; }
        }
        public override int ThisStep
        {
            get { return (int)RegistrationStep.Profile; }
        }
    }
}