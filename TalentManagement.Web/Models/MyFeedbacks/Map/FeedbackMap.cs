﻿using System.Linq;
using TalentManagement.Domain.Feedbacks;
using TalentManagement.Infrastructure;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Web.Models.MyFeedbacks.Map
{
    internal class FeedbackMap : IMap<FeedbackCreateModel,Feedback>
    {
        private readonly IPrincipalProvider _principalProvider;

        public FeedbackMap(IPrincipalProvider principalProvider)
        {
            _principalProvider = principalProvider;
        }

        public void Configure(IMapperConfiguration<FeedbackCreateModel, Feedback> configuration)
        {
            configuration.ForMember(d => d.Question, s => s.Question);
            configuration.ForMember(d => d.Responders, s => s.Users.Select(u => u.Id).ToArray());
            configuration.AfterMap((s, d) => _principalProvider.FillOWned(d).FillOWnedByCompany(d));
        }
    }
}