﻿using System.Linq;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.People.Map
{
    public class GroupingUserToTeamModelRowMap : IMap<IGrouping<string, User>, TeamRowModel>
    {
        private readonly IMapper<User, UserRowModel> _userToUserRowModelMapper;

        public GroupingUserToTeamModelRowMap(IMapper<User, UserRowModel> userToUserRowModelMapper)
        {
            _userToUserRowModelMapper = userToUserRowModelMapper;
        }

        public void Configure(IMapperConfiguration<IGrouping<string, User>, TeamRowModel> configuration)
        {
            configuration.ForMember(x => x.Name, x => x.Key);
            configuration.ForMember(x => x.Users, x => x.Select(y => _userToUserRowModelMapper.Map(y)).ToArray());
        }
    }
}