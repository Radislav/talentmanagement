﻿using System.IO;

namespace TalentManagement.Infrastructure.Parsers
{
    public interface IParser
    {
        object Parse(Stream content);
    }
}
