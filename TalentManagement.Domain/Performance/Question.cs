﻿using System;
using System.Diagnostics.Contracts;
using MongoDB.Bson;

namespace TalentManagement.Domain.Performance
{
    [AggregateRoot]
    public class Question : Entity
    {
        private Question()
        {
            Text = String.Empty;
            Settings = QuestionSetting.Required;
            AnswerType = AnswerType.Text;
            OrderNumber = 0;
        }

        [ContractInvariantMethod]
        private void Invariant()
        {
            Contract.Invariant(Text != null);
            Contract.Invariant(Settings != QuestionSetting.None);
            Contract.Invariant(AnswerType != AnswerType.None);
            Contract.Invariant(OrderNumber >= 0);
        }

        public Question(string text, QuestionSetting settings, AnswerType answerType)
        {
            Contract.Requires(false == String.IsNullOrEmpty(text));
            Contract.Requires(settings != QuestionSetting.None);
            Contract.Requires(answerType != AnswerType.None);

            Text = text;
            Settings = settings;
            AnswerType = answerType;
        }

        public Question(ObjectId id, string text,QuestionSetting settings,AnswerType answerType) : base(id)
        {
            Contract.Requires(false == String.IsNullOrEmpty(text));
            Contract.Requires(settings != QuestionSetting.None);
            Contract.Requires(answerType != AnswerType.None);

            Text = text;
            Settings = settings;
            AnswerType = answerType;
        }

        public string Text { get; private set; }
        public QuestionSetting Settings { get; private set; }
        public AnswerType AnswerType { get; private set; }
        public int OrderNumber { get; set; }
    }
}
