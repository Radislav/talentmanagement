﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using TalentManagement.Domain.Objectives;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data.Repositories
{
    internal class ActionItemRepository : Repository<ActionItem> , IActionItemRepository
    {
        public ActionItemRepository(IUnitOfWork unitOfWork, IPrincipalProvider principalProvider)
            : base(unitOfWork, principalProvider)
        {
        }

        public ActionItemsDictionary GetRelated(string owner, ActionItemStatus? status)
        {
            IEnumerable<Objective> objectives = UnitOfWork.Find<Objective>(Query.EQ("KeyResults.ActionItems.Owner", new BsonString(owner)));

            return  new ActionItemsDictionary(objectives.ToDictionary(o => o.Id, 
                                            o =>
                                            {
                                                var actionItems = o.KeyResults.SelectMany(kr => kr.ActionItems)
                                                                        .Where(ai => ai.Owner == owner);
                                                return FilterByStatus(actionItems, status);
                                            }));
        }

        public IEnumerable<ActionItem> GetNotRelated(string owner, ActionItemStatus? status)
        {
            return FilterByStatus(UnitOfWork.AsQueryable<ActionItem>().Where(ai => ai.Owner == owner), status);
        }

        private IEnumerable<ActionItem> FilterByStatus(IEnumerable<ActionItem> items, ActionItemStatus? status)
        {
            return status.HasValue ? items.Where(ai => ai.Status == status.Value) : items;
        }
        
    }
}
