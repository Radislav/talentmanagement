﻿using System;

namespace TalentManagement.Infrastructure.Security
{
    public class TenantPrincipalModel
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string[] Roles { get; set; }
        public bool IsAuthenticated { get; set; }
        public DateTime LastSignIn { get; set; }
        public string Team { get; set; }
    }
}