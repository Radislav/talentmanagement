﻿namespace TalentManagement.Infrastructure.Security
{
    public interface IAuthenticationService
    {
        LoginResult Login(string email, string password);
    }
}
