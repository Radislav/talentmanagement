﻿using System.Diagnostics.Contracts;
using MongoDB.Bson;

namespace TalentManagement.Domain.Events
{
    public class BadgeUpdatedArgs : IDomainEventArgs
    {
        public BadgeUpdatedArgs(ObjectId companyId, ObjectId badgeId,bool isEnabled)
        {
            Contract.Requires(companyId != ObjectId.Empty);
            Contract.Requires(badgeId != ObjectId.Empty);
            
            CompanyId = companyId;
            BadgeId = badgeId;
            IsEnabled = isEnabled;
        }

        public ObjectId CompanyId { get; private set; }
        public ObjectId BadgeId { get; private set; }
        public bool IsEnabled { get; private set; }
    }
}
