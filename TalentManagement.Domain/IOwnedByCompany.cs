﻿namespace TalentManagement.Domain
{
    public interface IOwnedByCompany 
    {
        string CompanyName { get; set; }
    }
}
