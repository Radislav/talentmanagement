<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <div>   
      <div style="padding: 5 5 5 5; font-size:15.0pt; backround-color: #CCCCCC">
        <p>Your company</p>
      </div>
      <div>        
        <div style="font-size:12.0pt;">
          <p>Some Men (your company) has invited you to join the best company</p>
        </div>
        <div>
          <p>The best company introduction here.</p>          
          <a>
            <xsl:attribute name="href"><xsl:value-of select="InvitationArgument/InvitationUrl"></xsl:value-of></xsl:attribute>
            Join the best company
          </a>
          <a style="padding-left:10px">
            <xsl:attribute name="href">
              <xsl:value-of select="InvitationArgument/HelpUrl"></xsl:value-of>
            </xsl:attribute>
            Why the best company
          </a>                   
        </div>        
      </div>
    </div>    
  </xsl:template>
</xsl:stylesheet>