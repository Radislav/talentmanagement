﻿namespace TalentManagement.Infrastructure.Email
{
    public class InvitationArgument
    {
        public string InvitationUrl { get;  set; }
        public string HelpUrl { get;  set; }
    }
}
