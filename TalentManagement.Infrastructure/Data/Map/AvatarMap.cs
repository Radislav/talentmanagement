﻿using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Data.Configuration;

namespace TalentManagement.Infrastructure.Data.Map
{
    class AvatarMap : EntityConfiguration<Avatar>
    {
        public AvatarMap()
        {
            AsCollection("Avatars")
                .WithDataSource(DataSourceType.Shared);

            Property(a => a.Image)
                .Required();

            Property(a => a.ImageType)
                .Required();
        }
    }
}
