﻿using System.Collections.Generic;
using System.Linq;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.ReviewCycleCreation.Map
{
    public class QuestionSetsMapper : IMapper<ReviewCycleModel,IEnumerable<QuestionSet>> 
    {
        public IEnumerable<QuestionSet> Map(ReviewCycleModel source)
        {
            return source.CreateStep2.QuestionSets.Select(set => new QuestionSet(set.SetType, set.Questions.Select(q => q.Id),
                                                                          source.GetDueDateFor(set.SetType)));
        }
    }
}