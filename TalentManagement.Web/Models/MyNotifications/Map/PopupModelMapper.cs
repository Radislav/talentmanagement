﻿using System.Linq;
using TalentManagement.Domain.Users;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.MyNotifications.Map
{
    public class PopupModelMapper : IMapper<User,PopupModel>
    {
        public PopupModel Map(User source)
        {
            return new PopupModel
                       {
                           Notifications = source.UserNotifications.Select(n => new NotificationPopupModel
                                                                                    {
                                                                                        Sender = n.SenderId,
                                                                                        CreatedAt = n.CreatedAt.AsLondDateString(),
                                                                                        Text = n.Text
                                                                                    }).ToArray()
                       };
        }
    }
}