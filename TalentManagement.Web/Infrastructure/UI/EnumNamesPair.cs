﻿namespace TalentManagement.Web.Infrastructure.UI
{
    public struct EnumNamesPair
    {
        private readonly string _name;
        private readonly string _displayName;

        public EnumNamesPair(string name, string dislpayName)
        {
            _name = name;
            _displayName = dislpayName;
        }

        public string Name { get { return _name; } }

        public string DisplayName {
            get { return _displayName; }
        }

        public bool AreAnyNameEqualTo(string @string)
        {
            return Name == @string || DisplayName == @string;
        }

    }
}