﻿namespace TalentManagement.Web.Models.MyPreferences
{
    [LocalizedDisplayName("Title",typeof(Resources.Models.MyPreferences.IndexModel))]
    public class IndexModel
    {
        public NotificationSettingModel[] Settings { get; set; }
    }
}