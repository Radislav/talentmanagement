﻿using System.Diagnostics.Contracts;

namespace TalentManagement.Domain.Events
{
    public class AvatarUpdatedArgs : IDomainEventArgs
    {
        public AvatarUpdatedArgs(string avatarId)
        {
            Contract.Requires(false == string.IsNullOrEmpty(avatarId));
            AvatarId = avatarId;
        }

        public string AvatarId { get; set; }
    }
}
