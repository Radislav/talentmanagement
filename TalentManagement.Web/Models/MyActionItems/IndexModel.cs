﻿namespace TalentManagement.Web.Models.MyActionItems
{
    public class IndexModel : ICanActivateButton
    {
        public IndexModel()
        {
            NewActionItem = new ActionItemModel();
        }

        public ActionItemsContainer Related { get; set; }
        public ActionItemsContainer NotRelated { get; set; }
        public ActionItemModel NewActionItem { get; set; }
        public string ActiveButton { get; set; }
    }
}