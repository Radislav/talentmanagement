﻿$(function () {
    
    answers.init(urls.saveAnswers(), 'answer');
    showSummary();

    function hidePanels() {
        $('.panel:visible').each(function () {
            $(this).hide('slide');
        });
    }

    function showSummary() {
        hidePanels();

        $('#summary').show('slide');
        $('#back-to-summary').css('display', 'none');
        $('#activity_stats .btn').removeClass('active');
    }

    function initShowPanel(panelName) {
        $('#show-' + panelName).click(function () {
            hidePanels();
            $('#' + panelName).show('slide');
            $('#back-to-summary').css('display', 'block');
        });
    }

    initShowPanel('personal');
    initShowPanel('objectives');
    initShowPanel('kudos');
    initShowPanel('peers');

    $('#back-to-summary').click(function () {
        showSummary();
    });
});


