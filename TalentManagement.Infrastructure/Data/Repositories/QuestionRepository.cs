﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data.Repositories
{
    internal class QuestionRepository : Repository<Question> , IQuestionRepository
    {
        public QuestionRepository(IUnitOfWork unitOfWork, IPrincipalProvider principalProvider)
            : base(unitOfWork,principalProvider)
        {
        }
        
        public void InsertQuestions(IEnumerable<Question> questions)
        {
            UnitOfWork.InsertBatch(questions);
        }


        public IEnumerable<Question> GetByIds(IEnumerable<ObjectId> questionIds)
        {
            return UnitOfWork.AsQueryable<Question>()
                             .Where(question => questionIds.Contains(question.Id))
                             .ToArray();
        }
    }
}
