﻿using MongoDB.Bson;

namespace TalentManagement.Web.Models.PerformanceAdministrate
{
    public class ReviewCycleModel
    {
        public string Name { get; set; }
        public string ReviewPeriod { get; set; }
        public string Progress { get; set; }
        public string Action { get; set; }
        public ObjectId Id { get; set; }
    }
}