﻿namespace TalentManagement.Domain.Objectives
{
    public class ObjectiveIcon : Entity , IPicture
    {
        public byte[] Image { get; set; }
        public string ImageType { get; set; }
    }
}
