﻿using MongoDB.Bson;

namespace TalentManagement.Web.Models.Teams
{
    public class TeamExpandPair
    {
        public ObjectId TeamId { get; set; }
        public bool IsExpanded { get; set; }
    }
}