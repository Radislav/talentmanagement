﻿using FluentValidation;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.ReviewCycleCreation.Validation
{
    public class QuestionModelValidator : AbstractValidator<QuestionModel>, TalentManagement.Infrastructure.Validation.IValidator<QuestionModel>
    {
        public QuestionModelValidator()
        {
            RuleFor(m => m.Text)
                .NotEmpty();

        }

        public TalentManagement.Infrastructure.Validation.ValidationResult ValidateInstance(QuestionModel instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}