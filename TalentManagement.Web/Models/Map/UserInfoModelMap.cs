﻿using System.Diagnostics.Contracts;
using TalentManagement.Infrastructure.Cache;
using TalentManagement.Infrastructure.Mapping;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Web.Models.Map
{
    class UserInfoModelMap : IMap<TenantIdentity,UserInfoModel>
    {
        private readonly IGlobalCache _globalCache;

        public UserInfoModelMap(IGlobalCache globalCache)
        {
            Contract.Requires(globalCache != null);

            _globalCache = globalCache;
        }

        public void Configure(IMapperConfiguration<TenantIdentity, UserInfoModel> configuration)
        {
            configuration.ForMember(d => d.FirstName, s => s.FirstName);
            configuration.ForMember(d => d.LastSignIn, s => s.LastSignIn);
            configuration.ForMember(d => d.SecondName, s => s.SecondName);
            configuration.ForMember(d => d.NotificationsCount, s => _globalCache.GetNotificationsCountFor(s.Name));
        }
    }
}