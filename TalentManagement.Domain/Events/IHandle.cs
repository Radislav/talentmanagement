﻿namespace TalentManagement.Domain.Events
{
    public interface IHandle<T>
       where T : IDomainEventArgs
    {
        void Handle(T args);
    }
}
