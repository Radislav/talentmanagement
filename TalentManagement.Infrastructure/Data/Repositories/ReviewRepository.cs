﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using TalentManagement.Domain.Performance;
using TalentManagement.Infrastructure.Security;

namespace TalentManagement.Infrastructure.Data.Repositories
{
    internal class ReviewRepository : Repository<Review> , IReviewRepository
    {
        private readonly IReviewCycleRepository _reviewCycleRepository;

        public ReviewRepository(IUnitOfWork unitOfWork, IPrincipalProvider principalProvider, IReviewCycleRepository reviewCycleRepository)
            : base(unitOfWork,principalProvider)
        {
            _reviewCycleRepository = reviewCycleRepository;
        }
        
        public void InsertReviews(IEnumerable<Review> reviews)
        {
            UnitOfWork.InsertBatch(reviews);
        }

        public IEnumerable<Review> GetBySetIds(IEnumerable<ObjectId> questionSetIds)
        {
            return UnitOfWork.AsQueryable<Review>()
                             .Where(review => questionSetIds.Contains(review.QuestionSetId))
                             .ToArray();
        }

        public IEnumerable<Review> GetByCycle(ReviewCycle cycle)
        {
            IEnumerable<ObjectId> setIds = cycle.QuestionSets.Select(set => set.Id);
            return GetBySetIds(setIds);
        }

        public IEnumerable<Review> GetByCycle(ObjectId cycleId)
        {
            return GetByCycle(_reviewCycleRepository.GetById(cycleId));
        }
    }
}
