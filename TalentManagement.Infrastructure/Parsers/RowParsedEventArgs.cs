﻿using System;

namespace TalentManagement.Infrastructure.Parsers
{
    public class RowParsedEventArgs : EventArgs
    {
        public RowParsedEventArgs(int rowNumber)
        {
            RowNumber = rowNumber;
        }

        public int RowNumber { get; private set; }
    }
}
