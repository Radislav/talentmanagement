﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using MongoDB.Bson;
using TalentManagement.Domain.Feeds;
using TalentManagement.Domain.Objectives;

namespace TalentManagement.Domain.Contracts
{
    [ContractClassFor(typeof(IFeedFactory))]
    internal  abstract class FeedFactoryContract : IFeedFactory
    {
        public Feed CreateEmptyFeed(FeedType feedType, OwnedDescriptor creationDescriptor)
        {
            Contract.Ensures(Contract.Result<Feed>() != null);
            return default(Feed);
        }

        public Feed CreateKudos(string content, ObjectId badgeId, IEnumerable<string> receivers, OwnedDescriptor creationDescriptory)
        {
            Contract.Requires(content != null);
            Contract.Requires(receivers != null);
            Contract.Requires(receivers.Any());
            Contract.Ensures(Contract.Result<Feed>() != null);
            
            return default(Feed);
        }

        public Feed CreateUpdatingFeed(string text, IScope updatedItemScope, OwnedDescriptor creationDescriptor)
        {
            Contract.Requires(updatedItemScope != null);
            Contract.Ensures(Contract.Result<Feed>() != null);
            return default(Feed);
        }

        public Feed CreateObjectiveFeed(Objective oldObjective, Objective newObjective, OwnedDescriptor creationDescriptor)
        {
            Contract.Requires(oldObjective != null);
            Contract.Requires(newObjective != null);
            Contract.Ensures(Contract.Result<Feed>() != null);
            return default(Feed);
        }
    }
}
