﻿using System.Linq;
using TalentManagement.Domain.Feeds;

namespace TalentManagement.Infrastructure.Data.Filter
{
    class FeedTypeFilter : FilterBase<Feed>
    {
        public override Domain.IFilterContext FilterContext { get; set; }

        public override Domain.IFilter<Feed> Filter(Domain.IFilterContext[] filterContexts)
        {
           var context = GetContextOrFail<FeedTypeContext>(filterContexts);
           if (context != null)
           {
               Data = Data.Where(d => d.FeedType == context.FeedType);
           }
            return this;
        }
    }
}
