﻿using System;
using System.Linq;
using System.Web.Mvc;
using MongoDB.Bson;
using TalentManagement.Domain.Companies;
using TalentManagement.Infrastructure.Mapping;

namespace TalentManagement.Web.Models.Companies.Map
{
    internal class GeneralTabMap : IMap<Company,GeneralTab>
    {
        public void Configure(IMapperConfiguration<Company, GeneralTab> configuration)
        {
            configuration.ForMember(d => d.LogoId, s => s.Logo == null ? new ObjectId() : s.Logo.Id);
            configuration.ForMember(d => d.CompanyName, s => s.Name);
            configuration.ForMember(d => d.CompanyId, s => s.Id);
            configuration.ForMember(d => d.Timezones,
                                    s => 
                                        TimeZoneInfo.GetSystemTimeZones()
                                                     .Select(tz => new SelectListItem{Text = tz.DisplayName,Value = tz.Id,Selected = tz.Id == s.Timezone})
                                                     .ToArray());
        }
    }
}