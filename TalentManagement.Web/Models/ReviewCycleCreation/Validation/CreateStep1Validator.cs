﻿using System;
using FluentValidation;
using TalentManagement.Infrastructure.Resources;
using TalentManagement.Web.Models.Validation;

namespace TalentManagement.Web.Models.ReviewCycleCreation.Validation
{
    public  class CreateStep1Validator : AbstractValidator<CreateStep1>, TalentManagement.Infrastructure.Validation.IValidator<CreateStep1>
    {
        public CreateStep1Validator(IResourceProvider resourceProvider)
        {
            RuleFor(m => m.Name)
                .NotEmpty();

            RuleFor(m => m.ReviewStart)
                .NotEmpty()
                .DateTime();

            RuleFor(m => m.ReviewEnd)
                .NotEmpty()
                .DateTime();

            RuleFor(m => m.ReviewStart)
                .Must((model, reviewStart) => DateTime.Parse(model.ReviewStart) <= DateTime.Parse(model.ReviewEnd))
                .When(model =>
                          {
                              DateTime start;
                              DateTime end;
                              return DateTime.TryParse(model.ReviewStart, out start) &&
                                     DateTime.TryParse(model.ReviewEnd, out end);
                          })
                .WithMessage(resourceProvider.ErrorDateStartAfterDateEnd(resourceProvider.ReviewCycleDateStart,
                                                                         resourceProvider.ReviewCycleDateEnd));

            RuleFor(m => m.SummaryDue)
                .NotEmpty()
                .DateTime()
                .DueDate();

            RuleFor(m => m.PearsDue)
                .NotEmpty()
                .DateTime()
                .DueDate();
                
            RuleFor(m => m.SelfDue)
                .NotEmpty()
                .DateTime()
                .DueDate();

            RuleFor(m => m.ManagerDue)
                .NotEmpty()
                .DateTime()
                .DueDate();

        }

        public TalentManagement.Infrastructure.Validation.ValidationResult ValidateInstance(CreateStep1 instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}