﻿using MongoDB.Bson;

namespace TalentManagement.Domain.Objectives
{
    public interface IObjectiveIconRepository : IRepository<ObjectiveIcon>
    {
        ObjectId[] GetIconIds();
    }
}
