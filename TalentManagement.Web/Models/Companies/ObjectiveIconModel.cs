﻿using System.Diagnostics.Contracts;
using MongoDB.Bson;
using TalentManagement.Domain.Objectives;

namespace TalentManagement.Web.Models.Companies
{
    public class ObjectiveIconModel
    {
        public ObjectiveIconModel(ObjectId objectiveId,ObjectId objectiveIconId)
        {
            ObjectiveId = objectiveId;
            ObjectiveIconId = objectiveIconId;
        }

        public ObjectiveIconModel(Objective objective)
        {
            Contract.Requires(objective != null);
            ObjectiveId = objective.Id;
            ObjectiveIconId = objective.Icon;
        }

        public ObjectId ObjectiveId { get; private set; }
        public ObjectId ObjectiveIconId { get; private set; }
    }
}