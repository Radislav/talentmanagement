﻿using FluentValidation;
using TalentManagement.Infrastructure.Validation;
using TalentManagement.Resources;

namespace TalentManagement.Web.Models.Validation
{
    public class InvitationModelValidator : AbstractValidator<InvitationModel> , TalentManagement.Infrastructure.Validation.IValidator<InvitationModel>
    {
        public InvitationModelValidator()
        {
            RuleFor(m => m.Email1)
                .Matches(RegularExpressions.EmailUserName)
                .When(m => false == string.IsNullOrEmpty(m.Email1))
                .WithLocalizedMessage(() => Errors.email_error);

            RuleFor(m => m.Email2)
                  .Matches(RegularExpressions.EmailUserName)
                  .When(m => false == string.IsNullOrEmpty(m.Email2))
                 .WithLocalizedMessage(() => Errors.email_error);

            RuleFor(m => m.Email3)
                  .Matches(RegularExpressions.EmailUserName)
                  .When(m => false == string.IsNullOrEmpty(m.Email3))
                 .WithLocalizedMessage(() => Errors.email_error);

            RuleFor(m => m.Email4)
                  .Matches(RegularExpressions.EmailUserName)
                  .When(m => false == string.IsNullOrEmpty(m.Email4))
                   .WithLocalizedMessage(() => Errors.email_error);

            RuleFor(m => m.Email5)
                  .Matches(RegularExpressions.EmailUserName)
                  .When(m => false == string.IsNullOrEmpty(m.Email5))
                   .WithLocalizedMessage(() => Errors.email_error);
        }
        
        public ValidationResult ValidateInstance(InvitationModel instance)
        {
            return this.ValidateAsModel(instance);
        }
    }
}