﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.DynamicProxy;
using TalentManagement.Infrastructure.Session;

namespace TalentManagement.Infrastructure.Data.Filter
{
    public class RepositoryInterceptor : IInterceptor
    {
        private readonly ISessionFactory _sessionFactory;

        public RepositoryInterceptor(ISessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;
        }

        public void Intercept(IInvocation invocation)
        {
            invocation.Proceed();
            var result = invocation.ReturnValue as IEnumerable;
            if (result != null)
            {
                /*var data = result.OfType<TModel>();
                Init(data);
                Filter();
                invocation.ReturnValue = AsEnumerable();*/
            }
        }
    }
}
