﻿namespace TalentManagement.Infrastructure.Email
{
    class EmptyArgument
    {
        private static readonly EmptyArgument Void = new EmptyArgument();
        public static EmptyArgument Value
        {
            get { return Void; }
        }
    }
}
